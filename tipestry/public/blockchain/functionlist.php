<!DOCTYPE html>
<html>
<head>
<style>
table, td, th {
    border: 1px solid black;
}

#table1 {
    border-collapse: separate;
}

#table2 {
    border-collapse: collapse;
}
</style>
</head>
<body>
<a style="color:#00F;" href="https://en.bitcoin.it/wiki/Original_Bitcoin_client/API_calls_list">Original_Bitcoin_client API_calls_list</a><br><br>
New Address : <- -> 
2Mtr9r9brBQx4AKH6TsQNo6j8GMYUiKnvXK <- -> 
2N21Gbd6zzR7BCeHDTF982Msv316siTCHKY<br>
BlockIO <- -> tester159@gmail.com <- ->Behala@345<br>


<table id="table2" width="1600">
<tbody><tr>
<th width="150"> Command </th>
<th width="150"> Parameters </th>
<th> Description </th>
<th width="50"> Requires unlocked wallet? (v0.4.0+)
</th></tr>
<tr>
<td> <code>addmultisigaddress</code> </td>
<td> &lt;nrequired&gt; &lt;'["key","key"]'&gt; [account] </td>
<td> Add a nrequired-to-sign multisignature address to the wallet. Each key is a bitcoin address or hex-encoded public key. If [account] is specified, assign address to [account]. Returns a string containing the address. </td>
<td> N
</td></tr>
<tr>
<td> <code>addnode</code> </td>
<td> &lt;node&gt; &lt;add/remove/onetry&gt; </td>
<td> <b>version 0.8</b> Attempts add or remove &lt;node&gt; from the addnode list or try a connection to &lt;node&gt; once. </td>
<td> N
</td></tr>
<tr>
<td> <code>backupwallet</code> </td>
<td> &lt;destination&gt; </td>
<td> Safely copies wallet.dat to destination, which can be a directory or a path with filename. </td>
<td> N
</td></tr>
<tr>
<td> <code>createmultisig</code> </td>
<td> &lt;nrequired&gt; &lt;'["key,"key"]'&gt; </td>
<td> Creates a multi-signature address and returns a json object </td>
<td>
</td></tr>
<tr>
<td> <code>createrawtransaction</code> </td>
<td> [{"txid":txid,"vout":n},...] {address:amount,...} </td>
<td> <b>version 0.7</b> Creates a <a href="/wiki/Raw_Transactions" title="Raw Transactions">raw transaction</a> spending given inputs. </td>
<td> N
</td></tr>
<tr>
<td> <code>decoderawtransaction</code> </td>
<td> &lt;hex string&gt; </td>
<td> <b>version 0.7</b> Produces a human-readable JSON object for a <a href="/wiki/Raw_Transactions" title="Raw Transactions">raw transaction</a>. </td>
<td> N
</td></tr>
<tr>
<td> <code>dumpprivkey</code> </td>
<td> &lt;bitcoinaddress&gt; </td>
<td> Reveals the private key corresponding to &lt;bitcoinaddress&gt; </td>
<td> Y
</td></tr>
<tr>
<td> <code>dumpwallet</code> </td>
<td> &lt;filename&gt; </td>
<td> <b>version 0.13.0</b> Exports all wallet private keys to file </td>
<td> Y
</td></tr>
<tr>
<td> <code>encryptwallet</code> </td>
<td> &lt;passphrase&gt; </td>
<td> Encrypts the wallet with &lt;passphrase&gt;. </td>
<td> N
</td></tr>
<tr>
<td> <code>getaccount</code> </td>
<td> &lt;bitcoinaddress&gt; </td>
<td> Returns the account associated with the given address. </td>
<td> N
</td></tr>
<tr style="background-color:#BB5348;">
<td> <code>getaccountaddress</code> </td>
<td> &lt;account&gt; </td>
<td> Returns the current bitcoin address for receiving payments to this account. If &lt;account&gt; does not exist, it will be created along with an associated new address that will be returned. </td>
<td> N
</td></tr>
<tr>
<td> <code>getaddednodeinfo</code> </td>
<td> &lt;dns&gt; [node] </td>
<td> <b>version 0.8</b> Returns information about the given added node, or all added nodes
<p>(note that onetry addnodes are not listed here)
If dns is false, only a list of added nodes will be provided,
otherwise connected information will also be available.
</p>
</td></tr>
<tr>
<td> <code>getaddressesbyaccount</code> </td>
<td> &lt;account&gt; </td>
<td> Returns the list of addresses for the given account. </td>
<td> N
</td></tr>
<tr>
<td> <code>getbalance</code> </td>
<td> [account] [minconf=1] </td>
<td> If [account] is not specified, returns the server's total available balance.<br>If [account] is specified, returns the balance in the account. </td>
<td> N
</td></tr>
<tr>
<td> <code>getbestblockhash</code> </td>
<td> </td>
<td> <b>version 0.9</b> Returns the hash of the best (tip) block in the longest block chain. </td>
<td> N
</td></tr>
<tr>
<td> <code>getblock</code> </td>
<td> &lt;hash&gt; </td>
<td> Returns information about the block with the given hash. </td>
<td> N
</td></tr>
<tr>
<td> <code>getblockcount</code> </td>
<td> </td>
<td> Returns the number of blocks in the longest block chain. </td>
<td> N
</td></tr>
<tr>
<td> <code>getblockhash</code> </td>
<td> &lt;index&gt; </td>
<td> Returns hash of block in best-block-chain at &lt;index&gt;; index 0 is the <a href="/wiki/Genesis_block" title="Genesis block">genesis block</a> </td>
<td> N
</td></tr>
<tr>
<td> <code>getblocknumber</code> </td>
<td> </td>
<td> <b>Deprecated</b>. <b>Removed in version 0.7</b>. Use getblockcount. </td>
<td> N
</td></tr>
<tr>
<td> <code>getblocktemplate</code> </td>
<td> [params] </td>
<td> Returns data needed to construct a block to work on. See <a href="/wiki/BIP_0022" title="BIP 0022"> BIP_0022</a> for more info on params.</td>
<td> N
</td></tr>
<tr>
<td> <code>getconnectioncount</code> </td>
<td> </td>
<td> Returns the number of connections to other nodes. </td>
<td> N
</td></tr>
<tr>
<td> <code>getdifficulty</code> </td>
<td> </td>
<td> Returns the proof-of-work difficulty as a multiple of the minimum difficulty. </td>
<td> N
</td></tr>
<tr>
<td> <code>getgenerate</code> </td>
<td> </td>
<td> Returns true or false whether bitcoind is currently generating hashes </td>
<td> N
</td></tr>
<tr>
<td> <code>gethashespersec</code> </td>
<td> </td>
<td> Returns a recent hashes per second performance measurement while generating. </td>
<td> N
</td></tr>
<tr>
<td> <code>getinfo</code> </td>
<td> </td>
<td> Returns an object containing various state info. </td>
<td> N
</td></tr>
<tr>
<td> <code>getmemorypool</code> </td>
<td> [data] </td>
<td> <b>Replaced in v0.7.0 with getblocktemplate, submitblock, getrawmempool</b> </td>
<td> N
</td></tr>
<tr>
<td> <code>getmininginfo</code> </td>
<td> </td>
<td> Returns an object containing mining-related information:
<ul><li> blocks</li>
<li> currentblocksize</li>
<li> currentblocktx</li>
<li> difficulty</li>
<li> errors</li>
<li> generate</li>
<li> genproclimit</li>
<li> hashespersec</li>
<li> pooledtx</li>
<li> testnet</li></ul>
</td>
<td> N
</td></tr>
<tr style="background-color:#0A0;">
<td> <code>getnewaddress</code> </td>
<td> [account] </td>
<td> Returns a new bitcoin address for receiving payments. If [account] is specified payments received with the address will be credited to [account]. </td>
<td> N
</td></tr>
<tr style="background-color:#0A0;">
<td> <code>getpeerinfo</code> </td>
<td> </td>
<td> <b>version 0.7</b> Returns data about each connected node. </td>
<td> N
</td></tr>
<tr>
<td> <code>getrawchangeaddress</code> </td>
<td> [account]</td>
<td> <b>version 0.9</b> Returns a new Bitcoin address, for receiving change. This is for use with raw transactions, NOT normal use. </td>
<td> N
</td></tr>
<tr>
<td> <code>getrawmempool</code> </td>
<td> </td>
<td> <b>version 0.7</b> Returns all transaction ids in memory pool </td>
<td> N
</td></tr>
<tr style="background-color:#0A0;">
<td> <code>getrawtransaction</code> </td>
<td> &lt;txid&gt; [verbose=0] </td>
<td> <b>version 0.7</b> Returns <a href="/wiki/Raw_Transactions" title="Raw Transactions">raw transaction</a> representation for given transaction id. </td>
<td> N
</td></tr>
<tr>
<td> <code>getreceivedbyaccount</code> </td>
<td> [account] [minconf=1] </td>
<td> Returns the total amount received by addresses with [account] in transactions with at least [minconf] confirmations. If [account] not provided return will include all transactions to all accounts. (version 0.3.24) </td>
<td> N
</td></tr>
<tr style="background-color:#0A0;">
<td> <code>getreceivedbyaddress</code> </td>
<td> &lt;bitcoinaddress&gt; [minconf=1] </td>
<td> Returns the amount received by &lt;bitcoinaddress&gt; in transactions with at least [minconf] confirmations. It correctly handles the case where someone has sent to the address in multiple transactions. Keep in mind that addresses are only ever used for receiving transactions. Works only for addresses in the local wallet, external addresses will always show 0. </td>
<td> N
</td></tr>
<tr>
<td> <code>gettransaction</code> </td>
<td> &lt;txid&gt; </td>
<td> Returns an object about the given transaction containing:
<ul><li> "amount"&nbsp;: total amount of the transaction</li>
<li> "confirmations"&nbsp;: number of confirmations of the transaction</li>
<li> "txid"&nbsp;: the transaction ID</li>
<li> "time"&nbsp;: time associated with the transaction<sup id="cite_ref-1" class="reference"><a href="#cite_note-1">[1]</a></sup>.</li>
<li> "details" - An array of objects containing:
<ul><li> "account"</li>
<li> "address"</li>
<li> "category"</li>
<li> "amount"</li>
<li> "fee"</li></ul></li></ul>
</td>
<td> N
</td></tr>
<tr>
<td> <code>gettxout</code> </td>
<td> &lt;txid&gt; &lt;n&gt; [includemempool=true] </td>
<td> Returns details about an unspent transaction output (UTXO) </td>
<td> N
</td></tr>
<tr>
<td> <code>gettxoutsetinfo</code> </td>
<td> </td>
<td> Returns statistics about the unspent transaction output (UTXO) set </td>
<td> N
</td></tr>
<tr>
<td> <code><a href="/wiki/Getwork" title="Getwork">getwork</a></code> </td>
<td> [data] </td>
<td> If [data] is not specified, returns formatted hash data to work on:
<ul><li> "midstate"&nbsp;: precomputed hash state after hashing the first half of the data</li>
<li> "data"&nbsp;: block data</li>
<li> "hash1"&nbsp;: formatted hash buffer for second hash</li>
<li> "target"&nbsp;: little endian hash target</li></ul>
<p>If [data] is specified, tries to solve the block and returns true if it was successful.
</p>
</td>
<td> N
</td></tr>
<tr>
<td> <code>help</code> </td>
<td> [command] </td>
<td> List commands, or get help for a command. </td>
<td> N
</td></tr>
<tr>
<td> <code>importprivkey</code> </td>
<td> &lt;bitcoinprivkey&gt; [label] [rescan=true]</td>
<td> Adds a private key (as returned by dumpprivkey) to your wallet. This may take a while, as a <a href="/wiki/How_to_import_private_keys#Import_Private_key.28s.29" title="How to import private keys">rescan</a> is done, looking for existing transactions. <b>Optional [rescan] parameter added in 0.8.0.</b> Note: There's no need to import public key, as in <a href="/wiki/Elliptic_Curve_Digital_Signature_Algorithm" title="Elliptic Curve Digital Signature Algorithm">ECDSA</a> (unlike RSA) this can be computed from private key. </td>
<td> Y
</td></tr>
<tr>
<td> <code>invalidateblock</code> </td>
<td> &lt;hash&gt; </td>
<td> Permanently marks a block as invalid, as if it violated a consensus rule.</td>
<td> N
</td></tr>
<tr>
<td> <code>keypoolrefill</code> </td>
<td> </td>
<td> Fills the keypool, requires wallet passphrase to be set. </td>
<td> Y
</td></tr>
<tr>
<td> <code>listaccounts</code> </td>
<td> [minconf=1] </td>
<td> Returns Object that has account names as keys, account balances as values. </td>
<td> N
</td></tr>
<tr>
<td> <code>listaddressgroupings</code> </td>
<td> </td>
<td> <b>version 0.7</b> Returns all addresses in the wallet and info used for coincontrol. </td>
<td> N
</td></tr>
<tr>
<td> <code>listreceivedbyaccount</code> </td>
<td> [minconf=1] [includeempty=false] </td>
<td> Returns an array of objects containing:
<ul><li> "account"&nbsp;: the account of the receiving addresses</li>
<li> "amount"&nbsp;: total amount received by addresses with this account</li>
<li> "confirmations"&nbsp;: number of confirmations of the most recent transaction included</li></ul>
</td>
<td> N
</td></tr>
<tr>
<td> <code>listreceivedbyaddress</code> </td>
<td> [minconf=1] [includeempty=false] </td>
<td> Returns an array of objects containing:
<ul><li> "address"&nbsp;: receiving address</li>
<li> "account"&nbsp;: the account of the receiving address</li>
<li> "amount"&nbsp;: total amount received by the address</li>
<li> "confirmations"&nbsp;: number of confirmations of the most recent transaction included</li></ul>
<p>To get a list of accounts on the system, execute bitcoind listreceivedbyaddress 0 true
</p>
</td>
<td> N
</td></tr>
<tr>
<td> <code>listsinceblock</code> </td>
<td> [blockhash] [target-confirmations] </td>
<td> Get all transactions in blocks since block [blockhash], or all transactions if omitted. [target-confirmations] intentionally <b>does not</b> affect the list of returned transactions, but only affects the returned "lastblock" value.<a rel="nofollow" class="external autonumber" href="https://github.com/bitcoin/bitcoin/pull/199#issuecomment-1514952">[1]</a> </td>
<td> N
</td></tr>
<tr>
<td> <code>listtransactions</code> </td>
<td> [account] [count=10] [from=0] </td>
<td> Returns up to [count] most recent transactions skipping the first [from] transactions for account [account]. If [account] not provided it'll return recent transactions from all accounts.
</td>
<td> N
</td></tr>
<tr>
<td> <code>listunspent</code> </td>
<td> [minconf=1] [maxconf=999999] </td>
<td> <b>version 0.7</b> Returns array of unspent transaction inputs in the wallet. </td>
<td> N
</td></tr>
<tr>
<td> <code>listlockunspent</code> </td>
<td> </td>
<td> <b>version 0.8</b> Returns list of temporarily unspendable outputs
</td></tr>
<tr>
<td> <code>lockunspent</code> </td>
<td> &lt;unlock?&gt; [array-of-objects] </td>
<td> <b>version 0.8</b> Updates list of temporarily unspendable outputs
</td></tr>
<tr>
<td> <code>move</code> </td>
<td> &lt;fromaccount&gt; &lt;toaccount&gt; &lt;amount&gt; [minconf=1] [comment] </td>
<td> Move from one account in your wallet to another </td>
<td> N
</td></tr>
<tr style="background-color:#BB5348;">
<td> <code>sendfrom</code> </td>
<td> &lt;fromaccount&gt; &lt;tobitcoinaddress&gt; &lt;amount&gt; [minconf=1] [comment] [comment-to] </td>
<td> &lt;amount&gt; is a real and is rounded to 8 decimal places. Will send the given amount to the given address, ensuring the account has a valid balance using [minconf] confirmations. Returns the transaction ID if successful (not in JSON object). </td>
<td> Y
</td></tr>
<tr>
<td> <code>sendmany</code> </td>
<td> &lt;fromaccount&gt; {address:amount,...} [minconf=1] [comment] </td>
<td> amounts are double-precision floating point numbers </td>
<td> Y
</td></tr>
<tr>
<td> <code>sendrawtransaction</code> </td>
<td> &lt;hexstring&gt; </td>
<td> <b>version 0.7</b> Submits <a href="/wiki/Raw_Transactions" title="Raw Transactions">raw transaction</a> (serialized, hex-encoded) to local node and network. </td>
<td> N
</td></tr>
<tr style="background-color:#0A0;">
<td> <code>sendtoaddress</code> </td>
<td> &lt;bitcoinaddress&gt; &lt;amount&gt; [comment] [comment-to] </td>
<td> &lt;amount&gt; is a real and is rounded to 8 decimal places. Returns the transaction ID &lt;txid&gt; if successful. </td>
<td> Y
</td></tr>
<tr>
<td> <code>setaccount</code> </td>
<td> &lt;bitcoinaddress&gt; &lt;account&gt; </td>
<td> Sets the account associated with the given address. Assigning address that is already assigned to the same account will create a new address associated with that account. </td>
<td> N
</td></tr>
<tr>
<td> <code>setgenerate</code> </td>
<td> &lt;generate&gt; [genproclimit] </td>
<td> &lt;generate&gt; is true or false to turn generation on or off.<br>Generation is limited to [genproclimit] processors, -1 is unlimited. </td>
<td> N
</td></tr>
<tr>
<td> <code>settxfee</code> </td>
<td> &lt;amount&gt; </td>
<td> &lt;amount&gt; is a real and is rounded to the nearest 0.00000001 </td>
<td> N
</td></tr>
<tr>
<td> <code>signmessage</code> </td>
<td> &lt;bitcoinaddress&gt; &lt;message&gt; </td>
<td> Sign a message with the private key of an address. </td>
<td> Y
</td></tr>
<tr>
<td> <code>signrawtransaction</code> </td>
<td> &lt;hexstring&gt; [{"txid":txid,"vout":n,"scriptPubKey":hex},...] [&lt;privatekey1&gt;,...] </td>
<td> <b>version 0.7</b> Adds signatures to a <a href="/wiki/Raw_Transactions" title="Raw Transactions">raw transaction</a> and returns the resulting raw transaction. </td>
<td> Y/N
</td></tr>
<tr>
<td> <code>stop</code> </td>
<td> </td>
<td> Stop bitcoin server. </td>
<td> N
</td></tr>
<tr>
<td> <code>submitblock</code> </td>
<td> &lt;hex data&gt; [optional-params-obj] </td>
<td> Attempts to submit new block to network. </td>
<td> N
</td></tr>
<tr>
<td> <code>validateaddress</code> </td>
<td> &lt;bitcoinaddress&gt; </td>
<td> Return information about &lt;bitcoinaddress&gt;. </td>
<td> N
</td></tr>
<tr>
<td> <code>verifymessage</code> </td>
<td> &lt;bitcoinaddress&gt; &lt;signature&gt; &lt;message&gt; </td>
<td> Verify a signed message. </td>
<td> N
</td></tr>
<tr>
<td> <code>walletlock</code> </td>
<td> </td>
<td> Removes the wallet encryption key from memory, locking the wallet. After calling this method, you will need to call walletpassphrase again before being able to call any methods which require the wallet to be unlocked. </td>
<td> N
</td></tr>
<tr>
<td> <code>walletpassphrase</code> </td>
<td> &lt;passphrase&gt; &lt;timeout&gt; </td>
<td> Stores the wallet decryption key in memory for &lt;timeout&gt; seconds. </td>
<td> N
</td></tr>
<tr>
<td> <code>walletpassphrasechange</code> </td>
<td> &lt;oldpassphrase&gt; &lt;newpassphrase&gt; </td>
<td> Changes the wallet passphrase from &lt;oldpassphrase&gt; to &lt;newpassphrase&gt;. </td>
<td> N
</td></tr></tbody></table>




<div>
<div class=" scroll" style="top: 20px; bottom: 20px;">

<ul id="markdown-toc" class="">
<li class=""><a href="#block-chain" id="markdown-toc-block-chain" class="">Block Chain</a>    <ul class="">
<li class=""><a href="#block-headers" id="markdown-toc-block-headers" class="">Block Headers</a>        <ul class="">
<li class=""><a href="#block-versions" id="markdown-toc-block-versions" class="">Block Versions</a></li>
<li class=""><a href="#merkle-trees" id="markdown-toc-merkle-trees" class="">Merkle Trees</a></li>
<li class=""><a href="#target-nbits" id="markdown-toc-target-nbits" class="">Target nBits</a></li>
</ul>
</li>
<li class=""><a href="#serialized-blocks" id="markdown-toc-serialized-blocks" class="">Serialized Blocks</a></li>
</ul>
</li>
<li class=""><a href="#transactions" id="markdown-toc-transactions" class="">Transactions</a>    <ul class="">
<li class=""><a href="#opcodes" id="markdown-toc-opcodes" class="">OpCodes</a></li>
<li class=""><a href="#address-conversion" id="markdown-toc-address-conversion" class="">Address Conversion</a></li>
<li class=""><a href="#raw-transaction-format" id="markdown-toc-raw-transaction-format" class="">Raw Transaction Format</a></li>
<li class=""><a href="#compactsize-unsigned-integers" id="markdown-toc-compactsize-unsigned-integers" class="">CompactSize Unsigned Integers</a></li>
</ul>
</li>
<li class=""><a href="#wallets" id="markdown-toc-wallets" class="">Wallets</a>    <ul class="">
<li class=""><a href="#deterministic-wallet-formats" id="markdown-toc-deterministic-wallet-formats" class="">Deterministic Wallet Formats</a>        <ul class="">
<li class=""><a href="#type-1-single-chain-wallets" id="markdown-toc-type-1-single-chain-wallets" class="">Type 1: Single Chain Wallets</a></li>
<li class=""><a href="#type-2-hierarchical-deterministic-hd-wallets" id="markdown-toc-type-2-hierarchical-deterministic-hd-wallets" class="">Type 2: Hierarchical Deterministic (HD) Wallets</a></li>
</ul>
</li>
</ul>
</li>
<li class=""><a href="#p2p-network" id="markdown-toc-p2p-network" class="">P2P Network</a>    <ul class="">
<li class=""><a href="#constants-and-defaults" id="markdown-toc-constants-and-defaults" class="">Constants And Defaults</a></li>
<li class=""><a href="#protocol-versions" id="markdown-toc-protocol-versions" class="">Protocol Versions</a></li>
<li class=""><a href="#message-headers" id="markdown-toc-message-headers" class="">Message Headers</a></li>
<li class=""><a href="#data-messages" id="markdown-toc-data-messages" class="">Data Messages</a>        <ul class="">
<li class=""><a href="#block" id="markdown-toc-block" class="">Block</a></li>
<li class=""><a href="#getblocks" id="markdown-toc-getblocks" class="">GetBlocks</a></li>
<li class=""><a href="#getdata" id="markdown-toc-getdata" class="">GetData</a></li>
<li class=""><a href="#getheaders" id="markdown-toc-getheaders" class="">GetHeaders</a></li>
<li class=""><a href="#headers" id="markdown-toc-headers" class="">Headers</a></li>
<li class=""><a href="#inv" id="markdown-toc-inv" class="">Inv</a></li>
<li class=""><a href="#mempool" id="markdown-toc-mempool" class="">MemPool</a></li>
<li class=""><a href="#merkleblock" id="markdown-toc-merkleblock" class="">MerkleBlock</a></li>
<li class=""><a href="#notfound" id="markdown-toc-notfound" class="">NotFound</a></li>
<li class=""><a href="#tx" id="markdown-toc-tx" class="">Tx</a></li>
</ul>
</li>
<li class=""><a href="#control-messages" id="markdown-toc-control-messages" class="">Control Messages</a>        <ul class="">
<li class=""><a href="#addr" id="markdown-toc-addr" class="">Addr</a></li>
<li class=""><a href="#alert" id="markdown-toc-alert" class="">Alert</a></li>
<li class=""><a href="#feefilter" id="markdown-toc-feefilter" class="">FeeFilter</a></li>
<li class=""><a href="#filteradd" id="markdown-toc-filteradd" class="">FilterAdd</a></li>
<li class=""><a href="#filterclear" id="markdown-toc-filterclear" class="">FilterClear</a></li>
<li class=""><a href="#filterload" id="markdown-toc-filterload" class="">FilterLoad</a></li>
<li class=""><a href="#getaddr" id="markdown-toc-getaddr" class="">GetAddr</a></li>
<li class=""><a href="#ping" id="markdown-toc-ping" class="">Ping</a></li>
<li class=""><a href="#pong" id="markdown-toc-pong" class="">Pong</a></li>
<li class=""><a href="#reject" id="markdown-toc-reject" class="">Reject</a></li>
<li class=""><a href="#sendheaders" id="markdown-toc-sendheaders" class="">SendHeaders</a></li>
<li class=""><a href="#verack" id="markdown-toc-verack" class="">VerAck</a></li>
<li class=""><a href="#version" id="markdown-toc-version" class="">Version</a></li>
</ul>
</li>
</ul>
</li>
<li class=" active"><a href="#bitcoin-core-apis" id="markdown-toc-bitcoin-core-apis" class="">Bitcoin Core APIs</a>    <ul class=" active">
<li class=""><a href="#hash-byte-order" id="markdown-toc-hash-byte-order" class="">Hash Byte Order</a></li>
<li class=" active"><a href="#remote-procedure-calls-rpcs" id="markdown-toc-remote-procedure-calls-rpcs" class="">Remote Procedure Calls (RPCs)</a>        <ul class=" active">
<li class=""><a href="#rpc-quick-reference" id="markdown-toc-rpc-quick-reference" class="">Quick Reference</a></li>
<li class=" active"><a href="#rpcs" id="markdown-toc-rpcs" class="">RPCs</a>            <ul class=" active">
<li class=" active"><a href="#abandontransaction" id="markdown-toc-abandontransaction" class=" active">AbandonTransaction</a></li>
<li class=""><a href="#addmultisigaddress" id="markdown-toc-addmultisigaddress" class="">AddMultiSigAddress</a></li>
<li class=""><a href="#addnode" id="markdown-toc-addnode" class="">AddNode</a></li>
<li class=""><a href="#addwitnessaddress" id="markdown-toc-addwitnessaddress" class="">AddWitnessAddress</a></li>
<li class=""><a href="#backupwallet" id="markdown-toc-backupwallet" class="">BackupWallet</a></li>
<li class=""><a href="#bumpfee" id="markdown-toc-bumpfee" class="">BumpFee</a></li>
<li class=""><a href="#clearbanned" id="markdown-toc-clearbanned" class="">ClearBanned</a></li>
<li class=""><a href="#createmultisig" id="markdown-toc-createmultisig" class="">CreateMultiSig</a></li>
<li class=""><a href="#createrawtransaction" id="markdown-toc-createrawtransaction" class="">CreateRawTransaction</a></li>
<li class=""><a href="#decoderawtransaction" id="markdown-toc-decoderawtransaction" class="">DecodeRawTransaction</a></li>
<li class=""><a href="#decodescript" id="markdown-toc-decodescript" class="">DecodeScript</a></li>
<li class=""><a href="#disconnectnode" id="markdown-toc-disconnectnode" class="">DisconnectNode</a></li>
<li class=""><a href="#dumpprivkey" id="markdown-toc-dumpprivkey" class="">DumpPrivKey</a></li>
<li class=""><a href="#dumpwallet" id="markdown-toc-dumpwallet" class="">DumpWallet</a></li>
<li class=""><a href="#encryptwallet" id="markdown-toc-encryptwallet" class="">EncryptWallet</a></li>
<li class=""><a href="#estimatefee" id="markdown-toc-estimatefee" class="">EstimateFee</a></li>
<li class=""><a href="#estimatepriority" id="markdown-toc-estimatepriority" class="">EstimatePriority</a></li>
<li class=""><a href="#fundrawtransaction" id="markdown-toc-fundrawtransaction" class="">FundRawTransaction</a></li>
<li class=""><a href="#generate" id="markdown-toc-generate" class="">Generate</a></li>
<li class=""><a href="#generatetoaddress" id="markdown-toc-generatetoaddress" class="">GenerateToAddress</a></li>
<li class=""><a href="#getaccountaddress" id="markdown-toc-getaccountaddress" class="">GetAccountAddress</a></li>
<li class=""><a href="#getaccount" id="markdown-toc-getaccount" class="">GetAccount</a></li>
<li class=""><a href="#getaddednodeinfo" id="markdown-toc-getaddednodeinfo" class="">GetAddedNodeInfo</a></li>
<li class=""><a href="#getaddressesbyaccount" id="markdown-toc-getaddressesbyaccount" class="">GetAddressesByAccount</a></li>
<li class=""><a href="#getbalance" id="markdown-toc-getbalance" class="">GetBalance</a></li>
<li class=""><a href="#getbestblockhash" id="markdown-toc-getbestblockhash" class="">GetBestBlockHash</a></li>
<li class=""><a href="#getblock" id="markdown-toc-getblock" class="">GetBlock</a></li>
<li class=""><a href="#getblockchaininfo" id="markdown-toc-getblockchaininfo" class="">GetBlockChainInfo</a></li>
<li class=""><a href="#getblockcount" id="markdown-toc-getblockcount" class="">GetBlockCount</a></li>
<li class=""><a href="#getblockhash" id="markdown-toc-getblockhash" class="">GetBlockHash</a></li>
<li class=""><a href="#getblockheader" id="markdown-toc-getblockheader" class="">GetBlockHeader</a></li>
<li class=""><a href="#getblocktemplate" id="markdown-toc-getblocktemplate" class="">GetBlockTemplate</a></li>
<li class=""><a href="#getchaintips" id="markdown-toc-getchaintips" class="">GetChainTips</a></li>
<li class=""><a href="#getconnectioncount" id="markdown-toc-getconnectioncount" class="">GetConnectionCount</a></li>
<li class=""><a href="#getdifficulty" id="markdown-toc-getdifficulty" class="">GetDifficulty</a></li>
<li class=""><a href="#getgenerate" id="markdown-toc-getgenerate" class="">GetGenerate</a></li>
<li class=""><a href="#gethashespersec" id="markdown-toc-gethashespersec" class="">GetHashesPerSec</a></li>
<li class=""><a href="#getinfo" id="markdown-toc-getinfo" class="">GetInfo</a></li>
<li class=""><a href="#getmemoryinfo" id="markdown-toc-getmemoryinfo" class="">GetMemoryInfo</a></li>
<li class=""><a href="#getmempoolancestors" id="markdown-toc-getmempoolancestors" class="">GetMemPoolAncestors</a></li>
<li class=""><a href="#getmempooldescendants" id="markdown-toc-getmempooldescendants" class="">GetMemPoolDescendants</a></li>
<li class=""><a href="#getmempoolentry" id="markdown-toc-getmempoolentry" class="">GetMemPoolEntry</a></li>
<li class=""><a href="#getmempoolinfo" id="markdown-toc-getmempoolinfo" class="">GetMemPoolInfo</a></li>
<li class=""><a href="#getmininginfo" id="markdown-toc-getmininginfo" class="">GetMiningInfo</a></li>
<li class=""><a href="#getnettotals" id="markdown-toc-getnettotals" class="">GetNetTotals</a></li>
<li class=""><a href="#getnetworkhashps" id="markdown-toc-getnetworkhashps" class="">GetNetworkHashPS</a></li>
<li class=""><a href="#getnetworkinfo" id="markdown-toc-getnetworkinfo" class="">GetNetworkInfo</a></li>
<li class=""><a href="#getnewaddress" id="markdown-toc-getnewaddress" class="">GetNewAddress</a></li>
<li class=""><a href="#getpeerinfo" id="markdown-toc-getpeerinfo" class="">GetPeerInfo</a></li>
<li class=""><a href="#getrawchangeaddress" id="markdown-toc-getrawchangeaddress" class="">GetRawChangeAddress</a></li>
<li class=""><a href="#getrawmempool" id="markdown-toc-getrawmempool" class="">GetRawMemPool</a></li>
<li class=""><a href="#getrawtransaction" id="markdown-toc-getrawtransaction" class="">GetRawTransaction</a></li>
<li class=""><a href="#getreceivedbyaccount" id="markdown-toc-getreceivedbyaccount" class="">GetReceivedByAccount</a></li>
<li class=""><a href="#getreceivedbyaddress" id="markdown-toc-getreceivedbyaddress" class="">GetReceivedByAddress</a></li>
<li class=""><a href="#gettransaction" id="markdown-toc-gettransaction" class="">GetTransaction</a></li>
<li class=""><a href="#gettxout" id="markdown-toc-gettxout" class="">GetTxOut</a></li>
<li class=""><a href="#gettxoutproof" id="markdown-toc-gettxoutproof" class="">GetTxOutProof</a></li>
<li class=""><a href="#gettxoutsetinfo" id="markdown-toc-gettxoutsetinfo" class="">GetTxOutSetInfo</a></li>
<li class=""><a href="#getunconfirmedbalance" id="markdown-toc-getunconfirmedbalance" class="">GetUnconfirmedBalance</a></li>
<li class=""><a href="#getwalletinfo" id="markdown-toc-getwalletinfo" class="">GetWalletInfo</a></li>
<li class=""><a href="#getwork" id="markdown-toc-getwork" class="">GetWork</a></li>
<li class=""><a href="#help" id="markdown-toc-help" class="">Help</a></li>
<li class=""><a href="#importaddress" id="markdown-toc-importaddress" class="">ImportAddress</a></li>
<li class=""><a href="#importmulti" id="markdown-toc-importmulti" class="">ImportMulti</a></li>
<li class=""><a href="#importprivkey" id="markdown-toc-importprivkey" class="">ImportPrivKey</a></li>
<li class=""><a href="#importprunedfunds" id="markdown-toc-importprunedfunds" class="">ImportPrunedFunds</a></li>
<li class=""><a href="#importwallet" id="markdown-toc-importwallet" class="">ImportWallet</a></li>
<li class=""><a href="#keypoolrefill" id="markdown-toc-keypoolrefill" class="">KeyPoolRefill</a></li>
<li class=""><a href="#listaccounts" id="markdown-toc-listaccounts" class="">ListAccounts</a></li>
<li class=""><a href="#listaddressgroupings" id="markdown-toc-listaddressgroupings" class="">ListAddressGroupings</a></li>
<li class=""><a href="#listbanned" id="markdown-toc-listbanned" class="">ListBanned</a></li>
<li class=""><a href="#listlockunspent" id="markdown-toc-listlockunspent" class="">ListLockUnspent</a></li>
<li class=""><a href="#listreceivedbyaccount" id="markdown-toc-listreceivedbyaccount" class="">ListReceivedByAccount</a></li>
<li class=""><a href="#listreceivedbyaddress" id="markdown-toc-listreceivedbyaddress" class="">ListReceivedByAddress</a></li>
<li class=""><a href="#listsinceblock" id="markdown-toc-listsinceblock" class="">ListSinceBlock</a></li>
<li class=""><a href="#listtransactions" id="markdown-toc-listtransactions" class="">ListTransactions</a></li>
<li class=""><a href="#listunspent" id="markdown-toc-listunspent" class="">ListUnspent</a></li>
<li class=""><a href="#lockunspent" id="markdown-toc-lockunspent" class="">LockUnspent</a></li>
<li class=""><a href="#move" id="markdown-toc-move" class="">Move</a></li>
<li class=""><a href="#ping-rpc" id="markdown-toc-ping-rpc" class="">Ping</a></li>
<li class=""><a href="#preciousblock" id="markdown-toc-preciousblock" class="">PreciousBlock</a></li>
<li class=""><a href="#prioritisetransaction" id="markdown-toc-prioritisetransaction" class="">PrioritiseTransaction</a></li>
<li class=""><a href="#pruneblockchain" id="markdown-toc-pruneblockchain" class="">PruneBlockChain</a></li>
<li class=""><a href="#removeprunedfunds" id="markdown-toc-removeprunedfunds" class="">RemovePrunedFunds</a></li>
<li class=""><a href="#sendfrom" id="markdown-toc-sendfrom" class="">SendFrom</a></li>
<li class=""><a href="#sendmany" id="markdown-toc-sendmany" class="">SendMany</a></li>
<li class=""><a href="#sendrawtransaction" id="markdown-toc-sendrawtransaction" class="">SendRawTransaction</a></li>
<li class=""><a href="#sendtoaddress" id="markdown-toc-sendtoaddress" class="">SendToAddress</a></li>
<li class=""><a href="#setaccount" id="markdown-toc-setaccount" class="">SetAccount</a></li>
<li class=""><a href="#setban" id="markdown-toc-setban" class="">SetBan</a></li>
<li class=""><a href="#setgenerate" id="markdown-toc-setgenerate" class="">SetGenerate</a></li>
<li class=""><a href="#setnetworkactive" id="markdown-toc-setnetworkactive" class="">SetNetworkActive</a></li>
<li class=""><a href="#settxfee" id="markdown-toc-settxfee" class="">SetTxFee</a></li>
<li class=""><a href="#signmessage" id="markdown-toc-signmessage" class="">SignMessage</a></li>
<li class=""><a href="#signmessagewithprivkey" id="markdown-toc-signmessagewithprivkey" class="">SignMessageWithPrivKey</a></li>
<li class=""><a href="#signrawtransaction" id="markdown-toc-signrawtransaction" class="">SignRawTransaction</a></li>
<li class=""><a href="#stop" id="markdown-toc-stop" class="">Stop</a></li>
<li class=""><a href="#submitblock" id="markdown-toc-submitblock" class="">SubmitBlock</a></li>
<li class=""><a href="#validateaddress" id="markdown-toc-validateaddress" class="">ValidateAddress</a></li>
<li class=""><a href="#verifychain" id="markdown-toc-verifychain" class="">VerifyChain</a></li>
<li class=""><a href="#verifymessage" id="markdown-toc-verifymessage" class="">VerifyMessage</a></li>
<li class=""><a href="#verifytxoutproof" id="markdown-toc-verifytxoutproof" class="">VerifyTxOutProof</a></li>
<li class=""><a href="#walletlock" id="markdown-toc-walletlock" class="">WalletLock</a></li>
<li class=""><a href="#walletpassphrase" id="markdown-toc-walletpassphrase" class="">
WalletPassphrase</a></li>
<li class=""><a href="#walletpassphrasechange" id="markdown-toc-walletpassphrasechange" class="">
WalletPassphraseChange</a></li>
</ul>
</li>
</ul>
</li>
<li class=""><a href="#http-rest" id="markdown-toc-http-rest" class="">HTTP REST</a>        <ul class="">
<li class=""><a href="#rest-quick-reference" id="markdown-toc-rest-quick-reference" class="">Quick Reference</a></li>
<li class=""><a href="#requests" id="markdown-toc-requests" class="">Requests</a>            <ul class="">
<li class=""><a href="#get-block" id="markdown-toc-get-block" class="">GET Block</a></li>
<li class=""><a href="#get-blocknotxdetails" id="markdown-toc-get-blocknotxdetails" class="">GET Block/NoTxDetails</a></li>
<li class=""><a href="#get-chaininfo" id="markdown-toc-get-chaininfo" class="">GET ChainInfo</a></li>
<li class=""><a href="#get-getutxos" id="markdown-toc-get-getutxos" class="">GET GetUtxos</a></li>
<li class=""><a href="#get-headers" id="markdown-toc-get-headers" class="">GET Headers</a></li>
<li class=""><a href="#get-mempoolcontents" id="markdown-toc-get-mempoolcontents" class="">GET MemPool/Contents</a></li>
<li class=""><a href="#get-mempoolinfo" id="markdown-toc-get-mempoolinfo" class="">GET MemPool/Info</a></li>
<li class=""><a href="#get-tx" id="markdown-toc-get-tx" class="">GET Tx</a></li>
</ul>
</li>
</ul>
</li>
</ul>
</li>
</ul>

<ul class="goback"><li class=""><a href="/en/developer-documentation" class="">Return To Overview</a></li></ul>
<ul class="reportissue"><li class=""><a href="https://github.com/bitcoin-dot-org/bitcoin.org/issues/new" onmouseover="updateIssue(event);" class="">Report An Issue</a></li></ul>
<ul class="editsource"><li class=""><a href="https://github.com/bitcoin-dot-org/bitcoin.org/edit/master/_data/devdocs/en/references/intro.md" onmouseover="updateSource(event);" class="">Edit On GitHub</a></li></ul>

</div>
</div>



</body>
</html>