$(document).ready(function () {
	$.ajaxSetup({
		headers: { 'X-CSRF-Token' : $('meta[name="_token"]').attr('content') }
	});    
	
	var isTermsChecked = $('.terms').is(':checked');

    if (! isTermsChecked) {
        $('.submit').attr('disabled', 'disabled');
    }
			
    $('.terms').on('change', function (e) {
        var isTermsChecked = $(this).is(':checked');

        if (isTermsChecked) {
            $('.submit').removeAttr('disabled');
        } else {
            $('.submit').attr('disabled', 'disabled');
        }
    });

    $('.withdraw_send_coins').on('change', function (e) {
        var send_amt = $('.withdraw_send_coins').val();		
		send_amt = parseInt(send_amt);
		
		var doge_available_amt = $('#doge_available_amt').val();
		
		if(doge_available_amt<send_amt){
			$('#amt_label').html('Amount : <span style="color:#F00;">Your available amount is '+doge_available_amt+'</span>');
			return false;
		}
		
        if(send_amt>=2 && send_amt<=50000000) {
            $('.withdraw_send_coins_sbmt').removeAttr('disabled');
        } else {
            $('.withdraw_send_coins_sbmt').attr('disabled', 'disabled');
        }
    });

    $('.withdraw_send_btccoins').on('change', function (e) {
        var send_amt = $('.withdraw_send_btccoins').val();		
		send_amt = parseFloat(send_amt);
		$('.withdraw_send_coins_sbmt').removeAttr('disabled');
		/*alert(send_amt);
		send_amt = send_amt+0.003;
		alert(send_amt);
		return false;*/
        
		if(send_amt>=0.00001 && send_amt<=3) {
            $('.withdraw_send_coins_sbmt').removeAttr('disabled');
        } else {
            $('.withdraw_send_coins_sbmt').attr('disabled', 'disabled');
        }
    });

	$('.load_domain').on('click', function (e) {
		var domain = $('#domain').val();
		var domain_val = domain;
		/*var m = url.split('/')[0].match(/[^.]+\.[^.]+$/);
		if()
		var domain = m[0];*/
		
		//domain = domain.hostname;
		
		//alert(domain);
		//return false;
		$("#domain").prop('disabled', true);
		$(".load_domain").prop('disabled', true);


		//var url = "scratch9944.com/web-development/javascript/";
		domain = domain.replace('http://','').replace('https://','').split(/[/?#]/)[0];		
		//alert(domain);
		
		$.ajax({ 
			url: "/siteheader",
			data: {'_token': $('input[name=_token]').val(),'domain' : domain},
			type: 'POST',
			success: function(httpcode){
				console.log(httpcode);
				//alert(httpcode);
				if(httpcode == 200){
					$('#http-search-form').attr("action","http://tipestry.com/sites");					
				}else{
					$('#http-search-form').attr("action","https://tipestry.com/sites");
				}
				
				$('#http-domain').val(domain_val);
				$('#http-search-form').submit();
			},
            error: function (data) {
				//console.log('Error:', data);
            }
		});
	});
	
});