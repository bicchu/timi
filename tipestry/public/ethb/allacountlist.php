<?php
require_once("./vendor/autoload.php");
use Web3\Web3;
use Web3\Providers\HttpProvider;
use Web3\RequestManagers\HttpRequestManager;
$web3 = new Web3(new HttpProvider(new HttpRequestManager('http://34.238.252.237/api/eth', 100.1)));

$web3->eth->accounts(function ($err, $accounts) use ($web3) {
  if ($err !== null) {
      echo 'Error: ' . $err->getMessage();
      return;
  }
  echo '<br>';
  foreach ($accounts as $account) {
      echo 'Account: ' . $account . PHP_EOL;
      $web3->eth->getBalance($account, function ($err, $balance) {
          if ($err !== null) {
              echo 'Error: ' . $err->getMessage();
              return;
          }
          echo 'Balance: ' . $balance . PHP_EOL.'<br>';
      });
  }
    echo '<hr>';
});

?>