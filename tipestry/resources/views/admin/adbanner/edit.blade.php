@extends ('layouts.admin')

@section ('content')
<?php 



/*
echo 'pageTitle +++ details *** users';
echo '<pre>';print_r($pageTitle);echo '</pre>';
echo '<pre>';print_r($details);echo '</pre>';
echo '<pre>';print_r($users);echo '</pre>';
exit;*/

?>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
					<h4 style="color:#3c8dbc;">{!! Session::has('msg') ? Session::get("msg") : '' !!}</h4>
					<form role="form" method="POST" id="reused_form" action="{{ route('admin.adbanner.editpost', $details[0]->id) }}" >
						{{ csrf_field() }}
						<input type="hidden" name="bannerid" value="{{ $details[0]->id }}" />
						<div class="form-group">
							<label for="name" class="col-md-3">User Name:</label>
							<?php	echo $users[$details[0]->user_id]['name'];	?>
						</div>
						<div class="form-group">
							<label for="name" class="col-md-3">Email ID:</label>
							<?php	echo $users[$details[0]->user_id]['email'];	?>
						</div>
						<div class="form-group">
							<label for="name" class="col-md-3">Amount:</label>
							<?php	echo '<b>$ '.$details[0]->paid_ammount.'</b>';	?>
						</div>
                        
                        
						<div class="form-group">
							<label for="name" class="col-md-3">Hash Code:</label>
							<?php	echo $details[0]->payment_hashcode;	?>
						</div>
						<div class="form-group">
							<label for="email" class="col-md-3">Date:</label>
							<?php echo $details[0]->banner_from_date;?> &nbsp;
							to &nbsp;
							<?php echo $details[0]->banner_to_date;?>
						</div>
				<?php /*  <div class="form-group">
							<label for="email" class="col-md-3">Banner Content:</label> */?>
							<?php /* echo $details[0]->banner_content; */?> 
						</div>
						<div class="form-group" class="col-md-3">
							<label for="email"  class="col-md-3">Banner Current Status:</label>
							<input type="radio" name="bannerstatus" value="0" <?php	if($details[0]->status == 0){echo ' checked ';}	?>  /> &nbsp; Pending &nbsp;  &nbsp; 
							<input type="radio" name="bannerstatus" value="1" <?php	if($details[0]->status == 1){echo ' checked ';}	?> /> &nbsp; Approved &nbsp;  &nbsp; 
							<input type="radio" name="bannerstatus" value="2" <?php	if($details[0]->status == 2){echo ' checked ';}	?> /> &nbsp; Rejected
						</div>
						<div class="form-group" class="col-md-3">
							<label for="email"  class="col-md-3">Payment Status:</label>
							<input type="radio" name="payment_status" value="0" <?php	if($details[0]->payment_status == 0){echo ' checked ';}	?>  /> &nbsp; Pending &nbsp;  &nbsp; 
							<input type="radio" name="payment_status" value="1" <?php	if($details[0]->payment_status == 1){echo ' checked ';}	?> /> &nbsp; Approved 
						</div>
						<div class="form-group" class="col-md-3">
							<label for="email"  class="col-md-3">Banner Location:</label>
							<?php	
								if($details[0]->banner_position == 'header'){
									$banner_position = 'header';
								}else{
									$banner_position = 'sidebar';
								}
							?>
							<input type="radio" name="banner_position" value="header" <?php	if($details[0]->banner_position == 'header'){echo ' checked ';}	?>  /> &nbsp; Header &nbsp;  &nbsp; 
							<input type="radio" name="banner_position" value="sidebar" <?php	if($details[0]->banner_position != 'header'){echo ' checked ';}	?> /> &nbsp; Sidebar 
						</div>
						<div class="form-group" class="col-md-3">
							<label for="email"  class="col-md-3">User Note:</label>
							<?php	echo $details[0]->user_comment;?>
						</div>
						<div class="form-group" class="col-md-3">
							<label for="email"  class="col-md-3">Admin Note:</label>
							<textarea name="admin_comment" ><?php	echo $details[0]->admin_comment;?></textarea>
						</div>
                        
                        
                        <div class="form-group">
							<label for="name" class="col-md-3">Selected Impression:</label>
							<?php	echo $details[0]->selected_impression;	?>
						</div>
                        
                        <div class="form-group">
							<label for="name" class="col-md-3">Used Impression:</label>
							<?php	echo $details[0]->used_impression;	?>
						</div>
                        <div class="form-group">
							<label for="name" class="col-md-3">Ammount Paid:</label>
							$<?php	echo $details[0]->paid_ammount;	?>
						</div>
                        
                        
                        <div class="form-group">
							<label for="name" class="col-md-3">Destination url:</label>
                            <a href="<?php  echo $details[0]->destination_url ?>" target="_blank";>
                            	<?php  echo $details[0]->destination_url;	?>
                            </a>
						</div>
                       <div class="form-group">
							<label for="name" class="col-md-3">Banner Content:</label>
                            <?php  echo $details[0]->banner_content;	?>
                        </div>
                        
						<div class="form-group">
							<label for="email" class="col-md-3">Image:</label><br>
							<img src="/upload_images/{{ $details[0]->banner_file }}" width="400" />
						</div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
						<div class="form-group">
							<label for="email" class="col-md-3">&nbsp;</label>
							<button type="button" class="btn btn-lg btn-default pull-left" id="btnContactUs" style="margin-right:20px;"  onclick='javascript:window.location.href="http://tipestry.com/admin/adbanner"'>Back</button> &nbsp; &nbsp; 						
							<button type="submit" class="btn btn-lg btn-success pull-left" style="margin-right:20px;">Submit</button> &nbsp; &nbsp;
							<button type="button" class="btn btn-lg btn-danger pull-left" style="margin-right:20px;" onclick="bannerdelete({{ $details[0]->id }})">Delete</button> &nbsp; &nbsp;
						</div>
					</form>
					<form role="form" method="POST" id="deletebannerform" action="{{ route('admin.adbanner.delete') }}" >
						{{ csrf_field() }}
						<input type="hidden" name="bannerid" id="bannerid" value="0" />
					</form>					
					<div id="success_message" style="width:100%; height:100%; display:none; "> <h3>Sent your message successfully!</h3> </div>
					<div id="error_message" style="width:100%; height:100%; display:none; "> <h3>Error</h3> Sorry there was an error sending your form. </div>	
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection