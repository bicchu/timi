@extends ('layouts.admin')

@section ('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">User list</h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
								<th>SL.</th>
								<th>User Name</th>
								<th align="center" style="text-align:center;">Date</th>
								<th>Location</th>
								<th>Impression Bought /Used</th>
                                <th>Hourly Impression Allocated / Hourly Used</th>
								<th>Payment</th>
								<th>Image</th>
								<th>Status</th>
                                <th>Banner Position</th>
								<th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($adbannerlist as $val)
							<?php if($val->status == 99){continue;}?>
                            <tr>
								<td>{{ $val->id }}</td>
								<td><?php	echo $users[$val->user_id]['name'].'<br>'.$users[$val->user_id]['email'];	?></td>
								<td align="center">
									{{ $val->banner_from_date }}<br>
										to <br>
									{{ $val->banner_to_date }}
								</td>
								<td align="center"><?php echo ucfirst($val->banner_position); ?></td>
								<td><?php echo $val->selected_impression.'/<br>'.$val->used_impression;?></td>
                                
                                <td><?php echo $val->alocate_count_per_hour .'/<br>'. $val->used_count_per_hour;?></td>
                                
                                
								<td align="center">
									<?php
										echo '<b>$'.$val->paid_ammount.'</b><br>';
										if($val->payment_status == 0){
											echo 'Pending';
										}else{
											echo 'Received';
										}
									?>
                                    
                                    
                                    
                                    
								</td>
								<td>
                                
                                <?php 
								/*
								$img_data = getimagesize('/upload_images/'.$val->banner_file);
								print_r($img_data);*/
								 ?>
                                <img id="myImg" src="/upload_images/{{ $val->banner_file }}" width="200" /></td>
								<td>
                                
                          <div id="myModal" class="modal">
                          <span class="close">&times;</span>
                          <img class="modal-content" id="img01">
                          <div id="caption"></div>
                          </div>
                                
                                
                                
                                
                                
                                
									<?php
										if($val->status == 0){echo '<span style="color:#f39c12">Pending</span>';}
										elseif($val->status == 1){echo '<span style="color:#00a65a">Approved</span>';}
										elseif($val->status == 2){echo '<span style="color:#dd4b39">Rejected</span>';}
									?>
								</td>
                                <td>{{ $val->banner_position }}</td>
                                
                                
                                
                                
                                <td>
									<?php
										$action_url = '/admin/adbanner/edit/'.$val->id;
										$delete_url = '/admin/adbanner/delete/'.$val->id;
									?>
									<a href="<?php echo $action_url; ?>" style="margin-right:10px; font-weight:bold; color:#367fa9;" >Edit</a>
									<a href="#" style="margin-right:10px; color:#dd4b39; font-weight:bold;" onclick="bannerdelete(<?php echo $val->id;?>)" >Delete</a>
									<?php /*
									<form method="POST"
                                          action="{{ route('admin.adbanner.destroy', $val) }}"
                                          class="inline"
                                          onsubmit="return confirm('Are you sure?');"
                                    >
										
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}

                                        <button type="submit"
                                                class="btn btn-xs btn-danger"
                                                data-toggle="tooltip"
                                                data-placement="top"
                                                title="Suspend">
                                            <i class="fa fa-times" aria-hidden="true"></i>
                                        </button>
                                    </form>		*/ ?>							
									
								</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
<form role="form" method="POST" id="deletebannerform" action="{{ route('admin.adbanner.delete') }}" >
	{{ csrf_field() }}
	<input type="hidden" name="bannerid" id="bannerid" value="0" />
</form>



<style>
body {font-family: Arial, Helvetica, sans-serif;}

#myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Caption of Modal Image */


/* Add Animation */
.modal-content, #caption {    
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}

/* The Close Button */
.close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.close:hover,
.close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
</style>

<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
    modal.style.display = "none";
}
</script>




@endsection



