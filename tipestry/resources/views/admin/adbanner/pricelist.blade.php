@extends ('layouts.admin')

@section ('content')

<?php
//print_r($pricelist);exit;
?>

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                
				
				<div class="box-header">
                    <h3 class="box-title">User list</h3>
                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                
				
				<!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
								<th>SL.</th>
								<th>Location</th>
								<th>Impression</th>
								<th>Price</th>	
                                <?php /*<th>Action</th>								*/ ?>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($pricelist as $val)
							<tr>
								<td>{{ $val->id }}</td>
								<td><?php	echo $val->location_name;	?></td>
								<td >{{ $val->impresion_num }}</td>
<?php /*<td id="price-<?php echo $val->id;?>"><?php	echo '<b>$'.$val->price.'</b>';?></td>
<td><button class="label label-success" onclick="price_hide(<?php echo $val->id;?>)" style="position:fixed">Edit</button></td> */?> 

<td>
	<span id="txt-<?php echo $val->id;?>"><?php	echo '<b>$'.$val->price.'</b>';?></span>
	<input type="text" id="input_price_<?php echo $val->id;?>" style="display:none;" value="<?php	echo $val->price;?>" />
</td>
<td>
	<input type="button" id="bouuton_<?php echo $val->id;?>" onclick="edit(<?php echo $val->id;?>)" value="Edit" />
	<input type="button" id="sbmt_btn_<?php echo $val->id;?>" onclick="sbmt_frm(<?php echo $val->id;?>)" value="Save" />
</td>
                           </tr>
                        @endforeach
                        </tbody>
                         
                    </table>
                </div>
				
				
            </div>
        </div>
    </div>

   <form class="" id="banner_price_update" method="POST" action="{{ route('admin.price-update') }}" style="display:none;">
                        {{ csrf_field() }}
     <input type="text" name="new_price" id="new_price" value="0">
     <input type="text" name="id_new" id="id_new" value="0">
     <input type="submit" value="submit">
    </form>
@endsection



