<footer class="main-footer">
    <strong>
        Copyright © {{ copyrightYears(2017) }} <a href="{{ url('/') }}">{{ config('app.name') }}</a>.
    </strong> All rights reserved.
</footer>