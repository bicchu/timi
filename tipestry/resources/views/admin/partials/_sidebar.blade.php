<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel" style="height: 65px;">
            <div class="info">
                <p>{{ $user->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Menu</li>

            <li class="active">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-dashboard" aria-hidden="true"></i>

                    <span>Dashboard</span>
                </a>
            </li>

            <li class="treeview">
                <a href="#"><i class="fa fa-users" aria-hidden="true"></i><span>Users</span></a>

                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('admin.users.index') }}">
                            <i class="fa fa-check" aria-hidden="true"></i>Active
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('admin.users.suspended') }}">
                            <i class="fa fa-times" aria-hidden="true"></i>Suspended
                        </a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-users" aria-hidden="true"></i><span>Ad Banners</span></a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('admin.adbanner.index') }}">
                            <i class="fa fa-check" aria-hidden="true"></i>Banner List
                        </a>
                    </li>
					<li>
                        <a href="{{ route('admin.adbanner-price') }}">
                            <i class="fa fa-check" aria-hidden="true"></i>Banner Price List
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>