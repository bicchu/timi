@extends ('layouts.admin')

@push ('styles')
    <style>
        .info-box-number {
            font-size: 14px;
        }
    </style>
@endpush

@section ('content')
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Users</span>
                    <span class="info-box-number">{{ $users }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-comments" aria-hidden="true"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Comments total</span>
                    <span class="info-box-number">{{ $commentsTotal }}</span>
                    <span class="info-box-text">Comments today</span>
                    <span class="info-box-number">{{ $commentsToday }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-bitcoin" aria-hidden="true"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Total given</span>
                    <span class="info-box-number">{{ $givenTotal }}</span>
                    <span class="info-box-text">Today given</span>
                    <span class="info-box-number">{{ $givenToday }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-money" aria-hidden="true"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Total withdrawn</span>
                    <span class="info-box-number">{{ $withdrawnTotal }}</span>

                    <span class="info-box-text">Today withdrawn</span>
                    <span class="info-box-number">{{ $withdrawnToday }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
@endsection