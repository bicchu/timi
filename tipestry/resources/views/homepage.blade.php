@extends ('layouts.app')

@section ('content')
    <div class="container">
        <br>

        @if (session()->has('message'))
            <div class="alert alert-info" role="alert">
                {{ session('message') }}
            </div>
        @endif

        <h1 class="trending-topics-heading">Trending topics</h1>

        <div class="row">
            <div class="col-md-12">
                <ul class="TrendingList" id="p3">
                    @foreach ($trendingTopics as $index => $topic)
                        <?php
							$action_url = action('TopicsController@show', [$topic->site, $topic]);
							$topic_url = $topic->site->url;
							$n = strpos($topic_url,'http://');
							if(isset($n) && $n>-1){
								$action_url = str_replace('https://','http://',$action_url);
							}
							else{
								$action_url = str_replace('http://','https://',$action_url);
							}
						?>
						<li>
						<?php /*<a href="{{ action('TopicsController@show', [$topic->site, $topic]) }}" class="TrendingList__item">*/ ?>
							<a href="<?php echo $action_url;?>" class="TrendingList__item">
                                <span class="TrendingList__number">
									{{ $index + 1 }}.
                                </span> {{ $topic->title }}
                            </a>&nbsp;&nbsp;
                            <a href="{{ action('SitesController@index', [$topic->site]) }}" class="text-muted">
                                <b>({{ $topic->site->host }})</b>
                            </a>
                            <div class="TrendingList__meta-block">
                                @if ($topic->comments->count() > 0)
									<span class="TrendingList__meta text-muted">
										Posted <b>{{ (new \Carbon\Carbon($topic->trand_date))->diffForHumans() }}</b> by <b>{{ $topic->user->name }}</b>
									</span>
									<span class="TrendingList__meta text-muted">
										<b>{{ $topic->comments->count() }}</b> comments
									</span>
								@else
									<span class="TrendingList__meta text-muted">
										Created at 
										<b>
											{{ (new \Carbon\Carbon($topic->created_at))->diffForHumans() }}
										</b>
										by <b>{{ $topic->user->name }}</b>
									</span>
								@endif
								
								@if ($topic->hasGifts('btc'))
                                    {!! $topic->renderTotalGifts('btc') !!}
                                @endif								
                                @if ($topic->hasGifts('bch'))
                                    {!! $topic->renderTotalGifts('bch') !!}
                                @endif								
                                @if ($topic->hasGifts('doge'))
                                    {!! $topic->renderTotalGifts('doge') !!}
                                @endif
								@if ($topic->hasGifts('eth'))
                                    {!! $topic->renderTotalGifts('eth') !!}
                                @endif
								@if ($topic->hasGifts('ethtip'))
                                    {!! $topic->renderTotalGifts('ethtip') !!}
                                @endif
								
								
								<?php /*
                                @if (count($topic->gifts))
                                    <span class="TrendingList__meta text-muted">
                                        {!! $topic->latestGift()->render() !!}
                                    </span>
                                @endif
								*/ ?>
                            </div>
                        </li>
						
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection
