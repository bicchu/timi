@extends ('layouts.app')

@section ('content')
    <div class="container">
        <h1>Dogecoin 货币</h1>
		<h4><b>平衡</b>: <?php echo number_format($btcbalance,8).' Doge';	?></h4>
		<h4><b>钱包地址</b>: <?php echo $btcaddress;?></h4>
		
		<?php
		
			if(!empty($withdraw_msg) && count($withdraw_msg)>0){
				if($withdraw_msg['status'] == 1){
					echo '<p style="color:#00A2E8; font-weight:bold; ">'.$withdraw_msg['message'].'</p>';
				}
				else{
					echo '<p style="color:#F00;">'.$withdraw_msg['message'].'</p>';
				}
			}
		?>
        <hr>
		
        <div class="row">
            <div class="col-md-8">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#deposit" aria-controls="deposit" role="tab" data-toggle="tab">存款硬币</a></li>
                    <li role="presentation"><a href="#withdraw" aria-controls="withdraw" role="tab" data-toggle="tab">撤回/发送硬币</a></li>
                    <li role="presentation"><a href="#btc-list" aria-controls="btc-list" role="tab" data-toggle="tab">钱包地址列表</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="deposit">
                        <div class="well">
                            <div class="form-group">
                                <label for="wallet_address">您的存款地址: </label>                                
                            </div>
                            <form action="{{ route('currencies.doge.regenerate') }}" method="POST">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;
                                        获取新的钱包地址
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div><!-- ./Deposit -->

                    <div role="tabpanel" class="tab-pane" id="withdraw">
                        <div class="well">
							<input type="hidden" id="doge_available_amt" value="" />
                            <form action="{{ route('currencies.doge.withdraw') }}" method="POST">
                                {{ csrf_field() }}
								<div class="form-group">
                                    <label for="wallet_address">收件人钱包地址:</label>
                                    <input type="text"
                                           id="wallet_address"
                                           name="recipient"
                                           class="form-control"
                                           placeholder="wallet address"
										   onkeyup = "dogewithdrawbtn()"
                                    >
                                </div>

                                <div class="form-group">
                                    <label for="amount">数量:</label>
                                    <input type="text"
                                           name="amount"
                                           id="amount"
                                           class="form-control withdraw_send_coins"
                                           placeholder="Amount"
										   onkeyup = "dogewithdrawbtn()"
                                    >
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-success withdraw_send_coins_sbmt" disabled>
                                        <i class="fa fa-exchange" aria-hidden="true"></i>&nbsp;
                                        撤回
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div><!-- ./Withdraw -->

                    <div role="tabpanel" class="tab-pane" id="btc-list">
                        <div class="well">
                            <ul class="list-group">
                                <?php /*@foreach (auth()->user()->dogeAddresses as $wallet)
                                    <li class="list-group-item{{ $loop->last ? ' active' : '' }}">
                                        @if ($loop->last)
                                            <span class="badge">
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </span>
                                        @endif
                                        {{ $wallet->address }}
                                    </li>
                                @endforeach*/?>
                            </ul>
                        </div>
                    </div><!-- ./List -->
                </div>

            </div>
        </div>
		
    </div>
@endsection