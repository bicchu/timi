@extends ('layouts.app')

@section ('content')
    <div class="container">
        <h1>ETH 货币</h1>
        <h4><b>平衡</b>: <?php echo number_format($btcbalance,8);?></h4>
        <h4><b>钱包地址</b>: <?php echo $btcaddress_org;?></h4>
		
		
		
		
		<p id="withdrawmsg" style="color:#00A2E8; font-weight:bold; display:none; "></p>
		<p id="withdrawmsgerr" style="color:#F00;  display:none;"></p>
        <hr>
		<div class="row">
            <div class="col-md-8">
				<!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#deposit" aria-controls="deposit" role="tab" data-toggle="tab">存款硬币</a></li>
                    <li role="presentation"><a href="#withdraw" aria-controls="withdraw" role="tab" data-toggle="tab">撤回/发送硬币</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="deposit">
                        <div class="well">
                            <div class="form-group">
                                <label for="wallet_address">您的存款地址: </label>
                                <input type="text"
                                       id="wallet_address"
                                       class="form-control"
                                       value="<?php echo $btcaddress_org;?>"
                                       readonly
                                >
                            </div>
                        </div>
                    </div><!-- ./Deposit -->

                    <div role="tabpanel" class="tab-pane" id="withdraw">
                        <div class="well" style="position:relative;">
							<?php /*<form action="{{ route('currencies.eth.withdraw') }}" method="POST">*/ ?>
							<form action="http://demo.tipestry.com/ethb/withdraw.php" method="POST">
                                {{ csrf_field() }}
								<input type="hidden" name="fromAccount" id="fromAccount" value="<?php echo $btcaddress_org;?>" />

                                <div class="form-group">
                                    <label for="wallet_address">收件人钱包地址:</label>
                                    <input type="text"
                                           id="recipient"
                                           name="recipient"
                                           class="form-control"
                                           placeholder="wallet address"
										   value=""
                                    >
                                </div>
								<img src="http://demo.tipestry.com/images/loader01.gif" class="loadergif" style="display:none; position:absolute; top:70px;" />
                                <div class="form-group">
                                    <label for="amount">数量:</label>
                                    <input type="text"
                                           name="amount"
                                           id="amount"
                                           class="form-control"
                                           placeholder="Amount"
                                    >
                                </div>

                                <div class="form-group">
                                    <button type="button" onclick="ethwithdraw()" class="btn btn-success">
                                        <i class="fa fa-exchange" aria-hidden="true"></i>&nbsp;
                                        撤回
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
	ethwithdraw = function(){		
		var fromAccount	= $('#fromAccount').val();
		var recipient	= $('#recipient').val();
		var amount		= $('#amount').val();
		amount = parseFloat(amount);
		amount = amount*1000000000000000000;

		$('.loadergif').show();
		
		if(recipient == ''){
			alert('Recipient account address can\'t be blank.');return false;
		}
		if(amount == ''){
			alert('Amount can\'t be blank.');return false;
		}
		
		var data = 'fromAccount='+fromAccount+'&recipient='+recipient+'&amount='+amount;
				
		$.ajax({
			type: "POST",
			url: "http://demo.tipestry.com/ethb/withdraw.php",
			data: data,
			dataType: "json",
			success: function(data) {
				//alert(data);
				//$('#recipient').val('');
				//$('#amount').val('');
				$('.loadergif').hide();
				//updatetransactiondata(data.msg);
				if(data.status == 1){
					//updatetransactiondata(data.msg);
					$('#withdrawmsg').html(data.msg);
					$('#withdrawmsg').show();
				}else{
					$('#withdrawmsgerr').html(data.msg);
					$('#withdrawmsgerr').show();
				}
			},
			error: function (error) {
				alert('error; ' + eval(error));
			}
		});
	}
		
	updatetransactiondata = function(){
		/*var transactonid = arguments[0];
		var 	
		$.ajax({
			type: "POST",
			url: "http://demo.tipestry.com/ethb/withdraw.php",
			data: data,
			dataType: "json",
			success: function(data) {
				//$('#recipient').val('');
				//$('#amount').val('');
				$('.loadergif').hide();
				updatetransactiondata(data.msg);
				if(data.status == 1){
					//updatetransactiondata(data.msg);
					$('#withdrawmsg').html(data.msg);
					$('#withdrawmsg').show();
				}else{
					$('#withdrawmsgerr').html(data.msg);
					$('#withdrawmsgerr').show();
				}
			}
		});*/
	}
</script>
@endsection