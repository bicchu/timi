<?php

if(count($trendingTopics)<1)
{
?>
<li id="hide_ad" style="border: 3px solid #ccc;">
	<div class="row">
		<div class="col-sm-12 imgborder">	
			No More Topics
		</div>
	</div>
</li>

<?php
}
else
{
?>


<?php if($header_banner_file != ''){	?>



<li style="border:2px solid rgb(150,150,150); margin-buttom:1%;"  id="hide_ad">
	<div class="row">
		<button id="hide" style="float:right; margin-right:15px; margin-top:-10px; display:none;">X</button>
		<div class="col-sm-3 imgborder1">
			<?php /*<span class="TrendingList__number1" style=" background-color: #fff; border-radius: 3px; color: #006621; display: inline-block; font-size: 11px; border: 1px solid #006621; padding: 1px 3px 0 2px; margin-left:2px; line-height: 11px; vertical-align: baseline;">ads.</span>*/ ?> 
			<a href="#">
				<div>
					<?php /*<img width="150" src="/images/tipestry-logo.png">*/ ?>
					<img width="80" style="border:inherit;"  src="/upload_images/<?php echo $header_banner_file;	?>">
				</div>
			</a>
		</div> 
		<div class="col-sm-9">
			<div class="moveleft1">
				<a href="#" class="TrendingList__item">
					<?php echo $banner_content;?>
				</a>&nbsp;&nbsp;
				<a href="http://<?php echo $destination_url;?>" target="_blank" class="text-muted"><b>(<?php echo $destination_url;?>)</b></a>
			</div> 
			<div class="TrendingList__meta-block moveleft">
				<span class="TrendingList__meta text-muted">Posted by <b><?php echo $banner_postedby;?></b></span>
			</div>
		</div>
	</div>
</li>








<?php /*
<li id="hide_ad" style="border: 3px solid #ccc;" >
	<div class="row">
		<div class="col-sm-12 imgborder">	
			<img width="100%" height="70%" style="border:inherit;"  src="/upload_images/<?php echo $header_banner_file;	?>">
		</div>
	</div>
</li>*/ ?>
<?php }	?>

@foreach ($trendingTopics as $index => $topic)                    
	<?php
		//$pagenum = 1;		
		if($index<50*($pagenum-1)){
			continue;
		}
		
		if($index>50*$pagenum-1){
			break;
		}
		//echo '<pre>';print_r($topic->screen_path);exit;
		$action_url = action('TopicsController@show', [$topic->site, $topic]);
		$topic_url 	= $topic->site->url;
		$n 			= strpos($topic_url,'http://');
		
		if(isset($n) && $n>-1){
			$action_url = str_replace('https://','http://',$action_url);
		}
		else{
			$action_url = str_replace('http://','https://',$action_url);
		}
	?>	
	<li>
		<div class="row">
			<div class="col-sm-3 imgborder">
				<span class="TrendingList__number" style="min-width:50px;">
					{{ $index + 1 }}.
				</span>
				<a href="<?php echo $action_url;?>" >
					<?php
						$topicsite 			= $topic->site;
						$topicsiteurl 		= $topicsite['url'];
						$topicsiteurl_isimg = -1;
						$img_ext = array('jpg','jpeg','png','gif');
						foreach($img_ext as $ext)
						{
							$pos = strripos(strtolower($topicsite['url']),$ext);
							if($pos>1){
								
								$topicsiteurl_isimg = @getimagesize($topicsiteurl);
								break;
							}
						}

						if($topicsiteurl_isimg == 1){
							echo '<img src="'.$topicsiteurl.'" width="150" />';
						}
						elseif(!empty($topic->screenshot) && $topic->screenshot != ''){
							echo "<img width=\"150\" src=\"data:image/jpeg;base64,".$topic->screenshot."\" />";
						}
						elseif(!empty($topic->site->screen_path) && $topic->site->screen_path != ''){
							echo "<img width=\"150\" src=\"https://tipestry.com/".$topic->site->screen_path."\" />";
						}
						else{
							echo "<img width=\"150\" src=\"https://tipestry.com/images/default-img.png\" />";
						}
					?>
				</a>
			</div>
			<div class="col-sm-9">
				<div class="moveleft1">
					<a href="<?php echo $action_url;?>" class="TrendingList__item">
						{{ ucfirst($topic->title) }}
					</a>&nbsp;&nbsp;
					<a href="{{ action('SitesController@index', [$topic->site]) }}" class="text-muted">
						<b>({{ $topic->site->host }})</b>
					</a>
				</div>
				<div class="TrendingList__meta-block moveleft">
					@if ($topic->comments->count() > 0)
						<?php
							$dtd = date('Y-m-d H:i:s',$topic->Orderby);
						?>
						<span class="TrendingList__meta text-muted">
							Posted <b>{{ (new \Carbon\Carbon($dtd))->diffForHumans() }}</b> by <b>{{ $topic->user->name }}</b>
						</span>
						<span class="TrendingList__meta text-muted">
							<b>{{ $topic->comments->count() }}</b> comments
						</span>
					@else
						<span class="TrendingList__meta text-muted">
							Created at 
							<b>
								{{ (new \Carbon\Carbon($topic->created_at))->diffForHumans() }}
							</b>
							by <b>{{ $topic->user->name }}</b>
						</span>
					@endif
					@if ($topic->hasGifts('btc'))
						{!! $topic->renderTotalGifts('btc') !!}
					@endif								
					@if ($topic->hasGifts('bch'))
						{!! $topic->renderTotalGifts('bch') !!}
					@endif								
					@if ($topic->hasGifts('doge'))
						{!! $topic->renderTotalGifts('doge') !!}
					@endif
					@if ($topic->hasGifts('eth'))
						{!! $topic->renderTotalGifts('eth') !!}
					@endif
					@if ($topic->hasGifts('ethtip'))
						{!! $topic->renderTotalGifts('ethtip') !!}
					@endif
					
				</div>
			</div>
		</div>							
	</li>	
@endforeach

<?php
}	
?>