<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Login Form</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" href="css/formlogin.css">
    <!--	<link rel="stylesheet" href="css/form-login.css">-->
    <style>
        body {
            padding: 70px;
            background: url(images/login.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }


    </style>
</head>

<body>
    <div class="main-content">

        <!-- You only need this form and the form-login.css -->

        <form class="form-login" method="post" action="#">

            <div class="form-log-in-with-email">

                <div class="form-white-background">
                    <div class="heading_logo">
                        
						
						<h1><a href="{{ url('/') }}"><img src="{{ asset('images/newlogo.png') }}"/></a></h1>
						
						<?php /*<h1>{{ config('app.name') }}<a href="{{ url('/') }}"><img src="{{ asset('images/logo.png') }}"/></a></h1>*/ ?>											
                    </div>
                    <div class="row margin-top">
                        <div class="col-md-6 col-xs-12 border-right">

                            <div class="content_inner">
                                <div class="signup_login">
                                    <div class="button">
                                        <a class="go" href="{{ route('social.redirect', 'google') }}"><i class="anc-go"> </i><span>Continue with Google</span>
                                            <div class="clear"></div>
                                        </a>
                                        <br>
                                        <br>
                                        <br>

                                        <!--
                                                                                <a class="tw" href="#"> <i class="anc-tw"> </i> <span>Continue with Facebook</span>
                                                                                    <div class="clear"> </div>
                                                                                </a>
                                        -->
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="text_extra">
                                    <span class="text_style">
                                        <a href="">Continue With Email.</a>By signing up you indicate that you have read and agree to the <a
                                                href="" target="_blank" rel=""
                                                style="color:#999">Terms of Service</a> and <a href="" target="_blank "
                                                                                               style="color:#999">Privacy Policy</a>.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-xs-12 ">

                            <div class="content_inner2">

                                <form action="{{ route('login') }}" method="POST">
                                    {{ csrf_field() }}

                                    <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email">Login</label>

                                        <input type="email" class="form-control" id="email" placeholder="Enter email"
                                               name="email">

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <input type="password" class="form-control" id="pwd" placeholder="Enter password"
                                               name="password">

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="checkbox">
                                        <a href="{{ route('password.request') }}">Forgot Password?</a>
                                        <button type="submit" class="btn btn-primary lgn">Login</button>
                                    </div>

                                </form>

                            </div>
                        </div>

                    </div>
                    <br>
                    <br>
                </div>

            </div>

        </form>

    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"
    >
    </script>
</body>

</html>