@extends('layouts.app')

@section('content')
<br>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Info!</strong> Tipestry is currently in beta testing and therefore we can take no responsibility for any loss of coins tipped, held, sent or received via the processes integrated into our web platform. Send and hold cryptocurrencies here at your own risk.
                    </div>

                    <form class="form-horizontal" id="reg_frm" <?php /*role="form" */ ?> method="POST" action="https://tipestry.com/index.php/register">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="terms"> <small>By checking this box, I represent that I have the authority to agree to the
                                        <a href="{{ url('terms') }}" target="_blank"><b>Terms and Conditions</b></a> and have read such terms and conditions.</small>
                                </label>
                            </div>
                        </div>

                        <div class="clearfix"></div>
						<div class="form-group">
							<div class="col-md-4">&nbsp;</div>
							<div class="col-md-6">
								<div class="g-recaptcha" data-sitekey="6LfkcEcUAAAAAPF1ha6zpF0nu_hQNH1idBeYHPc9"></div>
								<?php /*<div class="g-recaptcha" data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}"></div>*/ ?>
							</div>
						</div>
                        <div class="clearfix"></div>
                        <div class="form-group" style="margin-top: 20px;">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="buttom" class="btn btn-primary submit11" onclick="unicname()">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="row margin-bottom-10">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <?php /* <a href="{{ route('social.redirect', 'facebook') }}" class="btn btn-lg waves-effect waves-light  btn-block facebook">Facebook</a> */ ?>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <a href="{{ route('social.redirect', 'google') }}" class="btn btn-lg waves-effect waves-light btn-block google">Google+</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
function unicname()
{
	var username	= $('#name').val();
	var url 		= "/checkusername/"+username; 
	$.ajax({ 
		url: url,
		data: '',
		type: 'GET',
		success: function(response){
			if(response>0)
			{
				$('#name').val('');
				alert('Username already exits.');
				//return false;
			}
			else
			{
				alert('hi');
				$('#reg_frm').submit();
			}
		}
	});
}
</script>
@endsection
