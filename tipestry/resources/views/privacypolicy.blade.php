@extends ('layouts.app')

@section ('content')
    

	<style>
	
	.innerText{
		padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;}
	</style>
	
	<div class="container">
        <br>

        @if (session()->has('message'))
            <div class="alert alert-info" role="alert">
                {{ session('message') }}
            </div>
        @endif



        <h1 class="trending-topics-heading">Tipestry Go Privacy Policy</h1>

		<div id='ppBody'>
			<div class='row ppConsistencies' style="display:none;">
					<div class='col-2'>
					<div class="quick-links text-center">Information Collection</div>
					</div>
					<div class='col-2'>
					<div class="quick-links text-center">Information Usage</div>
					</div>
					<div class='col-2'>
					<div class="quick-links text-center">Information Protection</div>
					</div>
					<div class='col-2'>
					<div class="quick-links text-center">Cookie Usage</div>
					</div>
					<div class='col-2'>
					<div class="quick-links text-center">3rd Party Disclosure</div>
					</div>
					<div class='col-2'>
					<div class="quick-links text-center">3rd Party Links</div>
					</div>
					<div class='col-2'>&nbsp;</div>
			</div>
			<div style='clear:both;height:10px;'></div>
				<div class='ppConsistencies' style="display:none;">
				<div class='col-2'>
					<div class="col-12 quick-links2 gen-text-center">Google AdSense</div>
				</div>
				<div class='col-2'>
					<div class="col-12 quick-links2 gen-text-center">
						Fair Information Practices
						<div class="col-8 gen-text-left gen-xs-text-center" style="font-size:12px;position:relative;left:20px;">
							Fair information<br>
							Practices
						</div>
					</div>
				</div>
				<div class='col-2'>
					<div class="col-12 quick-links2 gen-text-center coppa-pad"> COPPA </div>
				</div>
				<div class='col-2'>
					<div class="col-12 quick-links2 quick4 gen-text-center caloppa-pad"> CalOPPA </div>
				</div>
				<div class='col-2'>
					<div class="quick-links2 gen-text-center">
						Our Contact Information<br>
					</div>
				</div>
			</div>
			<div style='clear:both;height:10px;'></div>
			<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;"  style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">
				This privacy policy has been compiled to better serve those who are concerned with how their 'Personally Identifiable Information' (PII) is being used online. PII, as described in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.<br>
			</div>
		<span id='infoCo'></span><br>
		<div class='grayText'><strong>What personal information do we collect from the people that visit our blog, website or app?</strong></div>
		<br />
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;" style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">When ordering or registering on our site, as appropriate, you may be asked to enter your email address  or other details to help you with your experience.</div>
		<br>
		<div class='grayText'><strong>When do we collect information?</strong></div>
		<br />
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;"  style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">We collect information from you when you register on our site or enter information on our site.</div>
		<br>
		<span id='infoUs'></span><br>
		<div class='grayText'><strong>How do we use your information? </strong></div>
		<br />
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;"  style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;"> We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:<br>
		<br>
		</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> To send periodic emails regarding your order or other products and services.</div>
		<span id='infoPro'></span><br>
		<div class='grayText'><strong>How do we protect your information?</strong></div>
		<br />
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;" >We do not use vulnerability scanning and/or scanning to PCI standards.</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">We only provide articles and information. We never ask for credit card numbers.</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">We do not use Malware Scanning.<br>
		<br>
		</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology. </div>
		<br>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">We implement a variety of security measures when a user enters, submits, or accesses their information to maintain the safety of your personal information.</div>
		<br>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">All transactions are processed through a gateway provider and are not stored or processed on our servers.</div>
		<span id='coUs'></span><br>
		<div class='grayText'><strong>Do we use 'cookies'?</strong></div>
		<br />
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">We do not use cookies for tracking purposes </div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;"><br>
		You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since browser is a little different, look at your browser's Help Menu to learn the correct way to modify your cookies.<br>
		</div>
		<br>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">If you turn cookies off, Some of the features that make your site experience more efficient may not function properly.that make your site experience more efficient and may not function properly.</div>
		<br>
		<span id='trDi'></span><br>
		<div class='grayText'><strong>Third-party disclosure</strong></div>
		<br />
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information unless we provide users with advance notice. This does not include website hosting partners and other parties who assist us in operating our website, conducting our business, or serving our users, so long as those parties agree to keep this information confidential. We may also release information when it's release is appropriate to comply with the law, enforce our site policies, or protect ours or others' rights, property or safety. <br>
		<br>
		However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses. </div>
		<span id='trLi'></span><br>
		<div class='grayText'><strong>Third-party links</strong></div>
		<br />
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">Occasionally, at our discretion, we may include or offer third-party products or services on our website. These third-party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.</div>
		<span id='gooAd'></span><br>
		<div class='blueText'><strong>Google</strong></div>
		<br />
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users. https://support.google.com/adwordspolicy/answer/1316548?hl=en <br>
		<br>
		</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">We use Google AdSense Advertising on our website.</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;"><br>
		Google, as a third-party vendor, uses cookies to serve ads on our site. Google's use of the DART cookie enables it to serve ads to our users based on previous visits to our site and other sites on the Internet. Users may opt-out of the use of the DART cookie by visiting the Google Ad and Content Network privacy policy.<br>
		</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;"><br>
		<strong>We have implemented the following:</strong></div>
		<br>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">We, along with third-party vendors such as Google use first-party cookies (such as the Google Analytics cookies) and third-party cookies (such as the DoubleClick cookie) or other third-party identifiers together to compile data regarding user interactions with ad impressions and other ad service functions as they relate to our website. </div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;"><br>
		<strong>Opting out:</strong><br>
		Users can set preferences for how Google advertises to you using the Google Ad Settings page. Alternatively, you can opt out by visiting the Network Advertising Initiative Opt Out page or by using the Google Analytics Opt Out Browser add on.</div>
		<span id='calOppa'></span><br>
		<div class='blueText'><strong>California Online Privacy Protection Act</strong></div>
		<br />
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">CalOPPA is the first state law in the nation to require commercial websites and online services to post a privacy policy.  The law's reach stretches well beyond California to require any person or company in the United States (and conceivably the world) that operates websites collecting Personally Identifiable Information from California consumers to post a conspicuous privacy policy on its website stating exactly the information being collected and those individuals or companies with whom it is being shared. -  See more at: http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf<br>
		</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;"><br>
		<strong>According to CalOPPA, we agree to the following:</strong><br>
		</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">Users can visit our site anonymously.</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">Once this privacy policy is created, we will add a link to it on our home page or as a minimum, on the first significant page after entering our website.<br>
		</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">Our Privacy Policy link includes the word 'Privacy' and can easily be found on the page specified above.</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;"><br>
		You will be notified of any Privacy Policy changes:</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> On our Privacy Policy Page<br>
		</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">Can change your personal information:</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> By emailing us</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;"><br>
		<strong>How does our site handle Do Not Track signals?</strong><br>
		</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">We honor Do Not Track signals and Do Not Track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place. </div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;"><br>
		<strong>Does our site allow third-party behavioral tracking?</strong><br>
		</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">It's also important to note that we do not allow third-party behavioral tracking</div>
		<span id='coppAct'></span><br>
		<div class='blueText'><strong>COPPA (Children Online Privacy Protection Act)</strong></div>
		<br />
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">When it comes to the collection of personal information from children under the age of 13 years old, the Children's Online Privacy Protection Act (COPPA) puts parents in control.  The Federal Trade Commission, United States' consumer protection agency, enforces the COPPA Rule, which spells out what operators of websites and online services must do to protect children's privacy and safety online.<br>
		<br>
		</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">We do not specifically market to children under the age of 13 years old.</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">Do we let third-parties, including ad networks or plug-ins collect PII from children under 13?</div>
		<span id='ftcFip'></span><br>
		<div class='blueText'><strong>Fair Information Practices</strong></div>
		<br />
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have played a significant role in the development of data protection laws around the globe. Understanding the Fair Information Practice Principles and how they should be implemented is critical to comply with the various privacy laws that protect personal information.<br>
		<br>
		</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;"><strong>In order to be in line with Fair Information Practices we will take the following responsive action, should a data breach occur:</strong></div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">We will notify you via email</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> Within 7 business days</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;"><br>
		We also agree to the Individual Redress Principle which requires that individuals have the right to legally pursue enforceable rights against data collectors and processors who fail to adhere to the law. This principle requires not only that individuals have enforceable rights against data users, but also that individuals have recourse to courts or government agencies to investigate and/or prosecute non-compliance by data processors.</div>
		<span id='canSpam'></span><br>
		<div class='blueText'><strong>CAN SPAM Act</strong></div>
		<br />
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations.<br>
		<br>
		</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;"><strong>We collect your email address in order to:</strong></div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> Send information, respond to inquiries, and/or other requests or questions</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> Market to our mailing list or continue to send emails to our clients after the original transaction has occurred.</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;"><br>
		<strong>To be in accordance with CANSPAM, we agree to the following:</strong></div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> Not use false or misleading subjects or email addresses.</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> Identify the message as an advertisement in some reasonable way.</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> Include the physical address of our business or site headquarters.</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> Monitor third-party email marketing services for compliance, if one is used.</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> Honor opt-out/unsubscribe requests quickly.</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> Allow users to unsubscribe by using the link at the bottom of each email.</div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;"><strong><br>
		If at any time you would like to unsubscribe from receiving future emails, you can email us at</strong></div>
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">feedback@tipestry.com and we will promptly remove you from <strong>ALL</strong> correspondence.</div>
		<br>
		<span id='ourCon'></span><br>
		<div class='blueText'><strong>Contacting Us</strong></div>
		<br />
		<div class='innerText' style="padding-bottom: 10px;
    padding-top: 10px;
    border-bottom: 2px solid #ccc;">If there are any questions regarding this privacy policy, you may contact us using the information below.<br>
		<br>
		</div>
		<div class='innerText'>Tipestry Go</div>
		<div class='innerText'>940 Stewart Drive #203</div>
		Sunnyvale, CA 94085
		<div class='innerText'>US</div>
		<div class='innerText'>feedback@tipestry.com</div>
		<div class='innerText'> (650) 605-3434</div>
		<div class='innerText'><br>Last Edited on 2018-01-24</div>
		</div>
    </div>
@endsection
