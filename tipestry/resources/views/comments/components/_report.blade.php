@if (Auth::check() && Auth::user()->can('report-comment', $comment))
    <div class="save-post">
        <a style="cursor:pointer;"
           title="Report comment"
           alt="Report comment"
           onclick="event.preventDefault();
                   document.getElementById('report-comment-{{ $comment->id }}').submit();">
            <i class="fa fa-flag-o spam-flag" aria-hidden="true" aria-label="Report"></i>
        </a>

        <form id="report-comment-{{ $comment->id }}"
              action="{{ action('CommentsController@report', $comment) }}"
              method="POST"
              style="display: none;"
        >
            {{ csrf_field() }}
        </form>
    </div>
@endif