<span>
    <a role="button" data-toggle="collapse" href="#replyCommentT-{{ $comment->id }}" aria-expanded="false"
       aria-controls="collapseExample">Reply</a>
</span>





<div class="collapse" id="replyCommentT-{{ $comment->id }}">
    <form method="POST" action="{{ action("$controller@replyComment", [$site, $comment]) }}" onsubmit="chg_text_reply({{ $comment->id }})">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="comment">Your Comment</label>

            <textarea name="content" id="reply_msg_{{ $comment->id }}" class="form-control" rows="3"></textarea>
        </div>

        <button type="submit" class="btn btn-default">Send</button>
    </form>
</div>