<span>
    @if (Auth::check() && Auth::user()->can('vote-comment', $comment))
        <a href="#"
           onclick="event.preventDefault();
                   document.getElementById('vote-comment-{{ $comment->id }}').submit();"
        >
            Up Vote
        </a>

        <form id="vote-comment-{{ $comment->id }}"
              action="{{ action('VotesController@store', $comment) }}"
              method="POST"
              style="display: none;"
        >
            {{ csrf_field() }}
        </form>
    @endif
</span>