@if (Auth::check() && Auth::user()->can('delete-comment', $comment))
    <div class="save-post">
        <a href="#"
           title="Delete comment"
           alt="Delete comment"
           onclick="event.preventDefault();
                   document.getElementById('delete-comment-{{ $comment->id }}').submit();">
            <i class="fa fa-trash trash-flag" aria-hidden="true" aria-label="Delete"></i>
        </a>

        <form id="delete-comment-{{ $comment->id }}"
              action="{{ action("CommentsController@destroy", $comment) }}"
              method="POST"
              style="display: none;"
        >
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
        </form>
    </div>
@endif