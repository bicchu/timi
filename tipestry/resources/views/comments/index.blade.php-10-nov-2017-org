<div class="post-comments">
    <form method="POST" action="{{ action("$controller@addComment", $model) }}">
        {{ csrf_field() }}

        <div class="form-group">
            <label for="comment">Your Comment:</label>

            <textarea name="content" class="form-control" rows="3" {{ ! auth()->check() ? 'disabled' : '' }}></textarea>
        </div>

        @if (auth()->check())
            <button type="submit" class="btn btn-default">Send</button>
        @else
            <p class="text-warning">
                Please <a href="{{ url('login') }}">login</a> to leave a comment.
            </p>
        @endif
    </form>

    <div class="comments-nav">
        <ul class="nav nav-pills">
            <li role="presentation">
                <a href="#">
                    There are {{ count($model->comments) }} comments
                </a>
            </li>
        </ul>
    </div>

    <div class="row">

        @if (count($comments))
            @foreach ($comments as $comment)
                <div class="media">
                    <!-- first comment -->
                    <div class="media-heading">
                        <button class="btn btn-default btn-xs" type="button" data-toggle="collapse" data-target="#collapseOne"
                                aria-expanded="false" aria-controls="collapseExample"
                        >
                            <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                        </button>

                        <span class="label label-info">{{ $comment->votes_count }}</span> <b>{{ $comment->user->name }}</b> {{ $comment->published }}

                        @if (count($comment->gifts))
                            <span class="TrendingList__meta text-muted pull-right">
                                @if ($comment->hasGifts('btc'))
                                    {!! $comment->renderTotalGifts('btc') !!}
                                @endif

                                @if ($comment->hasGifts('doge'))
                                    {!! $comment->renderTotalGifts('doge') !!}
                                @endif
                            </span>
                        @endif
                    </div>

                    <div class="panel-collapse collapse in" id="collapseOne">

                        <div class="media-left">
                            <div class="vote-wrap">
                                {{-- Report a comment --}}
                                @include ('comments.components._report', ['comment' => $comment])

                                {{-- Delete a comment --}}
                                @include ('comments.components._delete', ['comment' => $comment])
                            </div>
                            <!-- vote-wrap -->
                        </div>
                        <!-- media-left -->

                        <div class="media-body">
                            <p>
                                {{ $comment->content }}
                            </p>

                            <div class="comment-meta">
                                {{-- Comment vote --}}
                                @include ('comments.components._vote', ['comment' => $comment])

                                {{-- Comment reply --}}
                                @include ('comments.components._reply', ['site' => $model, 'comment' => $comment])

                                @if (Auth::check() && Auth::user()->can('give-coins', $comment))
                                    <give-coins :user="{{ $comment->user }}" :comment="{{ $comment }}"></give-coins>
                                @endif
                            </div>
                            <!-- comment-meta -->

                            @if (count($comment->children))
                                @foreach ($comment->children as $commentChildren)
                                    <div class="media">
                                        <!-- answer to the first comment -->

                                        <div class="media-heading">
                                            <button class="btn btn-default btn-collapse btn-xs" type="button" data-toggle="collapse"
                                                    data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseExample">
                                                <span class="glyphicon glyphicon-minus" aria-hidden="true"></span></button>
                                            <span class="label label-info">{{ $commentChildren->votes_count }}</span> <b>{{ $commentChildren->user->name }}</b> {{ $commentChildren->published }}

                                            @if (count($commentChildren->gifts))
                                                <span class="TrendingList__meta text-muted pull-right">
                                                    @if ($commentChildren->hasGifts('btc'))
                                                        {!! $commentChildren->renderTotalGifts('btc') !!}
                                                    @endif

                                                    @if ($commentChildren->hasGifts('doge'))
                                                        {!! $commentChildren->renderTotalGifts('doge') !!}
                                                    @endif
                                                </span>
                                            @endif
                                        </div>

                                        <div class="panel-collapse collapse in" id="collapseTwo">

                                            <div class="media-left">
                                                <div class="vote-wrap">
                                                    {{-- Report a comment --}}
                                                    @include ('comments.components._report', ['comment' => $commentChildren])

                                                    {{-- Delete a comment --}}
                                                    @include ('comments.components._delete', ['comment' => $commentChildren])
                                                </div>
                                                <!-- vote-wrap -->
                                            </div>
                                            <!-- media-left -->


                                            <div class="media-body">
                                                <p>
                                                    {{ $commentChildren->content }}
                                                </p>
                                                <div class="comment-meta">
                                                    {{-- Comment vote --}}
                                                    @include ('comments.components._vote', ['comment' => $commentChildren])

                                                    {{-- Comment reply --}}
                                                    @include ('comments.components._reply', ['site' => $model, 'comment' => $commentChildren])

                                                    @if (Auth::check() && Auth::user()->can('give-coins', $commentChildren))
                                                        <give-coins :user="{{ $commentChildren->user }}" :comment="{{ $commentChildren }}"></give-coins>
                                                    @endif
                                                </div>
                                                <!-- comment-meta -->

                                                @if (count($commentChildren->children))
                                                    @foreach ($commentChildren->children as $commentSubChildren)
                                                        <div class="media">
                                                            <!-- answer to the first comment -->

                                                            <div class="media-heading">
                                                                <button class="btn btn-default btn-collapse btn-xs" type="button" data-toggle="collapse"
                                                                        data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseExample">
                                                                    <span class="glyphicon glyphicon-minus" aria-hidden="true"></span></button>
                                                                <span class="label label-info">{{ $commentSubChildren->votes_count }}</span> <b>{{ $commentSubChildren->user->name }}</b> {{ $commentSubChildren->published }}

                                                                @if (count($commentSubChildren->gifts))
                                                                    <span class="TrendingList__meta text-muted pull-right">
                                                                        @if ($commentSubChildren->hasGifts('btc'))
                                                                            {!! $commentSubChildren->renderTotalGifts('btc') !!}
                                                                        @endif

                                                                        @if ($commentSubChildren->hasGifts('doge'))
                                                                            {!! $commentSubChildren->renderTotalGifts('doge') !!}
                                                                        @endif
                                                                    </span>
                                                                @endif
                                                            </div>

                                                            <div class="panel-collapse collapse in" id="collapseTwo">

                                                                <div class="media-left">
                                                                    <div class="vote-wrap">
                                                                        {{-- Report a comment --}}
                                                                        @include ('comments.components._report', ['comment' => $commentSubChildren])

                                                                        {{-- Delete a comment --}}
                                                                        @include ('comments.components._delete', ['comment' => $commentSubChildren])
                                                                    </div>
                                                                    <!-- vote-wrap -->
                                                                </div>
                                                                <!-- media-left -->


                                                                <div class="media-body">
                                                                    <p>
                                                                        {{ $commentSubChildren->content }}
                                                                    </p>
                                                                    <div class="comment-meta">
                                                                        {{-- Comment vote --}}
                                                                        @include ('comments.components._vote', ['comment' => $commentSubChildren])

                                                                        @if (Auth::check() && Auth::user()->can('give-coins', $commentSubChildren))
                                                                            <give-coins :user="{{ $commentSubChildren->user }}" :comment="{{ $commentSubChildren }}"></give-coins>
                                                                        @endif
                                                                    </div>
                                                                    <!-- comment-meta -->

                                                                </div>
                                                            </div>
                                                            <!-- comments -->

                                                        </div>
                                                        <!-- answer to the first comment -->
                                                    @endforeach
                                                @endif

                                            </div>
                                        </div>
                                        <!-- comments -->

                                    </div>
                                    <!-- answer to the first comment -->
                                @endforeach
                            @endif

                        </div>
                    </div>
                    <!-- comments -->

                </div>
                <!-- first comment -->
            @endforeach
        @else
            <p class="text-center">
                Be the first to leave a comment
            </p>
        @endif
    </div>
</div>
<!-- post-comments -->