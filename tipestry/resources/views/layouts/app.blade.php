<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="icon" type="image/png" href="{{ asset('images/favicon.png') }}" />
	
	
	
	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <?php /*<title>{{ config('app.name', 'Laravel') }}</title>*/ ?>
	<title>Tipestry - comment and tip cryptocurrency on any website.</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="{{ asset('css/date-picker-style.css') }}">
	
<?php /*	
	<link href="https://rawgit.com/shaneapen/Image-Preview-for-Links/master/image_preview_for_links.css" rel="stylesheet"> 
	<!-- MiniPreview stuff here -->
	<link href="{{ asset('css/jquery.minipreview.css') }}" rel="stylesheet">
*/ ?>

    <!-- Styles -->
    <link href="{{ asset('css/noty.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">


	<meta name="_token" content="{!! csrf_token() !!}" />	
    <!-- Scripts -->
	<meta name="google-site-verification" content="yRmWS01bzJM9RkwR9DHamykXgCo7g4fGSrf08VXrz70" />	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111370737-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-111370737-1');
	</script>

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            'user' => auth()->user()
        ]) !!};
    </script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
    <div id="app">
         
		@include ('partials._header')
		
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @yield('content')

        <alert @if (session('alert')) flash="{{ session('alert') }}" @endif></alert>
    
		
	</div>

	<?php /*
    <!-- Scripts -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-103656985-1', 'auto');
        ga('send', 'pageview');
    </script>*/ ?>

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/functions.js') }}"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="{{ asset('js/ad-banner.js') }}"></script>
  
  <script>
  $( function() {
		var dateFormat = "dd-mm-yy",
		from = $( "#from" )
        .datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 3,
		  minDate: new Date(2018, 2-1, 16),
		  dateFormat: 'dd-mm-yy' 
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
		  $('select[id="to"]').removeAttr('disabled');
        }),
      to = $( "#to" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 3,
		  minDate: new Date(2018, 2-1, 17),
		  dateFormat: 'dd-mm-yy' 
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
  </script>
<?php /*
	<script src="{{ asset('js/jquery.minipreview.js') }}"></script>
	<script type="text/javascript">
		$(function() {
			$('#p2 a').miniPreview({ prefetch: 'none' });
			$('#p3 a').miniPreview({ prefetch: 'none' });
		});
	</script> 
	<script src="https://rawgit.com/shaneapen/Image-Preview-for-Links/master/image_preview_for_links.js"></script>
*/ ?>

<style>
.leave_comnt {
	/*background:#099;*/}
.trgt{
	
	font-weight: bold;
    font-size: 14px;
    margin-top: 35px; 
	margin-right: 3px;
	/*color:red;*/}
	.big_gif{
		display:none;}
		
		.banner_gf {display:none;}
		
		@media (min-width: 965px) {	
.trgt{
	font-size: 16px ;/*!important*/
     margin-right: 3px;
	  margin-top: -50px;
	/* color:green;*/}	


}
@media (min-width: 1001px) {	
.trgt{
	font-size: 16px ;/*!important*/
     margin-right: 3px;
	  margin-top: -60px;
	/* color:green;*/}	


}
@media (min-width: 1088px) {
.leave_comnt { width: 218px;}

.trgt{
	font-size: 14 ;/*!important*/
     margin-right: 3px;
	  margin-top: -55px;
	 /*color:green;*/}

}	

@media (min-width: 1100px) {	
.trgt{
	font-size: 16px ;/*!important*/
     margin-right: 3px;
	  margin-top: 3px;
	/* color:green;*/}	


}

@media (min-width: 1388px) {
.leave_comnt { width: 301px;}

.trgt{
	font-size: 16px ;/*!important*/
     margin-right: 3px;
	  margin-top: 30px;
	/* color:green;*/}

}

@media (min-width: 1588px) {
.leave_comnt { width: 501px;}

.trgt{
	font-size: 18px;
     margin-right: 3px;
	 /*color:blue;*/}

}

@media (min-width: 1920px) { 
.leave_comnt { width: auto;
float:left !important;
margin-left:100px;}

.trgt{
	
	font-weight: bold;
    font-size: 18px;
    margin-top: 35px; margin-right: 30px;
	/*color:yellow;*/}
	
	/*.big_gif{
		display:block;}
		
		.banner_gf{display:none;}*/

}


</style>	


<script>

document.getElementById('timer').innerHTML =  1 + ":" + 00;

function startTimer() 
{
	var presentTime = document.getElementById('timer').innerHTML;
	var timeArray = presentTime.split(/[:]+/);
	var m = timeArray[0];
	var s = checkSecond((timeArray[1] - 1));
	if(s==59){m=m-1}
	if(m<0){
		alert('Your session is expired'); 
		location.reload();
	}
	else{
		document.getElementById('timer').innerHTML = m + ":" + s;
		setTimeout(startTimer, 1000);		
	}


}


function checkSecond(sec) 
{
	  if (sec < 10 && sec >= 0) {sec = "0" + sec}; // add zero in front of numbers < 10
	  if (sec < 0) {sec = "59"};
	  return sec;
}

function frm_sbmt()
{
	var banner_content		= $('#banner_content').val();
	var from_dtd 			= $('#from').val();
	var to_dtd 				= $('#to').val();
	var destination_url		= $('#destination_url').val();
	var payment_hashcode	= $('#payment_hashcode').val();
	
	var img	= $('#banner_file').val();
	
	
	
	if(banner_content == ''){
		alert('Enter Banner Content');
		$('.back_btn').click();
		$('#banner_content').focus();
		return false;
	}
	if(from_dtd == ''){
		alert('Enter From Date');
		$('.back_btn').click();
		$('#from').focus();
		return false;
	}
	if(to_dtd == ''){
		alert('Enter To Date');
		$('.back_btn').click();                   
		$('#to').focus();
		return false;
	}
	if(destination_url == ''){
		alert('Enter Destination URL');
		$('.back_btn').click();
		$('#destination_url').focus();
		return false;
	}
	if(payment_hashcode == ''){
		alert('Enter Payment Hashcode');
		$('#payment_hashcode').focus();
		return false;
	}
	
	
	if(img == ''){
		alert('Select a picture');
		$('.back_btn').click();                   
		$('#banner_file').focus();
		return false;
	}
	
	$('#ad_banner_request_frm').submit();             
}

<?php
	if(!empty($adbanner_price)){
?>



function myFunction() 
{
	document.getElementById("text").innerHTML = '$1.00';
	document.getElementById("text2").innerHTML = '$0.00';
	document.getElementById("text3").innerHTML = '$0.00';	
	$('select[id="mySelect"]').attr('disabled','disabled');	
	$('select[id="mySelect_drop"]').attr('disabled','disabled');	
	$('select[id="mySelect_drop3"]').attr('disabled','disabled');	
	$('#selected_impression').val(1);
	var checkBox = document.getElementById("1");
    var text = document.getElementById("text");
    if (checkBox.checked == true){
        text.style.display = "block";
		$('select[id="mySelect"]').removeAttr('disabled');
   		 document.getElementById("total").innerHTML = '$1.00';
		 $("#paid_ammount").val(1);
		 $("#impression_no").val(1000);
	} else {
       text.style.display = "none";
    }
}


function myFunction_d1()
{
    var x = document.getElementById("mySelect").value;
	$('#selected_impression').val(x);
	@if($adbanner_price->count() > 0)
	@foreach($adbanner_price as $role)
	@if($role->location_name=="Top Location")
		if(x == {{ $role->impresion_num }}){         //if(y == {{$role->impresion_num}}){
		//alert(123456);
	    document.getElementById("text").innerHTML = '${{$role->price}}';
		document.getElementById("total").innerHTML = '${{$role->price}}';
		$("#paid_ammount").val({{$role->price}});
		$("#impression_no").val({{$role->impresion_num}});
	}	
	@endif
	@endForeach
	@endif 
}

function myFunction2()
{
	document.getElementById("text").innerHTML = '$0.00';
	document.getElementById("text2").innerHTML = '$1.00';
	document.getElementById("text3").innerHTML = '$0.00';	
	$('select[id="mySelect"]').attr('disabled','disabled');	
	$('select[id="mySelect_drop"]').attr('disabled','disabled');	
	$('select[id="mySelect_drop3"]').attr('disabled','disabled');	
	$('#selected_impression').val(1);
    var checkBox = document.getElementById("2");
    var text = document.getElementById("text2");
    if (checkBox.checked == true){
		$('select[id="mySelect_drop"]').removeAttr('disabled');
        text.style.display = "block";
		
		document.getElementById("total").innerHTML = '$1.00';
		$("#paid_ammount").val(1);
		$("#impression_no").val(1000);
		
    } else {
       text.style.display = "none";
    }
}

function myFunction_d2() 
{
    var y = document.getElementById("mySelect_drop").value;
	

	$('#selected_impression').val(y);
	@if($adbanner_price->count() > 0)
	@foreach($adbanner_price as $role)
	@if($role->location_name=="Middle Location")
		if(y == {{$role->impresion_num}})
		{		
			document.getElementById("text2").innerHTML = '${{$role->price}}';
			document.getElementById("total").innerHTML = '${{$role->price}}';
			$("#paid_ammount").val({{$role->price}});
			$("#impression_no").val({{$role->impresion_num}});
		}
	
	@endif
	@endForeach
	@endif 	
}

function myFunction3()
{
	document.getElementById("text").innerHTML = '$0.00';
	document.getElementById("text2").innerHTML = '$0.00';
	document.getElementById("text3").innerHTML = '$1.00';
	$('select[id="mySelect"]').attr('disabled','disabled');	
	$('select[id="mySelect_drop"]').attr('disabled','disabled');	
	$('select[id="mySelect_drop3"]').attr('disabled','disabled');	
    var checkBox = document.getElementById("3");
    var text = document.getElementById("text3");
	$('#selected_impression').val(1);
    if (checkBox.checked == true){
		$('select[id="mySelect_drop3"]').removeAttr('disabled');
        text.style.display = "block";
		
		document.getElementById("total").innerHTML = '$1.00';
		$("#paid_ammount").val(1);
		$("#impression_no").val(1000);		
    } else {
       text.style.display = "none";
    }
}

function myFunction_d3() 
{
	var y = $("#mySelect_drop3").val();	
	$('#selected_impression').val(y);
		/* hello*/
		@foreach($adbanner_price as $role)		
			@if($role->location_name=="Sidebar Location")		

				if(y == <?php echo $role->impresion_num; ?>)
				{			
					document.getElementById("text3").innerHTML = '${{$role->price}}';
					document.getElementById("total").innerHTML = '${{$role->price}}';
					$("#paid_ammount").val({{$role->price}});
					$("#impression_no").val({{$role->impresion_num}});
				}

			@endif
		@endForeach
}


<?php
	}
?>



function hide()
{
	$('#view_placements_position').html($('input[name=banner_position]:checked').val());
	$('#view_impression').html($('#impression_no').val());
	$('#view_start_date').html($('#from').val());
	$('#view_end_date').html($('#to').val());
	$('#view_destination_url').html($('#destination_url').val());
	$('#view_total_price').html($('#paid_ammount').val());
	$('#payby_bitcoin').click();
	
	startTimer();

    var x = document.getElementById("myDIV");
    if (x.style.display === "none") { 
		$('.payment_hashcode_div').hide();
		$('#myDIV1').hide();
		x.style.display = "block";
    } else {
        x.style.display = "none";
		$('.payment_hashcode_div').show();
		$('#myDIV1').show();
		
    }
}

$(document).ready(function(){
	$('.radio-inline').click();
    $("button").click(function(){
        $("payment_hashcode_div").toggle();
    });
});


selectedcoin = function()
{
	var cointype 		= arguments[0];
	var paid_ammount	= $('#paid_ammount').val();	
	var refurl 		= '';
	$('#amount_to_be_paid').html('');
	if(cointype == 'bitcoin'){
		$('#wallet_address_txt').html('Bit Coin Wallet Address : ');
		$('#wallet_address_details').html('1ABGGwFug5bPtPSDxDV7cHWqvjxEcqRCm4');
		$('#amount_to_be_paid_txt').html('Amount to be paid in bitcoin : ');
		refurl = '/coinvertprice/bitcoin/'+paid_ammount;
	}
	else if(cointype == 'litecoin'){
		$('#wallet_address_txt').html('Lite Coin Wallet Address : ');
		$('#wallet_address_details').html('LdyU3SqtG96DFzvqqBAFsCoAAkdniw6h2v');
		$('#amount_to_be_paid_txt').html('Amount to be paid in litecoin : ');
		refurl = '/coinvertprice/litecoin/'+paid_ammount;
	}
	else if(cointype == 'ethereumcoin'){
		$('#wallet_address_txt').html('Ethereum Coin Wallet Address : ');
		$('#wallet_address_details').html('0x0907e3ee3BFD4244f4402b7F5bBFbF3130368a21');
		$('#amount_to_be_paid_txt').html('Amount to be paid in ethereumcoin : ');
		refurl = '/coinvertprice/ethereum/'+paid_ammount;
	}
	
	
	else if(cointype == 'dogecoin'){
		$('#wallet_address_txt').html('Dogecoin Coin Wallet Address : ');
		$('#wallet_address_details').html('DUKyyeRZEhzcRFu5wzu7RxEpYdim82cyrC');
		$('#amount_to_be_paid_txt').html('Amount to be paid in dogecoin : ');
		refurl = '/coinvertprice/dogecoin/'+paid_ammount;
	}
	
	
	
	else if(cointype == 'tipcoin'){
		$('#wallet_address_txt').html('Tip Coin Wallet Address : ');
		$('#wallet_address_details').html('');
		$('#amount_to_be_paid_txt').html('Amount to be paid in tipcoin : ');
		refurl = '';
	}
	//alert(loading);
	if(loading == 1){
		return false;
	}
	loading = 1;
	
	if(refurl != '')
	{
		$.ajax({ 
			url: refurl,
			data: '',
			type: 'GET',
			success: function(response){
				loading = 0;
				$('#amount_to_be_paid').html(response);
			}
		});
	}
}




</script>


</body>
</html>
