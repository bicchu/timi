<!doctype html>
<html lang="sr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>{{ config('app.name') }} | {{ $pageTitle }}</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{  asset('bower_components/AdminLTE/bootstrap/css/bootstrap.min.css') }}" type="text/css">

    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css">

    <!-- Theme style -->
    <link href="{{ asset("/bower_components/AdminLTE/dist/css/AdminLTE.min.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("/bower_components/AdminLTE/dist/css/skins/skin-blue.min.css") }}" rel="stylesheet" type="text/css">

    <!-- Page based styles -->
    @stack ('styles')
</head>
<body class="skin-blue">

<div class="wrapper">

    <!-- Header -->
@include ('admin.partials._header')

<!-- Siderbar -->
@include ('admin.partials._sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $pageTitle }}
            </h1>

            <!-- You can dynamically generate breadcrumbs here -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Footer -->
    @include('admin.partials._footer')
</div>

<!-- jQuery-->
<script src="{{ asset ("/bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>

<!-- Bootstrap -->
<script src="{{ asset ("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>

<!-- Page based scripts -->
@stack ('scripts')

<!-- AdminLTE App -->
<script src="{{ asset ("/bower_components/AdminLTE/dist/js/app.min.js") }}" type="text/javascript"></script>
<script>
bannerdelete = function(){
	var id = arguments[0];
	
	if(confirm('Are you sure?')){
		$('#bannerid').val(id);
		$('#deletebannerform').submit();
	}	
}


/*function edit(id)
	{
		//alert('hello');
		$(document).ready(function()
		{
			//alert('hello');
    		$("#price_hide").click(function()
			{
        		$("#price").hide();
				$("#inpu_txt-"+id).show();
    		});
		});
	}*/
	

function sbmt_frm(id)
{
	$('#id_new').val(id);
	var x = $('#input_price_'+id).val();
	$('#new_price').val(x);
	$('#banner_price_update').submit();
}

function edit(id)
{
	$('#input_price_'+id).show();
	$('#txt-'+id).hide();
	$('#sbmt_btn_'+id).show();
	$('#bouuton_'+id).hide();
}

</script>
</body>
</html>