@extends ('layouts.app')
@section ('content')
<div class="container">
    <h1>Ad your banner in Tipestry.</h1>
	<?php
	//echo '<pre>';print_r($adbanner_price);exit;
		/*if($withdraw_msg != ''){
			if($withdraw_msg == 'success'){
				echo '<p style="color:#0F0;">Successfully you transfer fund</p>';
			}
			else{
				echo '<p style="color:#F00;">Error in fund withdraw</p>';
			}
		}*/
	?>
    <hr>
    
    <div class="row">
		<h4 style="color:#3c8dbc;">{!! Session::has('msg') ? Session::get("msg") : '' !!}</h4>
        <div class="col-md-12">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#deposit" aria-controls="deposit" role="tab" data-toggle="tab">Ad Banner</a></li>
                <li role="presentation"><a href="#btc-list" aria-controls="btc-list" role="tab" data-toggle="tab">Banner History</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="deposit">
                    <div class="well">
                        <form action="{{ route('adbanner.add') }}" method="POST"  id="ad_banner_request_frm" enctype="multipart/form-data">
                            {{ csrf_field() }}					



						<div id="myDIV">
                            <div class="form-group">
								<label>Select one or more placements, choose the number of impressions, and then click "Ad Banner Request" below</label>
								<table width="100%">
									<tr class="spaceUnder">
										<td>
											<label class="lb_fr">
												<input type="radio" name="banner_position" value="header" id="1"  onchange="myFunction()"  class="radio-inline r_inp" />
												<span class="outside"><span class="inside"></span></span>
											</label>
										</td>
										<td>
											<label>Top Location</label><br>700x300
										</td>
										<td>
											
										</td>
										<td>
                                        
                                        

                                        
                                        

<select disabled  id="mySelect" onclick="myFunction_d1()">
@if($adbanner_price->count() > 0)
@foreach($adbanner_price as $role)
@if($role->location_name=="Top Location")
<option value="{{$role->impresion_num}}">{{$role->impresion_num}}</option>
@endif
@endForeach
@endif 
</select> &nbsp;
impression        
                                            
                                            
                                            
                                            
										</td>
										<td id="text" style="display:none;  margin-top: 30px;"> $500.00</td>
									</tr>
									<tr class="spaceUnder">
										<td>
											<label class="lb_fr">
												<input type="radio" name="banner_position" value="header" id="2"  onchange="myFunction2()"  class="radio-inline r_inp"  />
												<span class="outside"><span class="inside"></span></span>
											</label>
										</td>
										<td>
											<label>Middle Location</label><br>700x300
										</td>
										<td>
											
										</td>
										<td>
									
										
        <select disabled  id="mySelect_drop" onclick="myFunction_d2()">
                @if($adbanner_price->count() > 0)
                    @foreach($adbanner_price as $role)
                        @if($role->location_name=="Middle Location")
                        	<option value="{{$role->impresion_num}}">{{$role->impresion_num}}</option>
                        @endif
                    @endForeach
                @endif 
        </select> &nbsp;
											impression
										</td>
										<td id="text2" style="display:none;  margin-top: 30px;"> $500.00</td>
									</tr>
									<tr>
										<td>
											<label class="lb_fr">
												<input type="radio" name="banner_position" value="sidebar" id="3"  onchange="myFunction3()"  class="radio-inline r_inp" />
												<span class="outside"><span class="inside"></span></span>
											</label>
										</td>
										<td>
											<label>Sidebar Location</label><br>200x400
										</td>
										<td>
											
										</td>
										<td>
<select disabled  id="mySelect_drop3" onclick="myFunction_d3()">
@if($adbanner_price->count() > 0)
@foreach($adbanner_price as $role)
@if($role->location_name=="Sidebar Location")
<option value="{{$role->impresion_num}}">{{$role->impresion_num}}</option>
@endif
@endForeach
@endif 
</select> &nbsp;

							
	

											impression
										</td>
										<td id="text3" style="display:none;  margin-top: 30px;"> $500.00</td>
									</tr>
									<tr>
										<td colspan="4" align="right">
											<label>Total :</label> 
											<input type="text"
												   id="paid_ammount"
												   name="paid_ammount"
												   class="form-control"
												   value="0"
												   style="display:none;"
											>
											<input type="text"
												   id="impression_no"
												   name="impression_no"
												   class="form-control"
												   value="7500"
												   style="display:none;"
											>
										</td>
										<td id= "total">$0.00</td>
									</tr>
								</table>
							</div>
                            
                            
							<div class="form-group" style="display:none1;">
								<label for="wallet_address">Ad Content:</label>
								<input type="text"
									   id="banner_content"
									   name="banner_content"
									   class="form-control"
									   value=""
									   placeholder="AD content"
								>
							</div>

							
							<div class="form-group">
								<label for="wallet_address">Select Timeframe:</label><br>
								<input type="text"
									   id="from"
									   name="banner_from_date"
									   class="form-control"
									   value=""
                                       
									   style="width:20%; float:left; margin-right:15px;"
									   placeholder="Start Date"
								>
								&nbsp; &nbsp; 
								<input type="text"
									   id="to"
									   name="banner_to_date"
                                       
									   class="form-control col-md-3"
									   value=""
                                       
									   style="width:20%; float:left;"
									   placeholder="End Date"
								>
							</div>
							<div class="form-group">
								<label for="wallet_address">Upload Banner Image:</label>
								<input style="background: #00A2E8;
    color: #fff;
    padding: 10px; margin:15px 0px; width: 50%; max-width:454px; height:auto; border-radius:0px;" type='file' onchange="readURL(this);" /
									   id="banner_file"
									   name="banner_file"
									   class="form-control"
									   value=""
                                       
								>                                                                                                                                                                 
							</div>
                            <div class="form-group">

								<label for="wallet_address">Destination URL:</label><br>
								<input type="url"
									   id="destination_url"
									   name="destination_url"
									   class="form-control"
									   value=""
                                       
									   style=""
									   placeholder="eg. http://example.com"
								>
							</div>
                            <div class="form-group">
                                <input type="button" value = "Ad Banner Request" onclick="hide()" class="btn btn-action" style="margin-top:15px;">
                            </div>
                            
          </div>          
               <div id="myDIV1" style="display:none;">             
                            <div class="form-group">
								<label for="wallet_address" style="color:#00A2E8; font-size:20px;">Fee Summary</label>
							</div>
							<div class="row paymentrow">
								<label class="col-md-3">Placements : </label>
								<span id="view_placements_position" class="col-md-9"></span>
							</div>
							<div class="row paymentrow">
								<label class="col-md-3">Impression : </label>
								<span id="view_impression" class="col-md-9"></span>
							</div>
							<div class="row paymentrow">
								<label class="col-md-3">Total fee payable in USD : </label>
								<span id="view_total_price" class="col-md-9"></span>
							</div>
							<div class="row paymentrow">
								<label class="col-md-3">Start Date : </label>
								<span id="view_start_date" class="col-md-9"></span>
							</div>
							<div class="row paymentrow">
								<label class="col-md-3">End Date : </label>
								<span id="view_end_date" class="col-md-9"></span>
							</div>
							<div class="row paymentrow">
								<label class="col-md-3">Destination URL : </label>
								<span id="view_destination_url" class="col-md-9"></span>
							</div>
							<div class="row paymentrow">
								<label class="col-md-3">How do you want to pay? </label>
								<input type="radio" onclick="selectedcoin('bitcoin')" 		id="payby_bitcoin" 		name="currency_type" value="bitcoin" 		/> &nbsp; Bitcoin || 
								<input type="radio" onclick="selectedcoin('litecoin')"  	id="payby_litecoin" 		name="currency_type" value="litecoin" 		/> &nbsp; Litecoin || 
								<input type="radio" onclick="selectedcoin('ethereumcoin')"  id="payby_ethereumcoin" 	name="currency_type" value="ethereumcoin" 	/> &nbsp; Ethereum || 
                                
                                <input type="radio" onclick="selectedcoin('dogecoin')"  id="payby_dogecoin" 	name="currency_type" value="dogecoin" 	/> &nbsp; Dogecoin ||
                                
								<input type="radio" onclick="selectedcoin('tipcoin')"  		id="payby_tipcoin" 		name="currency_type" value="tipcoin" 		/> &nbsp; TIP		
							</div>
							<div class="row paymentrow">
								<label class="col-md-3" id="wallet_address_txt">Wallet Address : </label>
								<span class="col-md-9" id="wallet_address_details"></span>
							</div>	
							<div class="row paymentrow">
								<label class="col-md-3" id="amount_to_be_paid_txt"> &nbsp; </label>
								<span class="col-md-9" id="amount_to_be_paid"> &nbsp; </span>
							</div>	
							<div class="row paymentrow">
								<label class="col-md-3" id="amount_to_be_paid_txt">Awaiting Payment : </label>
								<span class="col-md-9" id="timer"></span>
							</div>
							<div class="form-group payment_hashcode_div" style="display:none;">
								<label for="wallet_address">Enter Payment Transaction Hash :</label>
								<input type="text"
									   id="payment_hashcode"
									   name="payment_hashcode"
									   class="form-control"
									   value=""
								>
							</div>																												
                            <div class="form-group" style="display:none;">
								<label for="wallet_address">Comment:</label>
								<input type="text"
									   id="user_comment"
									   name="user_comment"
									   class="form-control"
									   value="xxx"
								>								
							</div>																					
                            <div class="form-group">
								<input type="button" onclick="frm_sbmt()" class="btn btn-action" style="margin-top:15px;" value="Submit" />
								
								<?php /*
                                <button type="submit" class="btn btn-action" style="margin-top:15px;">
                                    Submit Request
                                </button>*/ ?> &nbsp; 
                                <button type="button" class="btn btn-action back_btn" style="margin-top:15px;"  onclick="hide()" >
                                    Back
                                </button>
                            </div>
							</div>
							<input type="hidden" name="selected_impression" id="selected_impression" value="0" />
                        </form>
                        
                        
                        
                        <img id="blah" class="preview_up_ld" style=" width:50%; max-width:500px;" src="/images/Up_bg-1.png" alt="your image" />
  <script>
function readURL(input) { if (input.files && input.files[0]) { var reader = new FileReader(); reader.onload = function (e) { $('#blah') .attr('src', e.target.result); }; reader.readAsDataURL(input.files[0]); } }
  </script>  
                    </div>
                </div><!-- ./Deposit -->

                <div role="tabpanel" class="tab-pane" id="btc-list">
                    <div class="well">
                    
                    
                     

                    
						<table class="table table-bordered" >
							<tr>
							<?php /*	<th>Banner Content</th>  */?>
								<th>Banner Location</th>
								<th>Banner Image</th>
								<th>Banner From Date</th>
								<th>Banner To Date</th>
                                <th>Status</th>
                                <th>Banner Position</th>
                                <th>Payment Hashcode</th>
                                <th>Selected Impression</th>
                                <th>Used Impression</th>
							</tr>
                        <?php
							foreach($adbanners as $val)
							{
								if($val->status == 99){continue;}
								echo '<tr>';
							//	echo '<td>'.$val->banner_content.'</td>';
								echo '<td>'.ucfirst($val->banner_position).'</td>';
								echo '<td><img id="myImg" src="/upload_images/'.$val->banner_file.'" width="70" /></td>';
								echo '<td>'.$val->banner_from_date.'</td>';
								echo '<td>'.$val->banner_to_date.'</td>';
								echo '<td>'.$val->status.'</td>';
								echo '<td>'.$val->banner_position.'</td>';
								echo '<td>'.$val->payment_hashcode.'</td>';
								echo '<td>'.$val->selected_impression.'</td>';
								echo '<td>'.$val->used_impression.'</td>';
								echo '</tr>';
							}
						?>
						</table>
                    </div>
                </div><!-- ./List -->
            </div>
        </div>
    </div>
</div>
@endsection