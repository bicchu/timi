<style>
.navbar-inverse .navbar-nav > li > a:hover{
	color:#000 !important;
}

</style>


<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
     <div class="col-md-2">
        <div class="navbar-header">        
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
			<?php /*<a class="navbar-brand" href="{{ url('/') }}">*/ ?>
           
			<a class="navbar-brand" href="http://tipestry.com/">
				<img class="logo" src="{{ asset('images/newlogo.png') }}" alt="{{ config('app.name') }}">
			</a>
          
			<?php /* 
            <a class="navbar-brand" href="{{ url('/') }}">
                <img class="logo" src="{{ asset('images/logo.png') }}" alt="{{ config('app.name') }}">
            </a>
            <a href="{{ url('/') }}" class="navbar-brand">
				{{ config('app.name') }}
                <sup class="sup-beta">BETA</sup>
            </a>
			*/ ?>
        </div>
        </div>
         <div class="col-md-10">
        <div id="navbar" class="navbar-collapse collapse">
            <?php /*<ul class="nav navbar-nav">
                <li><a href="{{ route('pages.using') }}">FAQ</a></li>
                <li><a href="{{ route('pages.about') }}">About</a></li>
            </ul>*/ ?>
            <form method="POST"
                  action="{{ action('SitesController@process') }}"
                  id="search-form"
                  class="navbar-form navbar-right"
                  autocomplete="off"
            >
                {{ csrf_field() }}

                <div class="form-group">
                    <label class="sr-only" for="domain">Site (example.com)</label>

                    <div class="input-group search-form-input">
                        <div class="input-group-addon hidden-xs">URL</div>
                        <input type="text" placeholder="Start or join a conversation on any website." id="domain" name="url" class="form-control">
                    </div>
                </div>
                <button type="button" class="btn btn-action load_domain">Load comments</button>
            </form>
			
            <form method="POST"  action=""  id="http-search-form">
                {{ csrf_field() }}
				<input type="hidden" placeholder="Start or join a conversation on any website." id="http-domain" name="url" class="form-control">
            </form>
			
            <form method="POST"  action="/search"  id="search_keyword_frm">
                {{ csrf_field() }}
				<input id="search_keyword" name="search_keyword" value="" type="hidden" placeholder="Start or join a conversation on any website." id="http-domain" name="url" class="form-control">
            </form>			
			
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
				<?php /*<li class="small_gif1">
<a href="#"  class="banner_gf" style="background: url(https://thumbs.gfycat.com/SlowJealousFox-size_restricted.gif) center center no-repeat;
    width: 140px;
    height: 152px;
    background-size: 467%;">&nbsp; </a>
					<img src="https://thumbs.gfycat.com/SlowJealousFox-size_restricted.gif" style="width:200" > 
				</li>   */?>         
				<li>
					<a href="http://tipestry.com/faq">
						FAQ 
<?php
	$useragent = $_SERVER['HTTP_USER_AGENT'];
	if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
	}else{	
?>						
						<span style="width:32px; vertical-align:middle; display: inline-block; margin-left:2px;">
							<img id="zspinner" class="img-responsive" src="/images/bit.gif">
						</span>
<?php
	}
?>
					</a>
				</li>
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li>
                        <?php 
							/*<a href="{{ url('login') }}">Login</a>*/ 
							$url = url('login');
							$url = str_replace('http:','https:',$url);
						?>
						<a href="http://tipestry.com/login">Login 
<?php
	$useragent = $_SERVER['HTTP_USER_AGENT'];
	if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
	}else{
?>						
						<span style="width:32px; vertical-align:middle; display: inline-block;  margin-left:2px;"><img id="zspinner" class="img-responsive" src="/images/bit.gif">
						</span>
<?php } ?>
						</a>
                    </li>
                    <li>
						<?php
							/*<a href="{{ route('register') }}">Register</a>*/ 
							$url = url('register');
							$url = str_replace('http:','https:',$url);
						?>
						<a href="http://tipestry.com/register">Register</a>
					</li>
                @else
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="cursor:pointer;">
                            Currencies<span class="caret"></span>
<?php
	$useragent = $_SERVER['HTTP_USER_AGENT'];
	if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
	}else{
?>
		<span style="width:32px; vertical-align:middle; display: inline-block;  margin-left:2px;">
		<img id="zspinner" class="img-responsive" src="/images/bit.gif">
		</span>
<?php
	}
?>	
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="http://tipestry.com/currencies/btc">
                                    <img src="http://tipestry.com/images/currencies/BB-2sec.gif" width="30" /> &nbsp; BTC (Bitcoin)
                                </a>
                            </li>
                            <?php /*<li>
                                <a href="{{ route('currencies.btccash') }}">
                                    <img src="http://tipestry.com/images/currencies/bch.gif" width="30" /> &nbsp; Bitcoin CASH (BCH)
                                </a>
                            </li>*/ ?>
                            <li>
                                <a href="{{ route('currencies.eth') }}">
                                    <img src="http://tipestry.com/images/currencies/eth.gif" width="30" /> &nbsp; Ethereum (ETH)
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('currencies.tip') }}">
									<img src="http://tipestry.com/images/currencies/ethtip.gif" width="30" /> &nbsp; Tipestry (TIP)
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('currencies.xrt') }}">
									<img src="http://tipestry.com/images/currencies/xrt.gif" width="30" /> Silk Route Coin (XRT)
                                </a>
                            </li><li>
                                <a href="{{ route('currencies.doge') }}">
                                    <img src="http://tipestry.com/images/currencies/doge.png" width="30" /> &nbsp; DOGE (Dogecoin)
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} 
							<span class="caret"></span>
							<?php if(notificationcnt()>0){echo '<span class="fa fa-envelope" style="color:#00A2E8;"></span> &nbsp;'.notificationcnt();}?>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ url('notification') }}">
                                    Notification
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('history') }}">
                                    History
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('logs') }}">
                                    Activity log
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
			<?php /*
			<ul class="nav navbar-nav navbar-right leave_comnt" >
				<!--style="font-weight: bold; font-size: 16px; margin-top: 35px; margin-right: 30px;"-->
            	<li  class="trgt">Leave comments and tip cryptocurrency across the web</li>
               <!-- <style>
.big_gif .banner_gf {
/* background-color:#063;
 background:url('https://thumbs.gfycat.com/SlowJealousFox-size_restricted.gif');
 width:10%;
 hight:50px;
 background-position: center;*//*
 margin-top:-25px;
}
</style>
                
                <li class="big_gif">
<a href="#"  class="banner_gf" style=" margin-top:-25px;background: url(https://thumbs.gfycat.com/SlowJealousFox-size_restricted.gif) center center no-repeat;

    width: 140px;
    height: 152px;
    background-size: 467%;">&nbsp; </a>
                </li> -->
                
            </ul>*/ ?>
        </div><!--/.navbar-collapse -->
        </div>
    </div>
</nav>

