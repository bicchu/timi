@extends ('layouts.app')

@section ('content')
<div class="container">
    <h1>BCH Currency</h1>
	<h4><b>Balance</b>:
		<?php 
			//echo 7777;exit;
			//echo '0.00';
			
			//$total_balance = (new App\Currencies\BtcCurrency)->getBalance(auth()->user()->btcAddressesString());
			//$total_balance += $btcBalance;
			if($btcBalance>0){
				echo $btcBalance.' BCH';
			}else{
				echo '0.00 BCH';
			}
			
		?>
	</h4>
	<h4><b>Wallet Address</b>: <?php echo $walletid;?></h4>
	<?php
		if(!empty($withdraw_msg) && count($withdraw_msg)>0){
			if($withdraw_msg['status'] == 1){
				echo '<p style="color:#00A2E8; font-weight:bold; ">'.$withdraw_msg['message'].'</p>';
			}
			else{
				echo '<p style="color:#F00;">'.$withdraw_msg['message'].'</p>';
			}
		}
	?>
    <hr>    
    
	<div class="row">
        <div class="col-md-8">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#deposit" aria-controls="deposit" role="tab" data-toggle="tab">Deposit coins coins</a></li>
                <li role="presentation"><a href="#withdraw" aria-controls="withdraw" role="tab" data-toggle="tab">Withdraw/Send coins</a></li>
            
			</ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="deposit">
                    <div class="well">
                        <div class="form-group">
                            <label for="wallet_address">Your deposit Address:</label>
                            <input type="text"
                                   id="wallet_address"
                                   class="form-control"
                                   value="<?php echo $walletid;?>"
                                   readonly
                            >
                        </div>

                        <form action="{{ route('currencies.btccash.regenerate') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group" style="display:none;">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;

                                    Get a new wallet address
                                </button>
                            </div>
                        </form>
                    </div>
                </div><!-- ./Deposit -->

                <div role="tabpanel" class="tab-pane" id="withdraw">
                    <div class="well">
                        <form action="{{ route('currencies.btccash.withdraw') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="wallet_address">Recipient Wallet Address:</label>
                                <input type="text"
                                       id="wallet_address"
                                       name="recipient"
                                       class="form-control"
                                       placeholder="wallet address"
									   onkeyup = "btcwithdrawbtn()"
                                >
                            </div>
                            <div class="form-group">
                                <label for="amount">Amount:</label>
                                <input type="text"
                                       name="amount"
                                       id="amount"
                                       class="form-control withdraw_send_btccoins"
                                       placeholder="Amount"
									   onkeyup = "btcwithdrawbtn()"
                                >
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success withdraw_send_coins_sbmt" disabled>
                                    <i class="fa fa-exchange" aria-hidden="true"></i>&nbsp;

                                    Withdraw
                                </button>
                            </div>
                        </form>
                    </div>
                </div><!-- ./Withdraw -->

           
            </div>

        </div>
    </div>
</div>
@endsection