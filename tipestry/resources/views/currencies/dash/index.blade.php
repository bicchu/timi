@extends ('layouts.app')

@section ('content')
    <div class="container">
        <h1>Dash Currency</h1>
        <h4><b>Wallet Address</b>: {{ Auth::user()->dashAddressDefault() }}</h4>
        <h4><b>Balance</b>: {{ (new App\Currencies\DashCurrency)->getBalance(Auth::user()->dashAddressesString()) }}</h4>

        <hr>

        <div class="row">
            <div class="col-md-8">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#deposit" aria-controls="deposit" role="tab" data-toggle="tab">Deposit coins coins</a></li>
                    <li role="presentation"><a href="#withdraw" aria-controls="withdraw" role="tab" data-toggle="tab">Withdraw/Send coins</a></li>
                    <li role="presentation"><a href="#btc-list" aria-controls="btc-list" role="tab" data-toggle="tab">Wallet Address List and Balance</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="deposit">
                        <div class="well">
                            <div class="form-group">
                                <label for="wallet_address">Your deposit Address:</label>
                                <input type="text"
                                       id="wallet_address"
                                       class="form-control"
                                       value="{{ Auth::user()->dashAddressDefault() ?? 'No address available' }}"
                                       readonly
                                >
                            </div>

                            <form action="{{ route('currencies.dash.regenerate') }}" method="POST">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;

                                        Get a new wallet address
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div><!-- ./Deposit -->

                    <div role="tabpanel" class="tab-pane" id="withdraw">
                        <div class="well">
                            <form action="{{ route('currencies.dash.withdraw') }}" method="POST">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="wallet_address">Recipient Wallet Address:</label>
                                    <input type="text"
                                           id="wallet_address"
                                           name="recipient"
                                           class="form-control"
                                           placeholder="wallet address"
                                    >
                                </div>

                                <div class="form-group">
                                    <label for="amount">Amount:</label>
                                    <input type="text"
                                           name="amount"
                                           id="amount"
                                           class="form-control"
                                           placeholder="Amount"
                                    >
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-exchange" aria-hidden="true"></i>&nbsp;

                                        Withdraw
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div><!-- ./Withdraw -->

                    <div role="tabpanel" class="tab-pane" id="btc-list">
                        <div class="well">
                            <ul class="list-group">
                                @foreach (auth()->user()->dashAddresses as $wallet)
                                    <li class="list-group-item{{ $loop->last ? ' active' : '' }}">
                                        @if ($loop->last)
                                            <span class="badge">
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </span>
                                        @endif
                                        {{ $wallet->address }}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div><!-- ./List -->
                </div>

            </div>
        </div>
    </div>
@endsection