@extends ('layouts.app')

@section ('content')
<div class="container">
    <h1>History</h1>

    <a href="?days=1" class="btn btn-primary{{ request()->input('days', 1) == '1' ? ' active' : '' }}">Last 24 hours</a>
    <a href="?days=3" class="btn btn-primary{{ request()->input('days', 1) == '3' ? ' active' : '' }}">Last 3 days</a>
    <a href="?days=7" class="btn btn-primary{{ request()->input('days', 1) == '7' ? ' active' : '' }}">Last week</a>

    <br>
    <hr>
    <br>

    <ul class="list-group">
        @foreach ($history as $item)
            <li class="list-group-item">
                {{ $item->created_at->diffForHumans() }}&nbsp;

                <a href="{{ action('SitesController@index', $item->site->token) }}">
                    {{ $item->site->url }}
                </a>
            </li>
        @endforeach
    </ul>
</div>
@endsection