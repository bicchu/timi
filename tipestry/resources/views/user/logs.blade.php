@extends ('layouts.app')

@section ('content')
    <div class="container">
        <h1>Logs</h1>

        <br><br>


        @if (count($logs))
            <ul class="list-group">
                @foreach ($logs as $log)
                    <li class="list-group-item">
                        {{ $log->created_at->diffForHumans() }}&nbsp;

                        <a href="{{ action('SitesController@index', $log->site->url) }}">
                            {{ $log->site->url }}
                        </a>

                        {{ $log->message }}
                    </li>
                @endforeach
            </ul>
        @else
            <div class="well">
                <p class="text-info">
                    There is no any logged message.
                </p>
            </div>
        @endif
    </div>
@endsection