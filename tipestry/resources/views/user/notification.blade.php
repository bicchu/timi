@extends ('layouts.app')

@section ('content')
<div class="container">
    <h1>Notification</h1>
    <a href="?type=unread" class="btn btn-primary{{ request()->input('type', 'unread') == 'unread' ? ' active' : '' }}">Unread</a>
    <a href="?type=read" class="btn btn-primary{{ request()->input('type', 'read') == 'read' ? ' active' : '' }}">Read</a>
    <br>
    <hr>
    <br>

	<table width="100%" border="1">
	<tr><td>Message List</td><td>Message Details</td></tr>
	<tr><td width="30%">
    <ul class="list-group">
		<?php $i = 0;?>
        @foreach ($notification as $item)
			<li class="list-group-item">			
                <?php
					$i++;
					echo '<a href="#" onclick="readmsg('.$item->id.',\''.$item->message.'\')">Notification : '.$i.'</a>';
				?>
            </li>
        @endforeach
    </ul>
	</td><td id="msgdetails" valign="top"></td></tr></table>
</div>
<script>
	readmsg = function(){
		var id		= arguments[0];
		var msg		= arguments[1];
		var url 	= "/notificationread/"+id; 
		$('#msgdetails').html(msg);
		
		$.ajax({ 
			url: url,
			data: '',
			type: 'GET',
			success: function(response){

			}
		});
	}
</script>
@endsection