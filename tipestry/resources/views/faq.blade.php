@extends ('layouts.app')

@section ('content')
    <div class="container">
        <br>

        @if (session()->has('message'))
            <div class="alert alert-info" role="alert">
                {{ session('message') }}
            </div>
        @endif

        <h1 class="trending-topics-heading">FAQ</h1>

        <div class="row">
            <div class="col-md-12">
                <ul class="TrendingList">
<li>
<a class="TrendingList__item">
<span class="TrendingList__number">1.</span> What is Tipestry.com?





</a>
<div class="TrendingList__meta-block">


<span class="TrendingList__meta text-muted">
<p>Tipestry.com is a platform where people can post comments about other websites and tip comments they like with cryptocurrency.</p>
</span>
</div>
</li>
<li>
<a class="TrendingList__item">
<span class="TrendingList__number">2.</span> What is Tipestry Go?
</a>
<div class="TrendingList__meta-block">
<span class="TrendingList__meta text-muted">
<p>Tipestry Go is an app for iPhone and iPad that lets users leave a comment at any physical location. Comments can be placed either at the user's current physical location or by selecting a spot on the map. Comments can be viewed on the map view, through a list of nearby posts within a selected range, or in a section showing popular posts worldwide.</p>
</span>
</div>
</li>
<li>
<a class="TrendingList__item">
<span class="TrendingList__number">3.</span> How is Tipestry.com different than other sites with comment sections?
</a>
<div class="TrendingList__meta-block">
<span class="TrendingList__meta text-muted">
<p>Although some sites feature comment sections or message boards, most do not. One of the goals of this project is to provide more places for people to interact and give feedback online, including the more obscure (or censored) corners of the web. </p>
</span>
</div>
</li>
<li>
<a class="TrendingList__item">
<span class="TrendingList__number">4.</span> How does Tipestry.com work?
</a>
<div class="TrendingList__meta-block">
<span class="TrendingList__meta text-muted">
<p>In the <strong>URL</strong> field on the top right of the screen, enter the address of the website you want to discuss and a comment section will load for that page.<br />
You can also check out what people around the web are currently talking about on the <strong>Trending</strong> section of the Tipestry homepage. </p>
</span>
</div>
</li>
<li>
<a class="TrendingList__item">
<span class="TrendingList__number">
5.
</span> 
Hey! What if I don't want people commenting on my site?
</a>
<div class="TrendingList__meta-block">
<span class="TrendingList__meta text-muted">
<p>To prevent abuse, in the future we will add an automatic way for most webmasters to opt out and prevent people from commenting on their site. For now, please email us at feedback@tipestry.com if you have an issue.<br />
However, we also value freedom of speech and part of the purpose of this platform is to provide a way for normal people to speak up when organizations are misbehaving. To that end, we will adopt a set of rules determining which sites can opt out and which can't. In general, private individuals and smaller organizations can opt out by default, but governments, mass media organizations, and publicly traded companies cannot. We plan to use a voting system to get input from the community on what the exact rules are and how they will change over time. </p>
</span>
</div>
</li>

<li>
<a class="TrendingList__item">
<span class="TrendingList__number">
6.
</span> 
What is tipping?
</a>
<div class="TrendingList__meta-block">
<span class="TrendingList__meta text-muted">
<p>Tipping is a way to show other users you appreciate their comment. It’s a more powerful version of a like or an upvote, as it involves automatically sending cryptocurrency to the user you’re tipping.</p>
<p>To tip someone, click on the <b>Give Coins</b> button next to their post and choose the type of coin and the amount you want to tip. An icon showing the tip will then show up next to their post and they will receive the coins in their account.</p>
</span>
</div>
</li>
<li>
<a class="TrendingList__item">
<span class="TrendingList__number">
7.
</span> 
What is cryptocurrency?
</a>
<div class="TrendingList__meta-block">
<span class="TrendingList__meta text-muted">
<p>Cryptocurrency is peer-to-peer digital money. It has several advantages over tradition currency, one of which is that it is much easier to send and receive online and across borders.</p>
<p>Bitcoin, the first cryptocurrency, was created by Satoshi Nakamoto in 2009. Since then, thousands of other coins and tokens with different features have been created. Right now Tipestry supports Bitcoin and Dogecoin, and we're currently working on integrating Ethereum, Bitcoin Cash,Litecoin, and our own TIP token.</p>
</span>
</div>
</li>
<li>
<a class="TrendingList__item">
<span class="TrendingList__number">
8.
</span> 
How can I add coins to my Tipestry account?
</a>
<div class="TrendingList__meta-block">
<span class="TrendingList__meta text-muted">
<p>You can send Bitcoins, Dogecoins, etc. from an online exchange or wallet to your Tipestry addresses, which can be found in the <b>Currency</b> link on the homepage. However, if you are new to cryptocurrencies, we recommend starting off by simply posting content to Tipestry and earning coins through tips.</p>
</span>
</div>
</li>
<li>
<a class="TrendingList__item">
<span class="TrendingList__number">
9.
</span> 
How can I keep my coins safe?
</a>
<div class="TrendingList__meta-block">
<span class="TrendingList__meta text-muted">
<p>Online cryptocurrency wallets, including the ones that come with Tipestry accounts, are inherently unsecure. The best way to secure your coins is in an offline wallet. Learn more about wallets and security at <a href="https://bitcoin.org/en/secure-your-wallet" target="_blank">https://bitcoin.org/en/secure-your-wallet</a>.</p>
<p>If you end up with a significant amount of coins in your Tipestry account (for example if you post something really amazing and someone tips you a full Bitcoin), it is highly recommended that you make a withdrawal to somewhere more secure. To withdrawal coins, click on the <b>Currencies</b> link on the homepage, select the currency, andchoose the <b>Withdraw/Send coins</b> option.</p>
</span>
</div>
</li>
<li>
<a class="TrendingList__item">
<span class="TrendingList__number">
10.
</span> 
Why isn't Tipestry a browser add-on?
</a>
<div class="TrendingList__meta-block">
<span class="TrendingList__meta text-muted">
<p>Tipestry started out as an add-on but we found it was too difficult to get people to try if they had to install software before they could post or even view the content. We will release a new add-on in the future once the user base is large enough for it to make sense.</p>
</span>
</div>
</li>
<li>
<a class="TrendingList__item">
<span class="TrendingList__number">
11.
</span> 
What will the TIP token do?
</a>
<div class="TrendingList__meta-block">
<span class="TrendingList__meta text-muted">
<p>The Tipestry (TIP) token will a serve several purposes:</p>
<ul>
	<li>Advertising on the site can be purchased with tokens.</li>
	<li>Premium services such as ad-free viewing can be purchased with tokens.</li>
	<li>Holders of the token will have proportional voting rights on things like moderation policy and moderator elections.</li>
	<li>Elected moderators can be compensated with tokens.</li>
</ul>
</span>
</div>
</li>
<li>
<a class="TrendingList__item">
<span class="TrendingList__number">
12.
</span> 
What are some use cases?
</a>
<div class="TrendingList__meta-block">
<span class="TrendingList__meta text-muted">
<ul>
	<li>Find out what people are saying about a particular website. Is the site legitimate – is it a scam, is its information up to date, is it fake news? Sometimes it’s hard to tell based just on a site’s content or its search engine rankings. A quick look at what other people are saying can serve as a helpful guide.</li>
	<li>Discuss sites where the comment sections have been removed. Many major news organizations have removed their comment sections in recent years. The justification often used is that the quality of the comments is generally too low to justify the cost of hosting them, and although that might be true in some cases, it also conveniently prevents readers from voicing contrary opinions.</li>
	<li>Discuss topics where there’s no obvious place to do so. For example, if I wanted to share my thoughts on rutabagas, but there’s no message board dedicated to that subject in my language, I can enter www.rutabagas.com on Tipestry and talk about it there.</li>
	<li>Discuss very specific topics. After watching a movie, you can sometimes find a forum to discuss it (although now with IMDB shutting down its message boards, even that can be difficult). However, what if you want to discuss a specific episode, or a specific song? With Tipestry you could go on the a Wikipedia page or summary for a particular episode or the lyrics page for a particular song and discuss is there.</li>
	<li>Find and share information about new product releases. For example, when software is updated and you encounter a bug, it’s often difficult to sift through years of forum posts about that software to find a discussion relevant to your problem. Going to that software’s homepage through Tipestry is a way to find the latest and most relevant discussion.</li>
</span>
</div>
</li>
<li>
<a class="TrendingList__item">
<span class="TrendingList__number">
13.
</span> 
How does moderation work?
</a>
<div class="TrendingList__meta-block">
<span class="TrendingList__meta text-muted">
<p>Moderation is an unfortunate necessity on sites that allow user-generated content. As important as free speech is, having no rules is impractical on a platform like Tipestry due to bad actors. Spam, illegal content (threats of violence, libel, etc.) and doxing all require moderation. What the exact rules are, however, is never easy to decide.</p>
<p>Rather than leave it up to us alone, our plan is to use blockchain to allow the community to transparently vote on moderation policies and elect moderators. Additionally, instead of relying on unpaid moderators to work out of the goodness of their hearts (or in order to put themselves in a position where they can push an agenda), elected moderators can be compensated for their efforts with cryptocurrency.</p>
</span>
</div>
</li>
<li>
<a class="TrendingList__item">
<span class="TrendingList__number">
14.
</span> 
What is the point of tipping?
</a>
<div class="TrendingList__meta-block">
<span class="TrendingList__meta text-muted">
<p>Traditional social media companies rely on their users to generate most of the value on their platforms. Then then typically show their gratitude by selling their users’ personal information to third party corporations and government agencies. This model is unfair and is due for a change.</p>
<p>Tipestry’s built-in cryptocurrency tipping is one way to address the problem. If someone posts something valuable, they can receive tips from us and from other users, allowing contributors to the platform to earn rewards for the value they create. Cryptocurrency also provides avenues for us (such as issuing our own tokens) to cover the costs of running the platform while respecting users’ privacy.</p>
</span>
</div>
</li>
<li>
<a class="TrendingList__item">
<span class="TrendingList__number">
15.
</span> 
How can I send feedback?
</a>
<div class="TrendingList__meta-block">
<span class="TrendingList__meta text-muted">
Please send any comments, bug reports, abuse complaints, or questions to <a href="mailto:feedback@tipestry.com" style="cursor:pointer;">feedback@tipestry.com</a>. 
</span>
</div>
</li>				
                </ul>
				<br><br>
            </div>
        </div>
    </div>
@endsection
