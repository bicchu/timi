@extends ('layouts.app')

@section ('content')
    <div class="container-fluid" style="padding-left: 0;">
        <div id="wrapper">
            <div class="row">
                <div class="col-md-9">
                    <div class="embed-responsive embed-responsive-4by3">
					<?php 
						$formattedUrl	= $site->formattedUrl;	
						$mysiteurl		= '';
						if($formattedUrl == '//msn.com/en-us/video/wonder/hidden-forest-of-bamboo-coral-may-be-1000-years-old/vi-AAxkGXL'){
							echo '<img src="/captures/msn-com-video.png" alt="" class="img-responsive">';
						}
						elseif($formattedUrl == '//thehill.com/homenews/administration/388417-the-memo-will-mueller-play-hardball-with-trump'){
							echo '<img src="/captures/thehill-com-homenews.png" alt="" class="img-responsive">';
						}
						elseif($site->screen_path)
						{ 
					?>
							<img src="{{ asset($site->screen_path) }}" alt="{{ $site->name }}" class="img-responsive">
                    <?php 
						}
						else
						{
							$pos 	= strpos($site->formattedUrl,'bloomberg.com');							
							$pos2 	= strpos($site->formattedUrl,'thesun.co.uk');
							$pos3 	= strpos($site->formattedUrl,'dailymail.co.uk');
							$pos5 	= strpos($site->formattedUrl,'coindesk.com');
							$pos6 	= strpos($site->formattedUrl,'webucator.com');
							$pos7 	= strpos($site->formattedUrl,'cnet.com');
							$pos8 	= strpos($site->formattedUrl,'cnn.com');
							$pos9 	= strpos($site->formattedUrl,'nypost.com');
							$pos10 	= strpos($site->formattedUrl,'msn.com');
							$pos11 	= strpos($site->formattedUrl,'cbsnews.com');
							$pos12 	= strpos($site->formattedUrl,'nbcnews.com');														
							$pos15 	= strpos($site->formattedUrl,'researchgate.net');														
							$pos101	= strpos($site->formattedUrl,'news.nationalgeographic.com');							
							$pos102	= strpos($site->formattedUrl,'irishtimes.com');				
							$pos103	= strpos($site->formattedUrl,'independent.co.uk');
							$pos13 	= strpos($site->formattedUrl,'theregister.co.uk');							
							$pos20 	= strpos($site->formattedUrl,'foxnews.com');
							
							/*** redirect with : https://www. */
							$pos21 	= strpos($site->formattedUrl,'rottentomatoes.com');
							$pos22 	= strpos($site->formattedUrl,'washingtontimes.com');
							$pos23 	= strpos($site->formattedUrl,'yahoo.com');
							$pos24 	= strpos($site->formattedUrl,'pcgamer.com');
							$pos25 	= strpos($site->formattedUrl,'researchgate.net');
							$pos150 	= strpos($site->formattedUrl,'sciencedaily.com');
							//$pos24 	= strpos($site->formattedUrl,'irishtimes.com');
														
							$pos50 = strpos($site->formattedUrl,'manchester.ac.uk');							
							$pos51 = strpos($site->formattedUrl,'dyn-web.com');							
							$pos52 = strpos($site->formattedUrl,'theguardian.com');
							$pos53 = strpos($site->formattedUrl,'investopedia.com');
							$pos54 = strpos($site->formattedUrl,'washingtonpost.com');
							$pos55 = strpos($site->formattedUrl,'metro.co.uk');
							
							$encrypted 			= base64_encode($site->formattedUrl);
																
							$dommain_arr1		= array();
							//$dommain_arr1[] 	= 'nytimes.com';
							$dommain_arr2[]		= 'i.imgur.com';
							$dommain_arr2[]		= 'bitcointalk.org';
							//$dommain_arr2[]		= 'itunes.apple.com';
							$domain_special[]	= 'medium.com';
							$domain_wwwspecial[] 	= 'nytimes.com';
							$domain_screenshot[] 	= 'itunes.apple.com';
							
							
							
							$dommain_arr1[] 	= 'fortune.com';
							$dommain_arr1[]		= 'yahoo.com';
							$dommain_arr1[]		= 'japantimes.co.jp';
							$dommain_arr1[]		= 'popsci.com';
							$dommain_arr1[]		= 'dailystar.co.uk';													
							$dommain_arr1[]		= 'independent.co.uk';
							$dommain_arr4[]		= 'metro.co.uk';
							$dommain_arr1[]		= 'theguardian.com';
							$dommain_arr1[]		= 'irishtimes.com';//css
							$dommain_arr1[]		= 'washingtonpost.com';//not open
							$dommain_arr1[]		= 'investopedia.com';
							$dommain_arr3[]		= 'manchester.ac.uk';//css							
							$dommain_arr1[]		= 'researchgate.net';
							$dommain_arr1[]		= 'pcgamer.com';
							$dommain_arr1[]		= 'forbes.com'; //not working
							$dommain_arr3[]		= 'zdnet.com';//not working
							$dommain_arr1[]		= 'commondreams.org';
							$dommain_arr1[]		= 'cnn.com';
							$dommain_arr1[]		= 'usatoday.com';
							$dommain_arr3[]		= 'utsouthwestern.edu';//css
							$dommain_arr1[]		= 'sciencedaily.com';// redirect
							$dommain_arr3[]		= 'sciencenewsline.com';
							$dommain_arr1[]		= 'theverge.com';
							$dommain_arr1[]		= 'huffingtonpost.com';														
							$dommain_arr1[]		= 'urmc.rochester.edu';//css
							$dommain_arr1[]		= 'newsweek.com';//not open
							$dommain_arr1[]		= 'newsmax.com';
							$dommain_arr1[]		= 'aol.com';//css														
							$dommain_arr1[]		= 'zerohedge.com';//css
							$dommain_arr1[]		= 'politico.com';//							
							$dommain_arr1[]		= 'mirror.co.uk';
							//$dommain_arr1[]	= 'axios.com'; // Need JS Modification
							$dommain_arr1[]		= 'weforum.org';//not done
							$dommain_arr1[]		= 'theregister.co.uk';//css
							$dommain_arr1[]		= 'coindesk.com';//css
							$dommain_arr1[]		= 'express.co.uk';//css
							$dommain_arr1[]		= 'infowars.com';
							$dommain_arr1[]		= 'rferl.org';//css
							$dommain_arr1[]		= 'thesun.co.uk';//css
							$dommain_arr1[]		= 'ktnv.com';
							$dommain_arr1[]		= 'rollingstone.com';
							$dommain_arr1[]		= 'express.co.uk';
							$dommain_arr1[]		= 'abcactionnews.com';
							$dommain_arr1[]		= 'hollywoodreporter.com';
							$dommain_arr1[]		= 'thestreet.com';
							$dommain_arr1[]		= 'engadget.com';
							$dommain_arr1[]		= 'marketwatch.com';
							$dommain_arr1[]		= 'reddit.com';
							//$dommain_arr1[]	= 'thelancet.com';//***
							$dommain_arr1[]		= 'barrons.com';
							$dommain_arr1[]		= 'cnbc.com';
							$dommain_arr1[]		= 'usnews.com';//Not Working
							$dommain_arr1[]		= 'kodak.com';// Not Working
							$dommain_arr1[]		= 'amazon.com'; //Not Working	


							$dommain_arr2[]		= 'coinmarketcap.com';//css							
							$dommain_arr2[]		= 'news.nationalgeographic.com';//css							
							$dommain_arr2[]		= 'arstechnica.com';							
							$dommain_arr2[]		= 'news.stanford.edu';
							$dommain_arr2[]		= 'sallysbakingaddiction.com';
							$dommain_arr2[]		= 'steemit.com';//character issue
							$dommain_arr3[]		= 'foxnews.com';//Css Not found
							$dommain_arr3[]		= 'scmp.com';
							$dommain_arr3[]		= 'thelancet.com';
							$dommain_arr3[]		= 'dw.com';//CSS not working
							$dommain_arr3[]		= 'dailymail.co.uk';//Css not working														
							$dommain_arr4[]		= 'money.cnn.com';//css
							$dommain_arr4[]		= 'thehill.com';//css														
							$dommain_arr5[]		= 'google.com.au';
							$dommain_arr5[]		= 'axios.com';
							//echo '<pre>';print_r($domain_arr1);exit;
							
							$domain 	= explode('/',str_replace('//','',$formattedUrl));
							$domain 	= $domain[0];
							$mysiteurl	= "";
							if(in_array($domain,$dommain_arr1)){
								//echo 44;exit;
								$mysiteurl	= 'https://www.'.str_replace('//','',base64_decode($encrypted));
								$myurl 		= 'https://tipestry.com/iframe.php?domaintype=1&domain='.base64_encode($domain).'&url='.$encrypted;								
							}
							elseif(in_array($domain,$dommain_arr2)){
								$mysiteurl	= 'https://'.str_replace('//','',base64_decode($encrypted));
								$myurl = 'https://tipestry.com/iframe.php?domaintype=2&domain='.base64_encode($domain).'&url='.$encrypted;
								
							}
							elseif(in_array($domain,$dommain_arr3)){
								$mysiteurl	= 'http://www.'.str_replace('//','',base64_decode($encrypted));
								$myurl = 'https://tipestry.com/iframe.php?domaintype=3&domain='.base64_encode($domain).'&url='.$encrypted;								
							}
							elseif(in_array($domain,$dommain_arr4)){
								
								$myurl = 'https://tipestry.com/iframe.php?domaintype=4&domain='.base64_encode($domain).'&url='.$encrypted;								
							}
							elseif(in_array($domain,$dommain_arr5)){
								$mysiteurl	= 'http://'.str_replace('//','',base64_decode($encrypted));
								$myurl = 'https://tipestry.com/captures/google.jpg';
								//$myurl = 'https://tipestry.com/iframe.php?domaintype=5&domain='.base64_encode($domain).'&url='.$encrypted;
								//echo $myurl;exit;
							}
							elseif(in_array($domain,$domain_special)){
								$mysiteurl	= 'https://'.str_replace('//','',base64_decode($encrypted));								
								$myurl = 'https://tipestry.com/iframe.php?domain_special='.base64_encode($mysiteurl);
								
							}							
							elseif(in_array($domain,$domain_wwwspecial)){
								$mysiteurl	= 'https://www.'.str_replace('//','',base64_decode($encrypted));								
								$myurl = 'https://tipestry.com/iframe.php?domain_special='.base64_encode($mysiteurl);
								
							}
							elseif(in_array($domain,$domain_screenshot)){
								
								$mysiteurl	= 'https://'.str_replace('//','',base64_decode($encrypted));
								@$googlePagespeedData = file_get_contents("https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url=".$mysiteurl."&screenshot=true");
								@$googlePagespeedData = json_decode($googlePagespeedData, true);
								@$screenshot = $googlePagespeedData['screenshot']['data'];
								@$screenshot = str_replace(array('_','-'),array('/','+'),$screenshot);
								/*echo "<img src=\"data:image/jpeg;base64,".$screenshot."\" />";exit;
								echo 777;exit;
								$myurl = 'https://tipestry.com/iframe.php?domain_special='.base64_encode($mysiteurl);
								*/
							}
							elseif(!empty($pos)){
								
								$myurl = 'https://tipestry.com/iframe.php?param1='.$encrypted;
							}
							elseif(!empty($pos51) || !empty($pos50)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param4='.$encrypted;
							}
							elseif(!empty($pos1)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param2='.$encrypted;
							}
							elseif(!empty($pos2)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param3='.$encrypted;
							}
							elseif(!empty($pos3) || !empty($pos15)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param4='.$encrypted;
							}
							elseif(!empty($pos5) || !empty($pos21) || !empty($pos22) || !empty($pos23) || !empty($pos24) || !empty($pos25)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param5='.$encrypted;
							}
							elseif(!empty($pos6) || !empty($pos7) || !empty($pos8) || !empty($pos9) || !empty($pos10) || !empty($pos11) || !empty($pos12)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param6='.$encrypted;
							}
							elseif(!empty($pos13) || !empty($pos53)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param13='.$encrypted;
							}
							elseif(!empty($pos54)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param14='.$encrypted;
							}							
							elseif(!empty($pos20)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param7='.$encrypted;
							}
							elseif(!empty($pos52)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param7='.$encrypted;
							}
							elseif(!empty($pos55)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param55='.$encrypted;
							}
							elseif(!empty($pos101) || !empty($pos102) || !empty($pos103)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param101='.$encrypted;
							}
							elseif(!empty($pos150)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param150='.$encrypted;
							}
							else{
								$myurl = $site->formattedUrl;
							}
							
							if(in_array($domain,$domain_screenshot)){
								echo "<img src=\"data:image/jpeg;base64,".$screenshot."\" width=\"100%\" />";
							}else{	
						?>
								<iframe  src="<?php echo $myurl;?>"  style="width: 100%!important;height: 100%!important;"></iframe>
						<?php
							}
							?>
						
                    <?php } ?>					
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-header">								
                                @if (Auth::check() && Auth::user()->can('delete-topic', $topic))
									<i class="fa fa-edit edit-flag" aria-hidden="true" aria-label="Edit" onclick="editpost()" style="cursor:pointer;" ></i>
									&nbsp; 								
									<a href="#"
                                       class="pull-left"
                                       style="margin-left: -20px;"
                                       title="Delete comment"
                                       alt="Delete comment"
                                       onclick="event.preventDefault();
                                               document.getElementById('delete-topic-{{ $topic->id }}').submit();">
                                        <i class="fa fa-trash trash-flag" aria-hidden="true" aria-label="Delete"></i>
                                    </a>

                                    <form id="delete-topic-{{ $topic->id }}"
                                          action="{{ action("TopicsController@destroy", $topic) }}"
                                          method="POST"
                                          style="display: none;"
                                    >
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form>
                                @endif

                                <h4>
                                    <b id="editposttitle_txt">{{ $topic->title }}</b>
									<div class="form-group" style="display:none;" id="editdiv">
										<input type="text" id="editposttitle" value="{{ $topic->title }}" placeholder="Title" required="required" class="form-control">
										<br>                                        
										<?php
											$msg  = $topic->message;									
											$msg  = str_replace('&lt;br&gt;',' <br>',$msg);
											//$msg1 = str_replace('"','&quot;',$msg);
										?>									
										<textarea name="editpostmsg" id="editpostmsg" rows="5" placeholder="Message" required="required" class="form-control"><?php	echo $msg;	?></textarea>
										
										<small>
											<a href="{{ action('SitesController@index', $site->token) }}" class="text-muted">
												{{ $site->url }}
											</a>
										</small>
										<br><br>
										<button type="button" style="display:none1; cursor:pointer;" class="btn btn-primary pull-right" onclick="updatepost()">Update</button>                               
										<button type="button" style="display:none1; cursor:pointer; margin-right:10px;" class="btn btn-default pull-right" onclick="canceleditpost()">Cancel</button>                                
										<br>
									</div>



									<small id="urltxt">
										<a href="{{ action('SitesController@index', $site->token) }}" class="text-muted">
											{{ $site->url }}
										</a>
									</small>
                                </h4>	
									<?php 
										if(!empty(\Auth::user()->email)){
											$currentuser_emailid = \Auth::user()->email;
											echo '<i class="fa fa-thumbs-up" 	style="color:#00A2E8; cursor:pointer;" 	onclick="upvote('.$website->id.')"></i> <span id="upvotetxt">'.$website->totalupvote.'</span> &nbsp; ';
											echo '<i class="fa fa-thumbs-down" 	style="color:#00A2E8; cursor:pointer;" 	onclick="downvote('.$website->id.')"></i> <span id="downvotetxt">'.$website->totaldownvote.'</span>';
											echo '';
											/*if($topic->user->email != $currentuser_emailid)
											{{{ $topic }}*/
									?>
												@if (Auth::check())
													<gift-coins :user="{{ $topic->user }}" 	:giftable="{{ $topic }}" type="website"></gift-coins>
												@endif
									<?php
											//}
										}else{
											$currentuser_emailid = '';
											echo '<i class="fa fa-thumbs-up" 	style="color:#00A2E8; cursor:pointer;" 	onclick="javascript:alert(\'Please logedin before voting.\')"></i> <span id="upvotetxt">'.$website->totalupvote.'</span> &nbsp; ';
											echo '<i class="fa fa-thumbs-down" 	style="color:#00A2E8; cursor:pointer;" 	onclick="javascript:alert(\'Please logedin before voting.\')"></i> <span id="downvotetxt">'.$website->totaldownvote.'</span>';											
										}
										
										
										if(!empty($websitegives) && count($websitegives)>0)
										{
											foreach($websitegives as $val)
											{
												if($val->wallettype == 'ethxrtcoin'){
													echo '<img src="http://demo.tipestry.com/images/currencies/xrt.gif" style="width:36px;" />';
													//echo '<img src="" style="width:36px;" />';
												}
												elseif($val->wallettype == 'ethtipcoin'){
													echo '<img src="http://demo.tipestry.com/images/currencies/ethtip.gif" style="width:36px;" />';													
												}
												elseif($val->wallettype == 'ethcoin'){
													echo '<img src="http://demo.tipestry.com/images/currencies/eth.gif" style="width:36px;" />';
												}
												elseif($val->wallettype == 'dogecoin'){
													echo '<img src="http://demo.tipestry.com/images/currencies/doge.png" style="width:36px;" />';
												}
												elseif($val->wallettype == 'bitcoin'){
													echo '<img src="http://demo.tipestry.com/images/currencies/btc.png" style="width:36px;" />';
												}
												elseif($val->wallettype == 'bchcoin'){
													echo '<img src="http://demo.tipestry.com/images/currencies/bch.gif" style="width:36px;" />';
												}
												echo number_format($val->sum,8);
												//.' &nbsp; &nbsp; '.$val->wallettype.' &nbsp; ';
												//echo '<pre>';print_r($val);echo '</pre>';
											}
										}
									?>
								<hr>
                            </div>
<?php	
	if($topic->user->email != $currentuser_emailid){
?>
		@if (Auth::check())
			<gift-coins
					:user="{{ $topic->user }}"
					:giftable="{{ $topic }}"
					type="topic"
			></gift-coins>
		@endif
<?php
	}
?>
                            <p>
                                <b>Author:</b> {{ $topic->user->name }} &nbsp; 
								<?php
									$dtd = date('Y-m-d H:i:s',$topic->Orderby);
								?>
								{{ (new \Carbon\Carbon($topic->created_at))->diffForHumans() }} &nbsp; 
                                
                                   
									
                                @if ($topic->hasGifts('btc'))
									 {!! $topic->renderTotalGifts('btc') !!}
								@endif								
                                @if ($topic->hasGifts('bch'))
									 {!! $topic->renderTotalGifts('bch') !!}
								@endif								
                                @if ($topic->hasGifts('eth'))
                                    {!! $topic->renderTotalGifts('eth') !!}
                                @endif
								@if ($topic->hasGifts('ethtip'))
                                    {!! $topic->renderTotalGifts('ethtip') !!}
                                @endif
								@if ($topic->hasGifts('ethxrtcoin'))
                                    {!! $topic->renderTotalGifts('ethxrtcoin') !!}
                                @endif
								@if ($topic->hasGifts('doge'))
                                    {!! $topic->renderTotalGifts('doge') !!}
                                @endif
								
                            </p>
                            
							
							
								<?php /*@if (Auth::check() && Auth::user()->can('report-topic', $topic))*/ ?>
									<div class="save-post">
										@if(!empty(Auth::user()->id) && Auth::user()->id > 0 && $reported == 0)
                                        
                        				   <a style="cursor:pointer;"
										   title="Report topic"
										   alt="Report topic"
										   onclick="event.preventDefault();
												   document.getElementById('report-topic-{{ $topic->id }}').submit();">
                                                                                                                                                         
											<i class="fa fa-flag-o spam-flag" aria-hidden="true" aria-label="Report"></i>
										</a>                                        

                                        @endif



										<form id="report-topic-{{ $topic->id }}"
											  action="{{ action('TopicsController@report', $topic) }}"
											  method="POST"
											  style="display: none;"
										>
											{{ csrf_field() }}
										</form>
									</div>
								<?php /*@endif*/ ?>

<?php
	//$msg = text.replace(urlRegex, '<a href="$1">$1</a>')
	
?>
							<p class="linebreak" id="editpostmsg_txt">
								<?php
									$msg  = $topic->message;									
									$msg  = str_replace('&lt;br&gt;',' <br>',$msg);
									$msg1 = str_replace('"','&quot;',$msg);
									echo $msg;
								?>
							</p>

							<input id="msglinebreak" style="display:none;" value="<?php echo $msg1; ?>">
							<?php /*<input id="msglinebreak" style="display:none;" value="<?php echo $topic->message; ?>">*/ ?>
                        </div>
                    </div>
					<?php
					//echo '<pre>';print_r($topic);exit;
					?>
                    @include ('comments.index', ['controller' => 'TopicsController', 'model' => $topic])
                </div>
            </div>
        </div>
    </div>
<?php
function txtformat($txt){
	$txt_arr = explode(' ',$txt);
	echo '<pre>';print_r($txt_arr);exit;
	$rtxt = $txt.'++++33';
	return $rtxt;
}
function plainUrlToLink($text = ''){
    $text = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $text));
    $data = '';
    foreach( explode(' ', $text) as $str){
        if (preg_match('#^http?#i', trim($str)) || preg_match('#^www.?#i', trim($str))) {
            $data .= '<a href="'.$str.'">'.$str.'</a> ';
        } else {
            $data .= $str .' ';
        }
    }
    return trim($data);
}


function plainUrlToLink1($txt_content,$target="_blank") {       
    $regex = "/(http|https)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,6}(\/\S*)+?/";
    if(preg_match($regex, $txt_content,$matches)) {
       return preg_replace($regex, "<a href='".$matches[0]."' target='".$target."'>".$matches[0]."</a> ", $txt_content);
    } 
    else {
       return $txt_content;
    }
}

?>
<script>
editpost = function(){
	$('#editposttitle_txt').hide();
	//$('#editposttitle').show();
	$('#editpostmsg_txt').hide();
	//$('#editpostmsg').show();
	$('#editdiv').show();
	$('#urltxt').hide();
}
canceleditpost = function(){
	$('#editposttitle_txt').show();
	//$('#editposttitle').show();
	$('#editpostmsg_txt').show();
	//$('#editpostmsg').show();
	$('#editdiv').hide();
	$('#urltxt').show();
}

updatepost = function(){
	var x = $('#editposttitle').val();
	var y = $('#editpostmsg').val();
	y = y.replace(/\n/g,"<br>");
	
	$('#editposttitle_txt').html(x);
	$('#editpostmsg_txt').html(y);
	canceleditpost();
	
	var id 		= <?php echo $topic->id;?>;
	var url 	= 'http://tipestry.com/updatetopic';
	var data 	= 'id='+id+'&title='+x+'&msg='+y;
	alert(456);
	$.ajax({ 
		url: url,
		data: data,
		type: 'POST',
		success: function(response){
			alert('success');
		}
	});	
	
}

</script>
@endsection