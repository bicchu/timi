@extends ('layouts.app')
@section ('content')


<div class="container-fluid" style="padding-left: 0;">
    <div id="wrapper">
        <div class="row">
            <div class="col-md-9">
                <div class="embed-responsive embed-responsive-4by3">
					<?php 
						$formattedUrl	= $site->formattedUrl;	
						$mysiteurl		= '';
						
						if($formattedUrl == '//msn.com/en-us/video/wonder/hidden-forest-of-bamboo-coral-may-be-1000-years-old/vi-AAxkGXL'){
							echo '<img src="/captures/msn-com-video.png" alt="" class="img-responsive">';
						}
						elseif($formattedUrl == '//thehill.com/homenews/administration/388417-the-memo-will-mueller-play-hardball-with-trump'){
							echo '<img src="/captures/thehill-com-homenews.png" alt="" class="img-responsive">';
						}
						elseif($site->screen_path)
						{ 
					?>
							<img src="{{ asset($site->screen_path) }}" alt="{{ $site->name }}" class="img-responsive">
                    <?php 
						}
						else
						{							
							$pos 	= strpos($site->formattedUrl,'bloomberg.com');							
							$pos2 	= strpos($site->formattedUrl,'thesun.co.uk');
							$pos3 	= strpos($site->formattedUrl,'dailymail.co.uk');
							$pos5 	= strpos($site->formattedUrl,'coindesk.com');
							$pos6 	= strpos($site->formattedUrl,'webucator.com');
							$pos7 	= strpos($site->formattedUrl,'cnet.com');
							$pos8 	= strpos($site->formattedUrl,'cnn.com');
							$pos9 	= strpos($site->formattedUrl,'nypost.com');
							$pos10 	= strpos($site->formattedUrl,'msn.com');
							$pos11 	= strpos($site->formattedUrl,'cbsnews.com');
							$pos12 	= strpos($site->formattedUrl,'nbcnews.com');														
							$pos15 	= strpos($site->formattedUrl,'researchgate.net');														
							$pos101	= strpos($site->formattedUrl,'news.nationalgeographic.com');							
							$pos102	= strpos($site->formattedUrl,'irishtimes.com');				
							$pos103	= strpos($site->formattedUrl,'independent.co.uk');
							$pos13 	= strpos($site->formattedUrl,'theregister.co.uk');							
							$pos20 	= strpos($site->formattedUrl,'foxnews.com');
							
							/*** redirect with : https://www. */
							$pos21 	= strpos($site->formattedUrl,'rottentomatoes.com');
							$pos22 	= strpos($site->formattedUrl,'washingtontimes.com');
							$pos23 	= strpos($site->formattedUrl,'yahoo.com');
							$pos24 	= strpos($site->formattedUrl,'pcgamer.com');
							$pos25 	= strpos($site->formattedUrl,'researchgate.net');
							$pos150 	= strpos($site->formattedUrl,'sciencedaily.com');
							//$pos24 	= strpos($site->formattedUrl,'irishtimes.com');
														
							$pos50 = strpos($site->formattedUrl,'manchester.ac.uk');							
							$pos51 = strpos($site->formattedUrl,'dyn-web.com');							
							$pos52 = strpos($site->formattedUrl,'theguardian.com');
							$pos53 = strpos($site->formattedUrl,'investopedia.com');
							$pos54 = strpos($site->formattedUrl,'washingtonpost.com');
							$pos55 = strpos($site->formattedUrl,'metro.co.uk');
							
							$encrypted 			= base64_encode($site->formattedUrl);
																
							$dommain_arr1		= array();
							//$dommain_arr1[] 	= 'nytimes.com';
							$dommain_arr2[]		= 'i.imgur.com';
							$dommain_arr2[]		= 'bitcointalk.org';
							//$dommain_arr2[]		= 'itunes.apple.com';
							$domain_special[]	= 'medium.com';
							$domain_wwwspecial[] 	= 'nytimes.com';
							$domain_screenshot[] 	= 'itunes.apple.com';
							
							
							
							$dommain_arr1[] 	= 'fortune.com';
							$dommain_arr1[]		= 'yahoo.com';
							$dommain_arr1[]		= 'japantimes.co.jp';
							$dommain_arr1[]		= 'popsci.com';
							$dommain_arr1[]		= 'dailystar.co.uk';													
							$dommain_arr1[]		= 'independent.co.uk';
							$dommain_arr4[]		= 'metro.co.uk';
							$dommain_arr1[]		= 'theguardian.com';
							$dommain_arr1[]		= 'irishtimes.com';//css
							$dommain_arr1[]		= 'washingtonpost.com';//not open
							$dommain_arr1[]		= 'investopedia.com';
							$dommain_arr3[]		= 'manchester.ac.uk';//css							
							$dommain_arr1[]		= 'researchgate.net';
							$dommain_arr1[]		= 'pcgamer.com';
							$dommain_arr1[]		= 'forbes.com'; //not working
							$dommain_arr3[]		= 'zdnet.com';//not working
							$dommain_arr1[]		= 'commondreams.org';
							$dommain_arr1[]		= 'cnn.com';
							$dommain_arr1[]		= 'usatoday.com';
							$dommain_arr3[]		= 'utsouthwestern.edu';//css
							$dommain_arr1[]		= 'sciencedaily.com';// redirect
							$dommain_arr3[]		= 'sciencenewsline.com';
							$dommain_arr1[]		= 'theverge.com';
							$dommain_arr1[]		= 'huffingtonpost.com';														
							$dommain_arr1[]		= 'urmc.rochester.edu';//css
							$dommain_arr1[]		= 'newsweek.com';//not open
							$dommain_arr1[]		= 'newsmax.com';
							$dommain_arr1[]		= 'aol.com';//css														
							$dommain_arr1[]		= 'zerohedge.com';//css
							$dommain_arr1[]		= 'politico.com';//							
							$dommain_arr1[]		= 'mirror.co.uk';
							//$dommain_arr1[]	= 'axios.com'; // Need JS Modification
							$dommain_arr1[]		= 'weforum.org';//not done
							$dommain_arr1[]		= 'theregister.co.uk';//css
							$dommain_arr1[]		= 'coindesk.com';//css
							$dommain_arr1[]		= 'express.co.uk';//css
							$dommain_arr1[]		= 'infowars.com';
							$dommain_arr1[]		= 'rferl.org';//css
							$dommain_arr1[]		= 'thesun.co.uk';//css
							$dommain_arr1[]		= 'ktnv.com';
							$dommain_arr1[]		= 'rollingstone.com';
							$dommain_arr1[]		= 'express.co.uk';
							$dommain_arr1[]		= 'abcactionnews.com';
							$dommain_arr1[]		= 'hollywoodreporter.com';
							$dommain_arr1[]		= 'thestreet.com';
							$dommain_arr1[]		= 'engadget.com';
							$dommain_arr1[]		= 'marketwatch.com';
							$dommain_arr1[]		= 'reddit.com';
							//$dommain_arr1[]	= 'thelancet.com';//***
							$dommain_arr1[]		= 'barrons.com';
							$dommain_arr1[]		= 'cnbc.com';
							$dommain_arr1[]		= 'usnews.com';//Not Working
							$dommain_arr1[]		= 'kodak.com';// Not Working
							$dommain_arr1[]		= 'amazon.com'; //Not Working	


							$dommain_arr2[]		= 'coinmarketcap.com';//css							
							$dommain_arr2[]		= 'news.nationalgeographic.com';//css							
							$dommain_arr2[]		= 'arstechnica.com';							
							$dommain_arr2[]		= 'news.stanford.edu';
							$dommain_arr2[]		= 'sallysbakingaddiction.com';
							$dommain_arr2[]		= 'steemit.com';//character issue
							$dommain_arr3[]		= 'foxnews.com';//Css Not found
							$dommain_arr3[]		= 'scmp.com';
							$dommain_arr3[]		= 'thelancet.com';
							$dommain_arr3[]		= 'dw.com';//CSS not working
							$dommain_arr3[]		= 'dailymail.co.uk';//Css not working														
							$dommain_arr4[]		= 'money.cnn.com';//css
							$dommain_arr4[]		= 'thehill.com';//css														
							$dommain_arr5[]		= 'google.com.au';
							$dommain_arr5[]		= 'axios.com';
							//echo '<pre>';print_r($domain_arr1);exit;
							
							$domain 	= explode('/',str_replace('//','',$formattedUrl));
							$domain 	= $domain[0];
							$mysiteurl	= "";
							if(in_array($domain,$dommain_arr1)){
								//echo 44;exit;
								$mysiteurl	= 'https://www.'.str_replace('//','',base64_decode($encrypted));
								//echo $mysiteurl ; exit ;
								$myurl 		= 'https://tipestry.com/iframe.php?domaintype=1&domain='.base64_encode($domain).'&url='.$encrypted;								
								
								
							
							}
							elseif(in_array($domain,$dommain_arr2)){
								$mysiteurl	= 'https://'.str_replace('//','',base64_decode($encrypted));
								$myurl = 'https://tipestry.com/iframe.php?domaintype=2&domain='.base64_encode($domain).'&url='.$encrypted;
								
							}
							elseif(in_array($domain,$dommain_arr3)){
								$mysiteurl	= 'http://www.'.str_replace('//','',base64_decode($encrypted));
								$myurl = 'https://tipestry.com/iframe.php?domaintype=3&domain='.base64_encode($domain).'&url='.$encrypted;								
							}
							elseif(in_array($domain,$dommain_arr4)){
								
								$myurl = 'https://tipestry.com/iframe.php?domaintype=4&domain='.base64_encode($domain).'&url='.$encrypted;								
							}
							elseif(in_array($domain,$dommain_arr5)){
								$mysiteurl	= 'http://'.str_replace('//','',base64_decode($encrypted));
								$myurl = 'https://tipestry.com/captures/google.jpg';
								//$myurl = 'https://tipestry.com/iframe.php?domaintype=5&domain='.base64_encode($domain).'&url='.$encrypted;
								//echo $myurl;exit;
							}
							elseif(in_array($domain,$domain_special)){
								$mysiteurl	= 'https://'.str_replace('//','',base64_decode($encrypted));								
								$myurl = 'https://tipestry.com/iframe.php?domain_special='.base64_encode($mysiteurl);
								
							}
							elseif(in_array($domain,$domain_wwwspecial)){
								$mysiteurl	= 'https://www.'.str_replace('//','',base64_decode($encrypted));								
								$myurl = 'https://tipestry.com/iframe.php?domain_special='.base64_encode($mysiteurl);
								
							}
							elseif(in_array($domain,$domain_screenshot)){
								
								$mysiteurl	= 'https://'.str_replace('//','',base64_decode($encrypted));
								@$googlePagespeedData = file_get_contents("https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url=".$mysiteurl."&screenshot=true");
								@$googlePagespeedData = json_decode($googlePagespeedData, true);
								@$screenshot = $googlePagespeedData['screenshot']['data'];
								@$screenshot = str_replace(array('_','-'),array('/','+'),$screenshot);
								/*echo "<img src=\"data:image/jpeg;base64,".$screenshot."\" />";exit;
								echo 777;exit;
								$myurl = 'https://tipestry.com/iframe.php?domain_special='.base64_encode($mysiteurl);
								*/
							}
							elseif(!empty($pos)){
								
								$myurl = 'https://tipestry.com/iframe.php?param1='.$encrypted;
							}
							elseif(!empty($pos51) || !empty($pos50)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param4='.$encrypted;
							}
							elseif(!empty($pos1)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param2='.$encrypted;
							}
							elseif(!empty($pos2)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param3='.$encrypted;
							}
							elseif(!empty($pos3) || !empty($pos15)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param4='.$encrypted;
							}
							elseif(!empty($pos5) || !empty($pos21) || !empty($pos22) || !empty($pos23) || !empty($pos24) || !empty($pos25)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param5='.$encrypted;
							}
							elseif(!empty($pos6) || !empty($pos7) || !empty($pos8) || !empty($pos9) || !empty($pos10) || !empty($pos11) || !empty($pos12)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param6='.$encrypted;
							}
							elseif(!empty($pos13) || !empty($pos53)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param13='.$encrypted;
							}
							elseif(!empty($pos54)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param14='.$encrypted;
							}							
							elseif(!empty($pos20)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param7='.$encrypted;
							}
							elseif(!empty($pos52)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param7='.$encrypted;
							}
							elseif(!empty($pos55)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param55='.$encrypted;
							}
							elseif(!empty($pos101) || !empty($pos102) || !empty($pos103)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param101='.$encrypted;
							}
							elseif(!empty($pos150)){
								//$encrypted = base64_encode($site->formattedUrl);
								$myurl = 'https://tipestry.com/iframe.php?param150='.$encrypted;
							}
							else{
								$myurl = $site->formattedUrl;
							}
							
							
							if(in_array($domain,$domain_screenshot)){
								echo "<img src=\"data:image/jpeg;base64,".$screenshot."\" width=\"100%\" />";
							}else{
						?>
								<iframe  src="<?php echo $myurl;?>"  style="width: 100%!important;height: 100%!important;"></iframe>
						<?php
							}					
						} 
					?>
                    
                    <script>document.write()</script>
					
                </div>
            </div>

            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">

                        <h2>Topics</h2>
                        <a href="<?php //echo $mysiteurl;?>" target="_blank" style="color:#000;" ><?php //echo $mysiteurl;?></a><br>
                        <!-- Button collapse new topic form -->
                        @if (Auth::check())
                            <br>						
							<button class="btn btn-success btn-sm"
                                    type="button"
                                    data-toggle="collapse"
                                    data-target="#addNewTopic"
                                    aria-expanded="false"
                                    aria-controls="addNewTopic"
                            >
                                <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Create a new topic
                            </button>
							<br>
                        @endif
						

                        <div class="collapse" id="addNewTopic">
                            <div class="well">
                                <form action="{{ action('TopicsController@store', $site) }}" method="POST" onsubmit="chg_text()">
                                    {{ csrf_field() }}
									<input type="hidden" name="mysiteurl" value="<?php echo $mysiteurl;?>" />
                                    <div class="form-group">
                                        <label>Title:</label>
                                        <input type="text" name="title" class="form-control" placeholder="Title" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Message:</label>
                                        <textarea name="msg" id="topic_msg" rows="5" class="form-control" placeholder="Message" required></textarea>
                                        <textarea name="message" id="topic_message" rows="5" class="form-control" placeholder="Message" style="display:none;"></textarea>
                                    </div>
                                    <?php /*<button type="button" class="btn btn-primary pull-right sbmt_frm_new_topic">Add</button>*/ ?>
									<button type="submit" style="display:none1;" class="btn btn-primary pull-right">Add</button>
                                </form>
                                <div class="clearfix"></div>
                            </div>
                        </div>						
                        @if (count($site->topics))
                            <ul class="TrendingList">
                                @foreach ($site->topics as $index => $topic)
                                    <li>
                                        <a href="{{ action('TopicsController@show', [$site, $topic]) }}"
                                           class="TrendingList__item"
                                        >
                                            <span class="TrendingList__number">
                                                {{ $index + 1 }}.
                                            </span> {{ $topic->title }}
                                        </a>&nbsp;&nbsp;

                                        <div class="TrendingList__meta-block">
                                            <span class="TrendingList__meta text-muted">
                                                Posted <b>{{ $topic->created_at->diffForHumans() }}</b> by <b>{{ $topic->user->name }}</b>
                                            </span>

                                            <span class="TrendingList__meta text-muted">
                                                <b>{{ $topic->comments->count() }}</b> comments
                                            </span>
                                        </div>

                                        <div>
                                            @if (count($topic->gifts))
                                                <span class="TrendingList__meta text-muted">
                                                    @if ($topic->hasGifts('btc'))
                                                        {!! $topic->renderTotalGifts('btc') !!}
                                                    @endif

                                                    @if ($topic->hasGifts('doge'))
                                                        {!! $topic->renderTotalGifts('doge') !!}
                                                    @endif
                                                </span>
                                            @endif
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        @else
                            <p class="text-info">
                                <?php /*Message: Add*/ ?>
                            </p>
                        @endif



						
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection