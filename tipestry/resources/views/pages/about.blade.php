@extends ('layouts.app')

@section ('content')
    <div class="container">
        <br>

        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/KIJVHo9RmTI"></iframe>
        </div>

        <h2>Talk</h2>
        <hr>

        <p class="text-default">
            <b>Tipestry lets you leave comments on any webpage. Some of the ways to use Tipestry are:</b>
        </p>

        <ul>
            <li>
                Find out what people are saying about a particular website. Is the site legitimate – is it a scam, is its information up to date, is it fake news? Sometimes it’s hard to tell based just on a site’s content or its search engine rankings. A quick look at what other people are saying can serve as a helpful guide.
            </li>

            <li>
                Discuss sites where the comment sections have been removed. Many major news organizations have removed their comment sections in recent years. The justification often used is that the quality of the comments is generally too low to justify the cost of hosting them, and although that might be true in some cases, it also conveniently prevents readers from voicing contrary opinions.
            </li>

            <li>
                Discuss topics where there’s no obvious place to do so. For example if I wanted to share my thoughts on rutabagas, but there’s no message board dedicated to that subject in my language, I can enter www.rutabagas.com on Tipestry and talk about it there.
            </li>

            <li>
                Find and share information about new product releases. For example, when software is updated and you encounter a bug, it’s often difficult to sift through years of forum posts about that software to find a discussion relevant to your problem. Going to that software’s homepage through Tipestry is a way to find the latest and most relevant discussion.
            </li>
        </ul>

        <h2>Vote (cooming soon)</h2>
        <hr>

        <p>
            Moderation is an unfortunate necessity on sites that allow user-generated content. As important as free speech is, having no rules is impractical on a platform like Tipestry due to bad actors. Spam, illegal content (threats of violence, libel, etc.) and doxing all require moderation. What the exact rules are, however, is never easy to decide.
        </p>

        <p>
            Rather than leave it up to us alone, our plan is to use blockchain to allow the community to vote on moderation policies and elect moderators. Additionally, instead of relying on unpaid moderators to work out of the goodness of their hearts (or in order to put themselves in a position where they can push an agenda), elected moderators can be compensated for their efforts with cryptocurrency.
        </p>

        <h2>Give</h2>
        <hr>

        <p>
            Traditional social media companies rely on their users to generate most of the value on their platforms. They then typically show their gratitude by selling their users’ personal information to third party corporations and government agencies. This model is unfair and is due for a change.
        </p>

        <p>
            Tipestry’s built-in cryptocurrency tipping is one way to address the problem. If someone posts something valuable, they can receive tips from us and from other users, allowing contributors to the platform to earn rewards for the value they create. Cryptocurrency also provides avenues for us (such as issuing our own tokens) to cover the costs of running the site without having to resort to violating the privacy of our users.
        </p>

		</p>
			<br>
		<p>

    </div>
@endsection