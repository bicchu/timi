@extends ('layouts.app')

@section ('content')
<div class="container">
  <style type="text/css">
@import url('https://themes.googleusercontent.com/fonts/css?kit=wAPX1HepqA24RkYW1AuHYA');ol{margin:0;padding:0}table td,table th{padding:0}.c11{color:#000000;font-weight:700;text-decoration:underline;vertical-align:baseline;font-size:12pt;font-family:"Times New Roman";font-style:italic}.c22{color:#000000;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:11pt;font-family:"Arial";font-style:normal}.c18{color:#333333;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:12pt;font-family:"Arial";font-style:italic}.c1{color:#000000;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:11pt;font-family:"Calibri";font-style:normal}.c0{color:#000000;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:12pt;font-family:"Times New Roman";font-style:normal}.c2{color:#000000;font-weight:700;text-decoration:none;vertical-align:baseline;font-size:12pt;font-family:"Times New Roman";font-style:normal}.c20{color:#000000;font-weight:700;text-decoration:none;vertical-align:baseline;font-size:16pt;font-family:"Times New Roman";font-style:normal}.c3{margin-left:18pt;padding-top:0pt;padding-bottom:0pt;line-height:1.0;text-align:justify}.c12{padding-top:0pt;padding-bottom:0pt;line-height:1.15;text-align:left}.c24{font-size:11pt;font-family:"Times New Roman";color:#0000ff;font-weight:400}.c10{padding-top:0pt;padding-bottom:0pt;line-height:1.0;text-align:left}.c23{padding-top:0pt;padding-bottom:0pt;line-height:1.0;text-align:center}.c5{font-size:12pt;font-family:"Times New Roman";color:#000000;font-weight:400}.c21{padding-top:0pt;padding-bottom:0pt;line-height:1.15;text-align:justify}.c8{font-size:12pt;font-family:"Calibri";color:#000000;font-weight:400}.c4{padding-top:0pt;padding-bottom:0pt;line-height:1.0;text-align:justify}.c16{font-size:12pt;font-family:"Times New Roman";font-weight:400}.c6{font-size:11pt;font-family:"Calibri";font-weight:400}.c17{max-width:468pt;padding:72pt 72pt 72pt 72pt}.c13{color:inherit;text-decoration:inherit}.c14{text-decoration:underline}.c19{height:11pt}.c9{background-color:#ffffff}.c7{margin-left:36pt}.c15{margin-left:22.5pt}.title{padding-top:24pt;color:#000000;font-weight:700;font-size:36pt;padding-bottom:6pt;font-family:"Arial";line-height:1.0;page-break-after:avoid;text-align:left}.subtitle{padding-top:18pt;color:#666666;font-size:24pt;padding-bottom:4pt;font-family:"Georgia";line-height:1.0;page-break-after:avoid;font-style:italic;text-align:left}li{color:#000000;font-size:11pt;font-family:"Arial"}p{margin:0;color:#000000;font-size:11pt;font-family:"Arial"}h1{padding-top:12pt;color:#000000;font-weight:700;font-size:24pt;padding-bottom:12pt;font-family:"Arial";line-height:1.0;text-align:left}h2{padding-top:11.2pt;color:#000000;font-weight:700;font-size:18pt;padding-bottom:11.2pt;font-family:"Arial";line-height:1.0;text-align:left}h3{padding-top:12pt;color:#000000;font-weight:700;font-size:14pt;padding-bottom:12pt;font-family:"Arial";line-height:1.0;text-align:left}h4{padding-top:12.8pt;color:#000000;font-weight:700;font-size:12pt;padding-bottom:12.8pt;font-family:"Arial";line-height:1.0;text-align:left}h5{padding-top:12.8pt;color:#000000;font-weight:700;font-size:9pt;padding-bottom:12.8pt;font-family:"Arial";line-height:1.0;text-align:left}h6{padding-top:18pt;color:#000000;font-weight:700;font-size:8pt;padding-bottom:18pt;font-family:"Arial";line-height:1.0;text-align:left}
</style>
  <div class="c9 c17">
    <p class="c10 c19"><span class="c22"></span></p>
    <p class="c23"><span class="c20">TERMS OF SERVICE AGREEMENT</span></p>
    <p class="c4"><span class="c0 c9">&nbsp;</span></p>
    <p class="c4"><span class="c9 c18">PLEASE READ THE FOLLOWING TERMS OF SERVICE AGREEMENT CAREFULLY. BY ACCESSING OR USING OUR SITES AND OUR SERVICES, YOU HEREBY AGREE TO BE BOUND BY THE TERMS AND ALL TERMS INCORPORATED HEREIN BY REFERENCE. IT IS THE RESPONSIBILITY OF YOU, THE USER, CUSTOMER, OR PROSPECTIVE CUSTOMER TO READ THE TERMS AND CONDITIONS BEFORE PROCEEDING TO USE THIS SITE. IF YOU DO NOT EXPRESSLY AGREE TO ALL OF THE TERMS AND CONDITIONS, THEN PLEASE DO NOT ACCESS OR USE OUR SITES OR OUR SERVICES. THIS TERMS OF SERVICE AGREEMENT IS EFFECTIVE AS OF 08/10/2017.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">ACCEPTANCE OF TERMS</span></p>
    <p class="c4"><span class="c2">&nbsp;</span></p>
    <p class="c4"><span class="c0">The following Terms of Service Agreement (the &quot;TOS&quot;) is a legally binding agreement that shall govern the relationship with our users and others which may interact or interface with Tipestry Inc., also known as Tipestry, located at 940 Stewart Drive #203, Sunnyvale, California 94085 and our subsidiaries and affiliates, in association with the use of the Tipestry website, which includes www.tipestry.com, (the &quot;Site&quot;) and its Services, which shall be defined below.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">DESCRIPTION OF WEBSITE SERVICES OFFERED</span></p>
    <p class="c4"><span class="c2">&nbsp;</span></p>
    <p class="c4"><span class="c0">The Site is an online discussion website which has the following description:</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">Comment on any website and tip people with cryptocurrency.</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">Any and all visitors to our site, despite whether they are registered or not, shall be deemed as &quot;users&quot; of the herein contained Services provided for the purpose of this TOS. Once an individual register&#39;s for our Services, through the process of creating an account, the user shall then be considered a &quot;member.&quot;</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c5">The user and/or member acknowledges and agrees that the Services provided and made available through our website and applications, which may include some mobile applications and that those applications may be made available on various social media networking sites and numerous other platforms and downloadable programs, are the sole property of </span><span class="c16">Tipestry Inc.</span><span class="c5">. </span><span class="c0">At its discretion, Tipestry Inc. may offer additional website Services and/or products, or update, modify or revise any current content and Services, and this Agreement shall apply to any and all additional Services and/or products and any and all updated, modified or revised Services unless otherwise stipulated. Tipestry Inc. does hereby reserve the right to cancel and cease offering any of the aforementioned Services and/or products. You, as the end user and/or member, acknowledge, accept and agree that Tipestry Inc. shall not be held liable for any such updates, modifications, revisions, suspensions or discontinuance of any of our Services and/or products. Your continued use of the Services provided, after such posting of any updates, changes, and/or modifications shall constitute your acceptance of such updates, changes and/or modifications, and as such, frequent review of this Agreement and any and all applicable terms and policies should be made by you to ensure you are aware of all terms and policies currently in effect. Should you not agree to the updated, revised or modified terms, you must stop using the provided Services forthwith.</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">Furthermore, the user and/or member understands, acknowledges and agrees that the Services offered shall be provided &quot;AS IS&quot; and as such Tipestry Inc. shall not assume any responsibility or obligation for the timeliness, missed delivery, deletion and/or any failure to store user content, communication or personalization settings.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">REGISTRATION</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">To register and become a &quot;member&quot; of the Site, you must be at least 18 years of age to enter into and form a legally binding contract. In addition, you must be in good standing and not an individual that has been previously barred from receiving Tipestry&#39;s Services under the laws and statutes of the United States or other applicable jurisdiction.</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">When you register, Tipestry may collect information such as your name, e-mail address, birth date, gender, mailing address, occupation, industry and personal interests. You can edit your account information at any time. Once you register with Tipestry and sign in to our Services, you are no longer anonymous to us.</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">Furthermore, the registering party hereby acknowledges, understands and agrees to:</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">a)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">furnish factual, correct, current and complete information with regards to yourself as may be requested by the data registration process, and</span></p>
    <p class="c10 c7"><span class="c1">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">b)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">maintain and promptly update your registration and profile information in an effort to maintain accuracy and completeness at all times.</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">If anyone knowingly provides any information of a false, untrue, inaccurate or incomplete nature, Tipestry Inc. will have sufficient grounds and rights to suspend or terminate the member in violation of this aspect of the Agreement, and as such refuse any and all current or future use of Tipestry Inc. Services, or any portion thereof.</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">It is Tipestry Inc.&#39;s priority to ensure the safety and privacy of all its visitors, users and members, especially that of children. Therefore, it is for this reason that the parents of any child under the age of 13 that permit their child or children access to the Tipestry website platform Services must create a &quot;family&quot; account, which will certify that the individual creating the &quot;family&quot; account is of 18 years of age and as such, the parent or legal guardian of any child or children registered under the &quot;family&quot; account. As the creator of the &quot;family&quot; account, s/he is thereby granting permission for his/her child or children to access the various Services provided, including, but not limited to, message boards, email, and/or instant messaging. It is the parent&#39;s and/or legal guardian&#39;s responsibility to determine whether any of the Services and/or content provided are age-appropriate for his/her child.</span></p>
    <p class="c4 par_unl"><span class="c2">&nbsp;</span></p>
    <p class="c4"><span class="c2">MEMBER ACCOUNT, USERNAME, PASSWORD AND SECURITY</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">When you set up an account, you are the sole authorized user of your account. You shall be responsible for maintaining the secrecy and confidentiality of your password and for all activities that transpire on or within your account. It is your responsibility for any act or omission of any user(s) that access your account information that, if undertaken by you, would be deemed a violation of the TOS. It shall be your responsibility to notify Tipestry Inc. immediately if you notice any unauthorized access or use of your account or password or any other breach of security. Tipestry Inc. shall not be held liable for any loss and/or damage arising from any failure to comply with this term and/or condition of the TOS.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">RISK OF LOSING CRYPTOCURRENCY</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c5">Tipestry is currently in beta testing and therefore we can take no responsibility for any loss of coins tipped, held, sent or received via the processes integrated into our web platform. Send and hold cryptocurrencies here at your own risk. Further, holding cryptocurrency in general, especially through a web service, involves substantial risk of loss both from malicious actors and technical failure. As a user or member of the Site, you herein acknowledge, understand and agree that cryptocurrency used on Tipestry can be lost at any time</span><span class="c0">&nbsp;and that Tipestry is under no obligation to either compensate or provide any form of reimbursement in any manner or nature.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">CONDUCT</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">As a user or member of the Site, you herein acknowledge, understand and agree that all information, text, software, data, photographs, music, video, messages, tags or any other content, whether it is publicly or privately posted and/or transmitted, is the expressed sole responsibility of the individual from whom the content originated. In short, this means that you are solely responsible for any and all content posted, uploaded, emailed, transmitted or otherwise made available by way of the Tipestry Services, and as such, we do not guarantee the accuracy, integrity or quality of such content. It is expressly understood that by use of our Services, you may be exposed to content including, but not limited to, any errors or omissions in any content posted, and/or any loss or damage of any kind incurred as a result of the use of any content posted, emailed, transmitted or otherwise made available by Tipestry.</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">Furthermore, you herein agree not to make use of Tipestry Inc.&#39;s Services for the purpose of:</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">a)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">uploading, posting, emailing, transmitting, or otherwise making available any content that shall be deemed unlawful, harmful, threatening, abusive, harassing, tortious, defamatory, vulgar, obscene, libelous, or invasive of another&#39;s privacy or which is hateful, and/or racially, ethnically, or otherwise objectionable;</span></p>
    <p class="c10 c7"><span class="c1">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">b)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">causing harm to minors in any manner whatsoever;</span></p>
    <p class="c12 c7"><span class="c1">&nbsp;</span></p>
    <p class="c7 c10"><span class="c6">c)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">impersonating any individual or entity, including, but not limited to, any Tipestry officials, forum leaders, guides or hosts or falsely stating or otherwise misrepresenting any affiliation with an individual or entity;</span></p>
    <p class="c10"><span class="c0">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">d)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">forging captions, headings or titles or otherwise offering any content that you personally have no right to pursuant to any law nor having any contractual or fiduciary relationship with;</span></p>
    <p class="c10 c7"><span class="c1">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">e)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">uploading, posting, emailing, transmitting or otherwise offering any such content that may infringe upon any patent, copyright, trademark, or any other proprietary or intellectual rights of any other party;</span></p>
    <p class="c12 c7"><span class="c1">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">f)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">uploading, posting, emailing, transmitting or otherwise offering any content that you do not personally have any right to offer pursuant to any law or in accordance with any contractual or fiduciary relationship;</span></p>
    <p class="c10"><span class="c0">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">g)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">uploading, posting, emailing, transmitting, or otherwise offering any unsolicited or unauthorized advertising, promotional flyers, &quot;junk mail,&quot; &quot;spam,&quot; or any other form of solicitation, except in any such areas that may have been designated for such purpose;</span></p>
    <p class="c10"><span class="c0">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">h)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">uploading, posting, emailing, transmitting, or otherwise offering any source that may contain a software virus or other computer code, any files and/or programs which have been designed to interfere, destroy and/or limit the operation of any computer software, hardware, or telecommunication equipment;</span></p>
    <p class="c12 c7"><span class="c1">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">i)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">disrupting the normal flow of communication, or otherwise acting in any manner that would negatively affect other users&#39; ability to participate in any real time interactions;</span></p>
    <p class="c10"><span class="c0">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">j)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">interfering with or disrupting any Tipestry Inc. Services, servers and/or networks that may be connected or related to our website, including, but not limited to, the use of any device software and/or routine to bypass the robot exclusion headers;</span></p>
    <p class="c12 c7"><span class="c1">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">k)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">intentionally or unintentionally violating any local, state, federal, national or international law, including, but not limited to, rules, guidelines, and/or regulations decreed by the U.S. Securities and Exchange Commission, in addition to any rules of any nation or other securities exchange, that would include without limitation, the New York Stock Exchange, the American Stock Exchange, or the NASDAQ, and any regulations having the force of law;</span></p>
    <p class="c10"><span class="c0">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">l)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">providing informational support or resources, concealing and/or disguising the character, location, and or source to any organization delegated by the United States government as a &quot;foreign terrorist organization&quot; in accordance to Section 219 of the Immigration Nationality Act;</span></p>
    <p class="c10"><span class="c0">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">m)&nbsp;&nbsp;&nbsp; </span><span class="c0">&quot;stalking&quot; or with the intent to otherwise harass another individual; and/or</span></p>
    <p class="c10"><span class="c0">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">n)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">collecting or storing of any personal data relating to any other member or user in connection with the prohibited conduct and/or activities which have been set forth in the aforementioned paragraphs.</span></p>
    <p class="c4 c7"><span class="c1">&nbsp;</span></p>
    <p class="c4"><span class="c5">Tipestry Inc. herein reserves the right to pre-screen, refuse and/or delete any content currently available through our Services. In addition, we reserve the right to remove and/or delete any such content that would violate the TOS </span><span class="c0">or which would otherwise be considered offensive to other visitors, users and/or members.</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">Tipestry Inc. herein reserves the right to access, preserve and/or disclose member account information and/or content if it is requested to do so by law or in good faith belief that any such action is deemed reasonably necessary for:</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4 c7"><span class="c6">a)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">compliance with any legal process;</span></p>
    <p class="c4 c7"><span class="c1">&nbsp;</span></p>
    <p class="c4 c7"><span class="c6">b)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">enforcement of the TOS;</span></p>
    <p class="c4 c7"><span class="c1">&nbsp;</span></p>
    <p class="c4 c7"><span class="c6">c)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">responding to any claim that therein contained content is in violation of the rights of any third party;</span></p>
    <p class="c4 c7"><span class="c1">&nbsp;</span></p>
    <p class="c4 c7"><span class="c6">d)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">responding to requests for customer service; or</span></p>
    <p class="c7 c21"><span class="c1">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">e)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">protecting the rights, property or the personal safety of Tipestry Inc., its visitors, users and members, including the general public.</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">Tipestry Inc. herein reserves the right to include the use of security components that may permit digital information or material to be protected, and that such use of information and/or material is subject to usage guidelines and regulations established by Tipestry Inc. or any other content providers supplying content services to Tipestry Inc.. You are hereby prohibited from making any attempt to override or circumvent any of the embedded usage rules in our Services. Furthermore, unauthorized reproduction, publication, distribution, or exhibition of any information or materials supplied by our Services, despite whether done so in whole or in part, is expressly prohibited.</span></p>
    <p class="c4 par_un"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">INTERSTATE COMMUNICATION</span></p>
    <p class="c4"><span class="c2">&nbsp;</span></p>
    <p class="c4"><span class="c16">Upon registration, you hereby acknowledge that by using www.tipestry.com to send electronic communications, which would include, but are not limited to, email, searches, instant messages, uploading of files, photos and/or videos, you will be causing communications to be sent through our computer network. </span><span class="c0">Therefore, through your use, and thus your agreement with this TOS, you are acknowledging that the use of this Service shall result in interstate transmissions.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c10"><span class="c2">CAUTIONS FOR GLOBAL USE AND EXPORT AND IMPORT COMPLIANCE</span></p>
    <p class="c4"><span class="c2">&nbsp;</span></p>
    <p class="c4"><span class="c5">Due to the global nature of the internet, through the use of our network you hereby agree to comply with all local rules relating to online conduct and that which is considered acceptable Content. Uploading, posting and/or transferring of software, technology and other technical data may be subject to the export and import laws of the United States and possibly other countries. Through the use of our network, you thus agree to comply with all applicable export and import laws, statutes and regulations, including, but not limited to, the Export Administration Regulations (</span><span class="c5 c14"><a class="c13" href="https://www.google.com/url?q=http://www.access.gpo.gov/bis/ear/ear_data.html&amp;sa=D&amp;ust=1503325165777000&amp;usg=AFQjCNGW5IMTTtWkJW3gUctipoEnRPo7DQ">http://www.access.gpo.gov/bis/ear/ear_data.html</a></span><span class="c5">), as well as the sanctions control program of the United States (</span><span class="c5 c14"><a class="c13" href="https://www.google.com/url?q=http://www.treasury.gov/resource-center/sanctions/Programs/Pages/Programs.aspx&amp;sa=D&amp;ust=1503325165777000&amp;usg=AFQjCNEl0iAFTCTONBDBHnvYsMq-_aZ8Cg">http://www.treasury.gov/resource-center/sanctions/Programs/Pages/Programs.aspx</a></span><span class="c0">). Furthermore, you state and pledge that you:</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">a)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c5">are not on the list of prohibited individuals which may be identified on any government export exclusion report (</span><span class="c14 c24"><a class="c13" href="https://www.google.com/url?q=http://www.bis.doc.gov/complianceandenforcement/liststocheck.htm&amp;sa=D&amp;ust=1503325165778000&amp;usg=AFQjCNGjdt3DZmUosCMEpQIYYJ4PWZBP0Q">http://www.bis.doc.gov/complianceandenforcement/liststocheck.htm</a></span><span class="c0">) nor a member of any other government which may be part of an export-prohibited country identified in applicable export and import laws and regulations;</span></p>
    <p class="c10 c7"><span class="c1">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">b)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">agree not to transfer any software, technology or any other technical data through the use of our network Services to any export-prohibited country;</span></p>
    <p class="c10 c7"><span class="c1">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">c)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">agree not to use our website network Services for any military, nuclear, missile, chemical or biological weaponry end uses that would be a violation of the U.S. export laws; and</span></p>
    <p class="c10 c7"><span class="c1">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">d)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">agree not to post, transfer nor upload any software, technology or any other technical data which would be in violation of the U.S. or other applicable export and/or import laws.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c10"><span class="c2">CONTENT PLACED OR MADE AVAILABLE FOR COMPANY SERVICES</span></p>
    <p class="c4"><span class="c2">&nbsp;</span></p>
    <p class="c4"><span class="c5">Tipestry Inc. shall not lay claim to ownership of any content submitted by any visitor, member,</span><span class="c16">&nbsp;</span><span class="c0">or user, nor make such content available for inclusion on our website Services. Therefore, you hereby grant and allow for Tipestry Inc. the below listed worldwide, royalty-free and non-exclusive licenses, as applicable:</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">a)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">The content submitted or made available for inclusion on the publicly accessible areas of Tipestry Inc.&#39;s sites, the license provided to permit to use, distribute, reproduce, modify, adapt, publicly perform and/or publicly display said Content on our network Services is for the sole purpose of providing and promoting the specific area to which this content was placed and/or made available for viewing. This license shall be available so long as you are a member of Tipestry Inc.&#39;s sites, and shall terminate at such time when you elect to discontinue your membership.</span></p>
    <p class="c10 c7"><span class="c1">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">b)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">Photos, audio, video and/or graphics submitted or made available for inclusion on the publicly accessible areas of Tipestry Inc.&#39;s sites, the license provided to permit to use, distribute, reproduce, modify, adapt, publicly perform and/or publicly display said Content on our network Services are for the sole purpose of providing and promoting the specific area in which this content was placed and/or made available for viewing. This license shall be available so long as you are a member of Tipestry Inc.&#39;s sites and shall terminate at such time when you elect to discontinue your membership.</span></p>
    <p class="c10"><span class="c0">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">c)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">For any other content submitted or made available for inclusion on the publicly accessible areas of Tipestry Inc.&#39;s sites, the continuous, binding and completely sub-licensable license which is meant to permit to use, distribute, reproduce, modify, adapt, publish, translate, publicly perform and/or publicly display said content, whether in whole or in part, and the incorporation of any such Content into other works in any arrangement or medium current used or later developed.</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c5">Those areas which may be deemed &quot;publicly accessible&quot; areas of Tipestry Inc.&#39;s sites are those such areas of our network properties which are meant to be available to the general public, and which would include message boards and groups that are openly available to both </span><span class="c16">users</span><span class="c0">&nbsp;and members. However, those areas which are not open to the public, and thus available to members only, would include our mail system and instant messaging.</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">CONTRIBUTIONS TO COMPANY WEBSITE</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">Tipestry Inc. provides an area for our users and members to contribute feedback to our website. When you submit ideas, documents, suggestions and/or proposals (&quot;Contributions&quot;) to our site, you acknowledge and agree that:</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">a)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">your contributions do not contain any type of confidential or proprietary information;</span></p>
    <p class="c10 c7"><span class="c1">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">b)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c8">Tipestry</span><span class="c0">&nbsp;shall not be liable or under any obligation to ensure or maintain confidentiality, expressed or implied, related to any Contributions;</span></p>
    <p class="c10 c7"><span class="c1">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">c)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c8">Tipestry</span><span class="c0">&nbsp;shall be entitled to make use of and/or disclose any such Contributions in any such manner as they may see fit;</span></p>
    <p class="c10 c7"><span class="c1">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">d)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">the contributor&#39;s Contributions shall automatically become the sole property of Tipestry; and</span></p>
    <p class="c10 c7"><span class="c1">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">e)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c8">Tipestry</span><span class="c0">&nbsp;is under no obligation to either compensate or provide any form of reimbursement in any manner or nature.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">INDEMNITY</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c5">All users</span><span class="c16">&nbsp;</span><span class="c5">and/or members</span><span class="c16">&nbsp;</span><span class="c5">herein agree to insure and hold Tipestry Inc., our subsidiaries, affiliates, agents, employees, officers, partners and/or licensors blameless or</span><span class="c16">&nbsp;not liable </span><span class="c5">for any claim or demand, which may include, but is not limited to, reasonable attorney fees made by any third party which may arise from any content a member or</span><span class="c16">&nbsp;</span><span class="c0">user of our site may submit, post, modify, transmit or otherwise make available through our Services, the use of<!--&#65533;--> Tipestry Services or your connection with these Services, your violations of the Terms of Service and/or your violation of any such rights of another person.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">COMMERCIAL REUSE OF SERVICES</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c5">The member or</span><span class="c16">&nbsp;</span><span class="c0">user herein agrees not to replicate, duplicate, copy, trade, sell, resell nor exploit for any commercial reason any part, use of, or access to Tipestry&#39;s sites.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">USE AND STORAGE GENERAL PRACTICES</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">You herein acknowledge that Tipestry Inc. may set up any such practices and/or limits regarding the use of our Services, without limitation of the maximum number of days that any email, message posting or any other uploaded content shall be retained by Tipestry Inc., nor the maximum number of email messages that may be sent and/or received by any member, the maximum volume or size of any email message that may be sent from or may be received by an account on our Service, the maximum disk space allowable that shall be allocated on Tipestry Inc.&#39;s servers on the member&#39;s behalf, and/or the maximum number of times and/or duration that any member may access our Services in a given period of time.<!--&#65533;--> In addition, you also agree that Tipestry Inc. has absolutely no responsibility or liability for the removal or failure to maintain storage of any messages and/or other communications or content maintained or transmitted by our Services. You also herein acknowledge that we reserve the right to delete or remove any account that is no longer active for an extended period of time. Furthermore, Tipestry Inc. shall reserve the right to modify, alter and/or update these general practices and limits at our discretion.</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">Any messenger service, which may include any web-based versions, shall allow you and the individuals with whom you communicate with the ability to save your conversations in your account located on Tipestry Inc.&#39;s servers. In this manner, you will be able to access and search your message history from any computer with internet access. You also acknowledge that others have the option to use and save conversations with you in their own personal account on www.tipestry.com. It is your agreement to this TOS which establishes your consent to allow Tipestry Inc. to store any and all communications on its servers.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">MODIFICATIONS</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">Tipestry Inc. shall reserve the right at any time it may deem fit, to modify, alter and or discontinue, whether temporarily or permanently, our service, or any part thereof, with or without prior notice. In addition, we shall not be held liable to you or to any third party for any such alteration, modification, suspension and/or discontinuance of our Services, or any part thereof.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">TERMINATION</span></p>
    <p class="c10"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">As a member of www.tipestry.com, you may cancel or terminate your account, associated email address and/or access to our Services by submitting a cancellation or termination request to feedback@tipestry.com.</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">As a member, you agree that Tipestry Inc. may, without any prior written notice, immediately suspend, terminate, discontinue and/or limit your account, any email associated with your account, and access to any of our Services. The cause for such termination, discontinuance, suspension and/or limitation of access shall include, but is not limited to:</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4 c7"><span class="c6">a)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">any breach or violation of our TOS or any other incorporated agreement, regulation and/or guideline;</span></p>
    <p class="c3"><span class="c0">&nbsp;</span></p>
    <p class="c4 c7"><span class="c6">b)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">by way of requests from law enforcement or any other governmental agencies;</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4 c7"><span class="c6">c)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">the discontinuance, alteration and/or material modification to our Services, or any part thereof;</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4 c7"><span class="c6">d)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">unexpected technical or security issues and/or problems;</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4 c7"><span class="c6">e)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">any extended periods of inactivity;</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4 c7"><span class="c6">f)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">any engagement by you in any fraudulent or illegal activities; and/or</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">g)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">the nonpayment of any associated fees that may be owed by you in connection with your www.tipestry.com account Services.</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">Furthermore, you herein agree that any and all terminations, suspensions, discontinuances, and or limitations of access for cause shall be made at our sole discretion and that we shall not be liable to you or any other third party with regards to the termination of your account, associated email address and/or access to any of our Services.</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">The termination of your account with www.tipestry.com shall include any and/or all of the following:</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4 c7"><span class="c6">a)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">the removal of any access to all or part of the Services offered within www.tipestry.com;</span></p>
    <p class="c4 c7"><span class="c1">&nbsp;</span></p>
    <p class="c4 c7"><span class="c6">b)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">the deletion of your password and any and all related information, files, and any such content that may be associated with or inside your account, or any part thereof; and</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4 c7"><span class="c6">c)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">the barring of any further use of all or part of our Services.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">ADVERTISERS</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">Any correspondence or business dealings with, or the participation in any promotions of, advertisers located on or through our Services, which may include the payment and/or delivery of such related goods and/or Services, and any such other term, condition, warranty and/or representation associated with such dealings, are and shall be solely between you and any such advertiser. Moreover, you herein agree that Tipestry Inc. shall not be held responsible or liable for any loss or damage of any nature or manner incurred as a direct result of any such dealings or as a result of the presence of such advertisers on our website.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">LINKS</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">Either Tipestry Inc. or any third parties may provide links to other websites and/or resources. Thus, you acknowledge and agree that we are not responsible for the availability of any such external sites or resources, and as such, we do not endorse nor are we responsible or liable for any content, products, advertising or any other materials, on or available from such third party sites or resources. Furthermore, you acknowledge and agree that Tipestry Inc. shall not be responsible or liable, directly or indirectly, for any such damage or loss which may be a result of, caused or allegedly to be caused by or in connection with the use of or the reliance on any such content, goods or Services made available on or through any such site or resource.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">PROPRIETARY RIGHTS</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">You do hereby acknowledge and agree that Tipestry Inc.&#39;s Services and any essential software that may be used in connection with our Services (&quot;Software&quot;) shall contain proprietary and confidential material that is protected by applicable intellectual property rights and other laws. Furthermore, you herein acknowledge and agree that any Content which may be contained in any advertisements or information presented by and through our Services or by advertisers is protected by copyrights, trademarks, patents or other proprietary rights and laws. Therefore, except for that which is expressly permitted by applicable law or as authorized by Tipestry Inc. or such applicable licensor, you agree not to alter, modify, lease, rent, loan, sell, distribute, transmit, broadcast, publicly perform and/or created any plagiaristic works which are based on Tipestry Inc. Services (e.g. Content or Software), in whole or part.</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">Tipestry Inc. herein has granted you personal, non-transferable and non-exclusive rights and/or license to make use of the object code or our Software on a single computer, as long as you do not, and shall not, allow any third party to duplicate, alter, modify, create or plagiarize work from, reverse engineer, reverse assemble or otherwise make an attempt to locate or discern any source code, sell, assign, sublicense, grant a security interest in and/or otherwise transfer any such right in the Software. Furthermore, you do herein agree not to alter or change the Software in any manner, nature or form, and as such, not to use any modified versions of the Software, including and without limitation, for the purpose of obtaining unauthorized access to our Services. Lastly, you also agree not to access or attempt to access our Services through any means other than through the interface which is provided by Tipestry Inc. for use in accessing our Services.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">WARRANTY DISCLAIMERS</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">YOU HEREIN EXPRESSLY ACKNOWLEDGE AND AGREE THAT:</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">a)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span> <span class="c0">THE USE OF TIPESTRY INC. SERVICES AND SOFTWARE ARE AT THE SOLE RISK BY YOU. OUR SERVICES AND SOFTWARE SHALL BE PROVIDED ON AN &quot;AS IS&quot; AND/OR &quot;AS AVAILABLE&quot; BASIS. TIPESTRY INC. AND OUR SUBSIDIARIES, AFFILIATES, OFFICERS, EMPLOYEES, AGENTS, PARTNERS AND LICENSORS EXPRESSLY DISCLAIM ANY AND ALL WARRANTIES OF ANY KIND WHETHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO ANY IMPLIED WARRANTIES OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.</span></p>
    <p class="c10 c7"><span class="c1">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">b)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c8">TIPESTRY INC.</span><span class="c0">&nbsp;AND OUR SUBSIDIARIES, OFFICERS, EMPLOYEES, AGENTS, PARTNERS AND LICENSORS MAKE NO SUCH WARRANTIES THAT (i) TIPESTRY INC. SERVICES OR SOFTWARE WILL MEET YOUR REQUIREMENTS; (ii) TIPESTRY INC. SERVICES OR SOFTWARE SHALL BE UNINTERRUPTED, TIMELY, SECURE OR ERROR-FREE; (iii) THAT SUCH RESULTS WHICH MAY BE OBTAINED FROM THE USE OF THE TIPESTRY INC. SERVICES OR SOFTWARE WILL BE ACCURATE OR RELIABLE; (iv) QUALITY OF ANY PRODUCTS, SERVICES, ANY INFORMATION OR OTHER MATERIAL WHICH MAY BE PURCHASED OR OBTAINED BY YOU THROUGH OUR SERVICES OR SOFTWARE WILL MEET YOUR EXPECTATIONS; AND (v) THAT ANY SUCH ERRORS CONTAINED IN THE SOFTWARE SHALL BE CORRECTED.</span></p>
    <p class="c10"><span class="c0">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">c)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">ANY INFORMATION OR MATERIAL DOWNLOADED OR OTHERWISE OBTAINED BY WAY OF TIPESTRY INC. SERVICES OR SOFTWARE SHALL BE ACCESSED BY YOUR SOLE DISCRETION AND SOLE RISK, AND AS SUCH YOU SHALL BE SOLELY RESPONSIBLE FOR AND HEREBY WAIVE ANY AND ALL CLAIMS AND CAUSES OF ACTION WITH RESPECT TO ANY DAMAGE TO YOUR COMPUTER AND/OR INTERNET ACCESS, DOWNLOADING AND/OR DISPLAYING, OR FOR ANY LOSS OF DATA THAT COULD RESULT FROM THE DOWNLOAD OF ANY SUCH INFORMATION OR MATERIAL.</span></p>
    <p class="c12 c7"><span class="c1">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">d)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">NO ADVICE AND/OR INFORMATION, DESPITE WHETHER WRITTEN OR ORAL, THAT MAY BE OBTAINED BY YOU FROM TIPESTRY INC. OR BY WAY OF OR FROM OUR SERVICES OR SOFTWARE SHALL CREATE ANY WARRANTY NOT EXPRESSLY STATED IN THE TOS.</span></p>
    <p class="c7 c12"><span class="c1">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">e)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">A SMALL PERCENTAGE OF SOME USERS MAY EXPERIENCE SOME DEGREE OF EPILEPTIC SEIZURE WHEN EXPOSED TO CERTAIN LIGHT PATTERNS OR BACKGROUNDS THAT MAY BE CONTAINED ON A COMPUTER SCREEN OR WHILE USING OUR SERVICES. CERTAIN CONDITIONS MAY INDUCE A PREVIOUSLY UNKNOWN CONDITION OR UNDETECTED EPILEPTIC SYMPTOM IN USERS WHO HAVE SHOWN NO HISTORY OF ANY PRIOR SEIZURE OR EPILEPSY. SHOULD YOU, ANYONE YOU KNOW OR ANYONE IN YOUR FAMILY HAVE AN EPILEPTIC CONDITION, PLEASE CONSULT A PHYSICIAN IF YOU EXPERIENCE ANY OF THE FOLLOWING SYMPTOMS WHILE USING OUR SERVICES: DIZZINESS, ALTERED VISION, EYE OR MUSCLE TWITCHES, LOSS OF AWARENESS, DISORIENTATION, ANY INVOLUNTARY MOVEMENT, OR CONVULSIONS.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">LIMITATION OF LIABILITY</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">YOU EXPLICITLY ACKNOWLEDGE, UNDERSTAND AND AGREE THAT TIPESTRY INC. AND OUR SUBSIDIARIES, AFFILIATES, OFFICERS, EMPLOYEES, AGENTS, PARTNERS AND LICENSORS SHALL NOT BE LIABLE TO YOU FOR ANY PUNITIVE, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES, INCLUDING, BUT NOT LIMITED TO, DAMAGES WHICH MAY BE RELATED TO THE LOSS OF ANY PROFITS, GOODWILL, USE, DATA AND/OR OTHER INTANGIBLE LOSSES, EVEN THOUGH WE MAY HAVE BEEN ADVISED OF SUCH POSSIBILITY THAT SAID DAMAGES MAY OCCUR, AND RESULT FROM:</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">a)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">THE USE OR INABILITY TO USE OUR SERVICE;</span></p>
    <p class="c10 c7"><span class="c1">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">b)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">THE COST OF PROCURING SUBSTITUTE GOODS AND SERVICES;</span></p>
    <p class="c10"><span class="c0"><!--&#65533;--></span></p>
    <p class="c10 c7"><span class="c6">c)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">UNAUTHORIZED ACCESS TO OR THE ALTERATION OF YOUR TRANSMISSIONS AND/OR DATA;</span></p>
    <p class="c10"><span class="c0">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">d)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">STATEMENTS OR CONDUCT OF ANY SUCH THIRD PARTY ON OUR SERVICE;</span></p>
    <p class="c10"><span class="c0">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">e)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">AND ANY OTHER MATTER WHICH MAY BE RELATED TO OUR SERVICE.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">RELEASE</span></p>
    <p class="c4"><span class="c2">&nbsp;</span></p>
    <p class="c4"><span class="c0">In the event you have a dispute, you agree to release Tipestry Inc. (and its officers, directors, employees, agents, parent subsidiaries, affiliates, co-branders, partners and any other third parties) from claims, demands and damages (actual and consequential) of every kind and nature, known and unknown, suspected or unsuspected, disclosed and undisclosed, arising out of or in any way connected to such dispute.</span></p>
    <p class="c4 par_unl"><span class="c2">&nbsp;</span></p>
    <p class="c4"><span class="c2">SPECIAL ADMONITION RELATED TO FINANCIAL MATTERS</span></p>
    <p class="c4"><span class="c2">&nbsp;</span></p>
    <p class="c4"><span class="c0">Should you intend to create or to join any service, receive or request any such news, messages, alerts or other information from our Services concerning companies, stock quotes, investments or securities, please review the above Sections Warranty Disclaimers and Limitations of Liability again. In addition, for this particular type of information, the phrase &quot;Let the investor beware&quot; is appropriate. Tipestry Inc.&#39;s content is provided primarily for informational purposes, and no content that shall be provided or included in our Services is intended for trading or investing purposes. Tipestry Inc. and our licensors shall not be responsible or liable for the accuracy, usefulness or availability of any information transmitted and/or made available by way of our Services, and shall not be responsible or liable for any trading and/or investment decisions based on any such information.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">EXCLUSION AND LIMITATIONS</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">THERE ARE SOME JURISDICTIONS WHICH DO NOT ALLOW THE EXCLUSION OF CERTAIN WARRANTIES OR THE LIMITATION OF EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES. THEREFORE, SOME OF THE ABOVE LIMITATIONS OF SECTIONS WARRANTY DISCLAIMERS AND LIMITATION OF LIABILITY MAY NOT APPLY TO YOU.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">THIRD PARTY BENEFICIARIES</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">You herein acknowledge, understand and agree, unless otherwise expressly provided in this TOS, that there shall be no third-party beneficiaries to this agreement.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">NOTICE</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">Tipestry Inc. may furnish you with notices, including those with regards to any changes to the TOS, including but not limited to email, regular mail, MMS or SMS, text messaging, postings on our website Services, or other reasonable means currently known or any which may be herein after developed. Any such notices may not be received if you violate any aspects of the TOS by accessing our Services in an unauthorized manner. Your acceptance of this TOS constitutes your agreement that you are deemed to have received any and all notices that would have been delivered had you accessed our Services in an authorized manner.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">TRADEMARK INFORMATION</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">You herein acknowledge, understand and agree that all of the Tipestry Inc. trademarks, copyright, trade name, service marks, and other Tipestry Inc. logos and any brand features, and/or product and service names are trademarks and as such, are and shall remain the property of Tipestry Inc.. You herein agree not to display and/or use in any manner the Tipestry Inc. logo or marks without obtaining Tipestry Inc.&#39;s prior written consent.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c10 c15"><span class="c2">COPYRIGHT OR INTELLECTUAL PROPERTY INFRINGEMENT CLAIMS NOTICE</span></p>
    <p class="c10 c15"><span class="c2">&amp; PROCEDURES</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">Tipestry Inc. will always respect the intellectual property of others, and we ask that all of our users do the same. With regards to appropriate circumstances and at its sole discretion, Tipestry Inc. may disable and/or terminate the accounts of any user who violates our TOS and/or infringes the rights of others. If you feel that your work has been duplicated in such a way that would constitute copyright infringement, or if you believe your intellectual property rights have been otherwise violated, you should provide to us the following information:</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">a)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">The electronic or the physical signature of the individual that is authorized on behalf of the owner of the copyright or other intellectual property interest;</span></p>
    <p class="c10 c7"><span class="c1">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">b)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">A description of the copyrighted work or other intellectual property that you believe has been infringed upon;</span></p>
    <p class="c10"><span class="c0"><!--&#65533;--></span></p>
    <p class="c10 c7"><span class="c6">c)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">A description of the location of the site which you allege has been infringing upon your work;</span></p>
    <p class="c10"><span class="c0">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">d)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">Your physical address, telephone number, and email address;</span></p>
    <p class="c10 c7"><span class="c1">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">e)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">A statement, in which you state that the alleged and disputed use of your work is not authorized by the copyright owner, its agents or the law;</span></p>
    <p class="c10"><span class="c0">&nbsp;</span></p>
    <p class="c10 c7"><span class="c6">f)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="c0">And finally, a statement, made under penalty of perjury, that the aforementioned information in your notice is truthful and accurate, and that you are the copyright or intellectual property owner, representative or agent authorized to act on the copyright or intellectual property owner&#39;s behalf.</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">The Tipestry Inc. Agent for notice of claims of copyright or other intellectual property infringement can be contacted as follows:</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">Mailing Address:</span></p>
    <p class="c4"><span class="c0">Tipestry Inc.</span></p>
    <p class="c4"><span class="c0">Attn: Copyright Agent</span></p>
    <p class="c4"><span class="c0">940 Stewart Drive #203</span></p>
    <p class="c4"><span class="c0">Sunnyvale, California 94085</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">Telephone: (650) 605-3434</span></p>
    <p class="c4"><span class="c0">Email: <!--&#65533;&#65533;&#65533;&#65533;&#65533;--> feedback@tipestry.com</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">CLOSED CAPTIONING</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">BE IT KNOWN, that Tipestry Inc. complies with all applicable Federal Communications Commission rules and regulations regarding the closed captioning of video content. For more information, please visit our website at www.tipestry.com.</span></p>
    <p class="c4 par_unl"><span class="c2">&nbsp;</span></p>
    <p class="c4"><span class="c2">GENERAL INFORMATION</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c11">ENTIRE AGREEMENT</span></p>
    <p class="c4"><span class="c0">This TOS constitutes the entire agreement between you and Tipestry Inc. and shall govern the use of our Services, superseding any prior version of this TOS between you and us with respect to Tipestry Inc. Services. You may also be subject to additional terms and conditions that may apply when you use or purchase certain other Tipestry Inc. Services, affiliate Services, third-party content or third-party software.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c11">CHOICE OF LAW AND FORUM</span></p>
    <p class="c4"><span class="c5">It is at the mutual agreement of both you and Tipestry Inc. with regard to the TOS that the relationship between the parties shall be governed by the laws of the state of California without regard to its conflict of law provisions and that any and </span><a id="id.gjdgxs"></a><span class="c0">all claims, causes of action and/or disputes, arising out of or relating to the TOS, or the relationship between you and Tipestry Inc., shall be filed within the courts having jurisdiction within the County of Santa Clara, California or the U.S. District Court located in said state. You and Tipestry Inc. agree to submit to the jurisdiction of the courts as previously mentioned, and agree to waive any and all objections to the exercise of jurisdiction over the parties by such courts and to venue in such courts.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c11">WAIVER AND SEVERABILITY OF TERMS</span></p>
    <p class="c4"><span class="c0">At any time, should Tipestry Inc. fail to exercise or enforce any right or provision of the TOS, such failure shall not constitute a waiver of such right or provision. If any provision of this TOS is found by a court of competent jurisdiction to be invalid, the parties nevertheless agree that the court should endeavor to give effect to the parties&#39; intentions as reflected in the provision, and the other provisions of the TOS remain in full force and effect.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c11">NO RIGHT OF SURVIVORSHIP NON-TRANSFERABILITY</span></p>
    <p class="c4"><span class="c0">You acknowledge, understand and agree that your account is non-transferable and any rights to your ID and/or contents within your account shall terminate upon your death. Upon receipt of a copy of a death certificate, your account may be terminated and all contents therein permanently deleted.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c11">STATUTE OF LIMITATIONS</span></p>
    <p class="c4"><span class="c5">You acknowledge, understand and agree that regardless of any statute or law to the contrary, any claim or action arising out of or related to the use of our Services or the TOS must be filed within </span><span class="c16">1 </span><span class="c0">year(s) after said claim or cause of action arose or shall be forever barred.</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c2">VIOLATIONS</span></p>
    <p class="c4"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">Please report any and all violations of this TOS to Tipestry Inc. as follows:</span></p>
    <p class="c4 par_unl"><span class="c0">&nbsp;</span></p>
    <p class="c4"><span class="c0">Mailing Address:</span></p>
    <p class="c4"><span class="c0">Tipestry Inc.</span></p>
    <p class="c4"><span class="c0">940 Stewart Drive #203</span> , <span class="c0">Sunnyvale, California 94085</span></p>
   <!-- <p class="c4"><span class="c0">Sunnyvale, California 94085</span></p>-->
    <p class="c4"><span class="c0"><!--&#65533;&#65533;&#65533;&#65533;&#65533;&#65533;&#65533;&#65533;&#65533;&#65533;&#65533;--></span></p>
    <p class="c4"><span class="c0">Email: <!--&#65533;&#65533;&#65533;&#65533;&#65533;--> feedback@tipestry.com</span></p>
  </div>
</div>
@endsection