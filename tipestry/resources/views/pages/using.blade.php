@extends ('layouts.app')

@section ('content')
    <div id="using-page">
        <div class="container">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/KIJVHo9RmTI"></iframe>
            </div>

            <h3 class="using-title">What is Tipestry?</h3>

            <p>
                Tipestry is a place where people can discuss any webpage and tip each other with Bitcoin and other cryptocurrencies.
            </p>

            <h3 class="using-title">
                How does it work?
            </h3>

            <p>
                In the <b>URL</b> field on the top right of the screen, enter the address of the website you want to discuss and a comment section will load on the right side of the screen.
            </p>

            <p>
                You can also check out what people around the web are currently talking about on the <b>Trending</b> section of the Tipestry homepage.
            </p>

            <h3 class="using-title">
                What is tipping?
            </h3>

            <p>
                Tipping is a way to show other users you appreciate their post or comment. It’s a more powerful version of a like or an upvote, as it involves automatically sending cryptocurrency to the user whose content you’re tipping.
            </p>

            <p>
                To tip someone, click on the <b>Give Coins</b> button next to their post and choose the type of coin and the amount you want to tip. An icon showing the tip will then show up next to their post and they will receive the coins in their account.
            </p>

            <h3 class="using-title">
                What is cryptocurrency?
            </h3>

            <p>
                Cryptocurrency is peer-to-peer digital currency. It has many advantages over tradition money, one of which is that it is much easier to send and receive online.
            </p>

            <p>
                Bitcoin, the first cryptocurrency, was created by Satoshi Nakamoto in 2009. Since then, thousands of other coins and tokens with different features have been created. Right now Tipestry supports Bitcoin and Dogecoin, and we’re currently working on integrating Ethereum, Litecoin, and our own Tipestry token.
            </p>

            <h3 class="using-title">
                How can I add coins to my Tipestry account?
            </h3>

            <p>
                You can send Bitcoins, Dogecoins, etc. from an online exchange or wallet to your Tipestry addresses, which can be found in the <b>Currency</b> link on the homepage. However, if you are new to cryptocurrencies, we recommend starting off by simply posting content to Tipestry and earning coins through tips.
            </p>

            <h3 class="using-title">
                How can I keep my coins safe?
            </h3>

            <p>
                Online cryptocurrency wallets, including the ones that come with Tipestry accounts, are inherently unsecure. The best way to secure your coins is in an offline wallet. Learn more about wallets and security <a href="https://bitcoin.org/en/secure-your-wallet">here</a>.
            </p>

            <p>
                If you end up with a significant amount of coins in your Tipestry account (for example if you post something really amazing and someone tips you a full Bitcoin), it is highly recommended that you make a withdrawal to somewhere more secure. To withdrawal coins, click on the <b>Currencies</b> link on the homepage, select the currency, and choose the <b>Withdraw/Send</b> coins option.
            </p>

            <h3 class="using-title">
                What will the Tipestry token do?
            </h3>

            <p>
                The Tipestry token will a serve several purposes:
            </p>

            <ul>
                <li>
                    Holders of the token will have proportional voting rights on things like moderation policy and moderator elections.
                </li>

                <li>
                    Elected moderators can be compensated with tokens.
                </li>

                <li>
                    Premium services such as ad-free viewing can be purchased with tokens.
                </li>

                <li>
                    Advertising on the site can be purchased with tokens.
                </li>
            </ul>

            <h3 class="using-title">
                How can I send feedback?
            </h3>

            <p>
                Please send any comments, bug reports, abuse complaints, or questions to <a
                        href="mailto:feedback@tipestry.com"><b>feedback@tipestry.com</b></a>.
            </p>
        </div>
    </div>
@endsection