
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * We'll initialize an global object for all Vue's events.
 */
import Event from './Event';

window.Event = new Event;

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('give-coins', require('./components/GiveCoins.vue'));
Vue.component('gift-coins', require('./components/GiftCoins.vue'));
Vue.component('Alert', require('./components/Alert.vue'));

const app = new Vue({
    el: '#app'
});
