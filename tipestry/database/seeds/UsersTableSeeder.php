<?php

use Illuminate\Database\Seeder;

use App\Currencies\BtcCurrency;
use App\{User, BtcAddress};

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        User::create([
//            'name' => 'Test Admin',
//            'email' => 'admin@test.com',
//            'password' => bcrypt('password'),
//            'is_admin' => true
//        ]);

        User::create([
            'name' => 'Test User 1',
            'email' => 'user1@test.com',
            'password' => bcrypt('password')
        ]);

        User::create([
            'name' => 'Test User 2',
            'email' => 'user2@test.com',
            'password' => bcrypt('password')
        ]);
    }
}
