<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCntPerHourToAdbanner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adbanner', function($table) {
			$table->string('alocate_count_per_hour')->default('');
			$table->string('used_count_per_hour')->default('');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         $table->dropColumn('alocate_count_per_hour');
		 $table->dropColumn('used_count_per_hour');
    }
}
