<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Adbanner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adbanner', function (Blueprint $table) {
            $table->increments('id');
            $table->string('banner_content');
            $table->string('banner_file');
            $table->string('banner_from_date');
			$table->string('banner_to_date');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('status');  
            $table->string('banner_position');
			$table->string('paid_ammount');
			$table->string('payment_status');
			$table->string('payment_hashcode');			
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adbanner');
    }
}
