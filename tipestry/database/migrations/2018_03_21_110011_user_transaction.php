<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallet_transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('from_wallet_id');
			$table->integer('to_wallet_id');
			$table->integer('amount');
			$table->integer('status');
			$table->timestamps();
        });
		Schema::table('wallet_transaction', function($table) {
			$table->foreign('user_id')->references('id')->on('users');
		});	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('wallet_transaction');
    }
}
