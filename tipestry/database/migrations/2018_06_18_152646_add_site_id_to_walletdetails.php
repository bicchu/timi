<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSiteIdToWalletdetails extends Migration
{
    public function up()
    {
		Schema::table('walletdetails', function($table) {
			$table->integer('site_id')->default(0);
		});
    }

    public function down()
    {
		Schema::table('walletdetails', function($table) {
			$table->dropColumn('site_id');
		});
    }
}
