<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewcolumnToAdbanner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('adbanner', function($table) {
			$table->string('selected_impression')->default(0);
			$table->string('used_impression')->default(0);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('adbanner', function($table) {
			$table->dropColumn('selected_impression');
			$table->dropColumn('used_impression');
		});
    }
}
