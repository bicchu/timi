<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentToAdbanner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('adbanner', function($table) {
			$table->string('user_comment')->default('');
			$table->string('admin_comment')->default('');
		});
	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('adbanner', function($table) {
			$table->dropColumn('user_comment');
			$table->dropColumn('admin_comment');
		});
    }
}
