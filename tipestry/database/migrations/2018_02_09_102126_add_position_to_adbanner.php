<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPositionToAdbanner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('adbanner', function($table) {
            $table->string('banner_position')->default('');
			$table->string('paid_ammount')->default(0);
			$table->string('payment_status')->default(0);
			$table->string('payment_hashcode')->default('');	
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('adbanner', function($table) {
			$table->dropColumn('banner_position');
			$table->dropColumn('paid_ammount');
			$table->dropColumn('payment_status');
			$table->dropColumn('payment_hashcode');
		});
    }
}
