<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserTransactionTopictype extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('wallet_transaction', function($table) {
			$table->string('topic_type')->default('topic');
			$table->string('coin_type')->default('btc');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		$table->dropColumn('topic_type');
		$table->dropColumn('coin_type');
    }
}
