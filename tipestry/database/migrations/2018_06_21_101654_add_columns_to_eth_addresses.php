<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToEthAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('eth_addresses', function($table) {
			$table->double('ethapibalance')->default(0);
			$table->double('ethtotaldeposit')->default(0);
			$table->double('tipapibalance')->default(0);
			$table->double('tiptotaldeposit')->default(0);
			$table->double('xrtapibalance')->default(0);
			$table->double('xrttotaldeposit')->default(0);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('eth_addresses', function($table) {
			$table->dropColumn('ethapibalance');
			$table->dropColumn('ethtotaldeposit');
			$table->dropColumn('tipapibalance');
			$table->dropColumn('tiptotaldeposit');
			$table->dropColumn('xrtapibalance');
			$table->dropColumn('xrttotaldeposit');
		});
    }
}
