<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Addnewwalletdetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('walletdetails', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('userid');
			$table->string('transactiontype');
			$table->string('receive_wallet_id',100)->default(0);
			$table->string('walletaddressid')->default('');
			$table->string('to_walletaddressid')->default('');
			$table->string('transactionid',100)->default(0);
			$table->integer('site_id')->default(0);
			$table->double('network_fee', 8, 2)->default(0);
			$table->double('amount',8,2);
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('walletdetails');
    }
}
