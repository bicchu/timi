<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('AKIAJR6QPLX55JRMOO5A'),
        'secret' => env('AhFQOffk0Ri7lB7KmDQgbagvqGkxOiGk0m0q4/9jpl/e'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id'     => env('FB_ID'),
        'client_secret' => env('FB_SECRET'),
        'redirect'      => env('FB_REDIRECT')
    ],

    'google' => [
        'client_id'     => env('GOOGLE_ID'),
        'client_secret' => env('GOOGLE_SECRET'),
        'redirect'      => env('GOOGLE_REDIRECT')
    ],

    'blockio' => [
        'btc_api_key'       => env('BTC_API_KEY'),
        'doge_api_key'      => env('DOGE_API_KEY'),
        'pin'               => env('BLOCKIO_PIN'),
        'version'           => env('BLOCKIO_VERSION')
    ],

    'blockchain' => [
        'api_key'       => '38649d685c5e4fdc8908392f5dffd38e'
    ]
];
