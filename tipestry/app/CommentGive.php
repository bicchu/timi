<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentGive extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'currency', 'amount', 'user_id'
    ];

    public $casts = [
        'amount' => 'decimal'
    ];

    /**
     * Render the representation of activity
     *
     * @return mixed
     */
    public function render()
    {
        return sprintf('<img style="width: 36px;" src="%s" alt="%s"><b>%s</b>', asset('images/currencies/' . $this->currency . '.png'), $this->currency, $this->amount);
    }
}
