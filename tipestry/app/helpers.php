<?php
use DB;
use Auth;
if (! function_exists('copyrightYears')) {
    /**
     * The dynamic copyright years
     *
     * return @mixed
     */
    function copyrightYears($startYear = 2017) {
        if ($startYear >= date('Y')) {
            return $startYear;
        }

        return sprintf('%d - %d', $startYear, date('Y'));
    }
	
	function notificationcnt()
	{
		$cnt = 0;
		/*if(!empty(Auth::user()))
		{
			$sql = "SELECT * FROM 'notification' where status = 1 and user_id = ".Auth::user()->id;
			$res = DB::select($sql);
			foreach($res as $val){
				$cnt++;
			}
		}*/
		return $cnt;
	}
}
