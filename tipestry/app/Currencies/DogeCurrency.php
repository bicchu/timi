<?php

namespace App\Currencies;

use App\Transaction;
use BlockIo;
use App\Currencies\Currency as CurrencyInterface;

class DogeCurrency implements CurrencyInterface {

    protected $blockIo;

    /**
     * BtcCurrency constructor.
     */
    public function __construct()
    {
        $this->blockIo = new BlockIo(
            config('services.blockio.doge_api_key'),
            config('services.blockio.pin'),
            config('services.blockio.version')
        );
    }

    /**
     * Generate a new wallet address
     *
     * @return mixed
     */
    public function generateKey()
    {
        $result = $this->blockIo->get_new_address();

        return $result->data->address;
    }

    /**
     * Get the balance for the specified address
     *
     * @param $address
     * @return mixed
     */
    public function getBalance($addresses)
    {
        $result = $this->blockIo->get_address_balance(['addresses' => $addresses]);

        return $result->data->available_balance;
    }

    /**
     * Transfer the money from one address to another
     *
     * @param $amount
     * @param $fromAddress
     * @param $toAddress
     */
    public function transfer($amount, $fromAddresses, $toAddress)
    {
		return $this->blockIo->withdraw_from_addresses([
            'amounts' => $amount,
            'from_addresses' => $fromAddresses,
            'to_addresses' => $toAddress
        ]);
    }
	
	
	
    public function get_network_fee_estimate($amount, $to_addresses)
    {        		
		return $this->blockIo->get_network_fee_estimate([
            'amounts' => $amount,
            'to_addresses' => $to_addresses
        ]);
    }	
	
    public function get_address_balance($addresses)
    {        		
		return $this->blockIo->get_address_balance([
            'addresses' => $addresses
        ]);
    }	
	
}