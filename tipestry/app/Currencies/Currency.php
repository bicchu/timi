<?php

namespace App\Currencies;

interface Currency {
    public function generateKey();

    public function getBalance($address);

    public function transfer($amount, $fromAddress, $toAddress);
}