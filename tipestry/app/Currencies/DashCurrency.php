<?php

namespace App\Currencies;

use App\Currencies\Currency as CurrencyInterface;
use BlockCypher\Api\Address;
use BlockCypher\Api\TX;
use BlockCypher\Api\TXInput;
use BlockCypher\Api\TXOutput;
use BlockCypher\Api\Wallet;
use BlockCypher\Auth\SimpleTokenCredential;
use BlockCypher\Client\AddressClient;
use BlockCypher\Client\TXClient;
use BlockCypher\Client\WalletClient;
use BlockCypher\Rest\ApiContext;

class DashCurrency implements CurrencyInterface {

    protected $api;

    /**
     * DashCurrency constructor.
     */
    public function __construct()
    {
//        $this->api = new ApiContext(
//            new SimpleTokenCredential(config('services.blockchain.api_key'))
//        );
//
//        $this->api->setCoin('dash');
//
//        $this->api->setConfig([
//            'mode' => 'sandbox'
//        ]);
        $this->api = ApiContext::create(
            'main', 'dash', 'v1',
            new SimpleTokenCredential(config('services.blockchain.api_key')),
            ['mode' => 'sandbox']
        );

    }

    /**
     * Generate the initial wallet address
     *
     * @return mixed
     */
    public function generateKey()
    {
        // Generate New Address
        $addressClient = new \BlockCypher\Client\AddressClient($this->api);
        $addressKeyChain = $addressClient->generateAddress();

        // Generate New Wallet
        $wallet = new Wallet();

        $name = str_random(8);
        $wallet->setName($name);

        $wallet->setAddresses([
            $addressKeyChain->getAddress()
        ]);

        $walletClient = new WalletClient($this->api);
        $createdWallet = $walletClient->create($wallet);

        return ['address' => $addressKeyChain->getAddress(), 'name' => $createdWallet->getName()];
    }

    /**
     * Generate a new wallet address for an existing user
     *
     * @return mixed
     */
    public function generateNewKey($name)
    {
//        $result = $this->fetchFromApi('post', 'https://api.blockcypher.com/v1/dash/main/wallets/' . $name . '/addresses/generate?token=' . config('services.blockchain.api_key'));
//
//        return ['address' => $result->address, 'name' => $result->name];
        $walletClient = new WalletClient($this->api);

        $walletGenerateAddressResponse = $walletClient->generateAddress($name);

        return ['address' => $walletGenerateAddressResponse->getAddress(), 'name' => $walletGenerateAddressResponse->getName()];
    }

    /**
     * Get the balance for the specified address
     *
     * @param $address
     * @return mixed
     */
    public function getBalance($addresses)
    {
        $balance = 0;

        if (str_contains($addresses, ',')) {
            $addresses = explode(',', $addresses);

            foreach ($addresses as $address) {
                $addressClient = new AddressClient($this->api);
                $addressBalance = $addressClient->getBalance($address);

                $balance += $addressBalance->getFinalBalance();
            }
        } else {
            $addressClient = new AddressClient($this->api);
            $addressBalance = $addressClient->getBalance($addresses);

            $balance += $addressBalance->getFinalBalance();
        }

        return $balance;
    }

    /**
     * Transfer the money from one address to another
     *
     * @param $amount
     * @param $fromAddress
     * @param $toAddress
     */
    public function transfer($amount, $fromAddresses, $toAddress)
    {
//        $params = json_decode(
//            '{"inputs":[{"addresses": ["' . $fromAddresses . '"]}],"outputs":[{"addresses": ["' . $toAddress . '"], "value": ' . $amount .  '}]}'
//        );
//
//        return $this->fetchFromApi(
//            'post',
//            'https://api.blockcypher.com/v1/dash/test/txs/new',
//            ['json' => $params]
//        );

        // Create a new instance of Transaction object
        $tx = new TX();

        // Tx inputs
        $input = new TXInput();

        // Handle multiple addresses
        if (str_contains($fromAddresses, ',')) {
            $addresses = explode(',', $fromAddresses);

            foreach ($addresses as $address) {
                $input->addAddress($address);
            }
        } else {
            $input->addAddress($fromAddresses);
        }

        $tx->addInput($input);

        // Tx outputs
        $output = new TXOutput();
        $output->addAddress($toAddress);
        $tx->addOutput($output);

        // Tx amount
        $output->setValue($amount); // Satoshis

        $txClient = new TXClient($this->api);
        $txSkeleton = $txClient->create($tx);

//        return $txSkeleton->getTx()->getTotal();
    }

    /**
     * Fetch the data from external api
     *
     * @param $url
     * @return mixed
     */
    protected function fetchFromApi($method, $url, array $options = [])
    {
        $response = (new \GuzzleHttp\Client())->$method($url, $options);

        return \GuzzleHttp\json_decode($response->getBody());
    }
}