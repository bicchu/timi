<?php

namespace App\Currencies;

use BlockCypher\Api\Address;
use BlockCypher\Api\TX;
use BlockCypher\Api\TXHex;
use BlockCypher\Api\TXInput;
use BlockCypher\Api\TXOutput;
use BlockCypher\Api\TXSkeleton;
use BlockCypher\Api\Wallet;
use BlockCypher\Auth\SimpleTokenCredential;
use BlockCypher\Client\AddressClient;
use BlockCypher\Client\TXClient;
use BlockCypher\Client\WalletClient;
use BlockCypher\Converter\JsonConverter;
use BlockCypher\Rest\ApiContext;
use App\Currencies\Currency as CurrencyInterface;

class EthCurrency implements CurrencyInterface {

    protected $apiKey;

    protected $api;

    /**
     * BtcCurrency constructor.
     */
    public function __construct()
    {
        $this->apiKey = config('services.blockchain.api_key');

        $this->api = ApiContext::create(
            'main', 'eth', 'v1',
            new SimpleTokenCredential(config('services.blockchain.api_key')),
            ['mode' => 'sandbox']
        );
    }

    /**
     * Generate a new wallet address
     *
     * @return mixed
     */
    public function generateKey()
    {
        $result = $this->fetchFromApi('post', 'https://api.blockcypher.com/v1/eth/main/addrs?token=' . $this->apiKey);

        return $result->address;
    }

    /**
     * Get the balance for the specified address
     *
     * @param $address
     * @return mixed
     */
    public function getBalance($addresses)
    {
//        $result = $this->fetchFromApi('get', "https://api.blockcypher.com/v1/eth/main/addrs/$addresses/balance");
//
//        return $result->final_balance;
        $balance = 0;

        if (str_contains($addresses, ',')) {
            $addresses = explode(',', $addresses);

            foreach ($addresses as $address) {
                $addressClient = new AddressClient($this->api);
                $addressBalance = $addressClient->getBalance($address);

                $balance += $addressBalance->getFinalBalance();
            }
        } else {
            $addressClient = new AddressClient($this->api);
            $addressBalance = $addressClient->getBalance($addresses);

            return $addressBalance;

            $balance += $addressBalance->getFinalBalance();
        }

        return $balance->final_balance;
    }

    /**
     * Transfer the money from one address to another
     *
     * @param $amount
     * @param $fromAddress
     * @param $toAddress
     */
    public function transfer($amount, $fromAddresses, $toAddress)
    {
        $tx = new TX();

        // Tx inputs
        $input = new TXInput();

        // Handle multiple addresses
        if (str_contains($fromAddresses, ',')) {
            $addresses = explode(',', $fromAddresses);

            foreach ($addresses as $address) {
                $input->addAddress($address);
            }
        } else {
            $input->addAddress($fromAddresses);
        }

        $tx->addInput($input);

        // Tx outputs
        $output = new TXOutput();
        $output->addAddress($toAddress);
        $tx->addOutput($output);

        // Tx amount
        $output->setValue($amount); // Satoshis


        $response = $this->fetchFromApi(
            'post',
            'https://api.blockcypher.com/v1/eth/main/txs/new?token=' . $this->apiKey,
            [
                'body' => $tx
            ]
        );

        return $response;

        $txSkeleton = new TXSkeleton($response);

        return $txSkeleton->create();

        $txClient = new TXClient($this->api);

        $txSkeleton = $txClient->create($response);

        dd($txSkeleton);

        return $txSkeleton->getTx()->getTotal();
    }

    /**
     * Fetch the data from external api
     *
     * @param $url
     * @return mixed
     */
    protected function fetchFromApi($method, $url, array $options = [])
    {
        $response = (new \GuzzleHttp\Client())->$method($url, $options);

        return $response->getBody();

        return \GuzzleHttp\json_decode($response->getBody());
    }
}