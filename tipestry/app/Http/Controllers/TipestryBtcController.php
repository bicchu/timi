<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;
use DB;
use AUTH;

class TipestryBtcController extends Controller
{
    /**
     * BtcController constructor.
     */
    public function __construct()
    {
		$this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 
	 
     
	 
    public function bitcoin(Request $request)
    {
		$user_id	= Auth::user()->id;
		$sql = "SELECT * FROM 'walletdetails' where userid='".$user_id."'";
		$res = DB::select($sql);
		$cnt = 0;
		$spent_amount = 0;
		foreach($res as $val){
			
			$spent_amount += $val->amount;
			//echo '<pre>';print_r($val->amount);echo '</pre>';//exit;
			
		}
		//$sql = "UPDATE 'adbanner' SET alocate_count_per_hour = 10000 where alocate_count_per_hour < 1";
		//$res = DB::update($sql);
		$btcBalance = 0;
		//$walletid = Auth::user()->btcAddressDefault();
		$walletid	= '';
		
		$sql = "SELECT * FROM 'btc_addresses' where user_id = ".$user_id." order by id asc limit 1";
		$res = DB::select($sql);
		$cnt = 0;
		
		foreach($res as $val){
			$walletid = $val->address;
			//echo '<pre>';print_r($val);
		}	
		
		if($walletid != '')
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "http://34.238.252.237/api/btc");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"getreceivedbyaddress\",\"params\":[\"".$walletid."\"]}");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_USERPWD, "bitcoinrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");

			$headers = array();
			$headers[] = "Content-Type: application/x-www-form-urlencoded";
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);
			if (curl_errno($ch)) {
				echo 'Error:' . curl_error($ch);
			}else{
				//echo '<pre>';print_r(json_decode($result));//exit;
				$balancedata 	= json_decode($result);
				if($balancedata->result>0){
					$btcBalance		+= $balancedata->result;
				}
			}
			curl_close ($ch);
		}
		echo $btcBalance;exit;
		//echo 77;exit;
		
		
		
		/*$walletbalance = new App\Currencies\BtcCurrency->getBalance(auth()->user()->btcAddressesString());
		echo '::::'.$walletbalance;exit;*/
		$sql = "select * from wallet_transaction where to_wallet_id = '".$walletid."'";
		$res = DB::select($sql);
		foreach($res as $val){
			//echo '<pre>';print_r($val->amount);echo '</pre>';exit;
			if($val->amount>0)
			$btcBalance += $val->amount;
		}
		
		$sql = "select * from wallet_transaction where from_wallet_id = '".$walletid."'";
		$res = DB::select($sql);
		foreach($res as $val){
			
			$btcBalance -= $val->amount;
		}		
		
		
		//echo $btcBalance;exit;
		$session_data 	= $request->session()->all();
		
		$withdraw_msg = '';
		if(!empty($session_data['withdraw_msg'])){
			$withdraw_msg = $session_data['withdraw_msg'];
			$request->session()->forget('withdraw_msg');
		}
		$btcBalance = $btcBalance-$spent_amount;
		return view('currencies.btc.index',compact('walletid','withdraw_msg','btcBalance'));
    }

    /**
     * Withdraw/Send money from one wallet to another
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function withdraw(Request $request)
    {
        $this->validate(request(), [
            'recipient' => 'required',
            'amount' => 'required|numeric'
        ]);

		
		$bal_amt = $this->user_balance();
		
		if($bal_amt >= request()->amount){		
			$response = (new BtcCurrency)->transfer(
				request()->amount,
				auth()->user()->btcAddressDefault(),
				request()->recipient
			);

			if ($response) {
				/*  Commented By Kush 05-dec-2017, getting array merge err on user addTransaction
				auth()->user()->addTransaction(new Transaction([
					'amount' => request()->amount,
					'type' => 'withdrawn',
					'coin' => 'btc'
				]));*/
			
			
				$transaction_arr['amount'] 	= request()->amount;
				$transaction_arr['type'] 	= 'withdrawn';
				$transaction_arr['coin'] 	= 'btc';			
				auth()->user()->addTransaction($transaction_arr);
			}
			$request->session()->put('withdraw_msg', 'success');
		}else{
			$request->session()->put('withdraw_msg', 'error');
		}
        return redirect()->back();
    }

    /**
     * Generate a new btc wallet address
     *
     * @return string
     */
    public function regenerate()
    {
        auth()->user()->btcAddresses()->save(new BtcAddress([
            'address' => (new BtcCurrency)->generateKey()
        ]));

        return redirect()->back();
    }

    /**
     * Gift someone with some btc coins
     *
     * @param User $user
     * @param $amount
     * @return \Illuminate\Http\JsonResponse
     */
    public function give($type, $model, User $user, $amount, $network_fee)
    {				
		//echo ' Type : '.$type;
		//echo '<pre>';print_r($model);echo '</pre>';exit;

		/*
        return response()->json([
            'status' => 'success',
            'data' => null,
            'message' => 'You successfully gifted coins!'
        ], 201);*/



		
		
		
		$btcBalance = 0;
		$walletid = Auth::user()->btcAddressDefault();
		
		/*$walletbalance = new App\Currencies\BtcCurrency->getBalance(auth()->user()->btcAddressesString());
		echo '::::'.$walletbalance;exit;*/
		$sql = "select * from wallet_transaction where to_wallet_id = '".$walletid."'";
		$res = DB::select($sql);
		foreach($res as $val){
			//echo '<pre>';print_r($val->amount);echo '</pre>';exit;
			$btcBalance += $val->amount;
		}
		
		$sql = "select * from wallet_transaction where from_wallet_id = '".$walletid."'";
		$res = DB::select($sql);
		foreach($res as $val){
			$btcBalance -= $val->amount;
		}		
		
		
		$account_balance = (new BtcCurrency)->getBalance(auth()->user()->btcAddressesString());
		$account_balance 	+= $btcBalance;
		
		if($account_balance < $amount){
            return response()->json([
                'success' => false
            ]);
		}







		
		
		
		$address1 = auth()->user()->btcAddressDefault();
		$address2 = $user->btcAddressDefault();
		
		
		$arr = [
			'from_wallet_id' => $address1,
			'to_wallet_id' => $address2,
			'amount' => $amount,
			'topic_id' => $model,
			'coin_type' => 'btc',
			'status' => 1
		];
		DB::table('wallet_transaction')->insert($arr);	
		
		
		/** Start : Modified for cnt gift ***/
        if ($type == 'comments') {
            Comment::findOrFail($model)->gifts()->save(new Gift([
                'currency' => 'btc',
                'amount' => $amount,
                'user_id' => auth()->user()->id
            ]));
        }

        if ($type == 'topics') {
            Topic::findOrFail($model)->gifts()->save(new Gift([
                'currency' => 'btc',
                'amount' => $amount,
                'user_id' => auth()->user()->id
            ]));
        }				
		/** End : Modified for cnt gift ***/
		
		
		//echo 86;exit;
		
		
		
		
		return response()->json([
            'status' => 'success',
            'data' => null,
            'message' => 'You successfully gifted coins!'
        ], 201);



		//echo 'amt ++++:: '.$amount.' Fee '.$network_fee;exit;
		$total_amt = $amount+$network_fee;        
		try {
            $response = (new BtcCurrency)->transfer(
                $total_amt,
                auth()->user()->btcAddressDefault(),
                $user->btcAddressDefault()
            );
        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'Sorry, something went wrong.'
            ]);
        }
		
		
		
        if ($type == 'comments') {
            Comment::findOrFail($model)->gifts()->save(new Gift([
                'currency' => 'btc',
                'amount' => $amount,
                'user_id' => auth()->user()->id
            ]));
        }

        if ($type == 'topics') {
            Topic::findOrFail($model)->gifts()->save(new Gift([
                'currency' => 'btc',
                'amount' => $amount,
                'user_id' => auth()->user()->id
            ]));
        }

        return response()->json([
            'status' => 'success',
            'data' => null,
            'message' => 'You successfully gifted coins!'
        ], 201);
    }
	
	/*Kus*/
    public function estimated_network_fee(User $user,$amtx)
    {
        $estimated_network_fee = 0;
		$userAddress =  $user->btcAddressDefault();
        try {
			$response = (new BtcCurrency)->get_network_fee_estimate($amtx,$userAddress);			
			$estimated_network_fee = $response->data->estimated_network_fee;
			if($response->status == 'success'){
			
			}
			else{
				//echo '<pre>';print_r($response);
			}					
		} catch (Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'Sorry, something went wrong.'
            ]);
        }

		$res = response()->json([
            'status' => 'success',
            'fee' => $estimated_network_fee,
            'message' => 'Estimated network fee.'
        ], 201);
		
		return $res; /* */		
		//return 40005;
	}

    public function user_balance()
    {
        $available_balance = 0;
		$userAddress =  auth()->user()->btcAddressDefault();
		try {
			$available_balance = (new BtcCurrency)->getBalance($userAddress);
		} catch (Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'Sorry, something went wrong.'
            ]);
        }	
		return $available_balance;
	}
	
	/*Kus*/	
}
