<?php

namespace App\Http\Controllers\Auth;

use App\{User, Social};
use Socialite;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        $providerKey = config('services.' . $provider);

        if (empty($providerKey)) {
            return redirect('/');
        }

        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        if (request()->input('denied') != '') {

            return redirect()->to('/login')
                ->with('status', 'danger')
                ->with('message', 'You did not share your profile data with our social app.');
        }

        $user = Socialite::driver($provider)->user();

        $socialUser = null;

        // Check is this email present
        $userCheck = User::where('email', '=', $user->email)->first();

        $email = $user->email;

        if (! $user->email) {
            $email = 'missing' . str_random(10);
        }

        if (! empty($userCheck)) {

            $socialUser = $userCheck;
        } else {
            $sameSocialId = Social::where('social_id', '=', $user->id)
                ->where('provider', '=', $provider )
                ->first();

            if (empty($sameSocialId)) {

                //There is no combination of this social id and provider, so create new one
                $newSocialUser = new User;
                $newSocialUser->email = $email;
                $newSocialUser->name = $user->name;
//                $name = explode(' ', $user->name);
//
//                if (count($name) >= 1) {
//                    $newSocialUser->first_name = $name[0];
//                }
//
//                if (count($name) >= 2) {
//                    $newSocialUser->last_name = $name[1];
//                }

                $newSocialUser->password = bcrypt(str_random(16));
//                $newSocialUser->token = str_random(64);
                $newSocialUser->save();

                $socialData = new Social;
                $socialData->social_id = $user->id;
                $socialData->provider= $provider;
                $newSocialUser->social()->save($socialData);

                $socialUser = $newSocialUser;
            }
            else {

                // Load this existing social user
                $socialUser = $sameSocialId->user;
            }
        }

        auth()->login($socialUser, true);

        return redirect('/');
    }
}
