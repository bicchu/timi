<?php
namespace App\Http\Controllers\Auth;
use App\User;
use App\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
		//$data['email'] = 'test-'.rand(999,9999).'-'.rand(999,9999).'@gmail.com';		
        return Validator::make($data, [
            'name' => 'required|max:255',            
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }


	/**
	* Create a new user instance after a valid registration.
	*
	* @param  array  $data
	* 
	*/

    public function create(array $data)
    {
		//$create_walletaddress['createaddress'] = 1;
		//$req->session()->put('create_walletaddress',$create_walletaddress);	
		$_SESSION['create_walletaddress'] = 1;
		return User::create([
			'name' => $data['name'],
			'email' => $data['email'],
			'password' => bcrypt($data['password']),
		]);
		
		
		//header('Location: https://tipestry.com/login');exit;
		//echo 77;exit;
		//return redirect('/');exit;
		//echo 'here';exit;
    }
}