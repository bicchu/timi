<?php

namespace App\Http\Controllers;

use Auth;
use App\{Comment, Site, Log};
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Site $site)
    {
        $this->validate($request, [
            'content' => 'required|min:2|max:255'
        ]);

        $comment = Comment::create([
            'user_id' => auth()->user()->id,
            'site_id' => $site->id,
            'content' => $request->input('content')
        ]);

        $this->logActivity($site, 'Comment: ' . $comment->content);

        return redirect()->back();
    }

    /**
     * Write a reply to some comment
     *
     * @param Request $request
     * @param Site $site
     * @param Comment $comment
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reply(Request $request, Site $site, Comment $comment)
    {
        $this->validate($request, [
            'content' => 'required|min:2|max:255'
        ]);

        $comment = Comment::create([
            'user_id' => auth()->user()->id,
            'site_id' => $site->id,
            'parent_id' => $comment->id,
            'content' => $request->input('content')
        ]);

        $this->logActivity($site, 'Comment: ' . $comment->content);

        return redirect()->back();
    }

    public function logActivity($site, $message)
    {
        return Log::create([
            'user_id' => auth()->user()->id,
            'site_id' => $site->id,
            'message' => $message
        ]);
    }

    /**
     * Report a inappropriate comment
     *
     * @param Comment $comment
     * @return mixed
     */
    public function report(Comment $comment)
    {
		//echo '<pre>';print_r($comment);exit;
        $user = Auth::user();
        $user->reportComment($comment);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();

        return redirect()->back();
    }
}
