<?php
namespace App\Http\Controllers;
use App\{User, BtcAddress, EthAddress, Transaction, Comment, Gift, Topic};
use App\Currencies\EthCurrency;
use Illuminate\Http\Request;
use DB;
use Auth;

class EthController extends Controller
{
    /**
     * EthController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
		if(empty(Auth::user()->id)){
			return redirect('/');
		}		
		$this->ethcontract 	= '0x1421ede58b8066fab03f6b28a8bf0b507e8cd062';
		$this->ethurl 		= 'https://api.tokenbalance.com/token/'.$this->ethcontract;
    }

	
	
    public function index(Request $request)
	{
		$btcbalance = 0;
		$userid	= Auth::user()->id;
		$withdraw_msg = array();
		
		$sql = "update eth_addresses set address = '0xff3120d6da94acc39dd5105b74c8506fe91f9ba6' where address = 'ff3120d6da94acc39dd5105b74c8506fe91f9ba6' ";
		DB::update($sql);
		$sql = "select * from eth_addresses where user_id = ".$userid;//"address";		
		
		
		$res = DB::select($sql);
		$btcaddress = '';
		foreach($res as $val)
		{
			$btcaddress = $val->address;
			$btcaddress_org	= $btcaddress;
			$balance	= $val->ethtotaldeposit;
			$balance	= number_format($balance,8,'.','');
			$btcbalance += $balance;			
			
			if(FALSE)
			{
				//echo $btcaddress;exit;
				//$btcaddress = '0fe777FA444Fae128169754877d51b665eE557Ee';
				if($btcaddress != '')
				{				
					//$btcaddress_org	= $btcaddress;
					$btcaddress 	= strtoupper($btcaddress);
					$result 		= exec("curl --data-binary '{\"jsonrpc\":\"2.0\",\"method\":\"eth_getBalance\",\"params\":[\"".$btcaddress."\",\"latest\"],\"id\":1}' -H 'content-type:application/json;' http://34.238.252.237/api/eth");
					
					//echo 'Result :: '.$result."<br><br>";
					$btcdata 	= json_decode($result);
					$balance	= $btcdata->result;
					$balance	= hexdec($balance);
					//echo 'Number :: '.$balance.'<br><br>';
					$balance	= $balance/1000000000000000000;
					$balance	= number_format($balance,8);
					$btcbalance += $balance;
					
					
					/*$ethurl		= $this->ethurl.'/'.$btcaddress;
					$btcdata 	= file_get_contents($ethurl);
					$btcdata 	= json_decode($btcdata);
					$balance	= $btcdata->eth_balance;
					$balance	= $balance/1000000000000000000;
					$balance	= number_format($balance,8);
					$btcbalance += $balance;*/
				}
			}			
		}
		
		$sql = "select `amount` as received_amt from `walletdetails` where transactiontype = 'received' and wallettype = 'ethcoin' and userid = ".$userid;
		$res = DB::select($sql);
		foreach($res as $val){
			if($val->received_amt>0){
				$btcbalance += $val->received_amt;//exit;
			}
		}
		
		$sql = "select `amount` as received_amt from `walletdetails` where transactiontype in ('gifted','withdrawal') and wallettype = 'ethcoin' and userid = ".$userid;
		$res = DB::select($sql);
		foreach($res as $val){
			if($val->received_amt>0){
				$btcbalance -= $val->received_amt;
			}
		}
		
		$session_data = $request->session()->all();		
		if(!empty($session_data['withdrawresult'])){
			$withdraw_msg = $session_data['withdrawresult'];			
			$request->session()->forget('withdrawresult');
		}
		
		return view('currencies.eth.index',compact('withdraw_msg','btcbalance','btcaddress','btcaddress_org'));			
    }

    /**
     * Withdraw/Send money from one wallet to another
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function withdraw(Request $request)
    {
		request()->amount	= floatval(request()->amount);
		$network_fee 	= 0.0001;
		$btcbalance 	= 0;
		$giftedby_userid	= Auth::user()->id;
		$sql = "select * from eth_addresses where user_id = ".$giftedby_userid;//"address";
		$res = DB::select($sql);
		//echo $sql;exit;
		$btcaddress = '';
		foreach($res as $val)
		{
			$btcaddress = $val->address;				
			if($btcaddress != '')
			{
				$btcaddress = '0x'.strtoupper($btcaddress);
				$result 	= exec("curl --data-binary '{\"jsonrpc\":\"2.0\",\"method\":\"eth_getBalance\",\"params\":[\"".$btcaddress."\",\"latest\"],\"id\":1}' -H 'content-type:application/json;' http://34.238.252.237/api/eth");
				//echo 'Result :: '.$result."<br><br>";
				$btcdata 	= json_decode($result);
				$balance	= $btcdata->result;
				$balance	= hexdec($balance);
				//echo 'Number :: '.$balance.'<br><br>';exit;
				$balance	= $balance/1000000000000000000;
				$balance	= number_format($balance,8);
				$btcbalance += $balance;

				
				/*$ethurl		= $this->ethurl.'/'.$btcaddress;
				$btcdata 	= file_get_contents($ethurl);
				$btcdata 	= json_decode($btcdata);
				$balance	= $btcdata->eth_balance;
				$balance	= $balance/1000000000000000000;
				$balance	= number_format($balance,18);
				$btcbalance += $balance;*/				
			}	
		}
		
		///echo 'BTC Balance 01 :: '.$btcbalance.' ::: '.request()->amount;//exit;
		$sql = "select sum(`amount`) as received_amt from `walletdetails` where transactiontype = 'received' and wallettype = 'ethcoin' and userid = ".$giftedby_userid;
		$res = DB::select($sql);
		$btcaddress = '';
		foreach($res as $val){
			$btcbalance += $val->received_amt;
		}
		//echo 'BTC Balance 02 :: '.$btcbalance.' ::: '.request()->amount;//exit;
		
		$sql = "select sum(`amount`) as received_amt from `walletdetails` where transactiontype in ('gifted','withdrawal') and wallettype = 'ethcoin' and userid = ".$giftedby_userid;
		$res = DB::select($sql);
		$btcaddress = '';
		foreach($res as $val){
			$btcbalance -= $val->received_amt;
		}
		//echo 'BTC Balance 03 :: '.$btcbalance.' ::: '.request()->amount;//exit;

		/*********************************/		
		//echo '+++'.$btcbalance.'...';
		if($btcbalance >= request()->amount+$network_fee)
		{
			
			
			$transactionid = request()->recipient;
			
			echo '...'.$transactionid;exit;
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "http://34.238.252.237/api/btc");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"sendtoaddress\",\"params\":[\"".request()->recipient."\",".request()->amount."]}");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_USERPWD, "bitcoinrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
			$headers = array();
			$headers[] = "Content-Type: application/x-www-form-urlencoded";
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$transactiondata 	= curl_exec($ch);
			//echo '+++<pre>';print_r(json_decode($transactiondata));
			$explorertransactiondata 	= json_decode($transactiondata);
			$explorertransactionid		= $explorertransactiondata->result;
			//echo 'explorertransactionid : '.$explorertransactionid;exit;
			if(!empty($explorertransactionid) && $explorertransactionid != ''){
				$amt = request()->amount+$network_fee;

				//id, userid, transactiontype, amount, created_at, updated_at, wallettype, transactionid, receive_wallet_id, network_fee
				//insert into 
				//walletdetails(userid,transactiontype,amount,wallettype,transactionid) 
				//values(150,'received',12,'bitcoin',987654321);

				$sql = "insert into `walletdetails`(userid,transactiontype,wallettype,amount,network_fee,transactionid,receive_wallet_id)
				values('".$giftedby_userid."','withdrawal','bitcoin',".$amt.",".$network_fee.",'".$explorertransactionid."','".request()->recipient."')";
				DB::insert($sql);

				$withdrawresult['status']	= 1;
				$withdrawresult['message']	= 'Successfully withdrawal '.request()->amount.' BTC.';
			}else{
				$errordata 				= $explorertransactiondata->error;
				$withdrawresult['status']	= 0;
				$withdrawresult['message']	= $errordata->message;
			}
			$request->session()->put('withdrawresult',$withdrawresult);
			if (curl_errno($ch)) {
				echo 'Error:' . curl_error($ch);
			}else{
			}
			curl_close ($ch);
		}	
		echo 'here :: ';exit;
		return redirect('/currencies/eth');
	}	 
	 
    public function withdraw_old()
    {
        $this->validate(request(), [
            'recipient' => 'required',
            'amount' => 'required'
        ]);

        $response = (new EthCurrency)->transfer(
            request()->amount,
            auth()->user()->ethAddressDefault(),
            request()->recipient
        );

        return redirect()->back();
    }

    /**
     * Gift someone with some btc coins
     *
     * @param User $user
     * @param $amount
     * @return \Illuminate\Http\JsonResponse
     */
    public function give($type, $model, User $user, $amount, $network_fee)
    {
		$amount				= floatval($amount);
		$giftedby_userid	= Auth::user()->id;
		$btcbalance 		= 0;
		if($giftedby_userid)
		{
			$sql = "select * from eth_addresses where user_id = ".$giftedby_userid;//"address";		
			$res = DB::select($sql);
			$btcaddress = '';
			foreach($res as $val)
			{
				$btcaddress = $val->address;
				//$btcaddress = '0fe777FA444Fae128169754877d51b665eE557Ee';
				if($btcaddress != '')
				{
					$btcaddress = strtoupper($btcaddress);
					$result 	= exec("curl --data-binary '{\"jsonrpc\":\"2.0\",\"method\":\"eth_getBalance\",\"params\":[\"".$btcaddress."\",\"latest\"],\"id\":1}' -H 'content-type:application/json;' http://34.238.252.237/api/eth");
					//echo 'Result :: '.$result."<br><br>";
					$btcdata 	= json_decode($result);
					$balance	= $btcdata->result;
					$balance	= hexdec($balance);
					//echo 'Number :: '.$balance.'<br><br>';exit;
					$balance	= $balance/1000000000000000000;
					$balance	= number_format($balance,18);
					$btcbalance += $balance;

					
					/*$ethurl		= $this->ethurl.'/'.$btcaddress;
					$btcdata 	= file_get_contents($ethurl);
					$btcdata 	= json_decode($btcdata);
					$balance	= $btcdata->eth_balance;
					$balance	= $balance/1000000000000000000;
					$balance	= number_format($balance,18);
					$btcbalance += $balance;*/			
				}	
			}
			
			$btcbalance = number_format($btcbalance,18);
			//echo $btcbalance.'***';exit;
			$sql = "select `amount` as received_amt from `walletdetails` where transactiontype = 'received' and wallettype = 'ethcoin' and userid = ".$giftedby_userid;
			$res = DB::select($sql);			
			foreach($res as $val){
				if($val->received_amt>0){
					$btcbalance += $val->received_amt;
				}
			}
			//echo '***'.$btcbalance.'***';
			
			$sql = "select `amount` as received_amt from `walletdetails` where transactiontype in ('gifted','withdrawal') and wallettype = 'ethcoin' and userid = ".$giftedby_userid;
			$res = DB::select($sql);
			foreach($res as $val){
				if($val->received_amt>0){
					//echo '...'.$val->received_amt.'....';
					$btcbalance -= $val->received_amt;
				}
			}		
			//echo ' ++'.number_format($btcbalance,8).'+++';exit;
			//echo 'here :::** '.$btcbalance;exit;		
			if($btcbalance >= $amount)
			{
				if ($type == 'website')
				{	
					$sql 		= "SELECT * FROM 'topics' where id = ".$model." LIMIT 1";
					$res 		= DB::select($sql);
					$site_id	= 0;
					foreach($res as $val){
						$site_id	= $val->site_id;
					}
					
					if($site_id > 0){
						$transactionid = time().rand(999,9999);
						$sql = "insert into `walletdetails`(userid,site_id,transactiontype,wallettype,amount,transactionid)
						values('0','".$site_id."','received','ethcoin',".$amount.",'".$transactionid."')";				
						//echo $sql;exit;
						DB::insert($sql);
						
						$sql = "insert into `walletdetails`(userid,site_id,transactiontype,wallettype,amount,transactionid)
						values('".$giftedby_userid."','".$site_id."','gifted','ethcoin',".$amount.",'".$transactionid."')";
						DB::insert($sql);
					}					
				}
				else
				{				
				
				
				
				$transactionid = time().rand(999,9999);
				$sql = "insert into `walletdetails`(userid,transactiontype,wallettype,amount,transactionid)
				values('".$user->id."','received','ethcoin',".$amount.",'".$transactionid."')";				
				//echo $sql;exit;
				DB::insert($sql);
				
				$sql = "insert into `walletdetails`(userid,transactiontype,wallettype,amount,transactionid)
				values('".$giftedby_userid."','gifted','ethcoin',".$amount.",'".$transactionid."')";
				DB::insert($sql);				

				$txt = 'Notification : You received '.number_format($amount,8).' ETH on Date '.date('d-m-Y').'.';
				DB::table('notification')->insert(['user_id' => $user->id, 'message' => $txt, 'status'=>1]);
				
				}
				
				/** Start : Modified for cnt gift *****/
				
				if ($type == 'comments') {
					Comment::findOrFail($model)->gifts()->save(new Gift([
						'currency' => 'eth',
						'amount' => $amount,
						'user_id' => auth()->user()->id
					]));/* */
					
					//echo 'type :::: ETH :::::'.$type.' model : '.$model;exit;
					
				}

				if ($type == 'topics') {
					Topic::findOrFail($model)->gifts()->save(new Gift([
						'currency' => 'eth',
						'amount' => $amount,
						'user_id' => auth()->user()->id
					]));
				}				
				/** End : Modified for cnt gift ***/

				return response()->json([
					'status' => 'success',
					'data' => null,
					'message' => 'You successfully gifted coins!'
				], 201);			
			}			
			
		}
		
		return response()->json([
			'status' => 'error',
			'data' => null,
			'message' => 'Sorry, You do not have sufficient balance.'
		]);  
    }	 
	 
    public function give_old(User $user, $amount)
    {
        try {
            (new EthCurrency)->transfer(
                $amount,
                auth()->user()->ethAddressDefault(),
                $user->ethAddressDefault()
            );
        } catch (Exception $e) {
            return response()->json([
                'success' => false
            ]);
        }

        return response()->json([
            'success' => true
        ]);
    }
}
