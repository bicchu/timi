<?php
namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use DB;

class WebsiteController extends Controller
{
    public function vote(Request $req)
	{
		$type 		= $req->type;
		$res 		= DB::table('websitelist')->select('*')->where('id', '=', $req->websiteid)->get();
		
		if($type == 1){
			$totalupvote	= $res[0]->totalupvote;
			$totalupvote++;
			DB::table('websitelist')->where('id', $req->websiteid)->update(['totalupvote' => $totalupvote]);
			echo $totalupvote;
		}elseif($type == 2){
			$totaldownvote	= $res[0]->totaldownvote;
			$totaldownvote++;
			DB::table('websitelist')->where('id', $req->websiteid)->update(['totaldownvote' => $totaldownvote]);
			echo $totaldownvote;
		}else{
			echo 0;
		}
	}
}
