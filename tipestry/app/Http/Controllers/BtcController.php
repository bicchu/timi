<?php
namespace App\Http\Controllers;
use App\CommentGive;
use App\{User, BtcAddress, Transaction, Comment, Gift, Topic};
use App\Currencies\BtcCurrency;
use Illuminate\Http\Request;
use DB;
use Auth;

class BtcController extends Controller
{
    /**
     * BtcController constructor.
     */
    public function __construct()
    {
		$this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
		$btcbalance = 0;
		$userid	= Auth::user()->id;
		
		//$sql = "select * from `walletdetails` order by id desc limit 5";
		//$res = DB::select($sql);
		//echo '<pre>';print_r($res);exit;
		
		$sql = "select * from btc_addresses where user_id = ".$userid;//"address";		
		$res = DB::select($sql);
		$btcaddress = '';
		foreach($res as $val)
		{
			$btcaddress = $val->address;
			if($btcaddress != '')
			{
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "http://34.238.252.237/api/btc");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"getreceivedbyaddress\",\"params\":[\"".$btcaddress."\"]}");
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_USERPWD, "bitcoinrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
				$headers = array();
				$headers[] = "Content-Type: application/x-www-form-urlencoded";
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				$result = curl_exec($ch);
				$btcdata = json_decode($result);
				
				//echo $result;exit;
				
				
				$btcbalance += $btcdata->result;
				if (curl_errno($ch)) {
					echo 'Error:' . curl_error($ch);
				}
				curl_close ($ch);			
			}	
		}
				
		//echo ' :: Here : '.$btcbalance;exit;
		//$sql = "select sum(`amount`) as received_amt from `walletdetails` where transactiontype = 'received' and wallettype = 'bitcoin' and userid = ".$userid;
		$sql = "select `amount` as received_amt from `walletdetails` where transactiontype = 'received' and wallettype = 'bitcoin' and userid = ".$userid;
		$res = DB::select($sql);
		foreach($res as $val){
			if($val->received_amt>0){
				//echo $val->received_amt;
				$btcbalance += $val->received_amt;//exit;
			}
		}
		
		//$sql = "select sum(`amount`) as received_amt from `walletdetails` where transactiontype in ('gifted','withdrawal') and wallettype = 'bitcoin' and userid = ".$userid;
		$sql = "select `amount` as received_amt from `walletdetails` where transactiontype in ('gifted','withdrawal') and wallettype = 'bitcoin' and userid = ".$userid;
		$res = DB::select($sql);
		foreach($res as $val){
			if($val->received_amt>0){
				$btcbalance -= $val->received_amt;
			}
		}		
		
		//echo '::::::'.number_format($btcbalance,8);exit;
		
		
		$session_data = $request->session()->all();
		$withdraw_msg = array();
		if(!empty($session_data['withdrawresult'])){
			$withdraw_msg = $session_data['withdrawresult'];			
			$request->session()->forget('withdrawresult');
		}
		//echo number_format($btcbalance, 5);exit;
		//echo 'Here :: '.floatval($btcbalance);exit;
		return view('currencies.btc.index',compact('withdraw_msg','btcbalance','btcaddress'));	
    }

    public function index_old_code(Request $request)
    {
		echo 111;exit;
		//$sql = "UPDATE 'adbanner' SET alocate_count_per_hour = 10000 where alocate_count_per_hour < 1";
		//$res = DB::update($sql);

		
		
		$btcBalance = 0;
		//$walletid = Auth::user()->btcAddressDefault();
		
		
		echo $user_id	= Auth::user()->id;
		exit;$sql = "SELECT * FROM 'btc_addresses' where user_id = ".$user_id." order by id asc limit 1";
		$res = DB::select($sql);
		$cnt = 0;
		foreach($res as $val){
			echo '<pre>';print_r($val);
		}		
		echo 77;
		exit;
		
		
		
		/*$walletbalance = new App\Currencies\BtcCurrency->getBalance(auth()->user()->btcAddressesString());
		echo '::::'.$walletbalance;exit;*/
		$sql = "select * from wallet_transaction where to_wallet_id = '".$walletid."'";
		$res = DB::select($sql);
		foreach($res as $val){
			//echo '<pre>';print_r($val->amount);echo '</pre>';exit;
			$btcBalance += $val->amount;
		}
		
		$sql = "select * from wallet_transaction where from_wallet_id = '".$walletid."'";
		$res = DB::select($sql);
		foreach($res as $val){
			$btcBalance -= $val->amount;
		}		
		
		
		//echo $btcBalance;exit;
		$session_data 	= $request->session()->all();
		
		$withdraw_msg = '';
		if(!empty($session_data['withdraw_msg'])){
			$withdraw_msg = $session_data['withdraw_msg'];
			$request->session()->forget('withdraw_msg');
		}
		return view('currencies.btc.index',compact('withdraw_msg','btcBalance'));
    }

    /**
     * Withdraw/Send money from one wallet to another
     *
     * @return \Illuminate\Http\RedirectResponse
     */

    public function withdraw(Request $request)
    {
		request()->amount	= floatval(request()->amount);			
		//$withdrawalamt	= request()->amount;
		//echo '+++++'.$withdrawalamt.'<br><br>';exit;
		
		$network_fee 	= 0.00001;
		$btcbalance 	= 0;
		$giftedby_userid	= Auth::user()->id;
		$sql = "select * from btc_addresses where user_id = ".$giftedby_userid;//"address";
		$res = DB::select($sql);
		//echo $sql;exit;
		$btcaddress = '';
		foreach($res as $val)
		{
			$btcaddress = $val->address;				
			if($btcaddress != '')
			{
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "http://34.238.252.237/api/btc");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"getreceivedbyaddress\",\"params\":[\"".$btcaddress."\"]}");
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_USERPWD, "bitcoinrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
				$headers = array();
				$headers[] = "Content-Type: application/x-www-form-urlencoded";
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				$result = curl_exec($ch);
				$btcdata = json_decode($result);
				//echo '<pre>';print_r($btcdata);echo '</pre>';exit;
				$btcbalance += $btcdata->result;
				if (curl_errno($ch)) {
					echo 'Error:' . curl_error($ch);
				}
				curl_close ($ch);			
			}	
		}
		
		//echo 'BTC Balance 01 :: '.$btcbalance.' ::: '.request()->amount.'<br>';//exit;
		$sql = "select sum(`amount`) as received_amt from `walletdetails` where transactiontype = 'received' and wallettype = 'bitcoin' and userid = ".$giftedby_userid;
		$res = DB::select($sql);
		$btcaddress = '';
		foreach($res as $val){
			if($val->received_amt>0){
				$btcbalance += $val->received_amt;
			}
		}
		//echo 'BTC Balance 02 :: '.$btcbalance.' ::: '.request()->amount.'<br>';//exit;
		
		$sql = "select sum(`amount`) as received_amt from `walletdetails` where transactiontype in ('gifted','withdrawal') and wallettype = 'bitcoin' and userid = ".$giftedby_userid;
		//echo '<br>'.$sql;
		$res = DB::select($sql);
		$btcaddress = '';
		foreach($res as $val){
			if($val->received_amt>0){
				//echo '<br>:::::'.$val->received_amt.'<br>';
				$btcbalance -= $val->received_amt;
			}
		}
		
		//echo 'BTC Balance 003 :: '.number_format($btcbalance,8).' ::: '.request()->amount.'<br>';exit;
		//echo number_format($btcbalance,8).'>>'.number_format(request()->amount,8).'+'.number_format($network_fee,8);exit;

		/*********************************/		
		
		$totalrequire = request()->amount+$network_fee;
		
		if($btcbalance >= $totalrequire)
		{
			//echo 'HERE :::: ';echo 'BTC Balance 03 :: '.$btcbalance.' ::: '.request()->amount;exit;
			$transactionid = request()->recipient;
			;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "http://34.238.252.237/api/btc");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"sendtoaddress\",\"params\":[\"".request()->recipient."\",".request()->amount."]}");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_USERPWD, "bitcoinrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
			$headers = array();
			$headers[] = "Content-Type: application/x-www-form-urlencoded";
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$transactiondata 	= curl_exec($ch);
			//echo '+++<pre>';print_r(json_decode($transactiondata));exit;
			$explorertransactiondata 	= json_decode($transactiondata);
			$explorertransactionid		= $explorertransactiondata->result;
			//echo 'explorertransactionid : '.$explorertransactionid;exit;
			if(!empty($explorertransactionid) && $explorertransactionid != ''){
				$amt = request()->amount+$network_fee;

				//id, userid, transactiontype, amount, created_at, updated_at, wallettype, transactionid, receive_wallet_id, network_fee
				//insert into 
				//walletdetails(userid,transactiontype,amount,wallettype,transactionid) 
				//values(150,'received',12,'bitcoin',987654321);

				$sql = "insert into `walletdetails`(userid,transactiontype,wallettype,amount,network_fee,transactionid,receive_wallet_id)
				values('".$giftedby_userid."','withdrawal','bitcoin',".$amt.",".$network_fee.",'".$explorertransactionid."','".request()->recipient."')";
				DB::insert($sql);

				$withdrawresult['status']	= 1;
				$withdrawresult['message']	= 'Successfully withdrawal '.request()->amount.' BTC.';
				$withdrawresult['message']	= 'Successfully withdrawal '.request()->amount.' BTC.<br> Transaction ID : '.$explorertransactionid;
			}else{
				$errordata 					= $explorertransactiondata->error;
				$withdrawresult['status']	= 0;
				$withdrawresult['message']	= $errordata->message;
			}
			$request->session()->put('withdrawresult',$withdrawresult);
			if (curl_errno($ch)) {
				echo 'Error:' . curl_error($ch);
			}else{
			}
			curl_close ($ch);
		}
		else{
			$withdrawresult['status']	= 0;
			$withdrawresult['message']	= 'Not enough fund available!';;
			$request->session()->put('withdrawresult',$withdrawresult);
		}
		return redirect('/currencies/btc');
	}	
	
	

    /**
     * Generate a new btc wallet address
     *
     * @return string
     */
    public function regenerate()
    {
        /*auth()->user()->btcAddresses()->save(new BtcAddress([
            'address' => (new BtcCurrency)->generateKey()
        ]));*/

        return redirect()->back();
    }

	
    public function give($type, $model, User $user, $amount, $network_fee)
    {
		$amount				= floatval($amount);
		$giftedby_userid	= Auth::user()->id;
		$btcbalance 		= 0;
		if($giftedby_userid)
		{
			$sql = "select * from btc_addresses where user_id = ".$giftedby_userid;//"address";		
			$res = DB::select($sql);
			$btcaddress = '';
			foreach($res as $val)
			{
				$btcaddress = $val->address;
				if($btcaddress != '')
				{
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "http://34.238.252.237/api/btc");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"getreceivedbyaddress\",\"params\":[\"".$btcaddress."\"]}");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_USERPWD, "bitcoinrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
					$headers = array();
					$headers[] = "Content-Type: application/x-www-form-urlencoded";
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					$result = curl_exec($ch);
					$btcdata = json_decode($result);
					$btcbalance += $btcdata->result;
					if (curl_errno($ch)) {
						echo 'Error:' . curl_error($ch);
					}
					curl_close ($ch);			
				}	
			}

			$sql = "select sum(`amount`) as received_amt from `walletdetails` where transactiontype = 'received' and wallettype = 'bitcoin' and userid = ".$giftedby_userid;
			$res = DB::select($sql);
			$btcaddress = '';
			foreach($res as $val){
				$btcbalance += $val->received_amt;
			}
			
			$sql = "select sum(`amount`) as received_amt from `walletdetails` where transactiontype in ('gifted','withdrawal') and wallettype = 'bitcoin' and userid = ".$giftedby_userid;
			$res = DB::select($sql);
			$btcaddress = '';
			foreach($res as $val){
				$btcbalance -= $val->received_amt;
			}			
			
			//echo 'here :::** '.$btcbalance;exit;		
			if($btcbalance >= $amount)
			{
				if ($type == 'website')
				{	
					$sql 		= "SELECT * FROM 'topics' where id = ".$model." LIMIT 1";
					$res 		= DB::select($sql);
					$site_id	= 0;
					foreach($res as $val){
						$site_id	= $val->site_id;
					}
					
					if($site_id > 0){
						$transactionid = time().rand(999,9999);
						$sql = "insert into `walletdetails`(userid,site_id,transactiontype,wallettype,amount,transactionid)
						values('0','".$site_id."','received','bitcoin',".$amount.",'".$transactionid."')";				
						//echo $sql;exit;
						DB::insert($sql);
						
						$sql = "insert into `walletdetails`(userid,site_id,transactiontype,wallettype,amount,transactionid)
						values('".$giftedby_userid."','".$site_id."','gifted','bitcoin',".$amount.",'".$transactionid."')";
						DB::insert($sql);
					}					
				}
				else
				{				
				
				
				$transactionid = time().rand(999,9999);
				/*$sql = "insert into `walletdetails`(userid,transactiontype,wallettype,amount,transactionid)
				values('".$user->id."','456','received','bitcoin',".$amount.",'".$transactionid."')";*/



				$sql = "insert into `walletdetails`(userid,transactiontype,wallettype,amount,transactionid)
				values('".$user->id."','received','bitcoin',".$amount.",'".$transactionid."')";				
				//echo $sql;exit;
				DB::insert($sql);
				
				$sql = "insert into `walletdetails`(userid,transactiontype,wallettype,amount,transactionid)
				values('".$giftedby_userid."','gifted','bitcoin',".$amount.",'".$transactionid."')";
				DB::insert($sql);				

				$txt = 'Notification : You received '.number_format($amount,8).' BTC on Date '.date('d-m-Y').'.';
				DB::table('notification')->insert(['user_id' => $user->id, 'message' => $txt, 'status'=>1]);				
				
				}
				/** Start : Modified for cnt gift ***/
				if ($type == 'comments') {
					
					
					Comment::findOrFail($model)->gifts()->save(new Gift([
						'currency' => 'btc',
						'amount' => $amount,
						'user_id' => auth()->user()->id
					]));
					//echo 'type :::::::::'.$type.' model : '.$model;exit;
				}

				if ($type == 'topics') {
					Topic::findOrFail($model)->gifts()->save(new Gift([
						'currency' => 'btc',
						'amount' => $amount,
						'user_id' => auth()->user()->id
					]));
				}				
				/** End : Modified for cnt gift ***/

				return response()->json([
					'status' => 'success',
					'data' => null,
					'message' => 'You successfully gifted coins!'
				], 201);			
			}			
			
		}
		
		return response()->json([
			'status' => 'error',
			'data' => null,
			'message' => 'Sorry, You do not have sufficient balance.'
		]);  
    }
	
	/*Kus*/
    public function estimated_network_fee(User $user,$amtx)
    {
        $estimated_network_fee = 0;
		$userAddress =  $user->btcAddressDefault();
        try {
			$response = (new BtcCurrency)->get_network_fee_estimate($amtx,$userAddress);			
			$estimated_network_fee = $response->data->estimated_network_fee;
			if($response->status == 'success'){
			
			}
			else{
				//echo '<pre>';print_r($response);
			}					
		} catch (Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'Sorry, something went wrong.'
            ]);
        }

		$res = response()->json([
            'status' => 'success',
            'fee' => $estimated_network_fee,
            'message' => 'Estimated network fee.'
        ], 201);
		
		return $res; /* */		
		//return 40005;
	}

    public function user_balance()
    {
        $available_balance = 0;
		$userAddress =  auth()->user()->btcAddressDefault();
		try {
			$available_balance = (new BtcCurrency)->getBalance($userAddress);
		} catch (Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'Sorry, something went wrong.'
            ]);
        }	
		return $available_balance;
	}
	
	/*Kus*/	
}
