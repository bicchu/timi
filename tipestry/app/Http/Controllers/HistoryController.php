<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
    /**
     * HistoryController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $days = request()->get('days', 1);

        if (! (int) $days) {
            abort(404, 'Sorry, not sorry!');
        }

        $date = (new Carbon())->subDays($days)->toDateTimeString();

        $history = auth()->user()->history()->whereDate('created_at', '>', $date)->get();

        return view('user.history', compact('history'));
    }
}
