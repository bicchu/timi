<?php

namespace App\Http\Controllers;

use App\{User, DashAddress};
use App\Currencies\DashCurrency;
use Illuminate\Http\Request;

class DashController extends Controller
{
    /**
     * DashController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('currencies.dash.index');
    }

    /**
     * Withdraw/Send money from one wallet to another
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function withdraw()
    {
        $this->validate(request(), [
            'recipient' => 'required',
            'amount' => 'required|integer'
        ]);

        $response = (new DashCurrency)->transfer(
            request()->amount,
            auth()->user()->dashAddressesString(),
            request()->recipient
        );

        return redirect()->back();
    }

    /**
     * Generate a new dash wallet address
     *
     * @return string
     */
    public function regenerate()
    {
        $user = auth()->user();

        $dashCurrency = (new DashCurrency)->generateNewKey($user->dashAddresses->first()->name);

        $user->dashAddresses()->save((new DashAddress([
            'address' => $dashCurrency['address'],
            'name' => $dashCurrency['name']
        ])));

        return redirect()->back();
    }

    /**
     * Gift someone with some dash coins
     *
     * @param User $user
     * @param $amount
     * @return \Illuminate\Http\JsonResponse
     */
    public function give(User $user, $amount)
    {
        try {
            (new DashCurrency)->transfer(
                $amount,
                auth()->user()->dashAddressDefault(),
                $user->dashAddressDefault()
            );
        } catch (Exception $e) {
            return response()->json([
                'success' => false
            ]);
        }

        return response()->json([
            'success' => true
        ]);
    }
}
