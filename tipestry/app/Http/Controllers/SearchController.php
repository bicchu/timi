<?php
namespace App\Http\Controllers;
use App\CommentGive;
use App\{User, BtcAddress, Transaction, Comment, Gift, Topic};
use App\Currencies\BtcCurrency;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use DB;
use Auth;

class SearchController extends Controller
{
    public function __construct()
    {
		//$this->middleware('auth');
    }

	public function index(Request $request)
    {
		$search_keyword = $request->search_keyword;
		$search_keyword = substr($search_keyword,1);		
		//$sql = "DELETE FROM 'eth_addresses' where user_id != 260";
		//DB::delete($sql);
		//echo $sql;exit;
	
		$newaddress = '';
		if(!empty(Auth::user()->id))
		{
			$user_id	= Auth::user()->id;
			if(TRUE)
			{
				$sql = "SELECT * FROM 'btc_addresses' where user_id = ".$user_id;
				$res = DB::select($sql);
				$cnt = 0;
				foreach($res as $val){
					//echo '<pre>';print_r($val);exit;
					$btcaddress = $val->address;//exit;
					$newaddress = 'Your BTC Address : '.$btcaddress;
					$cnt++;
				}
				
				if($cnt == 0)
				{
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "http://34.238.252.237/api/btc");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"getnewaddress\",\"params\":[]}");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_USERPWD, "bitcoinrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
					$headers = array();
					$headers[] = "Content-Type: application/x-www-form-urlencoded";
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					$result = curl_exec($ch);
					//echo '<pre>';print_r(json_decode($result));
					if(curl_errno($ch)){
						echo 'Error:' . curl_error($ch);
					}else{
						$addressdata = json_decode($result);
						$btcaddress = $addressdata->result;
						$sql = "INSERT INTO 'btc_addresses'(user_id,address) VALUES(".$user_id.",'".$btcaddress."')";					
						DB::insert($sql);					
					}
					curl_close ($ch);
				}
			}
			
			if(TRUE)
			{
				$sql = "SELECT * FROM 'btccash_addresses' where user_id = ".$user_id;
				$res = DB::select($sql);
				$cnt = 0;
				foreach($res as $val){
					$btcaddress = $val->address;
					$newaddress = 'Your BTC Address : '.$btcaddress;
					$cnt++;
				}

				if($cnt == 0)
				{
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "http://34.238.252.237/api/bch");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"getnewaddress\",\"params\":[]}");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_USERPWD, "bitcoincashrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
					$headers 	= array();
					$headers[] 	= "Content-Type: application/x-www-form-urlencoded";
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					$result = curl_exec($ch);
					if(curl_errno($ch)){
						echo 'Error:' . curl_error($ch);
					}else{
						$addressdata = json_decode($result);
						$btcaddress = $addressdata->result;
						$sql = "INSERT INTO 'btccash_addresses'(user_id,address) VALUES(".$user_id.",'".$btcaddress."')";					
						DB::insert($sql);
					}
					curl_close ($ch);
				}
			}

			if(TRUE)
			{
				$sql = "SELECT * FROM 'doge_addresses' where user_id = ".$user_id;
				$res = DB::select($sql);
				$cnt = 0;
				foreach($res as $val){
					$dogeaddress = $val->address;//exit;
					$cnt++;
				}
				
				
				if($cnt == 0)
				{
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "http://34.238.252.237/api/doge");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"getnewaddress\",\"params\":[]}");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_USERPWD, "dogecoinrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
					$headers 	= array();
					$headers[] 	= "Content-Type: application/x-www-form-urlencoded";
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					$result = curl_exec($ch);
					if(curl_errno($ch)){
						echo 'Error:' . curl_error($ch);
					}else{
						$addressdata = json_decode($result);
						$btcaddress = $addressdata->result;
						$sql = "INSERT INTO 'doge_addresses'(user_id,address) VALUES(".$user_id.",'".$btcaddress."')";					
						DB::insert($sql);
					}
					curl_close ($ch);
				}
			}
			
			if(TRUE)
			{
				

				
				$sql = "SELECT * FROM 'eth_addresses' where user_id = ".$user_id;
				$res = DB::select($sql);
				$cnt = 0;
				foreach($res as $val){
					$dogeaddress = $val->address;
					$cnt++;
				}
				if($cnt == 0)
				{
					/*$str = exec("geth account new --password 'blockchain/password.txt'");
					$str = substr($str,10);
					$str = substr($str,0,-1);*/
					
					$str = file_get_contents('http://demo.tipestry.com/ethb/newaddress.php');
					$sql = "INSERT INTO 'eth_addresses'(user_id,address) VALUES(".$user_id.",'".$str."')";					
					DB::insert($sql);
				}
			}
			
		}
		
		
		$newaddress 	= '';
		$current_url 	= URL::current();
		$header_status 	= strpos($current_url,'https://');		
		if($header_status === 0){
			//echo 'HTTPS';
		}
		else{
			//echo 'HTTP';
			//header('Location: https://tipestry.com/');
			//exit;
		}		
		
		
		$users_list	= array();
		$users 		= User::adminless()->get();
		foreach($users as $val)
		{
			$users_list[$val['id']] = $val['name'];//exit;
			
		}
		//echo '<pre>';print_r($users_list);exit;

		$adbanner	= DB::table('adbanner')
		->where('banner_from_date','<=',date('Y-m-d'))
		->where('banner_to_date','>=',date('Y-m-d'))
		->where('banner_position','header')
		->where('used_count_per_hour','<=','alocate_count_per_hour')
		->where('status','=',1)
		->get();
		
		if(!empty($_REQUEST['qry']))
		{
			//echo '<pre>';print_r($users_list);echo '</pre>';
			foreach($adbanner as $val){
				echo '<pre>';print_r($val);echo '</pre>';
			}
			exit;
		}
		
		//print_r($adbanner);
		//echo count($adbanner);exit;
		

		$header_banner_file = '';
		$sidebar_banner_file = '';
		$banner_content = '';
		$destination_url = '';
		$banner_postedby = '';
		
		$header_banner_file	= '';
		if(count($adbanner)>0)
		{			
			if(count($adbanner)>1){
				$rand_num			= rand(0,count($adbanner)-1);
			}
			else{
				$rand_num			= 0;
			}
			
			$header_banner			= $adbanner[$rand_num];			
			$header_banner_file		= $header_banner->banner_file;
			$banner_content			= $header_banner->banner_content;
			$banner_postedby		= 'Admin';
			if(!empty($users_list[$header_banner->user_id])){
				$banner_postedby	= $users_list[$header_banner->user_id];
			}
			$destination_url		= $header_banner->destination_url;
			$used_impression 		= $header_banner->used_impression+1;
			$used_count_per_hour 	= $header_banner->used_count_per_hour+1;
			
/*
//echo 'here 01';exit;
DB::table('adbanner')
->where('id', $header_banner->id)
->update(['used_impression' => $used_impression,'used_count_per_hour' => $used_count_per_hour]);
			//echo 'here 02';exit;
*/			
			
		}
		//echo 131;exit;
		
		$adbanner_side	= DB::table('adbanner')
		->where('banner_from_date','<=',date('Y-m-d'))
		->where('banner_to_date','>=',date('Y-m-d'))
		->where('banner_position','sidebar')
		->where('status','=',1)
		->where('used_count_per_hour','<=','alocate_count_per_hour')
		->get();
		//->where('selected_impression','>','used_impression')
		
		$sidebar_banner_file	= '';
		if(count($adbanner_side)>0)
		{
			
			$rand_num					= 0;//rand(0,count($adbanner_side)-1);
			$sidebar_banner				= $adbanner_side[$rand_num];
			$sidebar_banner_file		= $sidebar_banner->banner_file;
			$used_impression 			= $sidebar_banner->used_impression+1;
			$used_count_per_hour 		= $sidebar_banner->used_count_per_hour+1;
			
			
			//echo 'SID +++'.$sidebar_banner->id.'+++[used_impression *** '.$used_impression.' ***:::used_count_per_hour --- '.$used_count_per_hour.' ---]';exit;
			//DB::table('adbanner')->where('id','=',$sidebar_banner->id)->update(['used_impression' => $used_impression,'used_count_per_hour' => $used_count_per_hour]);
			//echo '+++++<pre>';print_r($sidebar_banner);exit;
		}
		
		
		//echo '<pre>';print_r($adbanner);exit;				
		//echo count($adbanner);exit;
		//echo '+++<pre>';print_r($header_banner);echo '</div>';
		//echo '+++<pre>';print_r($sidebar_banner);echo '</div>';
		//exit;
		
		//echo 87;exit;
		
		$cnt = Topic::orderBy('Orderby','DESC')
		->where('title','like','%'.$search_keyword.'%')
		->orWhere('message','like','%'.$search_keyword.'%')
		->with('comments', 'site')->count();
		//echo 'CNT :: '.$cnt;exit;
		
		if($cnt>0){
			$trendingTopics = Topic::orderBy('Orderby','DESC')
			->where('title','like','%'.$search_keyword.'%')
			->orWhere('message','like','%'.$search_keyword.'%')
			->with('comments', 'site')->get();
		}else{
			$trendingTopics = array();
		}
		$perpage = 100;
		
		
		
		//echo 475;exit;
		//echo ' ::: '.$newaddress;exit;
		return view('index', compact('search_keyword','newaddress','trendingTopics','perpage','header_banner_file','sidebar_banner_file','banner_content','destination_url','banner_postedby'));
    }
	
	public function index_old(Request $request)
    {
		$search_keyword = $request->search_keyword;
		$search_keyword = substr($search_keyword,1);
		echo 'Search Keyword :: '.$search_keyword.'<br><br>';
		$sql = "SELECT * FROM 'topics' WHERE title LIKE '%".$search_keyword."%' or message LIKE '%".$search_keyword."%' LIMIT 0,5";
		$res = DB::select($sql);
		foreach($res as $val)
		{
			//echo '<pre>';print_r($val);echo '</pre>';
		}


		//$search_keyword = 'test';
		$trendingTopics = Topic::orderBy('Orderby','DESC')->where('report','<',5)
		->where('title','like','%'.$search_keyword.'%')
		->orWhere('message','like','%'.$search_keyword.'%')
		->with('comments', 'site')->get();	
		$perpage = 10;
		
		//echo 777;
		echo '<pre>';print_r($trendingTopics);
		return view('homepagelist', compact('trendingTopics','pagenum','perpage','header_banner_file','banner_content','destination_url','banner_postedby'));
	}
	
	/*Kus*/	
}
