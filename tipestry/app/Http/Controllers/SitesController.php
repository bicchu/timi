<?php
namespace App\Http\Controllers;
use App\{Site, Comment, History, Log};
use App\Utilities\Domains;
use GuzzleHttp\Exception\ConnectException;
use JonnyW\PhantomJs\Client as  PhantomClient;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Mockery\Exception;

class SitesController extends Controller
{
    /**
     * Process the given url
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function process(Request $request)
    {
	
		
        $url = trim($request->input('url'));
        $extension = substr(strrchr(parse_url($url, PHP_URL_HOST),'.'),1);
		
		//echo $url;exit;
		
		/*
		echo $url.' ** '.$extension.' ++   ';//exit;
		
		if(filter_var($url, FILTER_VALIDATE_URL) === FALSE )
			echo 1;
		if(! $extension)
			echo 2;
		if(! Domains::has($extension))
			echo 3;
		echo '+++';
		exit;*/
		
		
		$pos 	= strpos($url,'wikipedia.org');
		//$url 	= 'webucator.com';
		if(FALSE)
		if(empty($pos))
		{
			if(filter_var($url, FILTER_VALIDATE_URL) === FALSE || ! $extension || ! Domains::has($extension))
			{
				$url = $this->normalizeUrl($url);
				//echo '+++'.$url;exit;
				//DB::enableQueryLog();
				$site = Site::where('url', 'LIKE', '%' . $url . '%')->first();
				//print_r(DB::getQueryLog());exit;
				if (! $site) {
					//echo 'here 01';exit;
					return redirect('/')->with('message', 'Invalid site.');
				}
				//echo 'here 02 :  '.$site->token;exit;
				//return commented on 04-dec-2017.. echo 2;exit;
				return redirect()->action('SitesController@index', $site->token);
			}
		}
		
		
		//echo 'here 03';exit;
		$client = new Client(['defaults' => [
            'verify' => false
        ]]);
        if (! preg_match('(https?://)', $url)) {
            $url = $this->isSecured('http://' . $url) ? 'https://' . $url : 'http://' . $url;
        }
		
		/*echo '7777';exit;*/
		//$url_list[] = 'google.com';
		//$url_list[] = 'google.com.au';
		$url_list[] = 'google.com';
		$url_list[] = 'facebook.com';
		$url_list[] = 'yahoo.com';
		if(in_array($url,$url_list))
		{
			try {
				$response = $client->get($url);
				if (key_exists('X-Frame-Options', $response->getHeaders())) {
					throw new Exception;
				}
			} catch (ConnectException $ex) {
				return redirect('/')->with('message', 'Invalid site.');
			} catch (Exception $ex) {
				$siteScreen = $this->captureScreen($url);
			}
		}
		/*echo '888';exit;*/
        $url = $this->normalizeUrl($url);
        $site = Site::firstOrCreate(
            ['url' => $url],
            ['url' => $url, 'token' => str_random(strlen($url)), 'screen_path' => $siteScreen ?? null]
        );
        //echo '...'.$site;exit;
        return redirect()->action('SitesController@index', $site->token);
    }

    /**
     * Normalize the given url to the particular standard
     *
     * @param $url
     * @return mixed|string
     */
    protected function normalizeUrl($url)
    {
		//$a="dddd";
		//echo "kkkkkkkkkkkk";exit;
		
        $url = rtrim(preg_replace("(www.)", "", $url), '/');
		return $url;
		//Kus Software : strtolower($url);  because wikipedia issue
		//404 https://en.wikipedia.org/wiki/target_corporation
		// fine https://en.wikipedia.org/wiki/Target_Corporation
        //return strtolower($url);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index(Site $site)
    {
		$comments = $site->comments()
                         ->where('parent_id', 0)
                         ->with('children')
                         ->withCount('votes')
                         ->get();
		
        if (auth()->check()) {
            $this->recordToHistory($site);
        }
		
		//echo '<pre>';print_r($site->url);exit;
        return view('site.index', compact('site', 'comments'))->withHeaders('X-Frame-Options', 'ALLOWALL');
    }

    /**
     * Record the specified site to the history
     *
     * @param Site $site
     * @return mixed
     */
    public function recordToHistory(Site $site)
    {
        return History::create([
            'user_id' => auth()->user()->id,
            'site_id' => $site->id,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addComment(Request $request, Site $site)
    {
		$this->validate($request, [
            'content' => 'required|min:2|max:2000'
        ]);

        $comment = $site->comments()->save(new Comment(array_merge(
            $request->all(), ['user_id' => auth()->user()->id]
        )));

        $this->logActivity($site, 'Comment: ' . $comment->content);

        return redirect()->back();
    }

    /**
     * Write a reply to some comment
     *
     * @param Request $request
     * @param Site $site
     * @param Comment $comment
     * @return \Illuminate\Http\RedirectResponse
     */
    public function replyComment(Request $request, Site $site, Comment $comment)
    {
        $this->validate($request, [
            'content' => 'required|min:2|max:255'
        ]);

        $comment = $site->comments()->save(new Comment(array_merge(
            $request->all(), ['user_id' => auth()->user()->id, 'parent_id' => $comment->id]
        )));

        $this->logActivity($site, 'Comment: ' . $comment->content);

        return redirect()->back();
    }

    /**
     * Log activity into the database
     *
     * @param $site
     * @param $message
     * @return mixed
     */
    public function logActivity($site, $message)
    {
        return Log::create([
            'user_id' => auth()->user()->id,
            'site_id' => $site->id,
            'message' => $message
        ]);
    }

    /**
     * Capture website's screen
     *
     * @return bool
     */
    private function captureScreen($url)
    {
        $client = PhantomClient::getInstance();
        $client->getEngine()->setPath('../bin/phantomjs');

        $width  = '1366';
        $height = '768';
        $top    = 0;
        $left   = 0;

        $imagePath = 'captures/' . str_random(12) . '.jpg';

        /**
         * @see JonnyW\PhantomJs\Http\CaptureRequest
         **/
        $request = $client->getMessageFactory()->createCaptureRequest($url, 'GET');
        $request->setOutputFile('../public/' . $imagePath);
        $request->setViewportSize($width, $height);
        $request->setCaptureDimensions($width, $height, $top, $left);

        /**
         * @see JonnyW\PhantomJs\Http\Response
         **/
        $response = $client->getMessageFactory()->createResponse();

        // Send the request
        if ($client->send($request, $response)) {
            return $imagePath;
        }

        return false;
    }

    /**
     * Check if the given url is secured - https
     *
     * @param $url
     * @return bool
     */
    public function isSecured($url)
    {
        $stream = stream_context_create (array("ssl" => array("capture_peer_cert" => true)));
        $read = fopen($url, "rb", false, $stream);
        $cont = stream_context_get_params($read);
        return isset($cont["options"]["ssl"]["peer_certificate"]);
    }
	
	
	public function coinvertprice($coin,$amt)
	{
		$api_url 		= 'https://api.coinmarketcap.com/v1/ticker/'.$coin.'/?convert=USD';
		$client 		= new \GuzzleHttp\Client();
		$request 		= $client->get($api_url);
		$data 			= $request->getBody();
		$data			= json_decode($data);
		
		//print_r($data);exit;
		$price_usd		= $data[0]->price_usd;
		$converted_amt	= $amt/$price_usd;	
		$price			= number_format($converted_amt,2);
		echo $price;exit;
		//echo $data[0]->market_cap_usd;
	}
}
