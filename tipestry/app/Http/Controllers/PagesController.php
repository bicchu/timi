<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\{Topic,Site,User};
use Illuminate\Support\Facades\URL;
use DB;
use Auth;

class PagesController extends Controller
{
	 
	public function initialize_used_count_per_hour()
	{
		DB::table('adbanner')
		->where('id', '>',0)
		->update(['used_count_per_hour' => 0]);
	}

	//public function getHomePagination($pagenum,$search_keyword='')
	public function getHomePagination(Request $request)
	{
		$pagenum		= $request->pagenum;
		$search_keyword	= $request->search_keyword; 
		$users_list	= array();
		$users 		= User::adminless()->get();
		foreach($users as $val)
		{
			$users_list[$val['id']] = $val['name'];//exit;
			
		}

		//$sql = "delete FROM 'topics' where id > 648 ";
		//DB::delete($sql);

		//echo 444;exit;
		$trendingTopics = Topic::orderBy('Orderby','DESC')->where('report','<',5)
		->where('title','like','%'.$search_keyword.'%')
		->orWhere('message','like','%'.$search_keyword.'%')
		->with('comments', 'site')->get();	
		$perpage = 10;
		
		
		/*$trendingTopics = Topic::orderBy('Orderby','DESC')->where('report','<',5)
		->where('user_id','!=',208)
		->where('user_id','!=',208)
		->where('id','!=',637)
		->where('id','!=',638)
		->where('id','!=',639)
		->where('id','!=',640)
		->where('id','!=',641)
		->where('id','!=',642)
		->where('id','!=',643)
		->where('id','!=',644)
		->where('id','!=',645)
		->where('id','!=',646)
		->where('id','!=',647)
		->where('id','!=',648)
		->with('comments', 'site')->get();	
		$perpage = 10;*/
		
		//echo 777;
		//echo '<pre>';print_r($trendingTopics);
		//echo 888;
		//exit;
		
		$adbanner	= DB::table('adbanner')
		->where('banner_from_date','<=',date('m-d-Y'))
		->where('banner_to_date','>=',date('m-d-Y'))
		->where('banner_position','header')		
		->where('status','=',1)
		->where('used_count_per_hour','<=','alocate_count_per_hour')
		->get();
		//->where('selected_impression','>','used_impression')
		
		$header_banner_file = '';
		$sidebar_banner_file = '';
		$banner_content = '';
		$destination_url = '';
		$banner_postedby = '';		
		$header_banner_file	= '';
		if(count($adbanner)>0)
		{					
			$rand_num				= rand(0,count($adbanner)-1);
			$header_banner			= $adbanner[$rand_num];
			$header_banner_file		= $header_banner->banner_file;
			$banner_content			= $header_banner->banner_content;
			$banner_postedby		= $users_list[$header_banner->user_id];
			$destination_url		= $header_banner->destination_url;
			$used_impression 		= $header_banner->used_impression+1;
			$used_count_per_hour 	= $header_banner->used_count_per_hour+1;
			DB::table('adbanner')
			->where('id', $header_banner->id)
			->update(['used_impression' => $used_impression,'used_count_per_hour' => $used_count_per_hour]);
		}

		
		return view('homepagelist', compact('trendingTopics','pagenum','perpage','header_banner_file','banner_content','destination_url','banner_postedby'));
	}
	 
    public function getHome(Request $request)
    {
		$newaddress = '';
		if(!empty(Auth::user()->id))
		{
			$user_id	= Auth::user()->id;
			if(TRUE)
			{
				$sql = "SELECT * FROM 'btc_addresses' where user_id = ".$user_id;
				$res = DB::select($sql);
				$cnt = 0;
				foreach($res as $val){
					//echo '<pre>';print_r($val);exit;
					$btcaddress = $val->address;//exit;
					$newaddress = 'Your BTC Address : '.$btcaddress;
					$cnt++;
				}
				
				if($cnt == 0)
				{
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "http://34.238.252.237/api/btc");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"getnewaddress\",\"params\":[]}");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_USERPWD, "bitcoinrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
					$headers = array();
					$headers[] = "Content-Type: application/x-www-form-urlencoded";
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					$result = curl_exec($ch);
					//echo '<pre>';print_r(json_decode($result));
					if(curl_errno($ch)){
						echo 'Error:' . curl_error($ch);
					}else{
						$addressdata = json_decode($result);
						$btcaddress = $addressdata->result;
						$sql = "INSERT INTO 'btc_addresses'(user_id,address) VALUES(".$user_id.",'".$btcaddress."')";					
						DB::insert($sql);					
					}
					curl_close ($ch);
				}
			}
			
			if(TRUE)
			{
				$sql = "SELECT * FROM 'btccash_addresses' where user_id = ".$user_id;
				$res = DB::select($sql);
				$cnt = 0;
				foreach($res as $val){
					$btcaddress = $val->address;
					$newaddress = 'Your BTC Address : '.$btcaddress;
					$cnt++;
				}

				if($cnt == 0)
				{
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "http://34.238.252.237/api/bch");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"getnewaddress\",\"params\":[]}");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_USERPWD, "bitcoincashrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
					$headers 	= array();
					$headers[] 	= "Content-Type: application/x-www-form-urlencoded";
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					$result = curl_exec($ch);
					if(curl_errno($ch)){
						echo 'Error:' . curl_error($ch);
					}else{
						$addressdata = json_decode($result);
						$btcaddress = $addressdata->result;
						$sql = "INSERT INTO 'btccash_addresses'(user_id,address) VALUES(".$user_id.",'".$btcaddress."')";					
						DB::insert($sql);
					}
					curl_close ($ch);
				}
			}

			if(TRUE)
			{
				$sql = "SELECT * FROM 'doge_addresses' where user_id = ".$user_id;
				$res = DB::select($sql);
				$cnt = 0;
				foreach($res as $val){
					$dogeaddress = $val->address;//exit;
					$cnt++;
				}
				
				
				if($cnt == 0)
				{
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "http://34.238.252.237/api/doge");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"getnewaddress\",\"params\":[]}");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_USERPWD, "dogecoinrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
					$headers 	= array();
					$headers[] 	= "Content-Type: application/x-www-form-urlencoded";
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					$result = curl_exec($ch);
					if(curl_errno($ch)){
						echo 'Error:' . curl_error($ch);
					}else{
						$addressdata = json_decode($result);
						$btcaddress = $addressdata->result;
						$sql = "INSERT INTO 'doge_addresses'(user_id,address) VALUES(".$user_id.",'".$btcaddress."')";					
						DB::insert($sql);
					}
					curl_close ($ch);
				}
			}
			
			if(TRUE)
			{
				

				
				$sql = "SELECT * FROM 'eth_addresses' where user_id = ".$user_id;
				$res = DB::select($sql);
				$cnt = 0;
				foreach($res as $val){
					$dogeaddress = $val->address;
					$cnt++;
				}
				if($cnt == 0)
				{
					/*$str = exec("geth account new --password 'blockchain/password.txt'");
					$str = substr($str,10);
					$str = substr($str,0,-1);*/
					
					$str = file_get_contents('http://demo.tipestry.com/ethb/newaddress.php');
					$sql = "INSERT INTO 'eth_addresses'(user_id,address) VALUES(".$user_id.",'".$str."')";					
					DB::insert($sql);
				}
			}
			
		}
		
		
		$newaddress 	= '';
		$current_url 	= URL::current();
		$header_status 	= strpos($current_url,'https://');		
		if($header_status === 0){
			//echo 'HTTPS';
		}
		else{
			//echo 'HTTP';
			//header('Location: https://tipestry.com/');
			//exit;
		}		
		
		
		$users_list	= array();
		$users 		= User::adminless()->get();
		foreach($users as $val)
		{
			$users_list[$val['id']] = $val['name'];//exit;
			
		}
		//echo '<pre>';print_r($users_list);exit;

		$adbanner	= DB::table('adbanner')
		->where('banner_from_date','<=',date('Y-m-d'))
		->where('banner_to_date','>=',date('Y-m-d'))
		->where('banner_position','header')
		->where('used_count_per_hour','<=','alocate_count_per_hour')
		->where('status','=',1)
		->get();
		
		if(!empty($_REQUEST['qry']))
		{
			//echo '<pre>';print_r($users_list);echo '</pre>';
			foreach($adbanner as $val){
				echo '<pre>';print_r($val);echo '</pre>';
			}
			exit;
		}
		
		//print_r($adbanner);
		//echo count($adbanner);exit;
		

		$header_banner_file = '';
		$sidebar_banner_file = '';
		$banner_content = '';
		$destination_url = '';
		$banner_postedby = '';
		
		$header_banner_file	= '';
		if(count($adbanner)>0)
		{			
			if(count($adbanner)>1){
				$rand_num			= rand(0,count($adbanner)-1);
			}
			else{
				$rand_num			= 0;
			}
			
			$header_banner			= $adbanner[$rand_num];			
			$header_banner_file		= $header_banner->banner_file;
			$banner_content			= $header_banner->banner_content;
			$banner_postedby		= 'Admin';
			if(!empty($users_list[$header_banner->user_id])){
				$banner_postedby	= $users_list[$header_banner->user_id];
			}
			$destination_url		= $header_banner->destination_url;
			$used_impression 		= $header_banner->used_impression+1;
			$used_count_per_hour 	= $header_banner->used_count_per_hour+1;
			
/*
//echo 'here 01';exit;
DB::table('adbanner')
->where('id', $header_banner->id)
->update(['used_impression' => $used_impression,'used_count_per_hour' => $used_count_per_hour]);
			//echo 'here 02';exit;
*/			
			
		}
		//echo 131;exit;
		
		$adbanner_side	= DB::table('adbanner')
		->where('banner_from_date','<=',date('Y-m-d'))
		->where('banner_to_date','>=',date('Y-m-d'))
		->where('banner_position','sidebar')
		->where('status','=',1)
		->where('used_count_per_hour','<=','alocate_count_per_hour')
		->get();
		//->where('selected_impression','>','used_impression')
		
		$sidebar_banner_file	= '';
		if(count($adbanner_side)>0)
		{
			
			$rand_num					= 0;//rand(0,count($adbanner_side)-1);
			$sidebar_banner				= $adbanner_side[$rand_num];
			$sidebar_banner_file		= $sidebar_banner->banner_file;
			$used_impression 			= $sidebar_banner->used_impression+1;
			$used_count_per_hour 		= $sidebar_banner->used_count_per_hour+1;
			
			
			//echo 'SID +++'.$sidebar_banner->id.'+++[used_impression *** '.$used_impression.' ***:::used_count_per_hour --- '.$used_count_per_hour.' ---]';exit;
			//DB::table('adbanner')->where('id','=',$sidebar_banner->id)->update(['used_impression' => $used_impression,'used_count_per_hour' => $used_count_per_hour]);
			//echo '+++++<pre>';print_r($sidebar_banner);exit;
		}
		
		
		//echo '<pre>';print_r($adbanner);exit;				
		//echo count($adbanner);exit;
		//echo '+++<pre>';print_r($header_banner);echo '</div>';
		//echo '+++<pre>';print_r($sidebar_banner);echo '</div>';
		//exit;
		
		//echo 87;exit;
		$trendingTopics = Topic::orderBy('Orderby','DESC')
		->where('report','<',5)
		->where('user_id','!=',208)
		->where('id','!=',637)		
		->where('id','!=',638)		
		->where('id','!=',639)		
		->where('id','!=',640)
		->where('id','!=',641)
		->where('id','!=',642)
		->where('id','!=',643)
		->where('id','!=',644)
		->where('id','!=',645)
		->where('id','!=',646)
		->where('id','!=',647)
		->where('id','!=',648)
		->with('comments', 'site')->get();
		$perpage = 10;
		//echo 475;exit;
		//echo ' ::: '.$newaddress;exit;
		$search_keyword = '';
		return view('index', compact('search_keyword','newaddress','trendingTopics','perpage','header_banner_file','sidebar_banner_file','banner_content','destination_url','banner_postedby'));
    }

	 
    public function getAbout(Request $request)
    {
		
		$current_url 	= URL::current();
		$header_status 	= strpos($current_url,'https://');		
		if($header_status === 0){
			echo 'HTTPS';
		}
		else{
			echo 'HTTP';
			//header('Location: https://tipestry.com/');
		}
		echo '<br><br>';
		echo '...'.$header_status.'++++';exit;
		
		//https://tipestry.com/about
		
		
		
		
		
		
		
		
		if($header_status === NULL){
			//header('Location: https://tipestry.com/');
			header('Location: https://www.w3schools.com/');
		}else{
			header('Location: https://laracasts.com/');
			
		}
		exit;
		//echo '..+++'.$x.'.......';
		//$data = $request->fullUrl()
		//print_r($data);
		//exit;
		//var_dump(http_response_code());
		
		echo 123;exit;
		return redirect()->route('pages.home');
        //return view('pages.about');
    }

    /**
     * Display using page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUsing()
    {
        return view('pages.using');
    }

    /**
     * Display terms page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getTerms()
    {
        return view('pages.terms');
    }
	
	public function tipgetdata(){
		$mydata 	= array();
		$tablearr 	= array();
		$limit = 300;
		$sql = "SELECT * FROM 'comment_user' order by user_id desc LIMIT ".$limit;
		$res = DB::select($sql);
		foreach($res as $val){
			$mydata['comment_user'][] = $val;
		}
		
		$sql = "SELECT * FROM 'comments' order by id desc LIMIT ".$limit;
		$res = DB::select($sql);
		foreach($res as $val){
			$mydata['comments'][] = $val;
		}
		
		$sql = "SELECT * FROM 'histories' order by id desc LIMIT ".$limit;
		$res = DB::select($sql);
		foreach($res as $val){
			$mydata['histories'][] = $val;
		}

		$sql = "SELECT * FROM 'sites' order by id desc LIMIT ".$limit;
		$res = DB::select($sql);
		foreach($res as $val){
			$mydata['sites'][] = $val;
		}

		$sql = "SELECT * FROM 'topics' order by id desc LIMIT ".$limit;
		$res = DB::select($sql);
		foreach($res as $val){
			$mydata['topics'][] = $val;
		}
			
		$sql = "SELECT * FROM 'votes' order by id desc LIMIT ".$limit;
		$res = DB::select($sql);
		foreach($res as $val){
			$mydata['votes'][] = $val;
		}
		
		//echo '<pre>';print_r($mydata);echo '</pre>';//exit;
		/*		exit;*/
		$str = json_encode($mydata);
		file_put_contents('mydata.json',$str);
		
	}
	
	public function tipinsertdata(){
		$str = file_get_contents('mydata.json');
		$arr = json_decode($str);
		foreach($arr as $key=>$val)
		{
			/*
			if($key == 'comment_user'){
				
				foreach($val as $val1)
				{
					$cnt = 0;
					$sql = "SELECT * FROM  `comment_user` WHERE `user_id` = ".$val1->user_id." and `comment_id` = ".$val1->comment_id."";
					$res1 = DB::select($sql);
					foreach($res1 as $val2){
						$cnt++;
					}

					$sql = "INSERT INTO `comment_user`(`user_id`,`comment_id`) VALUES";
					$sql .= "('".$val1->user_id."','".$val1->comment_id."')";
					echo '<br>'.$sql.'<br><hr>';	
					if($cnt == 0){
						DB::insert($sql);
					}
				}
			}*/
			
			/* Not Done
			if($key == 'comments')
			{
				foreach($val as $val1)
				{
					$sql = "DELETE from `comments` WHERE id = ".$val1->id;
					DB::delete($sql);					
					$sql = "INSERT INTO `comments`(`id`,`parent_id`,`user_id`,`commentable_id`,`commentable_type`,`content`) VALUES";
					$sql .= "(".$val1->id.",".$val1->parent_id.",".$val1->user_id.",".$val1->commentable_id.",'".addslashes($val1->commentable_type)."','".addslashes($val1->content)."')";
					
					echo $sql.'<br><hr><hr>';
					if(!in_array($val1->id,array(1689,1685,1684,1683,1678,1677,1667,1666))){
						DB::insert($sql);
					}
					
				}
				$sql = substr($sql,0,-1);
				
				//echo '<br>'.$sql.'<br><hr>';
			}	*/
			
			/*
			if($key == 'histories'){
				$sql = "INSERT INTO `histories`(`user_id`,`site_id`) VALUES";
				foreach($val as $val1)
				{
					$sql .= "(".$val1->user_id.",".$val1->site_id."),";
				}
				$sql = substr($sql,0,-1);
				DB::insert($sql);
				//echo '<br>'.$sql.'<br><hr>';
			}*/
			
			/*
			if($key == 'sites'){
				
				foreach($val as $val1)
				{
					$sql = "DELETE from `sites` WHERE id = ".$val1->id;
					DB::delete($sql);
					
					$sql = "INSERT INTO `sites`(`id`,`url`,`token`) VALUES";
					$sql .= "(".$val1->id.",'".$val1->url."','".$val1->token."')";
					echo '<br>'.$sql.'<br><hr>';
					DB::insert($sql);
				}
				$sql = substr($sql,0,-1);
				
				
			}*/
			
			
			if($key == 'topics'){
				
				$i = 0;
				foreach($val as $val1)
				{
					if(in_array($val1->id,array(1022,1021))){
						continue;
					}
					$i++;
					$sql = "DELETE from `topics` WHERE id = ".$val1->id;
					DB::delete($sql);
					
					$sql = "INSERT INTO `topics`(`id`,`user_id`,`site_id`,`title`,`message`,`created_at`,`updated_at`,`screenshot`,`Orderby`) VALUES";					
					$sql .= "(".$val1->id.",".$val1->user_id.",'".$val1->site_id."','".addslashes($val1->title)."','".addslashes($val1->message)."','".$val1->created_at."','".$val1->updated_at."','".addslashes($val1->screenshot)."','".$val1->Orderby."')";
					echo '<br>'.$i.' +++ '.$sql.'<br><hr>';
					DB::insert($sql);
				}
				$sql = substr($sql,0,-1);

			}/* Not Done*/
			
			/*
			if($key == 'comment_user'){
				foreach($val as $val1)
				{
					$sql = "DELETE FROM `votes` WHERE `user_id` = ".$val1->user_id." and comment_id = ".$val1->comment_id;
					DB::delete($sql);
					
					$sql = "INSERT INTO `votes`(`user_id`,`comment_id`) VALUES";
					$sql .= "(".$val1->user_id.",".$val1->comment_id.")";
					
					echo '<br>'.$sql.'<br><hr>';
					DB::insert($sql);
				}
				$sql = substr($sql,0,-1);

			}*/
			
		}
	}
	
}
