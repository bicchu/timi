<?php
namespace App\Http\Controllers;
use Auth;
use App\{Topic, Site, Comment};
use Illuminate\Http\Request;
use DB;

class NotificationController extends Controller
{
    public function notification()
    {
        $notification = array();
		$type = request()->input('type', 'unread');
		
		if($type == 'unread'){
			$notification = DB::table('notification')->select('*')->where('status', '=', 1)->get();
		}else{
			$notification = DB::table('notification')->select('*')->where('status', '=', 2)->get();
		}
		return view('user.notification', compact('notification'));
    }
	
	public function notificationread(Request $req){
		DB::table('notification')->where('id',$req->id)->update(['status' => 2]);
		echo 1;
	}
}