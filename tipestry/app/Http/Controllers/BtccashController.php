<?php
namespace App\Http\Controllers;
use App\CommentGive;
use App\{User, BtcAddress, Transaction, Comment, Gift, Topic};
use App\Currencies\BtcCurrency;
use Illuminate\Http\Request;
use DB;
use Auth;

class BtccashController extends Controller
{
    public function __construct()
    {
		$this->middleware('auth');
    }

	
    public function give($type, $model, User $user, $amount, $network_fee)
    {
		$amount				= floatval($amount);
		$giftedby_userid	= Auth::user()->id;
		$btcbalance 		= 0;
		if($giftedby_userid)
		{
			$sql = "select * from btccash_addresses where user_id = ".$giftedby_userid;//"address";		
			$res = DB::select($sql);
			$btcaddress = '';
			foreach($res as $val)
			{
				$btcaddress = $val->address;
				if($btcaddress != '')
				{
					$walletid	= $btcaddress;
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "http://34.238.252.237/api/bch");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"getreceivedbyaddress\",\"params\":[\"".$walletid."\"]}");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_USERPWD, "bitcoincashrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
					$headers = array();
					$headers[] = "Content-Type: application/x-www-form-urlencoded";
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

					$result = curl_exec($ch);
					if (curl_errno($ch)) {
						echo 'Error:' . curl_error($ch);
					}else{
						//echo '<pre>';print_r(json_decode($result));exit;
						$balancedata 	= json_decode($result);
						if($balancedata->result>0){
							$btcbalance		+= $balancedata->result;
						}
					}
					curl_close ($ch);		
				}	
			}
			
			//echo $btcbalance.':::';exit;

			$sql = "select sum(`amount`) as received_amt from `walletdetails` where transactiontype = 'received' and wallettype = 'bchcoin' and userid = ".$giftedby_userid;
			$res = DB::select($sql);
			$btcaddress = '';
			foreach($res as $val){
				$btcbalance += $val->received_amt;
			}
			
			$sql = "select sum(`amount`) as received_amt from `walletdetails` where transactiontype in ('gifted','withdrawal') and wallettype = 'bchcoin' and userid = ".$giftedby_userid;
			$res = DB::select($sql);
			$btcaddress = '';
			foreach($res as $val){
				$btcbalance -= $val->received_amt;
			}
			
			//echo 'here :::** '.$btcbalance;exit;		
			if($btcbalance >= $amount)
			{
				if ($type == 'website')
				{	
					$sql 		= "SELECT * FROM 'topics' where id = ".$model." LIMIT 1";
					$res 		= DB::select($sql);
					$site_id	= 0;
					foreach($res as $val){
						$site_id	= $val->site_id;
					}
					
					if($site_id > 0){
						$transactionid = time().rand(999,9999);
						$sql = "insert into `walletdetails`(userid,site_id,transactiontype,wallettype,amount,transactionid)
						values('0','".$site_id."','received','bchcoin',".$amount.",'".$transactionid."')";				
						//echo $sql;exit;
						DB::insert($sql);
						
						$sql = "insert into `walletdetails`(userid,site_id,transactiontype,wallettype,amount,transactionid)
						values('".$giftedby_userid."','".$site_id."','gifted','bchcoin',".$amount.",'".$transactionid."')";
						DB::insert($sql);
					}					
				}
				else
				{				
				
				
				$transactionid = time().rand(999,9999);
				/*$sql = "insert into `walletdetails`(userid,transactiontype,wallettype,amount,transactionid)
				values('".$user->id."','456','received','bitcoin',".$amount.",'".$transactionid."')";*/



				$sql = "insert into `walletdetails`(userid,transactiontype,wallettype,amount,transactionid)
				values('".$user->id."','received','bchcoin',".$amount.",'".$transactionid."')";				
				//echo $sql;exit;
				DB::insert($sql);
				
				$sql = "insert into `walletdetails`(userid,transactiontype,wallettype,amount,transactionid)
				values('".$giftedby_userid."','gifted','bchcoin',".$amount.",'".$transactionid."')";
				DB::insert($sql);				

				$txt = 'Notification : You received '.number_format($amount,8).' BCH on Date '.date('d-m-Y').'.';
				DB::table('notification')->insert(['user_id' => $user->id, 'message' => $txt, 'status'=>1]);
				
				}
				
				/** Start : Modified for cnt gift ***/
				if ($type == 'comments'){										
					Comment::findOrFail($model)->gifts()->save(new Gift([
						'currency' => 'bch',
						'amount' => $amount,
						'user_id' => auth()->user()->id
					]));
					//echo 'type :::::::::'.$type.' model : '.$model;exit;
				}

				if ($type == 'topics') {
					Topic::findOrFail($model)->gifts()->save(new Gift([
						'currency' => 'bch',
						'amount' => $amount,
						'user_id' => auth()->user()->id
					]));
				}				
				/** End : Modified for cnt gift ***/

				return response()->json([
					'status' => 'success',
					'data' => null,
					'message' => 'You successfully gifted coins!'
				], 201);			
			}			
			
		}
		
		return response()->json([
			'status' => 'error',
			'data' => null,
			'message' => 'Sorry, You do not have sufficient balance.'
		]);  
    }	
	
	
	public function withdraw(Request $request)
    {
		request()->amount	= floatval(request()->amount);
		$bchaddress 	= request()->recipient;
		$bchamt			= request()->amount;
		$network_fee	= 0.00001;
		
		// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://34.238.252.237/api/bch");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"sendtoaddress\",\"params\":[\"".$bchaddress."\",".$bchamt."]}");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_USERPWD, "bitcoincashrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
		$headers = array();
		$headers[] = "Content-Type: application/x-www-form-urlencoded";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$transactiondata = curl_exec($ch);
		if (curl_errno($ch)) {
			$withdrawresult['message']	= 'Error:' . curl_error($ch);
		}
		curl_close ($ch);

		
		//echo $transactiondata;
		$transactiondata = json_decode($transactiondata);
		//echo '<br><hr><br>';
		//echo '<pre>';print_r($transactiondata);echo '</pre>';
		

		if(!empty($transactiondata->result) && $transactiondata->result != ''){
			$explorertransactionid = $transactiondata->result;
			$giftedby_userid	= Auth::user()->id;
			$sql = "insert into `walletdetails`(userid,transactiontype,wallettype,amount,network_fee,transactionid,receive_wallet_id)
			values('".$giftedby_userid."','withdrawal','bchcoin',".$bchamt.",".$network_fee.",'".$explorertransactionid."','".$bchaddress."')";
			DB::insert($sql);
			
			$withdrawresult['status']	= 1;
			$withdrawresult['message']	= 'BCH Withdrawal Successful.<br>Tx Hash :: '.$transactiondata->result.'<br>';
		}
		else{
			$withdrawresult['status']	= 0;
			$error 		= $transactiondata->error;
			$message 	= $error->message;
			$withdrawresult['message']	= $message;
		}
		
		//echo '<pre>';print_r($withdrawresult);echo '</pre>';
		$request->session()->put('withdrawresult',$withdrawresult);
		
		//echo '<hr><hr>HERE<hr>';
		//exit;
		return redirect()->back();		
    }	
	
	
	
    public function index(Request $request)
    {
		//echo "BTC CONTROLLER";exit ;		
		$user_id	= Auth::user()->id;
		$sql = "SELECT * FROM 'walletdetails' where userid='".$user_id."'";
		$res = DB::select($sql);
		$cnt = 0;
		$spent_amount = 0;
		foreach($res as $val){
			$spent_amount += $val->amount;
		}
		$btcBalance = 0;
		$walletid	= '';
		$sql = "SELECT * FROM 'btccash_addresses' where user_id = ".$user_id." order by id asc limit 1";
		$res = DB::select($sql);
		$cnt = 0;
		
		foreach($res as $val){
			$walletid = $val->address;
		}	
		
		if($walletid != '')
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "http://34.238.252.237/api/bch");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"getreceivedbyaddress\",\"params\":[\"".$walletid."\"]}");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_USERPWD, "bitcoincashrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
			$headers = array();
			$headers[] = "Content-Type: application/x-www-form-urlencoded";
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);
			if (curl_errno($ch)) {
				echo 'Error:' . curl_error($ch);
			}else{
				//echo '<pre>';print_r(json_decode($result));exit;
				$balancedata 	= json_decode($result);
				if($balancedata->result>0){
					$btcBalance		+= $balancedata->result;
				}
			}
			curl_close ($ch);
		}
		
	
		$sql = "select sum(`amount`) as received_amt from `walletdetails` where transactiontype = 'received' and wallettype = 'bchcoin' and userid = ".$user_id;
		$res = DB::select($sql);
		$btcaddress = '';
		foreach($res as $val){
			$btcBalance += $val->received_amt;
		}
		
		$sql = "select sum(`amount`) as received_amt from `walletdetails` where transactiontype in ('gifted','withdrawal') and wallettype = 'bchcoin' and userid = ".$user_id;
		$res = DB::select($sql);
		$btcaddress = '';
		foreach($res as $val){
			$btcBalance -= $val->received_amt;
		}		
		
		
		
		/*$walletbalance = new App\Currencies\BtcCurrency->getBalance(auth()->user()->btcAddressesString());
		echo '::::'.$walletbalance;exit;*/
		/*$sql = "select * from wallet_transaction where to_wallet_id = '".$walletid."'";
		$res = DB::select($sql);
		foreach($res as $val){
			//echo '<pre>';print_r($val->amount);echo '</pre>';exit;
			if($val->amount>0)
			$btcBalance += $val->amount;
		}
		
		$sql = "select * from wallet_transaction where from_wallet_id = '".$walletid."'";
		$res = DB::select($sql);
		foreach($res as $val){
			
			$btcBalance -= $val->amount;
		}		
		*/
		
		
		
		
		//echo $btcBalance;exit;
		$session_data 	= $request->session()->all();
		
		$withdraw_msg = '';
		if(!empty($session_data['withdraw_msg'])){
			$withdraw_msg = $session_data['withdraw_msg'];
			$request->session()->forget('withdraw_msg');
		}


		$session_data = $request->session()->all();
		$withdraw_msg = array();
		if(!empty($session_data['withdrawresult'])){
			$withdraw_msg = $session_data['withdrawresult'];			
			$request->session()->forget('withdrawresult');
		}		
		//echo $btcBalance = $btcBalance-$spent_amount;exit;
		return view('currencies.btccash.index',compact('walletid','withdraw_msg','btcBalance'));
    }

 
	
	
	public function withdraw_old(Request $request)
    {
		$bchaddress 	= request()->recipient;
		$bchamt			= request()->amount;
		$network_fee	= 0.00001;
		
		// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://34.238.252.237/api/bch");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"sendtoaddress\",\"params\":[\"".$bchaddress."\",".$bchamt."]}");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_USERPWD, "bitcoincashrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
		$headers = array();
		$headers[] = "Content-Type: application/x-www-form-urlencoded";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);

		echo $result;exit;

		
		$giftedby_userid	= Auth::user()->id;
		$sql = "insert into `walletdetails`(userid,transactiontype,wallettype,amount,network_fee,transactionid,receive_wallet_id)
		values('".$giftedby_userid."','withdrawal','bitcoin',".$bchamt.",".$network_fee.",'".$explorertransactionid."','".request()->recipient."')";
		DB::insert($sql);
		echo '<br>++++++++++++++++++++++++++++<br>';exit;
		return redirect()->back();		
    }

    public function regenerate()
    {
        auth()->user()->btcAddresses()->save(new BtcAddress([
            'address' => (new BtcCurrency)->generateKey()
        ]));

        return redirect()->back();
    }
    
    
	
	
	
	public function give_old($type, $model, User $user, $amount, $network_fee)
    {
		//echo $giftedby_userid = 260;exit;
		echo $giftedby_userid	= Auth::user()->id;
		
		exit;
		
		$btcbalance = 0;
		if(TRUE)
		{
			//Auth()::user->id;
			$sql = "select * from btc_addresses where user_id = ".$giftedby_userid;//"address";		
			//exit;//echo $sql.'<br><br>';//exit;
			$res = DB::select($sql);
			$btcaddress = '';
			foreach($res as $val)
			{
				$btcaddress = $val->address;		
				//echo ' BTC Address : '.$btcaddress;
				//$btcaddress = '2Msm8fjJ7Fjy377NL8BQh2Wy1YnhGqc1Kec';
				
				if($btcaddress != '')
				{
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "http://34.238.252.237/api/bch");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"getreceivedbyaddress\",\"params\":[\"".$btcaddress."\"]}");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_USERPWD, "bitcoinrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
					$headers = array();
					$headers[] = "Content-Type: application/x-www-form-urlencoded";
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					$result = curl_exec($ch);
					$btcdata = json_decode($result);
					//echo '<pre>';print_r($btcdata);exit;
					$btcbalance += $btcdata->result;
					if (curl_errno($ch)) {
						echo 'Error:' . curl_error($ch);
					}
					curl_close ($ch);			
				}	
			}
		}
		//echo 'here ::: '.$btcbalance;exit;
		
		if($btcbalance >= $amount)
		{
			
			$sql = "select * from btc_addresses where user_id = ".$user->id." and address != '' ";
			$res = DB::select($sql);
			$giftedto_btcaddress = '';
			foreach($res as $val)
			{
				$giftedto_btcaddress = $val->address;
				break;
			}
			
			if($giftedto_btcaddress != '')
			{

				/*$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "http://34.238.252.237/api/bch");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"sendtoaddress\",\"params\":[\"".$giftedto_btcaddress."\",".$amount."]}");
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_USERPWD, "bitcoinrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
				$headers = array();
				$headers[] = "Content-Type: application/x-www-form-urlencoded";
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				$result = curl_exec($ch);
				//echo '+++<pre>';print_r(json_decode($result));
				if (curl_errno($ch)) {
					echo 'Error:' . curl_error($ch);
				}
				curl_close ($ch);
		
				
				$sql = "insert into `walletdetails`(userid,walletaddressid,transactiontype,amount)
				values('".$giftedby_userid."','123','gifted',".$amount.")";
						
						
				//echo $sql; //exit;
				DB::insert($sql);
				
		
				
				return response()->json([
					'status' => 'success',
					'data' => null,
					'message' => 'You successfully gifted coins!'
				], 201);*/
			}
		}
		
		return response()->json([
			'status' => 'error',
			'data' => null,
			'message' => 'Sorry, something went wrong.'
		]);   
		
    }
	
	/*Kus*/
    public function estimated_network_fee(User $user,$amtx)
    {
        $estimated_network_fee = 0;
		$userAddress =  $user->btcAddressDefault();
        try {
			$response = (new BtcCurrency)->get_network_fee_estimate($amtx,$userAddress);			
			$estimated_network_fee = $response->data->estimated_network_fee;
			if($response->status == 'success'){
			
			}
			else{
				//echo '<pre>';print_r($response);
			}					
		} catch (Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'Sorry, something went wrong.'
            ]);
        }

		$res = response()->json([
            'status' => 'success',
            'fee' => $estimated_network_fee,
            'message' => 'Estimated network fee.'
        ], 201);
		
		return $res; /* */		
		//return 40005;
	}

    public function user_balance()
    {
        $available_balance = 0;
		$userAddress =  auth()->user()->btcAddressDefault();
		try {
			$available_balance = (new BtcCurrency)->getBalance($userAddress);
		} catch (Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'Sorry, something went wrong.'
            ]);
        }	
		return $available_balance;
	}
	
	/*Kus*/	
}
