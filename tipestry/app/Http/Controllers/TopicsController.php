<?php

namespace App\Http\Controllers;
use Auth;
use App\{Topic, Site, Comment};
use Illuminate\Http\Request;
use DB;

class TopicsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

	
	
	public function updatetopic(Request $request)
	{
		//echo 123;exit;
		$id 	= $request->id;
		$title 	= $request->title;
		$msg 	= $request->msg;
		$sql 	= "UPDATE 'topics' SET title = '".$title."', message = '".$msg."' WHERE id = ".$id;
		//$sql 	= "UPDATE `topics` SET title = '$title' , msg = '$msg' WHERE id = $id ";
		echo $sql;
		DB::update($sql);
		echo 1;
	}
	
	public function update_Comment_message(Request $request)
	{
		$id 	= $request->id;
		$msg 	= $request->msg;
		$sql 	= "UPDATE 'comments' SET  content = '".$msg."' WHERE id = ".$id;
		DB::update($sql);
		echo 1;
	}	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function gift(Request $request)
    {
        return $request->all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Site $site)
    {
		$this->validate(request(), [
            'title' => 'required|min:2|max:200'
        ]);	
		
		$siteURL 	= $site->url;
		$siteData 	= $siteURL; //parse_url($siteURL);
		//$siteHost	= $siteData['host'];
		$cnt 		= DB::table('websitelist')->select('*')->where('websitedomain', '=', $site->url)->count();
		if($cnt == 0){
			DB::table('websitelist')->insert(['websitedomain' => $site->url, 'totalupvote' => 0, 'totaldownvote' => 0]);
		}
		//echo 777;exit;
/*
echo $siteURL.'<br><br>';//exit;
	$siteURL = "https://www.barrons.com/articles/jpmorgans-dimon-softens-on-bitcoin-1515539943";
	$googlePagespeedData = file_get_contents("https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url=".$siteURL."&screenshot=true");
	$googlePagespeedData = json_decode($googlePagespeedData, true);
	$screenshot = $googlePagespeedData['screenshot']['data'];
	$screenshot = str_replace(array('_','-'),array('/','+'),$screenshot); 
	echo "<img src=\"data:image/jpeg;base64,".$screenshot."\" />";exit;

*/


$screenshot = '';
if(TRUE)
{
	
	$siteURL = $request->mysiteurl;
	if($siteURL == ""){
		
		$siteURL = $site->url;
	}
	
	@$googlePagespeedData = file_get_contents("https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url=".$siteURL."&screenshot=true");
	@$googlePagespeedData = json_decode($googlePagespeedData, true);
	@$screenshot = $googlePagespeedData['screenshot']['data'];
	@$screenshot = str_replace(array('_','-'),array('/','+'),$screenshot); 
	//echo "<img src=\"data:image/jpeg;base64,".$screenshot."\" />";exit;
	//
}
        //echo '<pre>';print_r($request->message);exit;
		$topic = $site->addTopic(new Topic(array_merge(
            $request->all(),
            ['user_id' => auth()->user()->id, 'screenshot' => $screenshot, 'Orderby'=>time()]
        )));
		
		

        return redirect()->action('TopicsController@show', [$site, $topic]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Site $site, Topic $topic)
    {
		
		$websitegives = array();
		$sql = "SELECT sum(amount) sum, wallettype  FROM 'walletdetails' where transactiontype = 'received' and site_id = ".$site->id." group by wallettype  order by wallettype desc";		
		$res = DB::select($sql);
		foreach($res as $val){
			$websitegives[] = $val;
		}/**/
		
		
		//echo '<pre>';print_r($websitegives);echo '</pre>';
		//exit;
		//echo '<pre>';print_r($site->id);echo '</pre>';exit;
		//DB::enableQueryLog();
		
		//DB::table('websitelist')->truncate();
		
		$siteData 	= parse_url($site->url);
		$siteHost	= $siteData;//$siteData['host'];
		
		$cnt 		= DB::table('websitelist')->select('*')->where('websitedomain', '=', $site->url)->count();
		
		if($cnt == 0){
			DB::table('websitelist')->insert(['websitedomain' => $site->url, 'totalupvote' => 0, 'totaldownvote' => 0]);
		}

		$res 		= DB::table('websitelist')->select('*')->where('websitedomain', '=', $site->url)->get();
		$website 	= $res[0];


		
		$reported = 0;
		if(!empty(Auth::user()->id) && Auth::user()->id > 0)
		{
			$reported = DB::table('post_report')
				->select('*')
				->where('user_id', '=', Auth::user()->id)
				->where('topics_id', '=', $topic->id)
				->count();
		}
		/*$comments = $topic->comments()
            ->where('parent_id', 0)			
            ->with('children')
			->with('gifts')
            ->withCount('votes')			
			->orderBy('votes_count','DESC')			
            ->get();*/
		$comments = $topic->comments()
			->where('parent_id', 0)		
            ->with('children')
			->with('gifts')
            ->withCount('votes')			
			->orderBy('votes_count','DESC')			
            ->get();
		
		if(!empty($_REQUEST['test'])){
			echo '<pre>';print_r($site);echo '</pre><pre>';print_r($topic);echo '</pre><pre>';print_r($comments);echo '</pre><pre>';print_r($reported);exit;
		}
		//echo '<pre>';print_r($topic['created_at']);exit;
		return view('site.topics.show', compact('site', 'topic', 'comments','reported','website','websitegives'))->withHeaders('SERVER_PROTOCOL', 'HTTP/1.0');;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addComment(Topic $topic)
    {
		$time 	= time();
		$sql_update = "UPDATE 'topics' SET Orderby = '".$time."' WHERE id = ".$topic->id;
		DB::update(DB::raw($sql_update));		
		
		$this->validate(request(), [
            'content' => 'required|min:2|max:2000'
        ]);

        $comment = $topic->comments()->save(new Comment(array_merge(
            request()->all(), ['user_id' => auth()->user()->id]
        )));

//        $this->logActivity($topic, 'Comment: ' . $comment->content);

        return redirect()->back();
    }

    /**
     * Write a reply to some comment
     *
     * @param Request $request
     * @param Site $site
     * @param Comment $comment
     * @return \Illuminate\Http\RedirectResponse
     */
    public function replyComment(Topic $topic, Comment $comment)
    {
		//echo"thtghtrdhtdhed";die;
		if(empty (Auth::user()->id))
		{
			
			return redirect('/login');
		}
		
		$time 	= time();
		$sql_update = "UPDATE 'topics' SET Orderby = '".$time."' WHERE id = ".$topic->id;
		DB::update(DB::raw($sql_update));
	
        $this->validate(request(), [
            'content' => 'required|min:2|max:2000'
        ]);

        $comment = $topic->comments()->save(new Comment(array_merge(
            request()->all(), ['user_id' => auth()->user()->id, 'parent_id' => $comment->id]
        )));
		
		
		
//        $this->logActivity($site, 'Comment: ' . $comment->content);

        return redirect()->back();
    }

    /**
     * Log activity into the database
     *
     * @param $site
     * @param $message
     * @return mixed
     */
    public function logActivity($site, $message)
    {
        return Log::create([
            'user_id' => auth()->user()->id,
            'site_id' => $site->id,
            'message' => $message
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Topic $topic)
    {
        $topic->comments()->delete();

        $topic->delete();

        return redirect('/');
    }
    /**
     * Report a inappropriate comment
     *
     * @param Comment $comment
     * @return mixed
     */
    public function report(Topic $topic)
    {
		
		
		$sql_update = "UPDATE 'topics' SET report = report + 1 WHERE id = ".$topic->id;
		DB::update(DB::raw($sql_update));
		
		$xdata 	= array('user_id'=>Auth::user()->id,'topics_id'=>$topic->id);
		DB::table('post_report')->insert($xdata);	
		
		//$topic_id   = $topic->id;
		//$data 	= array('user_id'=>$topic_id);				
		// DB::table('report_user')->insert($data);
		/*echo '777';echo '<pre>';print_r($topic);exit;
		$user = Auth::user();
		//echo '888';
		$user->reportTopic();
		//echo '1452';exit;
		*/
		return redirect()->back();
    }	
}
