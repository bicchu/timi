<?php
namespace App\Http\Controllers;
use App\CommentGive;
use App\{User, BtcAddress, Transaction, Comment, Gift, Topic};
use App\Currencies\BtcCurrency;
use Illuminate\Http\Request;
use DB;
use Auth;

class EthdepositController extends Controller
{
    public function __construct()
    {
	}
    
    public function index(Request $request)
    {
		$i = 0;
		while($i<1)
		{
			$i++;
			$walletaddress = '';
			$sql = "select * from eth_addresses order by updated_at ASC limit 1";
			$res = DB::select($sql);
			foreach($res as $val){
				$walletaddress 		= $val->address;			
				$ethapibalance		= $val->ethapibalance;
				$ethtotaldeposit	= $val->ethtotaldeposit;
				$tipapibalance		= $val->tipapibalance;
				$tiptotaldeposit	= $val->tiptotaldeposit;
				$xrtapibalance		= $val->xrtapibalance;
				$xrttotaldeposit	= $val->xrttotaldeposit;
			}
			
			if($walletaddress != '')
			{
				$this->tipcontract 	= '0x1421ede58b8066fab03f6b28a8bf0b507e8cd062';
				$this->xrtcontract 	= '0xd217ad4191bc42b62a3a9ed05f5248795d450af4';
				$this->ethurl 		= 'https://api.tokenbalance.com/token/';
				
				$ethtipurl	= $this->ethurl.$this->tipcontract.'/'.$walletaddress;
				$ethxrturl	= $this->ethurl.$this->xrtcontract.'/'.$walletaddress;
				//echo '<a href="'.$ethtipurl.'">'.$ethtipurl.'</a><hr><br>';
				//echo '<a href="'.$ethxrturl.'">'.$ethxrturl.'</a><hr><br>';
				
				$ethtipfilecontent	= file_get_contents($ethtipurl);
				$ethxrtfilecontent	= file_get_contents($ethxrturl);

				//echo $ethtipfilecontent;echo '<br><hr>';
				//echo $ethxrtfilecontent;echo '<br><hr>';
				
				$ethtipfilecontent	= json_decode($ethtipfilecontent);
				$ethxrtfilecontent	= json_decode($ethxrtfilecontent);
				
				$ethbalance			= $ethtipfilecontent->eth_balance;
				$tipbalance			= $ethtipfilecontent->balance;
				$xrtbalance			= $ethxrtfilecontent->balance;
				
				$ethdeposit = '';
				if($ethbalance>$ethapibalance){
					$ethdeposit = $ethbalance-$ethapibalance;
					$ethdeposit = ', ethtotaldeposit = '.$ethdeposit;
				}
				
				$tipdeposit = '';
				if($tipbalance>$tipapibalance){
					$tipdeposit = $tipbalance-$tipapibalance;
					$tipdeposit = ', tiptotaldeposit = '.$tipdeposit;
				}
				
				$xrtdeposit = '';
				if($xrtbalance>$xrtapibalance){
					$xrtdeposit = $xrtbalance-$xrtapibalance;
					$xrtdeposit = ', xrttotaldeposit = '.$xrtdeposit;
				}
				
				$sql = "UPDATE eth_addresses SET 
				ethapibalance		= ".$ethbalance.$ethdeposit.",			
				tipapibalance		= ".$tipbalance.$tipdeposit.", 			
				xrtapibalance		= ".$xrtbalance.$xrtdeposit.", 
				updated_at			= '".date('Y-m-d H:i:s')."'
				WHERE address 		= '".$walletaddress."'";
				
				DB::update($sql);
				echo $sql.'<br><hr><br>';
				//echo 'Tip Balance :: '.$tipbalance.'<br>';
				//echo 'XRT Balance :: '.$xrtbalance.'<br>';
				//echo 'ETH Balance :: '.$ethbalance.'<br>';
				//print_r($ethtipfilecontent);echo '<br><hr>';
				//print_r($ethxrtfilecontent);
			}
		}
	}
}
