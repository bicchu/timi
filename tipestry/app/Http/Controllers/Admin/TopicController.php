<?php
namespace App\Http\Controllers\Admin;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;


class TopicController extends Controller
{
    public function index()
    {
		
		//$message 		= $request->session()->get('message');
        $pageTitle 		= 'Topics List';
		$topicslist 	= DB::table('topics')->orderBy('banner_from_date', 'DESC')->get();
		return view('admin.topics.index', compact('pageTitle', 'topicslist'));
    }
	
    public function delete(Request $req,$id)
    {
        $topicslist 	= DB::table('topics')->where('id', $id)->delete();
		Session::flash('message', "Topics deleted successfully!");
		return redirect('admin/topics');
    }
	
}