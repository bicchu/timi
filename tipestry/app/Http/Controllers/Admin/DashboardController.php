<?php

namespace App\Http\Controllers\Admin;

use App\{User, Comment, Transaction};
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = 'Dashboard';

        $users = User::count();

        $commentsTotal = Comment::count();
        $commentsToday = Comment::today()->count();

        $givenTotal = Transaction::ofType('given', 'App\Site')
                                    ->sum('amount');

        $givenToday = Transaction::ofType('given', 'App\Site')
                                    ->today()
                                    ->sum('amount');

        $withdrawnTotal = Transaction::ofType('withdrawn', 'App\User')
                                    ->sum('amount');

        $withdrawnToday = Transaction::ofType('withdrawn', 'App\User')
                                    ->today()
                                    ->sum('amount');

        return view('admin.index', compact(
            'pageTitle',
            'users',
            'commentsTotal',
            'commentsToday',
            'givenTotal',
            'givenToday',
            'withdrawnTotal',
            'withdrawnToday'
        ));
    }
}
