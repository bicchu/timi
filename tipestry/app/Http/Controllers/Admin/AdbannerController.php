<?php
namespace App\Http\Controllers\Admin;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class AdbannerController extends Controller
{
    public function index()
    {
        $pageTitle 		= 'Ad Banner List';
		$adbannerlist 	= DB::table('adbanner')->orderBy('banner_from_date', 'ASC')->get();
		$userlist	= DB::table('users')->get();
		$users		= array();
		foreach($userlist as $val){
			$users[$val->id]['name'] 	= $val->name;
			$users[$val->id]['email'] 	= $val->email;
		}		
        return view('admin.adbanner.index', compact('pageTitle', 'adbannerlist','users'));
    }

    public function edit(Request $request,$id = 0)
    {
        $pageTitle 	= 'Ad Banner Details';
		$details	= DB::table('adbanner')->where('id',$id)->get();
		$userlist	= DB::table('users')->get();
		$users		= array();
		
		
		foreach($userlist as $val){
			$users[$val->id]['name'] 	= $val->name;
			$users[$val->id]['email'] 	= $val->email;			
		}
		
		
		foreach($details as $val){
			//if($val->status == 0)echo '++++++++zero';
			//else echo '+++one';
			//exit;
		}
		
		return view('admin.adbanner.edit', compact('pageTitle', 'details','users'));
    }
	
	public function editpost(Request $request,$id = 0)
    {
		//$update_arr['status'] = $request->status;
		
		$bannerid  			= $request->bannerid;
		$bannerstatus  		= $request->bannerstatus;
		$payment_status		= $request->payment_status;
		$banner_position	= $request->banner_position;
		$admin_comment 		= $request->admin_comment;
		if(empty($admin_comment)){
			$admin_comment = '';
		}
		
//echo 'bannerid : '.$bannerid.' ** bannerstatus '.$bannerstatus.' ++ payment_status '.$payment_status.' -- banner_position '.$banner_position.' ... admin_comment '.$admin_comment.' ****';
		
		//echo 4454;exit;
		
		$details		= DB::table('adbanner')->where('id',$bannerid)->update(['status'=>$bannerstatus,'payment_status'=>$payment_status,'banner_position'=>$banner_position,'admin_comment'=>$admin_comment]);
		session()->flash('msg', 'Successfully done the operation.');
		$url = '/admin/adbanner/edit/'.$bannerid;
		return redirect($url);
    }
	
    public function delete(Request $request)
    {
		$bannerid  	= $request->bannerid;
		$details	= DB::table('adbanner')->where('id',$bannerid)->update(['status'=>99]);
		session()->flash('msg', 'Successfully done the operation.');
		$url = '/admin/adbanner/';
		return redirect($url);
    }
	
	public function price()
	{
		$pageTitle 		= 'Ad Banner Price List';
		$pricelist 	= DB::table('adbanner_price')->orderBy('id', 'ASC')->get();
		return view('admin.adbanner.pricelist', compact('pageTitle','pricelist'));
		
	}
	
	public function price_update(Request $req)
	{
		
		$price = $req->input('new_price');
		$id = $req->input('id_new');
		
		
		DB::table('adbanner_price')
		->where('id', '=',$id)
		->update(['price' => $price]);
		
		/*$sql = "update adbanner set price = 450 where id = 2";
		echo $sql;
		//echo 445;
		//$res = DB::update($sql);
		DB::table('adbanner')
		->where('id', '=',$id)
		->update(['price' => $price]);*/
		
		//echo 77;exit;
		
		session()->flash('msg', 'Successfully done the operation.');
		//$url = '/admin/adbanner/';
		$url = '/admin/adbanner-price/';
		return redirect($url);		
	}
		
}







