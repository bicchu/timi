<?php
namespace App\Http\Controllers;

use App\CommentGive;
use App\{User, BtcAddress, Transaction, Comment, Gift, Topic, Adbanner};
use App\Currencies\BtcCurrency;
use Illuminate\Http\Request;
use DB;
use Auth;

class AdbannerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
		$adbanner_price  = DB::table('adbanner_price')->orderBy('location_name', 'ASC')->get();
		//echo '<pre>';print_r($adbanner_price);exit;
		

		
		$user_id	= Auth::user()->id;;
		$adbanners  = DB::table('adbanner')->where('user_id',$user_id)->orderBy('banner_from_date', 'ASC')->get();
		
		//echo '<pre>';print_r($adbanner_price);exit;
		return view('adbanner.index_new',compact('msg','adbanners','adbanner_price'));
		/*//echo $user_id;
		if($user_id == 127)
		return view('adbanner.index_image',compact('msg','adbanners'));
		else
		return view('adbanner.index_new',compact('msg','adbanners'));*/
    }
	
	public function add(Request $request)
    {
		//echo '<pre>';print_r($request);exit;


		
		$insert_arr = array();
		$insert_arr['banner_file'] 			= time().'_'.$request->file('banner_file')->getClientOriginalName();						
		$request->file('banner_file')->move(base_path() . '/public/upload_images/', $insert_arr['banner_file']);
		$insert_arr['banner_content'] 		= $request->banner_content;
		$insert_arr['banner_from_date'] 	= str_replace('/','-',$request->banner_from_date);
		$insert_arr['banner_to_date']		= str_replace('/','-',$request->banner_to_date);
		$insert_arr['user_id']				= Auth::user()->id;;
		$insert_arr['status']				= 0;
		$insert_arr['banner_position']		= $request->banner_position;
		$insert_arr['paid_ammount']			= $request->paid_ammount;
		$insert_arr['payment_status']		= 0;
		$insert_arr['payment_hashcode']		= $request->payment_hashcode;
		$insert_arr['user_comment']			= $request->user_comment;
		$insert_arr['admin_comment']		= '';
		$insert_arr['destination_url'] 		= $request->destination_url;
		$insert_arr['selected_impression'] 	= $request->selected_impression;
		$insert_arr['used_impression'] 		= 0;

		$time_difference 			= strtotime($insert_arr['banner_to_date'])-strtotime($insert_arr['banner_from_date']);
		$no_of_days 				= $time_difference/(24*60*60);
		$no_of_days					= $no_of_days++;
		$alocate_count_per_hour		= $insert_arr['selected_impression']/($no_of_days*24);
		if($alocate_count_per_hour<1){
			$alocate_count_per_hour	= 1;
		}
		$insert_arr['alocate_count_per_hour']	= $alocate_count_per_hour;
		$insert_arr['used_count_per_hour']		= 0;
		//echo '<pre>';print_r($insert_arr);exit;
		
		$insert_arr['banner_from_date'] = date('Y-m-d',strtotime($insert_arr['banner_from_date']));
		$insert_arr['banner_to_date'] = date('Y-m-d',strtotime($insert_arr['banner_to_date']));
		
		DB::table('adbanner')->insert($insert_arr);
		session()->flash('msg', 'Successfully done the operation.');
		
		

		//$sql = "update 'adbanner' set alocate_count_per_hour = 100 where id = 72 ";
		//DB::update($sql);
		//$adbannerlist 	= DB::table('adbanner')->orderBy('id', 'DESC')->get();
		
		//echo '<pre>';print_r($insert_arr);echo '</pre>';
		//echo '<pre>';print_r($adbannerlist);exit;
		
		return redirect()->route('adbanner');

		//$msg = 'Ad Banner request sent successfully!';
		//return view('adbanner.index',compact('msg'));
    }	
}
