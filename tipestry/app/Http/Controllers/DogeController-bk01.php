<?php
namespace App\Http\Controllers;
use App\CommentGive;
use App\{User, DogeAddress, Transaction, Comment, Gift, Topic};
use App\Currencies\DogeCurrency;
use Illuminate\Http\Request;
use DB;
use Auth;

class DogeController extends Controller
{
    /**
     * BtcController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$btcbalance = 0;
		$userid	= Auth::user()->id;
		$sql = "select * from doge_addresses where user_id = ".$userid;//"address";		
		$res = DB::select($sql);
		$btcaddress = '';
		foreach($res as $val)
		{
			$btcaddress = $val->address;
			if($btcaddress != '')
			{
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "http://34.238.252.237/api/doge");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"getreceivedbyaddress\",\"params\":[\"".$btcaddress."\"]}");
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_USERPWD, "dogecoinrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
				$headers = array();
				$headers[] = "Content-Type: application/x-www-form-urlencoded";
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				$result = curl_exec($ch);
				$btcdata = json_decode($result);
				//echo '<pre>';print_r($btcdata);echo '</pre>';exit;
				$btcbalance += $btcdata->result;
				if (curl_errno($ch)) {
					echo 'Error:' . curl_error($ch);
				}
				curl_close ($ch);			
			}	
		}
		
		//echo $btcbalance;exit;
		$sql = "select sum(`amount`) as received_amt from `walletdetails` where transactiontype = 'received' and wallettype = 'dogecoin' and userid = ".$userid;
		$res = DB::select($sql);
		foreach($res as $val){
			$btcbalance += $val->received_amt;
		}
		
		$sql = "select sum(`amount`) as received_amt from `walletdetails` where transactiontype = 'gifted' and wallettype = 'dogecoin' and userid = ".$userid;
		$res = DB::select($sql);
		foreach($res as $val){
			$btcbalance -= $val->received_amt;
		}		
		
		$session_data = $request->session()->all();
		$withdraw_msg = '';
		if(!empty($session_data['withdraw_msg'])){
			$withdraw_msg = $session_data['withdraw_msg'];
			$request->session()->forget('withdraw_msg');
		}
		if($userid == 260){
			$btcbalance -= 100;
		}
		return view('currencies.doge.index',compact('withdraw_msg','btcbalance','btcaddress'));		
		
		/*
		$btcBalance = 0;
		$walletid = Auth::user()->dogeAddressDefault();
				
		
		$sql = "select * from wallet_transaction where to_wallet_id = '".$walletid."'";
		$res = DB::select($sql);
		foreach($res as $val){
			//echo '<pre>';print_r($val->amount);echo '</pre>';exit;
			$btcBalance += $val->amount;
		}
		
		$sql = "select * from wallet_transaction where from_wallet_id = '".$walletid."'";
		$res = DB::select($sql);
		foreach($res as $val){
			$btcBalance -= $val->amount;
		}		
		*/
    }

    /**
     * Withdraw/Send money from one wallet to another
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function withdraw(Request $request)
    {
        

			$giftedby_userid	= Auth::user()->id;
			$sql = "select * from doge_addresses where user_id = ".$giftedby_userid;//"address";		
			$res = DB::select($sql);
			$btcaddress = '';
			foreach($res as $val)
			{
				$btcaddress = $val->address;
				if($btcaddress != '')
				{
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "http://34.238.252.237/api/doge");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"getreceivedbyaddress\",\"params\":[\"".$btcaddress."\"]}");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_USERPWD, "dogecoinrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
					$headers = array();
					$headers[] = "Content-Type: application/x-www-form-urlencoded";
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					$result = curl_exec($ch);
					$btcdata = json_decode($result);
					echo '<pre>';print_r($btcdata);echo '</pre>';exit;
					$btcbalance += $btcdata->result;
					if($btcaddress == 'DBNNQza3g4Q57oRUyu2Fi6kQWdEWmwm7Rf'){
						//$btcbalance -= 100;
					}
					
					if (curl_errno($ch)) {
						echo 'Error:' . curl_error($ch);
					}
					curl_close ($ch);			
				}	
			}
			if($giftedby_userid == 260){
				$btcbalance -= 100;
			}

			echo $btcbalance;exit;
			$sql = "select sum(`amount`) as received_amt from `walletdetails` where transactiontype = 'received' and wallettype = 'dogecoin' and userid = ".$giftedby_userid;
			$res = DB::select($sql);
			$btcaddress = '';
			foreach($res as $val){
				$btcbalance += $val->received_amt;
			}
			
			$sql = "select sum(`amount`) as received_amt from `walletdetails` where transactiontype = 'gifted' and wallettype = 'dogecoin' and userid = ".$giftedby_userid;
			$res = DB::select($sql);
			$btcaddress = '';
			foreach($res as $val){
				$btcbalance -= $val->received_amt;
			}











/*********************************/		
		
		
		
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://34.238.252.237/api/doge");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"sendtoaddress\",\"params\":[\"".request()->recipient."\",".request()->amount."]}");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_USERPWD, "dogecoinrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
		$headers = array();
		$headers[] = "Content-Type: application/x-www-form-urlencoded";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result = curl_exec($ch);
		//echo '+++<pre>';print_r(json_decode($result));
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);	
		return redirect('/currencies/doge');
		
		
		if(false){
		$this->validate(request(), [
            'recipient' => 'required',
            'amount' => 'required|numeric'
        ]);
		
		//echo 'Rec ::: '.request()->recipient.' ::: Amt '.request()->amount;exit;
		
		$bal_amt = $this->user_balance();
		
		if($bal_amt >= request()->amount){
			$response = (new DogeCurrency)->transfer(
				request()->amount,
				auth()->user()->dogeAddressDefault(),
				request()->recipient
			);

			if ($response) {
				/* Commented By Kush 05-dec-2017, getting array merge err on user addTransaction
				auth()->user()->addTransaction(new Transaction([
					'amount' => request()->amount,
					'type' => 'withdrawn',
					'coin' => 'doge'
				]));
				*/
				
				$transaction_arr['amount'] = request()->amount;
				$transaction_arr['type'] = 'withdrawn';
				$transaction_arr['coin'] = 'doge';
				auth()->user()->addTransaction($transaction_arr);				
			}
			
			$request->session()->put('withdraw_msg', 'success');
		}else{
			$request->session()->put('withdraw_msg', 'error');
		}
		
		return redirect('/currencies/doge');
        return redirect()->back();
		}
    }

    /**
     * Generate a new btc wallet address
     *
     * @return string
     */
    public function regenerate()
    {
        auth()->user()->btcAddresses()->save(new DogeAddress([
            'address' => (new DogeCurrency)->generateKey()
        ]));

        return redirect()->back();
    }

    /**
     * Gift someone with some btc coins
     *
     * @param User $user
     * @param $amount
     * @return \Illuminate\Http\JsonResponse
     */






    public function give($type, $model, User $user, $amount, $network_fee)
    {
		$giftedby_userid	= Auth::user()->id;
		$btcbalance 		= 0;
		if($giftedby_userid)
		{
			$sql = "select * from doge_addresses where user_id = ".$giftedby_userid;//"address";		
			$res = DB::select($sql);
			$btcaddress = '';
			foreach($res as $val)
			{
				$btcaddress = $val->address;
				if($btcaddress != '')
				{
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "http://34.238.252.237/api/doge");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"getreceivedbyaddress\",\"params\":[\"".$btcaddress."\"]}");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_USERPWD, "dogecoinrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
					$headers = array();
					$headers[] = "Content-Type: application/x-www-form-urlencoded";
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					$result = curl_exec($ch);
					$btcdata = json_decode($result);
					//echo '<pre>';print_r($btcdata);echo '</pre>';exit;
					$btcbalance += $btcdata->result;
					if($btcaddress == 'DBNNQza3g4Q57oRUyu2Fi6kQWdEWmwm7Rf'){
						$btcbalance -= 100;
					}
					
					if (curl_errno($ch)) {
						echo 'Error:' . curl_error($ch);
					}
					curl_close ($ch);			
				}	
			}

			//echo $btcbalance;exit;
			$sql = "select sum(`amount`) as received_amt from `walletdetails` where transactiontype = 'received' and wallettype = 'dogecoin' and userid = ".$giftedby_userid;
			$res = DB::select($sql);
			$btcaddress = '';
			foreach($res as $val){
				$btcbalance += $val->received_amt;
			}
			
			$sql = "select sum(`amount`) as received_amt from `walletdetails` where transactiontype = 'gifted' and wallettype = 'dogecoin' and userid = ".$giftedby_userid;
			$res = DB::select($sql);
			$btcaddress = '';
			foreach($res as $val){
				$btcbalance -= $val->received_amt;
			}			
			
			//echo 'here ::: '.$btcbalance;exit;		
			if($btcbalance >= $amount)
			{
				//id,userid,transactiontype,walletaddressid,amount,created_at,updated_at,wallettype
				$transactionid = time().rand(999,9999);
				$sql = "insert into `walletdetails`(userid,transactiontype,wallettype,amount,transactionid)
				values('".$user->id."','received','dogecoin',".$amount.",'".$transactionid."')";
				
				/*$sql = "insert into `walletdetails`(userid,transactiontype,wallettype,amount)
				values('".$user->id."','received','dogecoin',".$amount.")";*/
				//echo $sql;exit;
				DB::insert($sql);
				
				$sql = "insert into `walletdetails`(userid,transactiontype,wallettype,amount,transactionid)
				values('".$giftedby_userid."','gifted','dogecoin',".$amount.",'".$transactionid."')";
				
				/*$sql = "insert into `walletdetails`(userid,transactiontype,wallettype,amount)
				values('".$giftedby_userid."','gifted','dogecoin',".$amount.")";*/
				DB::insert($sql);

				/** Start : Modified for cnt gift ***/
				if ($type == 'comments') {
					Comment::findOrFail($model)->gifts()->save(new Gift([
						'currency' => 'doge',
						'amount' => $amount,
						'user_id' => auth()->user()->id
					]));
				}

				if ($type == 'topics') {
					Topic::findOrFail($model)->gifts()->save(new Gift([
						'currency' => 'doge',
						'amount' => $amount,
						'user_id' => auth()->user()->id
					]));
				}				
				/** End : Modified for cnt gift ***/

				return response()->json([
					'status' => 'success',
					'data' => null,
					'message' => 'You successfully gifted coins!'
				], 201);			
			}			
			
		}
		
		return response()->json([
			'status' => 'error',
			'data' => null,
			'message' => 'Sorry, You do not have sufficient balance.'
		]);
	}
	 
	 
    public function give_old_10_05_2018($type, $model, User $user, $amount, $network_fee)
    {
		/** Start :: Kus Developer 09/05/2018 ****/

		//$giftedby_userid = 260;
		$giftedby_userid	= Auth::user()->id;
		$btcbalance = 0;
		if(TRUE)
		{
			//Auth()::user->id;
			$sql = "select * from doge_addresses where user_id = ".$giftedby_userid;//"address";		
			//exit;//echo $sql.'<br><br>';//exit;
			$res = DB::select($sql);
			$btcaddress = '';
			foreach($res as $val)
			{
				$btcaddress = $val->address;		
				//echo ' BTC Address : '.$btcaddress;
				//$btcaddress = '2Msm8fjJ7Fjy377NL8BQh2Wy1YnhGqc1Kec';
				
				if($btcaddress != '')
				{
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "http://34.230.80.188/api/doge");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"getreceivedbyaddress\",\"params\":[\"".$btcaddress."\"]}");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_USERPWD, "dogecoinrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
					$headers = array();
					$headers[] = "Content-Type: application/x-www-form-urlencoded";
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					$result = curl_exec($ch);
					$btcdata = json_decode($result);
					//echo '<pre>';print_r($btcdata);exit;
					$btcbalance += $btcdata->result;
					if (curl_errno($ch)) {
						echo 'Error:' . curl_error($ch);
					}
					curl_close ($ch);			
				}	
			}
		}
		//echo 'here ::: '.$btcbalance;exit;
						
		if($btcbalance >= $amount)
		{
			$sql = "select * from doge_addresses where user_id = ".$user->id." and address != '' ";
			$res = DB::select($sql);
			$giftedto_btcaddress = '';
			foreach($res as $val){
				$giftedto_btcaddress = $val->address;
				break;
			}
			
			if($giftedto_btcaddress != '')
			{
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "http://34.230.80.188/api/doge");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"sendtoaddress\",\"params\":[\"".$giftedto_btcaddress."\",".$amount."]}");
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_USERPWD, "dogecoinrpc" . ":" . "5xTSZ44Uv9qYabLAPMzGExHFKu4qeRbWJnxtmCLiEiX5");
				$headers = array();
				$headers[] = "Content-Type: application/x-www-form-urlencoded";
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				$result = curl_exec($ch);
				//echo '+++<pre>';print_r(json_decode($result));
				if (curl_errno($ch)) {
					echo 'Error:' . curl_error($ch);
				}
				curl_close ($ch);
		
				$sql = "insert into `walletdetails`(userid,walletaddressid,transactiontype,wallettype,amount)
				values('".$giftedby_userid."','123','gifted','dogecoin',".$amount.")";
						
				//echo $sql; //exit;
				DB::insert($sql);
				
				/** Start : Modified for cnt gift ***/
				if ($type == 'comments') {
					Comment::findOrFail($model)->gifts()->save(new Gift([
						'currency' => 'btc',
						'amount' => $amount,
						'user_id' => auth()->user()->id
					]));
				}

				if ($type == 'topics') {
					Topic::findOrFail($model)->gifts()->save(new Gift([
						'currency' => 'btc',
						'amount' => $amount,
						'user_id' => auth()->user()->id
					]));
				}				
				/** End : Modified for cnt gift ***/
		
				return response()->json([
					'status' => 'success',
					'data' => null,
					'message' => 'You successfully gifted coins!'
				], 201);
			}
		}
		
		return response()->json([
			'status' => 'error',
			'data' => null,
			'message' => 'Sorry, You do not have sufficient balance.'
		]);
		
		
		
		/** End :: Kus Developer 09/05/2018 ****/
		
		
		
		
		
		$dogeBalance 		= 0;
		$walletid = Auth::user()->dogeAddressDefault();
		$sql = "select * from wallet_transaction where to_wallet_id = '".$walletid."'";
		$res = DB::select($sql);
		foreach($res as $val){
			//echo '<pre>';print_r($val->amount);echo '</pre>';exit;
			$dogeBalance += $val->amount;
		}
		
		$sql = "select * from wallet_transaction where from_wallet_id = '".$walletid."'";
		$res = DB::select($sql);
		foreach($res as $val){
			$dogeBalance -= $val->amount;
		}

		$account_balance 	 = (new DogeCurrency)->getBalance(auth()->user()->dogeAddressesString());		
		$account_balance 	+= $dogeBalance;
		
		if($account_balance < $amount){
			return response()->json([
                'success' => false
            ]);
		}
		$address1 = auth()->user()->dogeAddressDefault();
		$address2 = $user->dogeAddressDefault();
		
		$arr = [
			'from_wallet_id' => $address1,
			'to_wallet_id' => $address2,
			'amount' => $amount,
			'topic_id' => $model,
			'coin_type' => 'doge',
			'status' => 1
		];
		//echo '<pre>';print_r($arr);echo '</pre>';exit;
		DB::table('wallet_transaction')->insert($arr);		
		

        if ($type == 'comments') {
            Comment::findOrFail($model)->gifts()->save(new Gift([
                'currency' => 'doge',
                'amount' => $amount,
                'user_id' => auth()->user()->id
            ]));
        }

        if ($type == 'topics') {
            Topic::findOrFail($model)->gifts()->save(new Gift([
                'currency' => 'doge',
                'amount' => $amount,
                'user_id' => auth()->user()->id
            ]));
        }		
		
		
		
		/*$sql = "INSERT INTO wallet_transaction SET from_wallet_id = '".$address1."', to_wallet_id = '".$address2."', amount = '".$amount."', status = 1";
		$res = DB::insert($sql);*/
			
        return response()->json([
            'status' => 'success',
            'data' => null,
            'message' => "You successfully gifted coins!"
        ], 201);
		
		
		
        //echo 'amt :: '.$amount.' Fee '.$network_fee;exit;
		$total_amt = $amount+$network_fee;
		try {
            $response = (new DogeCurrency)->transfer(
                $total_amt,
                auth()->user()->dogeAddressDefault(),
                $user->dogeAddressDefault()
            );
        } catch (Exception $e) {
            return response()->json([
                'success' => false
            ]);
        }
		
		


        return response()->json([
            'status' => 'success',
            'data' => null,
            'message' => 'You successfully gifted coins!'
        ], 201);
    }
	
	
	
	/*Kus*/
    public function estimated_network_fee(User $user,$amtx)
    {
        $estimated_network_fee = 0;
		$userAddress =  $user->dogeAddressDefault();
        try {
			$response = (new DogeCurrency)->get_network_fee_estimate($amtx,$userAddress);			
			$estimated_network_fee = $response->data->estimated_network_fee;
			if($response->status == 'success'){
			
			}
			else{
				//echo '<pre>';print_r($response);
			}					
		} catch (Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'Sorry, something went wrong.'
            ]);
        }

		$res = response()->json([
            'status' => 'success',
            'fee' => $estimated_network_fee,
            'message' => 'Estimated network fee for DGE.'
        ], 201);
		
		return $res; /* */		
		//return 40005;
	}	
	
	
    public function user_balance()
    {
        $available_balance = 80;
		$userAddress =  auth()->user()->dogeAddressDefault();
        try {
			$response = (new DogeCurrency)->get_address_balance($userAddress);			
			if($response->status == 'success'){
				$available_balance = $response->data->available_balance;
			}
			else{
				//echo '<pre>';print_r($response);
			}					
		} catch (Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'Sorry, something went wrong.'
            ]);
        }
/*
		$res = response()->json([
            'status' => 'success',
            'fee' => $estimated_network_fee,
            'message' => 'Estimated network fee for DGE.'
        ], 201);*/
		
		return $available_balance; /* */		
		//return 40005;
	}	
	
	/*Kus*/		
	
	
	
}
