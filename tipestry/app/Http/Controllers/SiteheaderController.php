<?php
namespace App\Http\Controllers;

use App\{User, DashAddress, Site, Comment, History, Log};
use App\Currencies\DashCurrency;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Storage;
use DB;
use Mail;

class SiteheaderController extends Controller
{
	public function siteheader(Request $request)
    {
		$domain = $request->domain;		
		$url = 'http://'.$domain;
		$ch = curl_init($url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_TIMEOUT,10);
		$output = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);		
		curl_close($ch);
		echo $httpcode;
	}
}
