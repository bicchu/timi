<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;

class Topic extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'message', 'user_id','screenshot','Orderby'
    ];

	public function renderTopicsGifts()
	{
		$str = '';
		/*$data['btc']['currency'] 	= 'btc';
		$data['btc']['amount'] 		= 0;
		$data['doge']['currency'] 	= 'doge';
		$data['doge']['amount'] 	= 0;
		
		$topic_id = $this->getid();
		$i = 0;
		$res = DB::table('wallet_transaction')->where('topic_id',$topic_id)->get();
		foreach($res as $val){
			$i++;
			if($val->coin_type == 'btc'){
				$data['btc']['amount'] += $val->amount;
			}
			elseif($val->coin_type == 'doge'){
				$data['doge']['amount'] += $val->amount;
			}
		}
		
		
		foreach($data as $key=>$val){
			if($val['amount']>0){
				$str .= sprintf('<img style="width: 22px;" src="%s" alt="%s">&nbsp;<strong>%s</strong>&nbsp;', asset('images/currencies/' . $val['currency'] . '.png'), $val['currency'], $val['amount']);
			}
		}	*/	
		return $str;		
	}
	
	public function getid(){
		return $this->id;
	}
	
    /**
     * Get the user that owns the topic
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the site that is associated with the specified topic
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    /**
     * Get all of the topic's comments.
     */
    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    /**
     * Get all the comment's gifts
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function gifts()
    {
        return $this->morphMany('App\Gift', 'giftable');
    }

    /**
     * Get the latest gift
     *
     * @return mixed
     */
    public function latestGift()
    {
        return $this->gifts()->orderBy('created_at', 'DESC')->first();
    }

    /**
     * Render total gifts for the specific currency
     *
     * @param $currency
     * @return string
     */
    public function renderTotalGifts($currency)
    {
		//echo '+++'.$currency;
		$amount = 0;
		$topic_id 	= $this->getid();
		//echo 'tid :: '.$topic_id;
		$i = 0;
		$res = DB::table('wallet_transaction')->where('topic_id',$topic_id)->where('coin_type',$currency)->get();
		//echo $res;exit;
		foreach($res as $val){
			$i++;
			$amount += $val->amount;
			//echo $amount;exit;
		}
				
        $amount += $this->gifts()->where('currency', $currency)->sum('amount');
		//echo $amount;exit;
		//return sprintf('<img style="width: 22px;" src="%s" alt="%s">&nbsp;<strong>%s</strong>&nbsp;', asset('images/currencies/' . $currency . '.png'), $currency, $amount);
		$amount = $this->convertFloat($amount);        
		//$amount = 1;
		if($amount > 0){
			
			$amount	 = (float)$amount;
			
			if($currency == 'eth'){
				return sprintf('<img style="width: 36px;" src="%s" alt="%s">&nbsp;<strong>%s</strong>&nbsp;', asset('images/currencies/' . $currency . '.gif'), $currency, $amount);
			}			
			elseif($currency == 'ethtip'){
				return sprintf('<img style="width: 36px;" src="%s" alt="%s">&nbsp;<strong>%s</strong>&nbsp;', asset('images/currencies/' . $currency . '.gif'), $currency, $amount);
			}			
			elseif($currency == 'btc'){
				return sprintf('<img style="width: 34px;" src="%s" alt="%s">&nbsp;<strong>%s</strong>&nbsp;', asset('images/currencies/BB-2sec.gif'), $currency, $amount);
			}
			elseif($currency == 'doge'){
				return sprintf('<img style="width: 34px;" src="%s" alt="%s">&nbsp;<strong>%s</strong>&nbsp;', asset('images/currencies/' . $currency . '.png'), $currency, $amount);
			}
			elseif($currency == 'bch'){
				return sprintf('<img style="width: 34px;" src="%s" alt="%s">&nbsp;<strong>%s</strong>&nbsp;', asset('images/currencies/' . $currency . '.gif'), $currency, $amount);
			}
			elseif($currency == 'ethxrtcoin'){
				return sprintf('<img style="width: 34px;" src="%s" alt="%s">&nbsp;<strong>%s</strong>&nbsp;', asset('images/currencies/xrt.gif'), $currency, $amount);
			}
			elseif($currency == 'xrt'){
				return sprintf('<img style="width: 34px;" src="%s" alt="%s">&nbsp;<strong>%s</strong>&nbsp;', asset('images/currencies/xrt.gif'), $currency, $amount);
			}
			//return sprintf('<img style="width: 32px;" src="%s" alt="%s">&nbsp;<strong>%s</strong>&nbsp;', asset('images/currencies/' . $currency . '.png'), $currency, $amount);
		}else{
			return '';
			
		}
    }

    /**
     * Chek if there is some gifts for the specified comment
     *
     * @param $currency
     * @return bool
     */
    public function hasGifts($currency)
    {
        return $this->gifts()->where('currency', $currency)->sum('amount') != 0;
    }

	function convertFloat($floatAsString)
	{
		$norm = strval(floatval($floatAsString));
		if (($e = strrchr($norm, 'E')) === false) {
			return $norm;
		}
		return number_format($norm, -intval(substr($e, 1)));
	}	
}
