<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gift extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'currency', 'amount', 'user_id', 'giftable_id', 'giftable_type'
    ];

    /**
     * Get all the owning giftable models.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function giftable()
    {
        return $this->morphTo();
    }

    /**
     * Render the representation of activity
     *
     * @return mixed
     */
    public function render()
    {
		$amt = $this->amount;
        //return sprintf('<img style="width: 36px;" src="%s" alt="%s"><b>%s</b>', asset('images/currencies/' . $this->currency . '.png'), $this->currency, $this->amount);
        return sprintf('<img style="width: 36px;" src="%s" alt="%s"><b>%s</b>', asset('images/currencies/' . $this->currency . '.png'), $this->currency, $amt);
    }
}
