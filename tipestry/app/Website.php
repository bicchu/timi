<?php
namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use DB;

class Website extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'site_id', 'parent_id', 'content'
    ];

    /**
     * Scope a query to only include today's comments
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeToday($query)
    {
        return $query->where('created_at', '>=', Carbon::today());
    }

    /**
     * Get url attribute
     *
     * @return string
     */
    public function getUrlAttribute()
    {
        return action('TopicsController@show', [$this->commentable->site, $this->commentable]);
    }

    /**
     * Get the time difference between published date and current time
     *
     * @return mixed
     */
    public function getPublishedAttribute()
    {
        return $this->updated_at->diffForHumans();
    }

    /**
     * Get the number of votes for the specified comment
     *
     * @return int
     */
    public function getVotesCountAttribute()
    {
        return count($this->votes);
    }

    /**
     * Get all of the owning commentable models.
     */
    public function commentable()
    {
        return $this->morphTo();
    }

    /**
     * The sub comments of the specified comment
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children() {
        return $this->hasMany(Website::class, 'parent_id', 'id');
    }

    /**
     * Get all the reports for the specified user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function reports()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * All the votes that are associated with the specified comment
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function votes()
    {
        return $this->belongsToMany(User::class, 'votes');
    }

    /**
     * Get all the comment's gifts
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function gifts()
    {
        return $this->morphMany('App\Gift', 'giftable');
    }

    /**
     * Get the user that wrote the comment
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Check if the specified comment is voted by the user
     *
     * @param User $user
     * @return mixed
     */
    public function votedByUser()
    {
        return !! count($this->votes->where('id', auth()->user()->id));
    }

    /**
     * Check if the comment is report by the the auth user
     *
     * @return bool
     */
    public function alreadyVoted()
    {
        return !! count($this->reports->where('id', auth()->user()->id));
    }

    /**
     * Get the latest gift
     *
     * @return mixed
     */
    public function latestGift()
    {
        return $this->gifts()->orderBy('created_at', 'DESC')->first();
    }

    /**
     * Render total gifts for the specific currency
     *
     * @param $currency
     * @return string
     */
    public function renderTotalGifts($currency)
    {
		//return echo $this->gifts()->id;
        //DB::enableQueryLog();
		
		$amount = $this->gifts()->where('currency', $currency)->sum('amount');	
			
		//$amount = 5;		
		//print_r(DB::getQueryLog());
		$amount 	= number_format($amount,5);
		$amount	 	= (string)$amount;
		$amount	 	= (float)$amount;
		
		
		
		
		if($currency == 'ethtip'){
			return sprintf('<img style="width: 36px;" src="%s" alt="%s">&nbsp;<strong>%s</strong>&nbsp;', asset('images/currencies/' . $currency . '.gif'), $currency, $amount);
		}
		if($currency == 'bch'){
			return sprintf('<img style="width: 36px;" src="%s" alt="%s">&nbsp;<strong>%s</strong>&nbsp;', asset('images/currencies/' . $currency . '.gif'), $currency, $amount);
		}
		
		
		if($currency == 'eth' || $currency == 'bch' || $currency == 'tip'){			
			return sprintf('<img style="width: 36px;" src="%s" alt="%s">&nbsp;<strong>%s</strong>&nbsp;', asset('images/currencies/' . $currency . '.gif'), $currency, $amount);
		}else{
			return sprintf('<img style="width: 36px;" src="%s" alt="%s">&nbsp;<strong>%s</strong>&nbsp;', asset('images/currencies/' . $currency . '.png'), $currency, $amount);
		}
		
    }

    /**
     * Chek if there is some gifts for the specified comment
     *
     * @param $currency
     * @return bool
     */
    public function hasGifts($currency)
    {
        
		
		return $this->gifts()->where('currency', $currency)->sum('amount') != 0;
    }

    /**
     * Delete the comment and his children
     */
    public function delete()
    {
        if ($this->children) {
            foreach ($this->children as $child) {
                $child->delete();
            }
        }

        return parent::delete();
    }
}
