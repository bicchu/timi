<?php

namespace App;

use App\Currencies\{BtcCurrency, DogeCurrency, EthCurrency, DashCurrency};
use App\Mail\UserCommentReported;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Mail;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    /**
     * Cast the attributes to their native types
     *
     * @var array
     */
    public $casts = [
        'is_admin' => 'boolean'
    ];

    /**
     * The User model boot method
     *
     * return @void
     */
    public static function boot()
    {
        parent::boot();

        static::created(function($model) {
			
			//echo '++++++';exit;
			/*$btc_arr 	= (new BtcCurrency)->generateKey();
			$doge_arr 	= (new DogeCurrency)->generateKey();
			echo '<pre>';print_r($btc_arr);echo '</pre>';//exit;
			echo '<pre>';print_r($btc_arr);exit;*/
			
            // BtcCurrency
            
			//echo 'here';exit;
			/*
			$model->btcAddresses()->save(new BtcAddress([
                'address' => (new BtcCurrency)->generateKey()
            ]));

            // DogeCurrency
            $model->dogeAddresses()->save(new DogeAddress([
                'address' => (new DogeCurrency)->generateKey()
            ]));
			//echo 7775;exit;
			*/


/*			
			//echo '++++++';exit;
			$btc_arr 	= (new BtcCurrency)->generateKey();
			$doge_arr 	= (new DogeCurrency)->generateKey();
			echo '<pre>';print_r($btc_arr);exit;
			
            // BtcCurrency
            $model->btcAddresses()->save(new BtcAddress([
                'address' => (new BtcCurrency)->generateKey()
            ]));

            // DogeCurrency
            $model->dogeAddresses()->save(new DogeAddress([
                'address' => (new DogeCurrency)->generateKey()
            ]));

			
			/*21-02-2018-*/
			
			
			
			
			
              // EthCurrency
//            $model->ethAddresses()->save(new EthAddress([
//                'address' => (new EthCurrency)->generateKey()
//            ]));
//
//          DashCurrency
//            $dashCurrency = (new DashCurrency)->generateKey();
//
//            $model->ethAddresses()->save(new DashAddress([
//                'address' => $dashCurrency['address'],
//                'name' => $dashCurrency['name']
//            ]));
        });
    }

    /**
     * Scope a query to only include popular users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAdminless($query)
    {
        return $query->where('is_admin', false);
    }

    /**
     * Check if the specified user is admin
     *
     * @return bool
     */
    public function isAdmin()
    {
        return !! $this->is_admin;
    }

    /**
     * Get the btc wallets that are associated with the specified user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function btcAddresses()
    {
        return $this->hasMany(BtcAddress::class);
    }

    /**
     * Get the most recent btc address
     *
     * @return mixed
     */
    public function btcAddressDefault()
    {
        return $this->btcAddresses->last()->address;
    }

    /**
     * Get the list of the addreses in string format
     *
     * @return string
     */
    public function btcAddressesString()
    {
        return join(',', array_column($this->btcAddresses->toArray(), 'address'));
    }

    /**
     * Get the doge wallets that are associated with the specified user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dogeAddresses()
    {
        return $this->hasMany(DogeAddress::class);
    }

    /**
     * Get the most recent doge address
     *
     * @return mixed
     */
    public function dogeAddressDefault()
    {
        return $this->dogeAddresses->last()->address;
    }

    /**
     * Get the list of the addreses in string format
     *
     * @return string
     */
    public function dogeAddressesString()
    {
        return join(',', array_column($this->dogeAddresses->toArray(), 'address'));
    }

    /**
     * Get the eth wallets that are associated with the specified user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ethAddresses()
    {
        return $this->hasMany(EthAddress::class);
    }

    /**
     * Get the most recent eth address
     *
     * @return mixed
     */
    public function ethAddressDefault()
    {
        return $this->ethAddresses->last()->address;
    }

    /**
     * Get the dash wallets that are associated with the specified user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dashAddresses()
    {
        return $this->hasMany(DashAddress::class);
    }

    /**
     * Get the most recent dash address
     *
     * @return mixed
     */
    public function dashAddressDefault()
    {
        return $this->dashAddresses->last()->address;
    }

    /**
     * Get the list of the DASH addreses in string format
     *
     * @return string
     */
    public function dashAddressesString()
    {
        return join(',', array_column($this->dashAddresses->toArray(), 'address'));
    }

    /**
     * All the comments that are reported by the specified user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function reportedComments()
    {
        return $this->belongsToMany(Comment::class);
    }

    /**
     * All the votes that are associated with the specified comment
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function votes()
    {
        return $this->belongsToMany(Comment::class, 'votes');
    }

    /**
     * Get the sites that was viewed by the specified user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function history()
    {
        return $this->hasMany(History::class);
    }

    /**
     * Get all the logs that are associated with the specified user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logs()
    {
        return $this->hasMany(Log::class);
    }

    /**
     * Get the social login that is associated with the specified user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function social()
    {
        return $this->hasOne(Social::class);
    }

    /**
     * Get all the transactions that are associated with the specified user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->morphMany('App\Transaction', 'transactable');
    }

    /**
     * Report a comment
     *
     * @param Comment $comment
     */
    public function reportComment(Comment $comment)
    {
        $limit = 5;
        if ($comment->reports->count() > $limit) {
            return $comment->delete();
        }
		//Mail::to('feedback@tipestry.com')->send(new UserCommentReported($this, $comment));
		$this->reportedComments()->attach($comment->id);

        if ($comment->fresh()->reports->count() >= $limit) {
            return $comment->delete();
        }
        return true;
    }

    /**
     * Report a Topic
     *
     * @param Topic $topic
     */
    public function reportTopic()
    {
		//echo 'Here';exit;
		/*$limit = 5;
        if ($topic->reports->count() > $limit) {
            return $topic->delete();
        }
		//Mail::to('feedback@tipestry.com')->send(new UserCommentReported($this, $topic));
		$this->reportedComments()->attach($topic->id);

        if ($topic->fresh()->reports->count() >= $limit) {
            return $topic->delete();
        }*/
        return true;
    }

	
	
	
	
    /**
     * Attach a new transaction to the specified user
     *
     * @param $transaction
     * @return false|\Illuminate\Database\Eloquent\Model
     */
    public function addTransaction($transaction)
    {
        return Transaction::create(array_merge($transaction, [
            'transactable_id' => $this->id,
            'transactable_type' => 'App\User'
        ]));
    }
}
