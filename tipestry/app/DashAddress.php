<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DashAddress extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'address', 'name'
    ];

    /**
     * Get the user that is associated with the specified btc address
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
