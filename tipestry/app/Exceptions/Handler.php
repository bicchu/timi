<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // Handle ajax requests
        if ($request->expectsJson() && ! $exception instanceof ValidationException) {
			//echo 1111;exit;
            return $this->renderJson($exception);
        }

        // Handle normal requests
//        if (! $request->expectsJson() && !$exception instanceof ValidationException) {
//            return $this->renderSession($exception);
//        }

		//echo '<pre>';print_r($exception);exit;
		//return view('errors.404', compact('exception'));
        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            //echo 3333;exit;
			return response()->json(['error' => 'Unauthenticated.'], 401);
        }
		//echo 4444;exit;
        return redirect()->guest(route('login'));
    }

    /**
     * Convert an exception into an json response.
     *
     * @param Exception $exception
     * @return \Illuminate\Http\JsonResponse
     */
    protected function renderJson(Exception $exception)
    {
        $response = [
            'status' => 'error',
            'data' => null,
            'message' => 'Sorry, something went wrong.'
        ];

        if (config('app.debug')) {
            $response['message'] = $exception->getMessage();
            $response['data']['exception'] = get_class($exception);
            $response['data']['trace'] = $exception->getTrace();
        }

        $status = 400;

        if ($this->isHttpException($exception)) {
            $status = $exception->getStatusCode();
        }
		//echo 5555;exit;
        return response()->json($response, $status);
    }

    /**
     * Handle the exceptions via session
     *
     * @param Exception $exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function renderSession(Exception $exception)
    {
        $response = 'Sorry, something went wrong.';

        if (config('app.debug')) {
            $response = $exception->getMessage();
        }

        $status = 400;

        if ($this->isHttpException($exception)) {
            $status = $exception->getStatusCode();
        }
		//echo 6666;exit;
        return redirect(null, $status)->back()->with('alert', $response);
    }
}
