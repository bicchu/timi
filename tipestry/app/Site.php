<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Site extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url', 'token', 'screen_path'
    ];

    /**
     * Get host attribute
     *
     * @return mixed
     */
    public function getHostAttribute()
    {
        return parse_url($this->url)['host'];
    }

    /**
     * Get formatted url
     *
     * @return mixed
     */
    public function getFormattedUrlAttribute()
    {
        return preg_replace('(https?://)', '//', $this->url);
    }

    /**
     * Get all the comments that are associated with the specified site
     */
    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    /**
     * Get all the site's topics
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function topics()
    {
        return $this->hasMany(Topic::class);
    }

    /**
     * Get all the transactions that are associated with the specified user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->morphMany('App\Transaction', 'transactable');
    }

    /**
     * Get the site by the given token
     *
     * @param $token
     * @return mixed
     */
    public static function withToken($token)
    {
        $site = static::whereToken($token)->first();

        if (is_null($site)) {
            throw new ModelNotFoundException;
        }

        return $site;
    }

    /**
     * Attach a new transaction to the specified site
     *
     * @param $transaction
     * @return false|\Illuminate\Database\Eloquent\Model
     */
    public function addTransaction($transaction)
    {
        return Transaction::create(array_merge($transaction, [
            'transactable_id' => $this->id,
            'transactable_type' => 'App\Site'
        ]));
    }

    /**
     * Add new topic for the specified site
     *
     * @param $topic
     * @return mixed
     */
    public function addTopic($topic)
    {
        //echo '<pre>';print_r($topic);exit;
		
		return $this->topics()->save($topic);
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'token';
    }
}
