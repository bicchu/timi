<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('vote-comment', function ($user, $comment) {
            return $user->id != $comment->user_id && ! $comment->votedByUser();
        });

        Gate::define('report-comment', function ($user, $comment) {
            return $user->id != $comment->user_id && ! $comment->alreadyVoted();
        });

        Gate::define('give-coins', function ($user, $comment) {
            return $user->id != $comment->user_id;
        });

        Gate::define('delete-comment', function ($user, $comment) {
            return $user->isAdmin() || $user->id == $comment->user_id;
        });

        Gate::define('delete-topic', function ($user, $topic) {
            return $user->isAdmin() || $user->id == $topic->user_id;
        });
    }
}
