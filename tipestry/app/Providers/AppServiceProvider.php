<?php

namespace App\Providers;

use Auth;


//use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
		//\URL::forceScheme('http');
		//$url->forceSchema('https');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Share the authenticated user across admin section
        View::composer('admin/*', function ($view) {
            $view->with('user', Auth::user());
        });
    }
}
