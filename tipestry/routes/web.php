<?php
Route::get('/awssendmail', function () {
	echo 'test';
});

Route::post('siteheader', ['as' => 'siteheader', 'uses' => 'SiteheaderController@siteheader']);

// Admin panel
Route::group([
    'prefix' => 'admin',
    'middleware' => ['auth', 'admin'],
    'namespace' => 'Admin\\'
], function () {
    // Dashboard
    Route::get('/', ['as' => 'admin.dashboard', 'uses' => 'DashboardController@index']);

    // Profile
    Route::get('profile', ['as' => 'admin.profile.index', 'uses' => 'ProfileController@index']);
    Route::patch('profile/edit', ['as' => 'admin.profile.update', 'uses' => 'ProfileController@update']);

    // Users
    Route::get('users', ['as' => 'admin.users.index', 'uses' => 'UsersController@index']);
    Route::get('users/suspended', ['as' => 'admin.users.suspended', 'uses' => 'UsersController@suspended']);
    Route::post('users/suspended/{user}/restore', ['as' => 'admin.users.restore', 'uses' => 'UsersController@restore']);
    Route::delete('users/{user}', ['as' => 'admin.users.destroy', 'uses' => 'UsersController@destroy']); 

	
	
    Route::get('adbanner', ['as' => 'admin.adbanner.index', 'uses' => 'AdbannerController@index']);
    Route::get('adbanner/edit/{id}', ['as' => 'admin.adbanner.edit', 'uses' => 'AdbannerController@edit']);
    Route::post('adbanner/editpost/{id}', ['as' => 'admin.adbanner.editpost', 'uses' => 'AdbannerController@editpost']);
    Route::post('adbanner/delete', ['as' => 'admin.adbanner.delete', 'uses' => 'AdbannerController@delete']);
    //Route::delete('adbanner/{adbanner}', ['as' => 'admin.adbanner.destroy', 'uses' => 'AdbannerController@destroy']);

	
    Route::get('adbanner-price', ['as' => 'admin.adbanner-price', 'uses' => 'AdbannerController@price']);
	
	Route::post('price-update', ['as' => 'admin.price-update', 'uses' => 'AdbannerController@price_update']);




    Route::get('topics', ['as' => 'admin.topics.index', 'uses' => 'TopicController@index']);
    Route::get('topics/delete/{id}', ['as' => 'admin.topics.delete', 'uses' => 'TopicController@delete']);
});

/*
Route::get('/', function () {
	$trendingTopics = App\Topic::orderBy('Orderby','DESC')->where('report','<',5) 
	->with('comments', 'site')->get();
	$perpage = 10;
	return view('index', compact('trendingTopics','perpage'));
});
*/



Route::get('/testingpage', function () {
	echo 785;
});



Route::get('/', ['as' => 'pages.home', 'uses' => 'PagesController@getHome']);
Route::get('/home', ['as' => 'pages.homepage', 'uses' => 'PagesController@getHome']);



// Pages
Route::get('using', ['as' => 'pages.using', 'uses' => 'PagesController@getUsing']);
Route::get('about', ['as' => 'pages.about', 'uses' => 'PagesController@getAbout']);
Route::get('terms', ['as' => 'pages.terms', 'uses' => 'PagesController@getTerms']);

// Sites
Route::post('sites', 'SitesController@process');
Route::get('sites/{site}', 'SitesController@index');
Route::post('sites/{site}/comments', 'SitesController@addComment');
Route::post('sites/{site}/comments/{comment}', 'SitesController@replyComment');

// Topics
Route::post('sites/{site}/topics', 'TopicsController@store');
Route::get('sites/{site}/topics/{topic}', 'TopicsController@show');
Route::post('topics/{topic}/comments', 'TopicsController@addComment');
Route::post('topics/{topic}/comments/{comment}', 'TopicsController@replyComment');
Route::delete('topic/{topic}', 'TopicsController@destroy');
//Route::post('api/topics/{topic}/gifts', 'TopicsController@gift');
//Route::post('api/topics/{topic}/currencies/dash/{user}/give/{amount}', 'TopicsController@gift');

// Comments
Route::post('{site}/comments', 'CommentsController@store');
Route::post('/comments/{comment}', 'CommentsController@reply');
Route::post('comments/{comment}/report', 'CommentsController@report');
Route::delete('comments/{comment}', 'CommentsController@destroy');


Route::post('topics/{topic}/report', 'TopicsController@report');

// Votes
Route::post('comments/{comment}/votes', 'VotesController@store');

// History
Route::get('history', 'HistoryController@index');

// Logs
Route::get('logs', 'LogsController@index');

Auth::routes();

// Currencies -> BTC
Route::get('currencies/btc', ['as' => 'currencies.btc', 'uses' => 'BtcController@index']);
Route::post('currencies/btc/withdraw', ['as' => 'currencies.btc.withdraw', 'uses' => 'BtcController@withdraw']);
Route::post('currencies/btc/regenerate', ['as' => 'currencies.btc.regenerate', 'uses' => 'BtcController@regenerate']);
//Route::post('api/{type}/{model}/currencies/btc/{user}/gift/{amount}', ['as' => 'currencies.btc.give', 'uses' => 'BtcController@give']);
Route::post('api/{type}/{model}/currencies/btc/{user}/gift/{amount}/{network_fee}', ['as' => 'currencies.btc.give', 'uses' => 'BtcController@give']);
Route::get('api/{type}/{model}/currencies/btc/{user}/gift/{amount}/{network_fee}', ['as' => 'currencies.btc.give', 'uses' => 'BtcController@give']);

// Currencies -> BTC Cash
Route::get('currencies/btccash', ['as' => 'currencies.btccash', 'uses' => 'BtccashController@index']);
Route::post('currencies/btccash/withdraw', ['as' => 'currencies.btccash.withdraw', 'uses' => 'BtccashController@withdraw']);
Route::post('currencies/btccash/regenerate', ['as' => 'currencies.btccash.regenerate', 'uses' => 'BtccashController@regenerate']);
Route::post('api/{type}/{model}/currencies/btccash/{user}/gift/{amount}/{network_fee}', ['as' => 'currencies.btccash.give', 'uses' => 'BtccashController@give']);
Route::get('api/{type}/{model}/currencies/btccash/{user}/gift/{amount}/{network_fee}', ['as' => 'currencies.btccash.give', 'uses' => 'BtccashController@give']);

// Currencies -> DOGE
Route::get('currencies/doge', ['as' => 'currencies.doge', 'uses' => 'DogeController@index']);
Route::post('currencies/doge/withdraw', ['as' => 'currencies.doge.withdraw', 'uses' => 'DogeController@withdraw']);
Route::post('currencies/doge/regenerate', ['as' => 'currencies.doge.regenerate', 'uses' => 'DogeController@regenerate']);
//Route::post('api/{type}/{model}/currencies/doge/{user}/gift/{amount}', ['as' => 'currencies.doge.give', 'uses' => 'DogeController@give']);
Route::post('api/{type}/{model}/currencies/doge/{user}/gift/{amount}/{network_fee}', ['as' => 'currencies.doge.give', 'uses' => 'DogeController@give']);
Route::get('api/{type}/{model}/currencies/doge/{user}/gift/{amount}/{network_fee}', ['as' => 'currencies.doge.give', 'uses' => 'DogeController@give']);

// Currencies -> ETH
Route::get('currencies/eth', ['as' => 'currencies.eth', 'uses' => 'EthController@index']);
Route::post('currencies/eth/withdraw', ['as' => 'currencies.eth.withdraw', 'uses' => 'EthController@withdraw']);
Route::post('api/comments/{comment}/currencies/eth/{user}/give/{amount}', ['as' => 'currencies.eth.give', 'uses' => 'EthController@give']);
Route::post('api/{type}/{model}/currencies/eth/{user}/gift/{amount}/{network_fee}', ['as' => 'currencies.eth.give', 'uses' => 'EthController@give']);
Route::get('api/{type}/{model}/currencies/eth/{user}/gift/{amount}/{network_fee}', ['as' => 'currencies.eth.give', 'uses' => 'EthController@give']);
/******/
Route::post('/eth/withdrawal', ['as' => 'eth.withdrawal', 'uses' => 'EthController@withdrawal']);
/******/




// Currencies -> TIP
Route::get('currencies/tip', ['as' => 'currencies.tip', 'uses' => 'TipController@index']);
Route::post('currencies/tip/withdraw', ['as' => 'currencies.tip.withdraw', 'uses' => 'TipController@withdraw']);
Route::post('api/comments/{comment}/currencies/tip/{user}/give/{amount}', ['as' => 'currencies.tip.give', 'uses' => 'TipController@give']);
Route::post('api/{type}/{model}/currencies/tip/{user}/gift/{amount}/{network_fee}', ['as' => 'currencies.tip.give', 'uses' => 'TipController@give']);
Route::get('api/{type}/{model}/currencies/tip/{user}/gift/{amount}/{network_fee}', ['as' => 'currencies.tip.givee', 'uses' => 'TipController@give']);


// Currencies -> XRT
Route::get('currencies/xrt', ['as' => 'currencies.xrt', 'uses' => 'XrtController@index']);
Route::post('currencies/xrt/withdraw', ['as' => 'currencies.xrt.withdraw', 'uses' => 'XrtController@withdraw']);
Route::post('api/comments/{comment}/currencies/xrt/{user}/give/{amount}', ['as' => 'currencies.xrt.give', 'uses' => 'XrtController@give']);
Route::post('api/{type}/{model}/currencies/xrt/{user}/gift/{amount}/{network_fee}', ['as' => 'currencies.xrt.give', 'uses' => 'XrtController@give']);
Route::get('api/{type}/{model}/currencies/xrt/{user}/gift/{amount}/{network_fee}', ['as' => 'currencies.xrt.givee', 'uses' => 'XrtController@give']);

Route::get('ethdepositcheck', ['as' => 'ethdepositcheck', 'uses' => 'EthdepositController@index']);


// Currencies -> DASH
Route::get('currencies/dash', ['as' => 'currencies.dash', 'uses' => 'DashController@index']);
Route::post('currencies/dash/withdraw', ['as' => 'currencies.dash.withdraw', 'uses' => 'DashController@withdraw']);
Route::post('currencies/dash/regenerate', ['as' => 'currencies.dash.regenerate', 'uses' => 'DashController@regenerate']);
Route::post('api/comments/{comment}currencies/dash/{user}/give/{amount}', ['as' => 'currencies.dash.give', 'uses' => 'DashController@give']);

// Socialite
Route::get('social/redirect/{provider}', ['as' => 'social.redirect', 'uses' => 'Auth\LoginController@redirectToProvider']);
Route::get('social/handle/{provider}', ['as' => 'social.handle', 'uses' => 'Auth\LoginController@handleProviderCallback']);


Route::post('/api/estimated_network_fee/btc/{user}/{amt}', ['as' => 'estimated_network_fee', 'uses' => 'BtcController@estimated_network_fee']);
Route::post('/api/estimated_network_fee/doge/{user}/{amt}', ['as' => 'estimated_network_fee', 'uses' => 'DogeController@estimated_network_fee']);

Route::get('websitevote/{type}/{websiteid}', ['as' => 'websitevote', 'uses' => 'WebsiteController@vote']);


// Test
use JonnyW\PhantomJs\Client;
Route::get('/faq', ['as' => 'faqlist', 'uses' => 'FaqController@index']);
Route::get('/privacypolicy', ['as' => 'privacypolicy', 'uses' => 'FaqController@privacypolicy']);

// Home Page Pagination
Route::get('/homepage/{pagenum}', ['as' => 'pages.homepagination', 'uses' => 'PagesController@getHomePagination']);
Route::get('/homepage/{pagenum}/{search_keyword}', ['as' => 'pages.homepagination1', 'uses' => 'PagesController@getHomePagination']);
/*
Route::get('/homepage/{pagenum}', function($pagenum){
	$trendingTopics = App\Topic::orderBy('Orderby','DESC')->where('report','<',5) 
	->with('comments', 'site')->get();	
	
	$perpage = 10;
	return view('homepagelist', compact('trendingTopics','pagenum','perpage'));
});
*/

Route::get('/coinvertprice/{coin}/{paid_ammount}', ['as' => 'coinvertprice', 'uses' => 'SitesController@coinvertprice']);
Route::get('/notification', ['as' => 'notification', 'uses' => 'NotificationController@notification']);
Route::get('/notificationread/{id}', ['as' => 'notificationread', 'uses' => 'NotificationController@notificationread']);

Route::any('/search', ['as' => 'search', 'uses' => 'SearchController@index']);
Route::any('/updatetopic', ['as' => 'updatetopic', 'uses' => 'TopicsController@updatetopic']);
Route::any('/update_Comment_message', ['as' => 'update_Comment_message', 'uses' => 'TopicsController@update_Comment_message']);


/* Start : Ad Banner */

Route::get('/ad-banner', ['as' => 'adbanner', 'uses' => 'AdbannerController@index']);
Route::post('/ad-banner/add', ['as' => 'adbanner.add', 'uses' => 'AdbannerController@add']);

/* End : Ad Banner */

Route::get('/initialize_used_count_per_hour', ['as' => 'initialize_used_count_per_hour', 'uses' => 'PagesController@initialize_used_count_per_hour']);


//Route::get('currencies/bitcoin', ['as' => 'currencies.bitcoin', 'uses' => 'TipestryBtcController@bitcoin']);
//Route::get('/kussoftiframecall', ['as' => 'kussoftiframecall','param1' => 'value1', 'uses' => 'KussoftController@iframecall']);
/*
Route::get('/currencies/bitcoin/', function () {
	echo 'here';
});
*/

Route::get('/currencies/bitcoin/',  'TipestryBtcController@bitcoin');
Route::get('currencies/mybtc',  'MybtcController@index');


Route::get('/testquery/', function () {
	echo 'UID :: '.Auth::user()->id.' +++';
	
	
	$xdata 	= array('user_id'=>4,'topics_id'=>20);				
	DB::table('post_report')->insert($xdata);	
	echo 'here 0001222444';
	exit;	
	
	$sql = "SELECT topics.id topics_id, site_id, url, screenshot, screen_path 
	FROM 'topics' 
	LEFT JOIN 'sites' 
	ON topics.site_id = sites.id 
	WHERE screenshot = '' 	
	AND topics.id IN (".$id.")	
	AND sites.id NOT IN(SELECT id FROM 'sites' where screen_path != '') 
	order by  topics.id DESC 
	limit 0,1";	
	echo $sql.'<br><br><br>';
	$results = DB::select( DB::raw($sql) );
	
	$tid = 0;
	$screenshot = '123';
	foreach($results as $val)
	{
		$siteURL = $val->url;
		$googlePagespeedData = file_get_contents("https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url=$siteURL&screenshot=true");
		$googlePagespeedData = json_decode($googlePagespeedData, true);
		$screenshot = $googlePagespeedData['screenshot']['data'];
		$tid = $val->topics_id;
		$screenshot = str_replace(array('_','-'),array('/','+'),$screenshot); 
	}

	echo $sql_update = "UPDATE 'topics' SET screenshot = '".$screenshot."' WHERE id = ".$tid;
	DB::update(DB::raw($sql_update));
	exit;
});




Route::get('/screenshot', function(){
	
	$siteURL = "https://barrons.com/articles/jpmorgans-dimon-softens-on-bitcoin-1515539943";
	
	$googlePagespeedData = file_get_contents("https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url=".$siteURL."&screenshot=true");
	$googlePagespeedData = json_decode($googlePagespeedData, true);
	$screenshot = $googlePagespeedData['screenshot']['data'];
	$screenshot = str_replace(array('_','-'),array('/','+'),$screenshot); 
	echo "<img src=\"data:image/jpeg;base64,".$screenshot."\" />";exit;
});

Route::get('/sitelist', function(){
	$results = DB::select( DB::raw("SELECT * FROM 'topics' order by id desc limit 0,10") );
	echo '<pre>';print_r($results);exit;
	
	//dictionary.com
	//DB::insert( DB::raw("INSERT INTO 'sites' SET url = 'dictionary.com', token = 'str_random(strlen($url))' ") );
	
	$results = DB::select( DB::raw("SELECT * FROM 'sites' order by id desc") );
	echo '<style>table, th, td {border: 1px thin black;}</style>';
	echo '<table style="padding-top:2px;"><tr><td>ID</td><td width="30">Details</td><td>URL</td><td>Status</td><tr>';
	foreach($results as $val){
		//echo '<pre>';print_r($val);exit;
		echo '<tr><td>'.$val->id.'</td>';
		echo '<td style="padding-left:20px;">'.$val->created_at.'<br>'.$val->updated_at.'</td>';
		echo '<td style="padding-left:20px;"><a href="'.$val->url.'">'.substr($val->url,0,100).'</a></td><td></td></tr>';
	}
	echo '</table>';
	//echo '<pre>';print_r();exit;	
	
	
	$sql = 'select "topics".*, MAX(c.created_at) as trand_date 
	from "topics" 
	left join "comments" as "c" 
	on "c"."commentable_id" = "topics"."id" 
	and "c"."commentable_type" = ? 
	group by "c"."commentable_id" 
	order by "topics"."created_at" desc,
	"c"."created_at" desc';
	
	
	DB::enableQueryLog();
	
	$trendingTopics = App\Topic::leftJoin('comments as c', function ($join) {
        $join->on('c.commentable_id', '=', 'topics.id')
             ->where('c.commentable_type', '=', 'App\\Topic');
    })
	->groupBy('c.commentable_id')
	->select('topics.*', DB::raw('MAX(c.created_at) as trand_date'))
	->orderBy('topics.created_at', 'DESC')
	->orderBy('c.created_at', 'DESC')
	->with('comments', 'site')->get();
	
	foreach($trendingTopics as $val)
	{
		echo '<pre>';print_r($val);echo '</pre>';exit;
	}
	echo '<pre>';print_r(DB::getQueryLog());echo '</pre><hr><hr><hr><pre>';
	//print_r($trendingTopics);
	exit;
	
	
	$results = DB::select( DB::raw("SELECT * FROM 'topics' order by id desc") );
	echo '<pre>';print_r($results);
});

/*
function isSecured($url)
{
    $stream = stream_context_create (array("ssl" => array("capture_peer_cert" => true)));

    $read = fopen($url, "rb", false, $stream);

    $cont = stream_context_get_params($read);

    return isset($cont["options"]["ssl"]["peer_certificate"]);
}
*/
/**
 * Fetch the data from external api
 *
 * @param $url
 * @return mixed
 */
 /*
function fetchFromApi($method, $url, array $options = [])
{
    $response = (new \GuzzleHttp\Client())->$method($url, $options);

    return \GuzzleHttp\json_decode($response->getBody());
}
*/


Route::get('checkusername/{name}',  'UnicuserController@index');

Route::get('/tipgetdata', ['as' => 'tipgetdata', 'uses' => 'PagesController@tipgetdata']);
Route::get('/tipinsertdata', ['as' => 'tipinsertdata', 'uses' => 'PagesController@tipinsertdata']);
